# Build Variants
We simply have 3 productFlavors right now, dev, qa3 and prod.

# Libraries

This application is developed in Android Studio 2.0 and makes use of the following libraries:

## Dagger 2

A dependency injection library that utilises compile-time class generation (see build.gradle). In a nutshell, this library allows you to define Components that provide (through a collection of Modules) objects that can be injected into Fragments and Activities at runtime. 

Most of these objects are singletons, or more accurately, scoped to the lifespan of the Application, such as the User or API instance. By injecting these into Fragments (see BaseFragment) we remove the need to reference static instances which may cause memory leaks, as well as giving us the ability to swap out the implementation and test them via their defining Modules.

## Retrofit

A REST API client library that generates requests and parses response JSON automatically into Java classes based on simple annotations. See Api.java, from this class Retrofit generates the code needed to call and parse these APIs. ApiModule is the Dagger module in which the setup and creation of our single REST client is created.

## RxAndroid

Reactive coding provides a mechanism to better schedule and respond to asynchronous operations. RxAndroid, built on RxJava is being used here to call and process the response of Retrofit API calls. Infact Retrofit supports returning "Reactive" Observables out of the box instead of providing the API calls a callback to run on the UI Thread. 

In a nutshell, api.someApiCall(params) returns an Observable stream on which we subscribe and handle the response. The major benefit is that we can schedule which threads the call happens on (Network/IO) and the response (UI/Main), as well as using Rx's many operators to combine, process and manipulate the data ready for UI display.

## Google AppCompat

We're using the latest (v23 at time of writing) App Compat libraries to provide Material design on devices back to API 16.

## Google Play Services 

Used for location services.


## Activity Flow

1. LoadingActivity - (launch Activity) show logos/animation
2. CoachActivity - tutorial screens
3. LoginActivity - login anonymously to gain a dpdsession if we have no consumerId yet.
4. StartupChecksActivity - get Consumer/Content updates based on deviceId
5. Then one of the following (depending on user state):
    - AuthPinActivity (to request and authenticate new pin) -> 6.
    - ProfileSetupActivity (to set up Consumer/ConsumerProperties) -> 7.
    - YourDeliveriesActivity (if already authenticated with a ConsumerProperty) -> 8.
6. AuthPinActivity
    - sets user.setConsumerId() and either:
        - if deviceConflict -> DeviceConflictActivity -> LoginActivity -> StartupChecksActivity
        - log in with auditUser (authenticated) -> StartupChecksActivity
7. ProfileSetupActivity
    - sets properties on Consumer and adds a ConsumerProperty
    - PUTs Consumer to server and moves onto YourDeliveriesActivity
8. YourDeliveriesActivity - displays list of parcels