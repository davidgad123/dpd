package com.dpd.ui.fragment;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.wearable.view.CardFrame;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.common.Constants;
import com.dpd.AppConstant;
import com.dpd.YourDPDApp;
import com.dpd.ui.activity.MainActivity;
import com.dpd.yourdpd.R;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.Asset;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.Wearable;
import com.model.ActiveParcel;
import com.util.WearableUtil;

import java.io.InputStream;

public class MainCardFragment extends Fragment {
    private ActiveParcel parcel;

    public static MainCardFragment newInstance() {
        return new MainCardFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        int layoutId = bundle.getInt(AppConstant.BUNDLE_LAYOUT_ID);
        int column = bundle.getInt(AppConstant.BUNDLE_COLUMN_INDEX);
        parcel = (ActiveParcel) bundle.getSerializable(AppConstant.BUNDLE_ACTIVE_PARCEL);

        View root = inflater.inflate(layoutId, null);
        if (parcel != null) {
            initUI(root, parcel.parcelStatusType, column);
        }

        if (layoutId == R.layout.fragment_cards_no_authorized) {
            initUIForUnAuthorized(root);
        }
        return root;
    }

    private void initUIForUnAuthorized(View view) {
        // Support round screen.
        ImageView imgDpdCube = (ImageView) view.findViewById(R.id.img_dpd_cube);
        if (imgDpdCube != null) {
            ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) imgDpdCube.getLayoutParams();
            params.setMarginEnd(getResources().getDimensionPixelSize(YourDPDApp.isRound() ? R.dimen.card_dpd_cube_margin_end_round : R.dimen.card_dpd_cube_margin_end));
            imgDpdCube.setLayoutParams(params);
        }

        view.findViewById(R.id.login_on_phone).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final WearableUtil wearableUtil = ((MainActivity) getActivity()).getWearableUtil();
                if (wearableUtil.isConnected()) {
                    wearableUtil.sendDataAndRequest(Constants.REQUEST_OPEN_APP_TO_AUTH, null);
                } else {
                    wearableUtil.startConnection();
                    wearableUtil.setWearableConnectionListener(new WearableUtil.WearableConnectListener() {
                        @Override
                        public void connectionSuccess() {
                            wearableUtil.sendDataAndRequest(Constants.REQUEST_OPEN_APP_TO_AUTH, null);
                        }

                        @Override
                        public void connectionFailed() {
                        }
                    });
                }

                getActivity().finish();
            }
        });
    }

    private void initUI(View view, Constants.ParcelStatusType parcelStatusType, int column) {
        // Support round screen.
        ImageView imgDpdCube = (ImageView) view.findViewById(R.id.img_dpd_cube);
        if (imgDpdCube != null) {
            ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) imgDpdCube.getLayoutParams();
            params.setMarginEnd(getResources().getDimensionPixelSize(YourDPDApp.isRound() ? R.dimen.card_dpd_cube_margin_end_round : R.dimen.card_dpd_cube_margin_end));
            imgDpdCube.setLayoutParams(params);
        }

        // Support round screen and animate photo caption.
        CardFrame photoCaptionFrame = (CardFrame) view.findViewById(R.id.photo_caption_frame);
        if (photoCaptionFrame != null) {
            // Support round screen
            ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) photoCaptionFrame.getLayoutParams();
            params.setMargins(params.leftMargin, params.topMargin, params.rightMargin,
                    getResources().getDimensionPixelSize(YourDPDApp.isRound() ? R.dimen.photo_caption_margin_bottom_round : R.dimen.photo_caption_margin_bottom));
            photoCaptionFrame.setLayoutParams(params);

            // Animate caption when click.
            photoCaptionFrame.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //animatePhotoCaption(v);
                }
            });
        }

        switch (parcelStatusType) {
            case LOADING:
            case UNKNOWN:
            case RFI:
                showInfoAtFirstCard(view);
                break;
            case DELIVERED:
                if (column == 0) {
                    showInfoAtFirstCard(view);
                } else {
                    showDeliveredImage(view, column);
                }
                break;
            case COLLECT_FROM_DEPOT:
                if (column == 0) {
                    showInfoAtFirstCard(view);
                } else if (column == 1) {
                    if (parcel.depotDiaryDate != null)
                        showCollectFromDepot(view);
                    else
                        showShopInfo(view);
                } else if (column == 2) {
                    showShopInfo(view);
                }
                break;
            case COLLECT_FROM_PICKUP:
                if (column == 0) {
                    showInfoAtFirstCard(view);
                } else if (column == 1) {
                    if (parcel.depotDiaryDate != null)
                        showCollectFromPickup(view);
                    else
                        showShopInfo(view);
                } else if (column == 2) {
                    showShopInfo(view);
                }
                break;
            case COLLECT_FROM_PICKUP_WITH_PASS:
                if (column == 0) {
                    showInfoAtFirstCard(view);
                } else if (column == 1) {
                    showShopInfo(view);
                } else if (column == 2) {
                    showPickupPass(view);
                }
                break;
            case OUT_FOR_DELIVERY:
                if (column == 0) {
                    showInfoAtFirstCard(view);
                } else if (column == 1) {
                    showInfoAtOutForDelivery(view);
                }
                break;
            case ISSUE:
                showIssueInformation(view, column);
                break;
            case SHOW_ESTIMATED_DELIVERY_DATE:
                if (column == 0) {
                    showInfoAtFirstCard(view);
                } else {
                    showEstimatedDateInfo(view);
                }
        }
    }

    private void animatePhotoCaption(View v) {
        v.clearAnimation();
        v.clearAnimation();
        v.setPivotX(v.getWidth() / 2);
        v.setPivotY(v.getHeight() / 2);

        WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics metrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(metrics);

        int[] test = new int[2];
        v.getLocationOnScreen(test);

        int locationY = test[1];
        boolean midUpper = (locationY + v.getHeight() / 2) < metrics.heightPixels / 2;
        int offsetY = (metrics.heightPixels - v.getHeight()) -
                (midUpper ? locationY : (metrics.heightPixels - locationY - v.getHeight())) * 2;

        ObjectAnimator animVertical = ObjectAnimator.ofFloat(v, View.TRANSLATION_Y, midUpper ? 0 : -offsetY);
        animVertical.setDuration(300);

        AnimatorSet set = new AnimatorSet();
        set.play(animVertical);
        set.start();
    }

    private void showInfoAtFirstCard(View view) {
        TextView cardTitle = (TextView) view.findViewById(R.id.card_title);
        TextView cardNumber = (TextView) view.findViewById(R.id.card_number);
        TextView cardContent = (TextView) view.findViewById(R.id.card_content);

        cardTitle.setText(parcel.organisation);
        cardNumber.setText(parcel.parcelNumber);
        cardContent.setText(WearableUtil.formatText(parcel.trackingStatusCurrent, getActivity()));
    }

    private void showCollectFromDepot(View view) {
        TextView cardTitle = (TextView) view.findViewById(R.id.card_title);
        TextView cardNumber = (TextView) view.findViewById(R.id.card_number);
        TextView cardContent = (TextView) view.findViewById(R.id.card_content);

        cardTitle.setText(parcel.organisation);
        cardNumber.setText(parcel.parcelNumber);
        String formattedString = String.format(getString(R.string.parcel_location_collect_from_depot_message),
                parcel.depotName,
                parcel.depotDiaryDate,
                parcel.depotStartTime,
                parcel.depotEndTime);
        cardContent.setText(formattedString);
    }

    private void showCollectFromPickup(View view) {
        TextView cardTitle = (TextView) view.findViewById(R.id.card_title);
        TextView cardNumber = (TextView) view.findViewById(R.id.card_number);
        TextView cardContent = (TextView) view.findViewById(R.id.card_content);

        cardTitle.setText(parcel.organisation);
        cardNumber.setText(parcel.parcelNumber);
        String formattedString = String.format(getString(R.string.parcel_location_collect_from_pickup_message),
                parcel.shopShortAddress,
                parcel.depotDiaryDate,
                "12:00"); // (sic), it's always 12:00
        cardContent.setText(formattedString);
    }

    public String getDriverName(ActiveParcel parcel) {
        return TextUtils.isEmpty(parcel.driverName) ? getString(R.string.your_driver) : parcel.driverName;
    }

    private void showInfoAtOutForDelivery(View view) {
        TextView cardTitle = (TextView) view.findViewById(R.id.card_title);
        TextView cardNumber = (TextView) view.findViewById(R.id.card_number);
        TextView cardContent = (TextView) view.findViewById(R.id.card_content);

        cardTitle.setText(parcel.organisation);
        cardNumber.setText(parcel.parcelNumber);

        // When under 1 hour, the text should state 'X minutes away from you'
        // When over 1 hour the text should state: 'X hour and X minutes away from you'
        // When it hits a 1 hour with no minutes it should state: 'X hour away from you'
        String parcelDescription;
        int hours = parcel.estimatedMinsToStop / 60;
        int minutes = parcel.estimatedMinsToStop % 60;
        if (hours > 0) {
            if (minutes == 0)
                parcelDescription = getString(
                        R.string.parcel_location_estimated_hours_to_stop, getDriverName(parcel),
                        hours, hours > 1 ? "s" : "");
            else
                parcelDescription = getString(
                        R.string.parcel_location_estimated_hours_minutes_to_stop, getDriverName(parcel),
                        hours, hours > 1 ? "s" : "", minutes);
        } else
            parcelDescription = getString(
                    R.string.parcel_location_estimated_minutes_to_stop, getDriverName(parcel),
                    minutes);
        cardContent.setText(parcelDescription);
    }

    private void showShopInfo(View view) {
        TextView cardTitle = (TextView) view.findViewById(R.id.card_title);
        TextView cardNumber = (TextView) view.findViewById(R.id.card_number);
        TextView cardContent = (TextView) view.findViewById(R.id.shop_address);

        cardTitle.setText(parcel.organisation);
        cardNumber.setText(parcel.parcelNumber);
        cardContent.setText(parcel.shopAddress);
    }

    private void showPickupPass(final View view) {
        TextView cardTitle = (TextView) view.findViewById(R.id.card_title);
        TextView cardNumber = (TextView) view.findViewById(R.id.card_number);

        cardTitle.setText(parcel.organisation);
        cardNumber.setText(parcel.parcelNumber);
        view.findViewById(R.id.card_view_detail).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent view_detail = new Intent();
                view_detail.setAction(AppConstant.OPEN_APP_RECEIVER);
                view_detail.putExtra(Constants.PARCEL_CODE, parcel.parcelCode);
                getActivity().sendBroadcast(view_detail);
            }
        });
    }

    private void showEstimatedDateInfo(View view) {
        TextView cardTitle = (TextView) view.findViewById(R.id.card_title);
        TextView cardNumber = (TextView) view.findViewById(R.id.card_number);
        TextView cardContent = (TextView) view.findViewById(R.id.card_content);

        cardTitle.setText(parcel.organisation);
        cardNumber.setText(parcel.parcelNumber);
        String strContent = String.format(getString(R.string.show_estimated_date_text), parcel.estimatedDeliveryDate);
        cardContent.setText(strContent);
    }

    private void showIssueInformation(View view, int column) {
        TextView cardTitle = (TextView) view.findViewById(R.id.card_title);
        TextView cardNumber = (TextView) view.findViewById(R.id.card_number);
        ImageView imgIssueIcon = (ImageView) view.findViewById(R.id.img_issueIcon);
        TextView cardContent = (TextView) view.findViewById(R.id.card_content);

        cardTitle.setText(parcel.organisation);
        cardNumber.setText(parcel.parcelNumber);
        imgIssueIcon.setImageResource(getResIconID(parcel.issueCode));

        String strContent;
        if (column == 0) {
            strContent = String.format(getString(R.string.parcel_delayed_message), parcel.issueName);
        } else {
            strContent = String.format(getString(R.string.parcel_delayed_estimate_date), parcel.estimatedDeliveryDate);
        }
        cardContent.setText(strContent);
    }

    private void showDeliveredImage(final View view, int column) {
        if (parcel.photos == null)
            return;

        DataMap item = parcel.photos.get(column - 1); // minus 1 means first screen is not the image screen

        // image screen starts at second screen
        final ImageView imgPhoto = (ImageView) view.findViewById(R.id.img_photo);
        final TextView txtPhotoCaption = (TextView) view.findViewById(R.id.txt_photo_caption);
        final Asset asset = item.getAsset(Constants.PARCEL_ASSET_PHOTOS);

        String caption = item.getString(Constants.PARCEL_ASSET_PHOTO_IMAGE_NAME);
        if (caption != null)
            txtPhotoCaption.setText(caption);

        if (asset != null) if (getActivity() instanceof MainActivity) {
            WearableUtil wearableUtil = ((MainActivity) getActivity()).getWearableUtil();
            final GoogleApiClient apiClient = wearableUtil.getApiClient();

            if (wearableUtil.isConnected()) {
                GetBitmap getBitmap = new GetBitmap(imgPhoto);
                getBitmap.execute(asset, apiClient);
            } else {
                wearableUtil.startConnection();
                wearableUtil.setWearableConnectionListener(new WearableUtil.WearableConnectListener() {
                    @Override
                    public void connectionSuccess() {
                        GetBitmap getBitmap = new GetBitmap(imgPhoto);
                        getBitmap.execute(asset, apiClient);
                    }

                    @Override
                    public void connectionFailed() {
                    }
                });
            }
        }
    }

    private Bitmap loadBitmapFromAsset(Asset asset, GoogleApiClient apiClient) {
        InputStream assetInputStream = Wearable.DataApi.getFdForAsset(apiClient, asset).await().getInputStream();
        if (assetInputStream == null) return null;
        return BitmapFactory.decodeStream(assetInputStream);
    }

    public int getResIconID(String issueCode) {
        try {
//        0   Unexpected Delay     images/icon-exclamation.png
//        1   Fire                 images/icon-fire.png
//        2   Flood                images/icon-flood.png
//        3   Snow                 images/icon-snow.png
//        4   Civil Unrest         images/icon-civil.png
//        5   No Sailing           images/icon-sailing.png
//        6   Traffic Incident     images/icon-traffic.png
//        7   High Winds           images/icon-wind.png
//        8   Flight Delay         images/plane-icon.png
//        9   Olympic Delay        images/icon-olympics.png
//        10  Local Event          images/icon-exclamation.png
            switch (issueCode) {
                case "0":
                    return R.drawable.ic_exclamation;
                case "1":
                    return R.drawable.ic_fire;
                case "2":
                    return R.drawable.ic_flood;
                case "3":
                    return R.drawable.ic_snow;
                case "4":
                    return R.drawable.ic_civil_unrest;
                case "5":
                    return R.drawable.ic_sailing;
                case "6":
                    return R.drawable.ic_traffic;
                case "7":
                    return R.drawable.ic_high_wind;
                case "8":
                    return R.drawable.ic_plane;
                case "9":
                    return R.drawable.ic_olympics;
                case "10":
                    return R.drawable.ic_exclamation;
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        return R.drawable.ic_exclamation;
    }

    private class GetBitmap extends AsyncTask<Object, Void, Drawable> {
        private ImageView mImage;

        public GetBitmap(ImageView image) {
            mImage = image;
        }

        @Override
        protected Drawable doInBackground(Object... params) {
            Asset asset = (Asset) params[0];
            GoogleApiClient client = (GoogleApiClient) params[1];
            Drawable img = new BitmapDrawable(getResources(), loadBitmapFromAsset(asset, client));
            return img;
        }

        @Override
        protected void onPostExecute(Drawable drawable) {
            if (drawable != null)
                mImage.setImageDrawable(drawable);
        }
    }
}
