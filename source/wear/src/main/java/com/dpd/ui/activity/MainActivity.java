package com.dpd.ui.activity;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.util.Log;
import android.view.View;
import android.view.WindowInsets;

import com.common.Constants;
import com.dpd.AppConstant;
import com.dpd.YourDPDApp;
import com.dpd.receiver.ParcelDataListener;
import com.dpd.receiver.ParcelDataReceiver;
import com.dpd.service.PhoneDataListenerService;
import com.dpd.ui.fragment.MainActivityFragment;
import com.dpd.ui.fragment.MainCardFragment;
import com.dpd.yourdpd.R;
import com.google.android.gms.wearable.DataMap;
import com.util.WearableUtil;

import java.util.ArrayList;

public class MainActivity extends Activity implements MainActivityFragment.OnFragmentInteractionListener {
    private static final String TAG = MainActivity.class.getSimpleName();

    private static final int FRAGMENT_YOUR_DELIVERIES = 1;
    private static final int FRAGMENT_NO_DELIVERIES = 2;
    private static final int FRAGMENT_NO_AUTHORIZED = 3;

    private int fragmentType = 0;
    private WearableUtil wearableUtil;
    private String firstParcelCode;

    private ParcelDataListener dataListener;
    private ParcelDataReceiver dataReceiver;

    private ContentLoadingProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressBar = (ContentLoadingProgressBar) findViewById(R.id.progressBar);
        progressBar.setOnApplyWindowInsetsListener(new View.OnApplyWindowInsetsListener() {
            @Override
            public WindowInsets onApplyWindowInsets(View v, WindowInsets insets) {
                if (insets.isRound()) {
                    Log.d(TAG, "Round screen");
                } else {
                    Log.d(TAG, "Not round screen");
                }

                YourDPDApp.setRound(insets.isRound());
                return insets;
            }
        });

        initData();

        // Handle notification.
        int requestCode = getIntent().getIntExtra(Constants.REQUEST_CODE, 0);
        if (requestCode == Constants.REQUEST_DISMISS_NOTIFICATION) {
            firstParcelCode = getIntent().getStringExtra(Constants.NOTIFICATION_PARCEL_CODE);
            closeNotification(getIntent());
        }

        // Register service.
        IntentFilter filter = new IntentFilter("com.dpd.yourdpd.BroadcastReceiver");
        dataReceiver = new ParcelDataReceiver();
        registerReceiver(dataReceiver, filter);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        int requestCode = intent.getIntExtra(Constants.REQUEST_CODE, 0);
        Bundle bundle = intent.getBundleExtra(Constants.BUNDLE);
        switch (requestCode) {
            case Constants.RESPONSE_PARCEL_HISTORY:
                if (bundle != null) {
                    // common module send and receive array data
                    // in real there is only one parcel data.
                    ArrayList<DataMap> sentData = DataMap.fromBundle(bundle).getDataMapArrayList(Constants.EXTRA_BUNDLE);
                    int parcelSize = sentData.size();
                    if (parcelSize != 0) {
                        switchContent(FRAGMENT_YOUR_DELIVERIES, bundle);
                    } else {
                        switchContent(FRAGMENT_NO_DELIVERIES, null);
                    }
                }
                hideProgress();
                break;
            case Constants.SEND_THROWABLE:
                switchContent(FRAGMENT_NO_AUTHORIZED, null);
                hideProgress();
                break;
            case Constants.REQUEST_DISMISS_NOTIFICATION:
                if (fragmentType != FRAGMENT_YOUR_DELIVERIES)
                    initData();

                String parcelCode = intent.getStringExtra(Constants.NOTIFICATION_PARCEL_CODE);
                dataListener.parcelUpdated(parcelCode);
                closeNotification(intent);
                break;
            case Constants.REQUEST_REFRESH:
                initData();
                break;
        }
    }

    private void switchContent(int fragment_index, Bundle bundle) {
        Fragment fragment = null;
        if (bundle == null) bundle = new Bundle();

        fragmentType = fragment_index;
        switch (fragmentType) {
            case FRAGMENT_YOUR_DELIVERIES:
                fragment = MainActivityFragment.newInstance();
                break;
            case FRAGMENT_NO_DELIVERIES:
                fragment = MainCardFragment.newInstance();
                bundle.putInt(AppConstant.BUNDLE_LAYOUT_ID, R.layout.fragment_cards_no_delivery);
                break;
            case FRAGMENT_NO_AUTHORIZED:
                fragment = MainCardFragment.newInstance();
                bundle.putInt(AppConstant.BUNDLE_LAYOUT_ID, R.layout.fragment_cards_no_authorized);
                break;
        }

        if (fragment != null) {
            try {
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = this.getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.layout_container, fragment).commit();
            } catch (Exception e) {
                e.printStackTrace();
                fragmentType = 0;
            }
        }
    }

    private void initData() {
        showProgress();

        wearableUtil = new WearableUtil(getApplicationContext());
        wearableUtil.setWearableConnectionListener(new WearableUtil.WearableConnectListener() {
            @Override
            public void connectionSuccess() {
                wearableUtil.sendDataAndRequest(Constants.REQUEST_PARCEL_HISTORY, null);
            }

            @Override
            public void connectionFailed() {
                hideProgress();
            }
        });
        wearableUtil.startConnection();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(dataReceiver);
        super.onDestroy();
    }

    private void showProgress() {
        progressBar.show();
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        progressBar.hide();
        progressBar.setVisibility(View.GONE);
    }

    private void closeNotification(Intent intent) {
        // delete notification from phone
        int notificationID = intent.getIntExtra(AppConstant.CLOSE_NOTIFICATION_ID, 0);
        if (notificationID != 0) {
            Intent dismissIntent = new Intent(this, PhoneDataListenerService.class);
            dismissIntent.setAction(Constants.ACTION_DISMISS);
            dismissIntent.putExtra(AppConstant.CLOSE_NOTIFICATION_ID, notificationID);
            startService(dismissIntent);
            // delete notification from wearable
            NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(getApplicationContext());
            notificationManagerCompat.cancel(notificationID);
        }
    }

    public WearableUtil getWearableUtil() {
        return wearableUtil;
    }

    public void setOnParcelDataListener(ParcelDataListener listener) {
        dataListener = listener;
        dataReceiver.setDataListener(dataListener);
    }

    @Override
    public void onSetParcelDataListener() {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                dataListener.parcelUpdated(firstParcelCode);
                firstParcelCode = null;
            }
        });
    }
}
