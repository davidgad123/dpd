package com.dpd.ui.customview;

import android.content.Context;
import android.support.wearable.view.GridViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class CustomizedGridView extends GridViewPager {
    private boolean mDisableHorizontalScroll = false;
    private float xDistance, yDistance, lastX, lastY;

    public CustomizedGridView(Context context) {
        super(context);
    }

    public CustomizedGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomizedGridView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                setDownPosition(ev);
                break;
            case MotionEvent.ACTION_MOVE:
                if (isVerticalScroll(ev)) {
                    if (mDisableHorizontalScroll)
                        return false;
                }
        }
        return super.onInterceptTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                setDownPosition(ev);
                break;
            case MotionEvent.ACTION_MOVE:
                if (isVerticalScroll(ev)) {
                    if (mDisableHorizontalScroll)
                        return false;
                }
        }
        return super.onTouchEvent(ev);
    }

    private void setDownPosition(MotionEvent ev) {
        xDistance = yDistance = 0f;
        lastX = ev.getX();
        lastY = ev.getY();
    }

    private boolean isVerticalScroll(MotionEvent ev) {
        final float curX = ev.getX();
        final float curY = ev.getY();
        xDistance += Math.abs(curX - lastX);
        yDistance += Math.abs(curY - lastY);
        lastX = curX;
        lastY = curY;
        return xDistance < yDistance;
    }

    public void setHorizontalScrollDisabled(boolean disabled) {
        mDisableHorizontalScroll = disabled;
    }
}
