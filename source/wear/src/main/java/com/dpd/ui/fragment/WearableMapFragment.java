package com.dpd.ui.fragment;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dpd.AppConstant;
import com.dpd.util.GMapsUtils;
import com.dpd.yourdpd.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.model.ActiveParcel;

public class WearableMapFragment extends Fragment {
    private static String TAG = WearableMapFragment.class.getSimpleName();

    private ViewGroup mapContainer;
    private MapFragment mapFragment;
    private ActiveParcel parcel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_map, null);

        if (getArguments() != null) {
            this.parcel = (ActiveParcel) getArguments().getSerializable(AppConstant.BUNDLE_ACTIVE_PARCEL);
        }

        if (parcel == null) {
            Log.e(TAG, "Cannot show google map without parcel.");
            return root;
        }

        mapContainer = (ViewGroup) root.findViewById(R.id.map_container);
        init();
        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void init() {
        if (mapFragment == null) {
            mapFragment = MapFragment.newInstance();
        }
        if (!mapFragment.isAdded()) {
            final FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
            fragmentTransaction.add(mapContainer.getId(), mapFragment);
            fragmentTransaction.commit();
        }

        switch (parcel.parcelStatusType) {
            case OUT_FOR_DELIVERY:
                showOutForDelivery();
                break;
            case COLLECT_FROM_PICKUP:
            case COLLECT_FROM_PICKUP_WITH_PASS:
                showPickupDirection();
                break;
            case COLLECT_FROM_DEPOT:
                showCollectFromDepot();
                break;
        }
    }

    private void showOutForDelivery() {
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap map) {
                final LatLng houseLatLng = new LatLng(parcel.deliveryLatitude, parcel.deliveryLongitude);
                final LatLng driverLatLng = new LatLng(parcel.routeLatitude, parcel.routeLongitude);

                final LatLngBounds.Builder boundsBuilder = LatLngBounds.builder();
                boundsBuilder.include(houseLatLng);
                boundsBuilder.include(driverLatLng);

                map.addMarker(new MarkerOptions()
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin_house_360))
                        .position(houseLatLng)
                        .alpha(1f)
                        .anchor(0.5f, 1.0f)
                        .title(getString(R.string.your_address)));
                map.addMarker(new MarkerOptions()
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin_driver_360))
                        .position(driverLatLng)
                        .alpha(1f)
                        .anchor(0.5f, 1.0f)
                        .title(getString(R.string.your_driver)));

                // zoom to bounds
                final LatLng[] latLngs = new LatLng[]{houseLatLng, driverLatLng};
                final BitmapFactory.Options bmpOptions = GMapsUtils.getDimensionFromResource(getActivity(), R.drawable.ic_pin_house_360);
                map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                    @Override
                    public void onMapLoaded() {
                        GMapsUtils.zoomWithPadding(mapFragment, latLngs, 40, bmpOptions.outWidth, bmpOptions.outHeight);
                    }
                });

                // Disable marker click listener
                map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {
                        return true;
                    }
                });
            }
        });
    }

    private void showCollectFromDepot() {
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(final GoogleMap map) {
                final LatLng houseLatLng = new LatLng(parcel.deliveryLatitude, parcel.deliveryLongitude);
                final LatLng shopLatLng = new LatLng(parcel.shopLatitude, parcel.shopLongitude);

                final LatLngBounds.Builder boundsBuilder = LatLngBounds.builder();
                boundsBuilder.include(houseLatLng);
                boundsBuilder.include(shopLatLng);

                map.addMarker(new MarkerOptions()
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin_house_360))
                        .position(houseLatLng)
                        .alpha(1f)
                        .anchor(0.5f, 1.0f)
                        .title(getString(R.string.your_address)));
                map.addMarker(new MarkerOptions()
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.card_icon_360))
                        .position(shopLatLng)
                        .alpha(1f)
                        .anchor(0.5f, 0.5f)
                        .title(parcel.organisation));

                // zoom to bounds
                final LatLng[] latLngs = new LatLng[]{houseLatLng, shopLatLng};
                final BitmapFactory.Options bmpOptions = GMapsUtils.getDimensionFromResource(getActivity(), R.drawable.ic_pin_house_360);
                map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                    @Override
                    public void onMapLoaded() {
                        GMapsUtils.zoomWithPadding(mapFragment, latLngs, 40, bmpOptions.outWidth, bmpOptions.outHeight);
                    }
                });

                // Disable marker click listener
                map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {
                        return true;
                    }
                });
            }
        });
    }

    private void showPickupDirection() {
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap map) {
                final LatLng houseLatLng = new LatLng(parcel.deliveryLatitude, parcel.deliveryLongitude);
                final LatLng shopLatLng = new LatLng(parcel.shopLatitude, parcel.shopLongitude);

                final LatLngBounds.Builder boundsBuilder = LatLngBounds.builder();
                boundsBuilder.include(houseLatLng);
                boundsBuilder.include(shopLatLng);

                map.addMarker(new MarkerOptions()
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin_house_360))
                        .position(houseLatLng)
                        .alpha(1f)
                        .anchor(0.5f, 1.0f)
                        .title(getString(R.string.your_address)));
                map.addMarker(new MarkerOptions()
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.card_icon_360))
                        .position(shopLatLng)
                        .alpha(1f)
                        .anchor(0.5f, 0.5f)
                        .title(parcel.organisation));

                // zoom to bounds
                final LatLng[] latLngs = new LatLng[]{houseLatLng, shopLatLng};
                final BitmapFactory.Options bmpOptions = GMapsUtils.getDimensionFromResource(getActivity(), R.drawable.ic_pin_house_360);
                map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                    @Override
                    public void onMapLoaded() {
                        GMapsUtils.zoomWithPadding(mapFragment, latLngs, 40, bmpOptions.outWidth, bmpOptions.outHeight);
                    }
                });

                // Disable marker click listener
                map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {
                        return true;
                    }
                });
            }
        });
    }
}
