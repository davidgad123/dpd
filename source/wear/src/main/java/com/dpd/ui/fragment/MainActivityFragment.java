package com.dpd.ui.fragment;

import android.app.Fragment;
import android.graphics.Point;
import android.os.Bundle;
import android.support.wearable.view.DotsPageIndicator;
import android.support.wearable.view.GridViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.common.Constants;
import com.dpd.YourDPDApp;
import com.dpd.adapter.MainGridViewAdapter;
import com.dpd.receiver.ParcelDataListener;
import com.dpd.ui.activity.MainActivity;
import com.dpd.ui.customview.CustomizedGridView;
import com.dpd.yourdpd.R;
import com.google.android.gms.wearable.DataMap;
import com.model.ActiveParcel;
import com.model.ActiveParcelArray;

import java.util.ArrayList;
import java.util.List;

public class MainActivityFragment extends Fragment {

    private CustomizedGridView mGridViewPager;
    private MainGridViewAdapter mGridAdapter;
    private DotsPageIndicator mPageIndicator;
    private LinearLayout mLinearDots;
    private View mView;

    private ActiveParcelArray lstModels = new ActiveParcelArray();
    private OnFragmentInteractionListener listener;

    public static MainActivityFragment newInstance() {
        return new MainActivityFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_main, null);
        initUI(mView);
        return mView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        listener = (OnFragmentInteractionListener) getActivity();

        if (getActivity() instanceof MainActivity) {
            ((MainActivity) getActivity()).setOnParcelDataListener(new ParcelDataListener() {
                @Override
                public void parcelReceived(Bundle bundle) {
                    showParcelList(getParcelFromBundle(bundle), true);
                }

                @Override
                public void parcelUpdated(String parcelCode) {
                    updateParcel(parcelCode);
                }
            });
            listener.onSetParcelDataListener();
        }
    }

    private void updateParcel(String parcelCode) {
        if (mGridAdapter != null) {
            int index = lstModels.getParcelIndex(parcelCode);
            if (index >= 0) {
                ActiveParcel parcel = lstModels.getParcel(index);
                parcel.parcelStatusType = Constants.ParcelStatusType.LOADING;
                parcel.isLoading = false;
                mGridViewPager.setCurrentItem(index, 0);
                mGridAdapter.notifyDataSetChanged();
            }
        }
    }

    private List<ActiveParcel> getParcelFromBundle(Bundle bundle) {
        if (bundle == null) return null;

        ArrayList<ActiveParcel> parcelList = new ArrayList<>();
        ArrayList<DataMap> dataList = DataMap.fromBundle(bundle).getDataMapArrayList(Constants.EXTRA_BUNDLE);
        for (int i = 0; i < dataList.size(); i++) {
            DataMap mapItem = dataList.get(i);
            ActiveParcel parcel = new ActiveParcel();
            parcel.parcelNumber = mapItem.getString(Constants.PARCEL_NUMBER);
            parcel.parcelCode = mapItem.getString(Constants.PARCEL_CODE);
            parcel.organisation = mapItem.getString(Constants.PARCEL_ORGANISATION);
            parcel.trackingStatusCurrent = mapItem.getString(Constants.PARCEL_TRACKING_STATUS);
            parcel.parcelStatusType = Constants.ParcelStatusType.fromValue(mapItem.getInt(Constants.PARCEL_TYPE));
            parcel.estimatedMinsToStop = mapItem.getInt(Constants.PARCEL_ESTIMATED_MINS_TO_STOP);
            parcel.deliveryLatitude = mapItem.getFloat(Constants.PARCEL_DELIVERY_LATITUDE);
            parcel.deliveryLongitude = mapItem.getFloat(Constants.PARCEL_DELIVERY_LONGITUDE);
            parcel.driverName = mapItem.getString(Constants.PARCEL_DRIVER_NAME);
            parcel.routeLatitude = mapItem.getFloat(Constants.PARCEL_ROUTE_LATITUDE);
            parcel.routeLongitude = mapItem.getFloat(Constants.PARCEL_ROUTE_LONGITUDE);
            parcel.estimatedDeliveryDate = mapItem.getString(Constants.PARCEL_ESTIMATED_DATE);
            parcel.shopAddress = mapItem.getString(Constants.PARCEL_SHOP_LOCATION_ADDRESS);
            parcel.shopShortAddress = mapItem.getString(Constants.PARCEL_SHOP_LOCATION_SHORT_ADDRESS);
            parcel.shopLongitude = mapItem.getFloat(Constants.PARCEL_SHOP_LONGITUDE);
            parcel.shopLatitude = mapItem.getFloat(Constants.PARCEL_SHOP_LATITUDE);
            parcel.issueName = mapItem.getString(Constants.PARCEL_ISSUE_NAME);
            parcel.issueCode = mapItem.getString(Constants.PARCEL_ISSUE_CODE);
            parcel.depotDiaryDate = mapItem.getString(Constants.PARCEL_DEPOT_DIARY_DATE);
            parcel.depotName = mapItem.getString(Constants.PARCEL_DEPOT_NAME);
            parcel.depotStartTime = mapItem.getString(Constants.PARCEL_DEPOT_START_TIME);
            parcel.photos = mapItem.getDataMapArrayList(Constants.PARCEL_ASSET_PHOTOS);
            parcel.depotEndTime = mapItem.getString(Constants.PARCEL_DEPOT_END_TIME);
            parcelList.add(parcel);
        }
        return parcelList;
    }

    private void showParcelList(List<ActiveParcel> parcelList, boolean isUpdate) {
        if (parcelList == null) return;

        for (ActiveParcel parcel : parcelList)
            if (parcel != null) lstModels.updateParcel(parcel, isUpdate);

        if (mGridAdapter != null)
            mGridAdapter.notifyDataSetChanged();

        if (mPageIndicator != null) {
            Point point = mGridViewPager.getCurrentItem();
            int row = point.y, column = point.x;
            mPageIndicator.onPageSelected(row, column);
        }
    }

    private void initUI(View view) {
        if (getArguments() != null) {
            Bundle parcelData = getArguments();
            showParcelList(getParcelFromBundle(parcelData), false);
        }

        mGridAdapter = new MainGridViewAdapter(getActivity(), lstModels, getFragmentManager());
        mGridViewPager = (CustomizedGridView) view.findViewById(R.id.pager_card);
        mGridViewPager.setAdapter(mGridAdapter);
        mPageIndicator = (DotsPageIndicator) view.findViewById(R.id.page_indicator);
        mPageIndicator.setPager(mGridViewPager);
        mPageIndicator.setOnPageChangeListener(new GridViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, int i1, float v, float v1, int i2, int i3) {
            }

            @Override
            public void onPageSelected(int row, int column) {
                refreshPageIndicator();
            }

            @Override
            public void onPageScrollStateChanged(int i) {
            }
        });

        // Support round screen.
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) mPageIndicator.getLayoutParams();
        params.setMargins(params.leftMargin, params.topMargin, params.rightMargin,
                getResources().getDimensionPixelSize(YourDPDApp.isRound() ? R.dimen.card_page_indicator_margin_bottom_round : R.dimen.card_page_indicator_margin_bottom));
        mPageIndicator.setLayoutParams(params);
    }

    private void refreshPageIndicator() {
        Point point = mGridViewPager.getCurrentItem();
        int row = point.y, column = point.x;

        mPageIndicator.setVisibility(View.VISIBLE);
        ActiveParcel parcel = lstModels.getParcel(row);
        switch (parcel.parcelStatusType) {
            case DELIVERED:
                if (column >= 1)
                    mPageIndicator.setVisibility(View.INVISIBLE);
                break;
            case COLLECT_FROM_DEPOT:
            case COLLECT_FROM_PICKUP:
                if (parcel.depotDiaryDate != null) {
                    if (column >= 3)
                        mPageIndicator.setVisibility(View.INVISIBLE);
                } else {
                    if (column >= 2)
                        mPageIndicator.setVisibility(View.INVISIBLE);
                }
                break;
            case COLLECT_FROM_PICKUP_WITH_PASS:
                if (column >= 3)
                    mPageIndicator.setVisibility(View.INVISIBLE);
                break;
            case OUT_FOR_DELIVERY:
                if (column >= 2)
                    mPageIndicator.setVisibility(View.INVISIBLE);
                break;
        }
    }

    public interface OnFragmentInteractionListener {
        void onSetParcelDataListener();
    }
}
