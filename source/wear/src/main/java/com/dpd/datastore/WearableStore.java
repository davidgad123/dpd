package com.dpd.datastore;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.model.ActiveParcelArray;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;

public class WearableStore {
    private static final String TAG = WearableStore.class.getSimpleName();

    private Context context;
    private Gson gson;
    private SharedPreferences prefs;
    private ActiveParcelArray activeParcelArray;

    private static final String ACTIVE_PARCELS_FILENAME = "parcel_active_filename";

    public WearableStore(Context context) {
        final GsonBuilder gsonBuilder = new GsonBuilder();

        this.context = context.getApplicationContext();
        this.prefs = PreferenceManager.getDefaultSharedPreferences(context);
        this.gson = gsonBuilder.create();
    }

    private void loadActiveParcelHistory() {
        activeParcelArray = (ActiveParcelArray) readJsonFromDisk(ACTIVE_PARCELS_FILENAME, ActiveParcelArray.class);
    }

    private void saveActiveParcelHistory() {
        writeJsonToDisk(ACTIVE_PARCELS_FILENAME, activeParcelArray);
    }

    private Object readJsonFromDisk(String filename, Type clazz) {
        final String filePath =
                context.getFilesDir().getAbsolutePath() + "/" + filename;
        String json = null;
        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader bufferedReader = null;
        try {
            FileReader fileReader = new FileReader(filePath);
            bufferedReader = new BufferedReader(fileReader);
            String receiveString;

            while ((receiveString = bufferedReader.readLine()) != null) {
                stringBuilder.append(receiveString);
            }
            json = stringBuilder.toString();

        } catch (FileNotFoundException e) {
            Log.w(TAG, "Unable to read " + filename + " from disk, file not found");

        } catch (IOException ioe) {
            Log.e(TAG, "Unable to read " + filename + " from disk, IOException", ioe);

        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        if (json != null) {
            final JsonParser parser = new JsonParser();
            final JsonElement jsonElement = parser.parse(json);
            try {
                return gson.fromJson(jsonElement, clazz);
            } catch (JsonSyntaxException e) {
                Log.e(TAG, "Unable to parse " + filename + " json", e);
            }
        }
        return null;
    }

    private void writeJsonToDisk(String filename, Object object) {
        if (object == null) {
            Log.w(TAG, "Unable to " + filename + " to disk, object is null");
            return;
        }

        final String json = gson.toJson(object);
        FileWriter writer = null;
        try {
            final String filePath = context.getFilesDir().getAbsolutePath() + "/" + filename;
            writer = new FileWriter(filePath);
            writer.write(json);

        } catch (IOException e) {
            Log.e(TAG, "Cannot write " + filename + " to disk", e);

        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
