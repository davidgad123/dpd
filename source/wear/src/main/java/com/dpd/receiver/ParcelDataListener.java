package com.dpd.receiver;

import android.os.Bundle;

public interface ParcelDataListener {
    void parcelReceived(Bundle bundle);
    void parcelUpdated(String parcelCode);
}