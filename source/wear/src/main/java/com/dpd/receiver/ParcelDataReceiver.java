package com.dpd.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.common.Constants;

public class ParcelDataReceiver extends BroadcastReceiver {
    private static final String TAG = ParcelDataReceiver.class.getSimpleName();

    private ParcelDataListener listener;

    public void setDataListener(ParcelDataListener listener) {
        this.listener = listener;
    }

    @Override
    public void onReceive(final Context context, Intent intent) {
        Log.d(TAG, "Parcel data received.");
        int requestCode = intent.getIntExtra(Constants.REQUEST_CODE, 0);
        switch (requestCode) {
            case Constants.RESPONSE_PARCEL_DETAIL:
                if (listener != null)
                    listener.parcelReceived(intent.getBundleExtra(Constants.BUNDLE));
                break;
        }
    }
}
