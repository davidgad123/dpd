package com.dpd.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationManagerCompat;
import android.support.wearable.activity.ConfirmationActivity;
import android.util.Log;

import com.common.Constants;
import com.dpd.AppConstant;
import com.dpd.service.PhoneDataListenerService;
import com.google.android.gms.wearable.DataMap;
import com.util.WearableUtil;

import java.util.ArrayList;

public class PhoneAppOpenListener extends BroadcastReceiver {
    private WearableUtil wearableUtil;

    @Override
    public void onReceive(final Context context, Intent intent) {
        final String parcelCode = intent.getStringExtra(Constants.PARCEL_CODE);
        final int notificationID = intent.getIntExtra(AppConstant.CLOSE_NOTIFICATION_ID, 0);
        Log.d("PhoneAppReceiver", "Started");

        wearableUtil = new WearableUtil(context);
        wearableUtil.startConnection();
        if (wearableUtil.isConnected()) {
            sendNotificationInfoToPhone(parcelCode, context, notificationID);
        } else {
            wearableUtil.setWearableConnectionListener(new WearableUtil.WearableConnectListener() {
                @Override
                public void connectionSuccess() {
                    sendNotificationInfoToPhone(parcelCode, context, notificationID);
                }

                @Override
                public void connectionFailed() {
                }
            });
        }
    }

    private void sendNotificationInfoToPhone(String parcelCode, Context context, int notificationID) {
        ArrayList<DataMap> list_dataMap = new ArrayList<DataMap>();
        DataMap item = new DataMap();
        item.putString(Constants.PARCEL_CODE, parcelCode);
        list_dataMap.add(item);
        wearableUtil.sendDataAndRequest(Constants.REQUEST_OPEN_APP_PARCEL_DETAIL, list_dataMap);

        Intent animationIntent = new Intent(context, ConfirmationActivity.class);
        animationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        animationIntent.putExtra(ConfirmationActivity.EXTRA_ANIMATION_TYPE, ConfirmationActivity.OPEN_ON_PHONE_ANIMATION);
        context.startActivity(animationIntent);
        //delete notification from phone

        if (notificationID != 0) {
            Intent dismissIntent = new Intent(context, PhoneDataListenerService.class);
            dismissIntent.setAction(Constants.ACTION_DISMISS);
            dismissIntent.putExtra(AppConstant.CLOSE_NOTIFICATION_ID, notificationID);
            context.startService(dismissIntent);
            //delete notification from wearable
            NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(context);
            notificationManagerCompat.cancel(notificationID);
        }
    }
}
