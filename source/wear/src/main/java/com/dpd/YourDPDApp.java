package com.dpd;

import android.app.Application;

public class YourDPDApp extends Application {
    private static YourDPDApp app;
    private static boolean round;

    public static boolean isRound() {
        return round;
    }

    public static void setRound(boolean b) {
        round = b;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
        round = false;
    }
}
