package com.dpd.util;

import android.content.Context;
import android.support.v4.app.NotificationManagerCompat;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

public class CommonUtil {
    private static float DIP_SCALE = -1;

    public static int dpToPx(int dp, Context context) {
        if (DIP_SCALE == -1) DIP_SCALE = context.getResources().getDisplayMetrics().density;
        return (int) (dp * DIP_SCALE + 0.5f); // 0.5f for rounding
    }

    public static void setListViewHeightToTotal(ListView listView) {
        ListAdapter mAdapter = listView.getAdapter();

        int totalHeight = 0;
        for (int i = 0; i < mAdapter.getCount(); i++) {
            View mView = mAdapter.getView(i, null, listView);
            mView.measure(
                    MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED),
                    MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
            totalHeight += mView.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (mAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    public static void removeNotification(Context context, int notificationId) {
        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(context);
        notificationManagerCompat.cancel(notificationId);
    }
}
