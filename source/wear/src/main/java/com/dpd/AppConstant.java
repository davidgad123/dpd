package com.dpd;

public class AppConstant {
    public final static String BUNDLE_LAYOUT_ID = "layout_id";
    public final static String BUNDLE_ACTIVE_PARCEL = "active_parcel";
    public final static String BUNDLE_COLUMN_INDEX = "column_index";

    public final static String OPEN_APP_RECEIVER = "com.dpd.yourdpd.open.app.receiver";
    public final static String CLOSE_NOTIFICATION_ID = "notification_id";
}
