package com.dpd.push;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.AbsoluteSizeSpan;

import com.common.Constants;
import com.dpd.AppConstant;
import com.dpd.ui.activity.MainActivity;
import com.dpd.yourdpd.R;

public class NotificationUtil {
    @TargetApi(Build.VERSION_CODES.KITKAT_WATCH)
    public static void showNotification(int id, String title, String content, String parcelCode, Context context) {
        // this intent is triggered by tapping "View details" button
        Intent viewIntent = new Intent(context, MainActivity.class);
        viewIntent.putExtra(Constants.NOTIFICATION_PARCEL_CODE, parcelCode);
        viewIntent.putExtra(Constants.REQUEST_CODE, Constants.REQUEST_DISMISS_NOTIFICATION);
        viewIntent.putExtra(AppConstant.CLOSE_NOTIFICATION_ID, id);
        PendingIntent intentViewDetail = PendingIntent.getActivity(context, 0, viewIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        // this intent will trigger activity of Phone app.
        Intent triggerIntent = new Intent();
        triggerIntent.setAction(AppConstant.OPEN_APP_RECEIVER);
        triggerIntent.putExtra(Constants.PARCEL_CODE, parcelCode);
        triggerIntent.putExtra(AppConstant.CLOSE_NOTIFICATION_ID, id);
        PendingIntent intentViewOnPhone = PendingIntent.getBroadcast(context, 0, triggerIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        // this intent will be sent when the user swipes the notification to dismiss it
        Intent dismissIntent = new Intent(Constants.ACTION_DISMISS);
        dismissIntent.setAction(Constants.ACTION_DISMISS);
        dismissIntent.putExtra(AppConstant.CLOSE_NOTIFICATION_ID, id);
        PendingIntent pendingDeleteIntent = PendingIntent.getService(context, 0, dismissIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Bitmap bit_backgroundNotification = BitmapFactory.decodeResource(context.getResources(), R.drawable.notification_back_28);
        NotificationCompat.WearableExtender wearableExtender = new NotificationCompat.WearableExtender().setHintHideIcon(false)
                //.setDisplayIntent(pendingViewIntent)
                .setBackground(bit_backgroundNotification)
                .setHintAvoidBackgroundClipping(false);

        Spannable sb = new SpannableString(content);
        sb.setSpan(new AbsoluteSizeSpan(25), 0, content.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.card_icon_360)
                .setContentTitle(context.getString(R.string.your_dpd))
                .setContentText(sb)
                .extend(wearableExtender)
                .setAutoCancel(true)
                .setDeleteIntent(pendingDeleteIntent)
                .addAction(R.drawable.view_detail_icon_360, context.getString(R.string.view_detail), intentViewDetail)
                .addAction(R.drawable.view_phone_icon_360, context.getString(R.string.view_on_phone), intentViewOnPhone);

        Notification notification = builder.build();
        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(context);
        notificationManagerCompat.notify(id, notification);
    }

//    public static void dismissNotification(Context context, int notification_id) {
//        new DismissNotificationCommand(context, notification_id).execute();
//    }
//
//    private static class DismissNotificationCommand implements GoogleApiClient.ConnectionCallbacks, ResultCallback<DataApi.DeleteDataItemsResult>, GoogleApiClient.OnConnectionFailedListener {
//        private static final String TAG = "DismissNotification";
//
//        private final GoogleApiClient mGoogleApiClient;
//        private int notification_id;
//        public DismissNotificationCommand(Context context, int notification_id) {
//            mGoogleApiClient = new GoogleApiClient.Builder(context)
//                    .addApi(Wearable.API)
//                    .addConnectionCallbacks(this)
//                    .addOnConnectionFailedListener(this)
//                    .build();
//            this.notification_id = notification_id;
//        }
//
//        public void execute() {
//            mGoogleApiClient.connect();
//        }
//
//        @Override
//        public void onConnected(Bundle bundle) {
//            final Uri dataItemUri =
//                    new Uri.Builder().scheme(WEAR_URI_SCHEME).path(Constants.NOTIFICATION_PATH).build();
//            if (Log.isLoggable(TAG, Log.DEBUG)) {
//                Log.d(TAG, "Deleting Uri: " + dataItemUri.toString());
//            }
//            Wearable.DataApi.deleteDataItems(
//                    mGoogleApiClient, dataItemUri).setResultCallback(this);
//        }
//
//        @Override
//        public void onConnectionSuspended(int i) {
//            Log.d(TAG, "onConnectionSuspended");
//        }
//
//        @Override
//        public void onResult(DataApi.DeleteDataItemsResult deleteDataItemsResult) {
//            if (!deleteDataItemsResult.getStatus().isSuccess()) {
//                Log.e(TAG, "dismissWearableNotification(): failed to delete DataItem");
//            }
//            mGoogleApiClient.disconnect();
//        }
//
//        @Override
//        public void onConnectionFailed(ConnectionResult connectionResult) {
//            Log.d(TAG, "onConnectionFailed");
//        }
//    }
}
