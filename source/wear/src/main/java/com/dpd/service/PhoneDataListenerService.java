package com.dpd.service;

import android.content.Intent;

import com.common.Constants;
import com.dpd.AppConstant;
import com.dpd.push.NotificationUtil;
import com.dpd.ui.activity.MainActivity;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.WearableListenerService;
import com.util.WearableUtil;

import java.util.ArrayList;

public class PhoneDataListenerService extends WearableListenerService {
    private WearableUtil wearableUtil;

    @Override
    public void onCreate() {
        super.onCreate();
        wearableUtil = new WearableUtil(getApplicationContext());
        wearableUtil.startConnection();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (null != intent) {
            String action = intent.getAction();
            if (Constants.ACTION_DISMISS.equals(action)) {
                int notification_id = intent.getIntExtra(AppConstant.CLOSE_NOTIFICATION_ID, 0);
                sendNotificationCloseNotify(notification_id);
            }
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDataChanged(DataEventBuffer dataEvents) {
        for (DataEvent dataEvent : dataEvents) {
            if (dataEvent.getType() == DataEvent.TYPE_CHANGED) {
                // Handle notification.
                if (Constants.NOTIFICATION_PATH.equals(dataEvent.getDataItem().getUri().getPath())) {
                    // Title, content, and parcel code.
                    DataMapItem dataMapItem = DataMapItem.fromDataItem(dataEvent.getDataItem());
                    int id = dataMapItem.getDataMap().getInt(Constants.NOTIFICATION_ID);
                    String title = dataMapItem.getDataMap().getString(Constants.NOTIFICATION_TITLE);
                    String content = dataMapItem.getDataMap().getString(Constants.NOTIFICATION_CONTENT);
                    String parcelCode = dataMapItem.getDataMap().getString(Constants.NOTIFICATION_PARCEL_CODE);

                    NotificationUtil.showNotification(id, title, content, parcelCode, getApplicationContext());

                //  Handle data.
                } else if (Constants.DATA_REQUEST_PATH.equals(dataEvent.getDataItem().getUri().getPath())) {
                    //
                    DataMapItem dataMapItem = DataMapItem.fromDataItem(dataEvent.getDataItem());
                    int requestCode = dataMapItem.getDataMap().getInt(Constants.REQUEST_CODE);
                    processRequest(requestCode, dataMapItem.getDataMap());
                }
            }
        }
    }

    private void processRequest(int requestCode, DataMap extra) {
        switch (requestCode) {
            case Constants.SEND_THROWABLE: {
                Intent intent = new Intent(this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(Constants.BUNDLE, extra.toBundle());
                intent.putExtra(Constants.REQUEST_CODE, requestCode);
                startActivity(intent);
                break;
            }
            case Constants.REQUEST_REFRESH: {
                Intent intent = new Intent(this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(Constants.REQUEST_CODE, requestCode);
                startActivity(intent);
                break;
            }
            case Constants.RESPONSE_PARCEL_HISTORY: {
                Intent intent = new Intent(this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(Constants.BUNDLE, extra.toBundle());
                intent.putExtra(Constants.REQUEST_CODE, requestCode);
                startActivity(intent);
                break;
            }
            case Constants.RESPONSE_PARCEL_DETAIL: {
                Intent intent = new Intent();
                intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
                intent.setAction("com.dpd.yourdpd.BroadcastReceiver");
                intent.putExtra(Constants.BUNDLE, extra.toBundle());
                intent.putExtra(Constants.REQUEST_CODE, requestCode);
                sendBroadcast(intent);
                break;
            }
        }
    }

    private void sendNotificationCloseNotify(final int notification_id) {
        final ArrayList<DataMap> lstDataMap = new ArrayList<>();
        DataMap item = new DataMap();
        item.putInt(Constants.NOTIFICATION_ID, notification_id);
        lstDataMap.add(item);

        if (wearableUtil.isConnected())
            wearableUtil.sendDataAndRequest(Constants.REQUEST_DISMISS_NOTIFICATION, lstDataMap);
        else {
            wearableUtil.startConnection();
            wearableUtil.setWearableConnectionListener(new WearableUtil.WearableConnectListener() {
                @Override
                public void connectionSuccess() {
                    wearableUtil.sendDataAndRequest(Constants.REQUEST_DISMISS_NOTIFICATION, lstDataMap);
                }

                @Override
                public void connectionFailed() {
                }
            });
        }
    }
}
