package com.dpd.adapter;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.support.wearable.view.FragmentGridPagerAdapter;
import android.text.TextUtils;

import com.dpd.AppConstant;
import com.dpd.ui.fragment.LoadingFragment;
import com.dpd.ui.fragment.MainCardFragment;
import com.dpd.ui.fragment.WearableMapFragment;
import com.dpd.yourdpd.R;
import com.model.ActiveParcel;
import com.model.ActiveParcelArray;
import com.util.WearableUtil;

public class MainGridViewAdapter extends FragmentGridPagerAdapter {

    private ActiveParcelArray parcelArray;
    private Context context;

    public MainGridViewAdapter(Context context, ActiveParcelArray parcelArray, FragmentManager manager) {
        super(manager);
        this.context = context;
        this.parcelArray = parcelArray;
    }

    @Override
    public Fragment getFragment(int row, int column) {
        ActiveParcel parcel = parcelArray.activeParcels.get(row);
        Fragment fragment = null;
        switch (parcel.parcelStatusType) {
            case LOADING:
                fragment = showLoading(column, parcel);
                break;
            case DELIVERED:
                fragment = showDelivered(column, parcel);
                break;
            case COLLECT_FROM_DEPOT:
                fragment = showCollectFromDepot(column, parcel);
                break;
            case COLLECT_FROM_PICKUP:
                fragment = showCollectFromPickupDirections(column, parcel);
                break;
            case COLLECT_FROM_PICKUP_WITH_PASS:
                fragment = showCollectFromPickupPass(column, parcel);
                break;
            case OUT_FOR_DELIVERY:
                fragment = showOutDelivery(column, parcel);
                break;
            case ISSUE:
                fragment = showIssueInformation(column, parcel);
                break;
            case SHOW_ESTIMATED_DELIVERY_DATE:
                fragment = showEstimatedDate(column, parcel);
                break;
            default:
                fragment = showUnknown(column, parcel);
                break;
        }
        return fragment;
    }

    @Override
    public int getRowCount() {
        return parcelArray.activeParcels.size();
    }

    @Override
    public int getColumnCount(int row) {
        ActiveParcel parcel = parcelArray.activeParcels.get(row);
        switch (parcel.parcelStatusType) {
            case LOADING:
                return 2;
            case DELIVERED:
                int sizeOfImage = 0;
                if (parcel.photos != null)
                    sizeOfImage = parcel.photos.size();
                return 1 + sizeOfImage; // here 1 mean the first screen.
            case COLLECT_FROM_DEPOT:
            case COLLECT_FROM_PICKUP:
                if (parcel.depotDiaryDate != null)
                    return 4;
                else
                    return 3;
            case COLLECT_FROM_PICKUP_WITH_PASS:
                return 4;
            case OUT_FOR_DELIVERY:
                return 3;
            case ISSUE:
                if (!TextUtils.isEmpty(parcel.estimatedDeliveryDate))
                    return 2;
                else
                    return 1;
            case SHOW_ESTIMATED_DELIVERY_DATE:
                return 2;
            default:
                return 1;
        }
    }

    private Fragment showOutDelivery(int column, ActiveParcel parcel) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstant.BUNDLE_ACTIVE_PARCEL, parcel);

        Fragment card;
        if (column != 2) {
            card = new MainCardFragment();
            bundle.putInt(AppConstant.BUNDLE_LAYOUT_ID, R.layout.fragment_cards);
            bundle.putInt(AppConstant.BUNDLE_COLUMN_INDEX, column);
        } else {
            card = new WearableMapFragment();
        }
        card.setArguments(bundle);

        return card;
    }

    private Fragment showLoading(int column, ActiveParcel parcel) {
        Bundle bundle = new Bundle();
        Fragment card;
        if (column == 0) {
            card = new MainCardFragment();
            bundle.putSerializable(AppConstant.BUNDLE_ACTIVE_PARCEL, parcel);
            bundle.putInt(AppConstant.BUNDLE_COLUMN_INDEX, column);
            bundle.putInt(AppConstant.BUNDLE_LAYOUT_ID, R.layout.fragment_cards);
        } else {
            requestParcelData(parcel);
            card = new LoadingFragment();
        }
        card.setArguments(bundle);

        return card;
    }

    private void requestParcelData(final ActiveParcel parcel) {
        final WearableUtil wearableUtil = new WearableUtil(context);
        wearableUtil.setWearableConnectionListener(new WearableUtil.WearableConnectListener() {
            @Override
            public void connectionSuccess() {
                wearableUtil.requestParcelData(parcel);
                parcel.isLoading = true;
            }

            @Override
            public void connectionFailed() {
            }
        });
        wearableUtil.startConnection();
    }

    private Fragment showUnknown(int column, ActiveParcel parcel) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstant.BUNDLE_ACTIVE_PARCEL, parcel);
        bundle.putInt(AppConstant.BUNDLE_COLUMN_INDEX, column);

        Fragment card = new MainCardFragment();
        bundle.putInt(AppConstant.BUNDLE_LAYOUT_ID, R.layout.fragment_cards);
        card.setArguments(bundle);

        return card;
    }

    private Fragment showCollectFromDepot(int column, ActiveParcel parcel) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstant.BUNDLE_ACTIVE_PARCEL, parcel);
        bundle.putInt(AppConstant.BUNDLE_COLUMN_INDEX, column);

        Fragment card;
        if (parcel.depotDiaryDate != null) {
            if (column == 0 || column == 1) {
                card = new MainCardFragment();
                bundle.putInt(AppConstant.BUNDLE_LAYOUT_ID, R.layout.fragment_cards);
            } else if (column == 2) {
                card = new MainCardFragment();
                bundle.putInt(AppConstant.BUNDLE_LAYOUT_ID, R.layout.fragment_cards_shop_address);
            } else {
                card = new WearableMapFragment();
            }
        } else {
            if (column == 0) {
                card = new MainCardFragment();
                bundle.putInt(AppConstant.BUNDLE_LAYOUT_ID, R.layout.fragment_cards);
            } else if (column == 1) {
                card = new MainCardFragment();
                bundle.putInt(AppConstant.BUNDLE_LAYOUT_ID, R.layout.fragment_cards_shop_address);
            } else {
                card = new WearableMapFragment();
            }
        }
        card.setArguments(bundle);

        return card;
    }

    private Fragment showCollectFromPickupDirections(int column, ActiveParcel parcel) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstant.BUNDLE_ACTIVE_PARCEL, parcel);
        bundle.putInt(AppConstant.BUNDLE_COLUMN_INDEX, column);

        Fragment card;
        if (parcel.depotDiaryDate != null) {
            if (column == 0 || column == 1) {
                card = new MainCardFragment();
                bundle.putInt(AppConstant.BUNDLE_LAYOUT_ID, R.layout.fragment_cards);
            } else if (column == 2) {
                card = new MainCardFragment();
                bundle.putInt(AppConstant.BUNDLE_LAYOUT_ID, R.layout.fragment_cards_shop_address);
            } else {
                card = new WearableMapFragment();
            }
        } else {
            if (column == 0) {
                card = new MainCardFragment();
                bundle.putInt(AppConstant.BUNDLE_LAYOUT_ID, R.layout.fragment_cards);
            } else if (column == 1) {
                card = new MainCardFragment();
                bundle.putInt(AppConstant.BUNDLE_LAYOUT_ID, R.layout.fragment_cards_shop_address);
            } else {
                card = new WearableMapFragment();
            }
        }
        card.setArguments(bundle);

        return card;
    }

    private Fragment showCollectFromPickupPass(int column, ActiveParcel parcel) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstant.BUNDLE_ACTIVE_PARCEL, parcel);
        bundle.putInt(AppConstant.BUNDLE_COLUMN_INDEX, column);

        Fragment card;
        if (column == 0) {
            card = new MainCardFragment();
            bundle.putInt(AppConstant.BUNDLE_LAYOUT_ID, R.layout.fragment_cards);
        } else if (column == 1) {
            card = new MainCardFragment();
            bundle.putInt(AppConstant.BUNDLE_LAYOUT_ID, R.layout.fragment_cards_shop_address);
        } else if (column == 2) {
            card = new MainCardFragment();
            bundle.putInt(AppConstant.BUNDLE_LAYOUT_ID, R.layout.fragment_cards_pass);
        } else {
            card = new WearableMapFragment();
        }
        card.setArguments(bundle);

        return card;
    }

    private Fragment showIssueInformation(int column, ActiveParcel parcel) {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstant.BUNDLE_LAYOUT_ID, R.layout.fragment_cards_issue);
        bundle.putSerializable(AppConstant.BUNDLE_ACTIVE_PARCEL, parcel);
        bundle.putInt(AppConstant.BUNDLE_COLUMN_INDEX, column);

        Fragment card = new MainCardFragment();
        card.setArguments(bundle);

        return card;
    }

    private Fragment showEstimatedDate(int column, ActiveParcel parcel) {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstant.BUNDLE_LAYOUT_ID, R.layout.fragment_cards);
        bundle.putSerializable(AppConstant.BUNDLE_ACTIVE_PARCEL, parcel);
        bundle.putInt(AppConstant.BUNDLE_COLUMN_INDEX, column);

        Fragment card = new MainCardFragment();
        card.setArguments(bundle);

        return card;
    }

    private Fragment showDelivered(int column, ActiveParcel parcel) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstant.BUNDLE_ACTIVE_PARCEL, parcel);
        bundle.putInt(AppConstant.BUNDLE_COLUMN_INDEX, column);

        Fragment card = new MainCardFragment();
        if (column == 0) {
            bundle.putInt(AppConstant.BUNDLE_LAYOUT_ID, R.layout.fragment_cards);
        } else {
            bundle.putInt(AppConstant.BUNDLE_LAYOUT_ID, R.layout.fragment_photo);
        }
        card.setArguments(bundle);

        return card;
    }
}
