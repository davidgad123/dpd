package com.dpd.wearable;

import com.dpd.yourdpd.AppComponent;
import com.dpd.yourdpd.PerActivity;
import com.dpd.yourdpd.datastore.DataStore;
import com.dpd.yourdpd.model.User;
import com.dpd.yourdpd.net.Api;

import dagger.Component;

@PerActivity
@Component(dependencies = AppComponent.class, modules = WearableModule.class)
public interface WearableComponent {
    void inject(BaseService service);
    Api api();
    User user();
    DataStore dataStore();
}
