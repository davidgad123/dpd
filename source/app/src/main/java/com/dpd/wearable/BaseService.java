package com.dpd.wearable;

import com.dpd.yourdpd.YourDPDApp;
import com.dpd.yourdpd.datastore.DataStore;
import com.dpd.yourdpd.model.User;
import com.dpd.yourdpd.net.Api;

import javax.inject.Inject;

public class BaseService {
    protected @Inject DataStore dataStore;
    protected @Inject Api api;
    protected @Inject User user;
    protected WearableComponent serviceComponent;

    public BaseService() {
        serviceComponent = DaggerWearableComponent.builder()
                .appComponent(YourDPDApp.getAppComponent())
                .wearableModule(new WearableModule())
                .build();
        serviceComponent.inject(this);
    }
}
