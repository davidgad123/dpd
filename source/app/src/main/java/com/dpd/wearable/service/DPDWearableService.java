package com.dpd.wearable.service;

import android.content.Intent;
import android.support.v4.app.NotificationManagerCompat;

import com.common.Constants;
import com.dpd.yourdpd.model.Parcel;
import com.dpd.yourdpd.model.ParcelHistory;
import com.dpd.yourdpd.net.APIError;
import com.dpd.yourdpd.ui.LoadingActivity;
import com.dpd.yourdpd.ui.yourdeliveries.ParcelDetailActivity;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.WearableListenerService;
import com.model.ActiveParcel;
import com.util.WearableUtil;

import java.util.ArrayList;
import java.util.List;

public class DPDWearableService extends WearableListenerService {
    private WearableUtil wearableUtil = null;

    @Override
    public void onCreate() {
        super.onCreate();
        wearableUtil = new WearableUtil(getApplicationContext());
        wearableUtil.startConnection();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDataChanged(DataEventBuffer dataEvents) {
        for (DataEvent dataEvent : dataEvents) {
            if (dataEvent.getType() == DataEvent.TYPE_CHANGED) {
                if (Constants.DATA_REQUEST_PATH.equals(dataEvent.getDataItem().getUri().getPath())) {
                    DataMapItem dataMapItem = DataMapItem.fromDataItem(dataEvent.getDataItem());
                    int requestCode = dataMapItem.getDataMap().getInt(Constants.REQUEST_CODE);
                    ArrayList<DataMap> extra = dataMapItem.getDataMap().getDataMapArrayList(Constants.EXTRA_BUNDLE);
                    processRequest(requestCode, extra);
                }
            }
        }
    }

    private void processRequest(int requestCode, ArrayList<DataMap> extra) {
        switch (requestCode) {
            case Constants.REQUEST_PARCEL_HISTORY:
                getParcelData(requestCode, null);
                break;
            case Constants.REQUEST_PARCEL_DETAIL:
                getParcelData(requestCode, extra.get(0).getString(Constants.PARCEL_CODE));
                break;
            case Constants.REQUEST_OPEN_APP_PARCEL_DETAIL:
                openParcelDetailScreen(extra);
                break;
            case Constants.REQUEST_DISMISS_NOTIFICATION:
                dismissNotification(extra);
                break;
            case Constants.REQUEST_OPEN_APP_TO_AUTH:
                openAppToAuth();
                break;
        }
    }

    private void getParcelData(int requestCode, Object o) {
        DPDWearableDataProvider data = new DPDWearableDataProvider();
        data.setGetParcelDataListener(new DPDWearableDataProvider.WearableDataProviderListener() {
            @Override
            public void onLoadedParcelHistory(ParcelHistory parcelHistory) {
                if (parcelHistory != null)
                    sendParcelDataToWatch(parcelHistory, Constants.RESPONSE_PARCEL_HISTORY);
                else
                    sendThrowableToWatch(Constants.ThrowableType.UNAUTHORIZED, null);
            }

            @Override
            public void onLoadSuccess(ActiveParcel activeParcel) {
                sendParcelDataToWatch(activeParcel, Constants.RESPONSE_PARCEL_DETAIL);
            }

            @Override
            public void onLoadFailed(Object o, List<APIError> errors) {
                //sendParcelDataToWatch(o);
            }

            @Override
            public void onThrowable(Throwable throwable) {
                processThrowable(throwable);
            }
        });

        if (requestCode == Constants.REQUEST_PARCEL_HISTORY)
            data.getAllParcelData();
        else
            data.getParcelDetail((String) o);
    }

    private void processThrowable(Throwable throwable) {
        if (throwable.getMessage().equals("401 Unauthorized")) {
            sendThrowableToWatch(Constants.ThrowableType.UNAUTHORIZED, throwable.getMessage());
        }
    }

    private void sendThrowableToWatch(Constants.ThrowableType throwableType, String detailMessage) {
        final ArrayList<DataMap> dataToWatch = new ArrayList<>();
        DataMap mapItem = new DataMap();
        mapItem.putInt(Constants.THROWABLE_TYPE, throwableType.ordinal());
        mapItem.putString(Constants.THROWABLE_MESSAGE, detailMessage);
        dataToWatch.add(mapItem);

        if (wearableUtil.isConnected())
            wearableUtil.sendDataAndRequest(Constants.SEND_THROWABLE, dataToWatch);
        else {
            wearableUtil.startConnection();
            wearableUtil.setWearableConnectionListener(new WearableUtil.WearableConnectListener() {
                @Override
                public void connectionSuccess() {
                    wearableUtil.sendDataAndRequest(Constants.SEND_THROWABLE, dataToWatch);
                }

                @Override
                public void connectionFailed() {
                }
            });
        }
    }

    private void sendParcelDataToWatch(Object object, final int response) {
        if (object == null) return;

        final ArrayList<DataMap> dataToWatch = new ArrayList<>();
        if (response == Constants.RESPONSE_PARCEL_HISTORY) {
            ParcelHistory parcelHistory = (ParcelHistory) object;
            for (int i = 0; i < parcelHistory.getActiveParcels().size(); i++) {
                Parcel parcel = parcelHistory.getActiveParcels().get(i);
                DataMap mapItem = new DataMap();
                mapItem.putString(Constants.PARCEL_NUMBER, parcel.parcelNumber);
                mapItem.putString(Constants.PARCEL_CODE, parcel.parcelCode);
                mapItem.putString(Constants.PARCEL_ORGANISATION, parcel.getShipperName());
                mapItem.putString(Constants.PARCEL_TRACKING_STATUS, parcel.trackingStatusCurrent);
                mapItem.putInt(Constants.PARCEL_TYPE, Constants.ParcelStatusType.LOADING.getValue());
                dataToWatch.add(mapItem);
            }
        } else if (object instanceof ActiveParcel) {
            DataMap mapItem = new DataMap();
            ActiveParcel activeParcel = (ActiveParcel) object;
            mapItem.putString(Constants.PARCEL_NUMBER, activeParcel.parcelNumber);
            mapItem.putString(Constants.PARCEL_CODE, activeParcel.parcelCode);
            mapItem.putString(Constants.PARCEL_ORGANISATION, activeParcel.organisation);
            mapItem.putString(Constants.PARCEL_TRACKING_STATUS, activeParcel.trackingStatusCurrent);
            mapItem.putInt(Constants.PARCEL_TYPE, activeParcel.parcelStatusType.getValue());
            mapItem.putFloat(Constants.PARCEL_DELIVERY_LATITUDE, activeParcel.deliveryLatitude);
            mapItem.putFloat(Constants.PARCEL_DELIVERY_LONGITUDE, activeParcel.deliveryLongitude);
            mapItem.putInt(Constants.PARCEL_ESTIMATED_MINS_TO_STOP, activeParcel.estimatedMinsToStop);
            mapItem.putString(Constants.PARCEL_DRIVER_NAME, activeParcel.driverName);
            mapItem.putFloat(Constants.PARCEL_ROUTE_LATITUDE, activeParcel.routeLatitude);
            mapItem.putFloat(Constants.PARCEL_ROUTE_LONGITUDE, activeParcel.routeLongitude);
            mapItem.putString(Constants.PARCEL_SHOP_LOCATION_ADDRESS, activeParcel.shopAddress);
            mapItem.putString(Constants.PARCEL_SHOP_LOCATION_SHORT_ADDRESS, activeParcel.shopShortAddress);
            mapItem.putFloat(Constants.PARCEL_SHOP_LATITUDE, activeParcel.shopLatitude);
            mapItem.putFloat(Constants.PARCEL_SHOP_LONGITUDE, activeParcel.shopLongitude);
            mapItem.putString(Constants.PARCEL_ESTIMATED_DATE, activeParcel.estimatedDeliveryDate);
            mapItem.putString(Constants.PARCEL_ISSUE_NAME, activeParcel.issueName);
            mapItem.putString(Constants.PARCEL_ISSUE_CODE, activeParcel.issueCode);
            mapItem.putString(Constants.PARCEL_DEPOT_DIARY_DATE, activeParcel.depotDiaryDate);
            mapItem.putString(Constants.PARCEL_DEPOT_NAME, activeParcel.depotName);
            mapItem.putString(Constants.PARCEL_DEPOT_START_TIME, activeParcel.depotStartTime);
            mapItem.putString(Constants.PARCEL_DEPOT_END_TIME, activeParcel.depotEndTime);
            mapItem.putDataMapArrayList(Constants.PARCEL_ASSET_PHOTOS, activeParcel.photos);
            dataToWatch.add(mapItem);
        }

        if (wearableUtil.isConnected())
            wearableUtil.sendDataAndRequest(response, dataToWatch);
        else {
            wearableUtil.startConnection();
            wearableUtil.setWearableConnectionListener(new WearableUtil.WearableConnectListener() {
                @Override
                public void connectionSuccess() {
                    wearableUtil.sendDataAndRequest(response, dataToWatch);
                }

                @Override
                public void connectionFailed() {
                }
            });
        }
    }

    private void openParcelDetailScreen(ArrayList<DataMap> extra) {
        if (extra != null) {
            DataMap item = extra.get(0);
            String parcelCode = item.getString(Constants.PARCEL_CODE);
            Intent intent = new Intent(getApplicationContext(), ParcelDetailActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(ParcelDetailActivity.EXTRA_PARCEL_CODE, parcelCode);
            startActivity(intent);
        }
    }

    private void openAppToAuth() {
        Intent intent = new Intent(getApplicationContext(), LoadingActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void dismissNotification(ArrayList<DataMap> extra) {
        if (extra != null) {
            DataMap item = extra.get(0);
            int notification_id = item.getInt(Constants.NOTIFICATION_ID);
            NotificationManagerCompat.from(this).cancel(notification_id);
        }
    }
}
