package com.dpd.wearable.service;

import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.Log;

import com.common.Constants;
import com.crashlytics.android.Crashlytics;
import com.dpd.wearable.BaseService;
import com.dpd.yourdpd.BuildConfig;
import com.dpd.yourdpd.model.Base64Photo;
import com.dpd.yourdpd.model.Depot;
import com.dpd.yourdpd.model.ImageType;
import com.dpd.yourdpd.model.Parcel;
import com.dpd.yourdpd.model.ParcelEvent;
import com.dpd.yourdpd.model.ParcelHistory;
import com.dpd.yourdpd.model.Photo;
import com.dpd.yourdpd.model.PickupLocation;
import com.dpd.yourdpd.model.RouteView;
import com.dpd.yourdpd.net.APIError;
import com.dpd.yourdpd.net.response.GetDepotResponse;
import com.dpd.yourdpd.net.response.GetImageResponse;
import com.dpd.yourdpd.net.response.GetParcelDetailsResponse;
import com.dpd.yourdpd.net.response.GetParcelEventsResponse;
import com.dpd.yourdpd.net.response.GetPickupLocationResponse;
import com.dpd.yourdpd.net.response.GetRouteViewOnRouteResponse;
import com.dpd.yourdpd.util.DateUtils;
import com.dpd.yourdpd.util.PhotoUtils;
import com.google.android.gms.wearable.Asset;
import com.google.android.gms.wearable.DataMap;
import com.model.ActiveParcel;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.functions.Func2;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class DPDWearableDataProvider extends BaseService {
    private final static String TAG = DPDWearableDataProvider.class.getSimpleName();

    private WearableDataProviderListener mListener = null;

    public void getAllParcelData() {
        if (api == null)
            return;

        // Load parcel history.
        ParcelHistory parcelHistory = dataStore.getParcelHistory();
        if (mListener != null)
            mListener.onLoadedParcelHistory(parcelHistory);

        // Load each parcel details
//        if (parcelHistory != null) {
//            List<Parcel> parcelList = parcelHistory.getActiveParcels();
//            for (int i = 0; i < parcelList.size(); i++) {
//                Parcel parcel = parcelList.get(i);
//                if (parcel.shipperDetails == null)
//                    getParcelDetailByURL(parcel.parcelCode);
//                else
//                    getParcelDetailDataByType(parcel);
//            }
//        }
    }

    public void getParcelDetail(String parcelCode) {
        if (api == null || dataStore == null || dataStore.getParcelHistory() == null) {
            if (mListener != null)
                mListener.onLoadFailed(parcelCode, null);
            return;
        }

        ParcelHistory parcelHistory = dataStore.getParcelHistory();
        Parcel parcel = parcelHistory.getParcelWithParcelCode(parcelCode);
        if (parcel == null) {
            if (mListener != null)
                mListener.onLoadFailed(parcelCode, null);
            return;
        }

        if (parcel.shipperDetails == null)
            getParcelDetailByURL(parcelCode);
        else
            getParcelDetailDataByType(parcel);
    }

    private void getParcelDetailByURL(final String parcelCode) {
        api.getParcelDetails(parcelCode)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<GetParcelDetailsResponse>() {
                    @Override
                    public void call(GetParcelDetailsResponse response) {
                        if (response.hasErrors()) {
                            Log.e(TAG, "Parcel detail error: " + response.printErrors());
                            if (BuildConfig.LOG_ERRORS_TO_SERVER && !BuildConfig.DEBUG) {
                                Crashlytics.logException(new Exception(response.printErrors()));
                            }
                            if (mListener != null)
                                mListener.onLoadFailed(parcelCode, response.getErrors());
                            return;
                        }
                        Parcel parcel = response.getParcel();
                        // ensure fully-populated parcel data available for further action Fragments
                        dataStore.getParcelHistory().replaceParcel(parcelCode, parcel);
                        getParcelDetailDataByType(parcel);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (mListener != null)
                            mListener.onThrowable(throwable);
                    }
                });
    }

    private synchronized void getParcelDetailDataByType(Parcel parcel) {
        switch (parcel.parcelStatusType) {
            case OUT_FOR_DELIVERY:
                getRouteView(parcel);
                break;
            case COLLECT_FROM_DEPOT:
                getCollectFromDepot(parcel);
                break;
            case COLLECT_FROM_PICKUP:
                getFromPickUp(parcel);
                break;
            case COLLECT_FROM_PICKUP_WITH_PASS:
                getFromPickUpWithPass(parcel);
                break;
            case SHOW_ESTIMATED_DELIVERY_DATE:
                getEstimatedDeliveryDate(parcel);
                break;
            case DELIVERED:
                getDeliveredInfo(parcel);
                break;
            case ISSUE:
                getIssues(parcel);
                break;
            case UNKNOWN:
                getUnknownInfo(parcel);
                break;
        }
    }

    private void getUnknownInfo(Parcel parcel) {
        final ActiveParcel activeParcel = convertParcelToActiveParcel(parcel);
        if (mListener != null)
            mListener.onLoadSuccess(activeParcel);
    }

    private void getDeliveredInfo(final Parcel parcel) {
        final ActiveParcel activeParcel = convertParcelToActiveParcel(parcel);
        api.getParcelEvents(parcel.parcelCode)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<GetParcelEventsResponse>() {
                    @Override
                    public void call(GetParcelEventsResponse response) {
                        if (response.hasErrors()) {
                            Log.e(TAG, "Parcel detail error: " + response.printErrors());
                            if (BuildConfig.LOG_ERRORS_TO_SERVER && !BuildConfig.DEBUG) {
                                Crashlytics.logException(new Exception(response.printErrors()));
                            }
                            if (mListener != null)
                                mListener.onLoadFailed(parcel, response.getErrors());
                            return;
                        }
                        new CompositeSubscription(getPhotos(response.getParcelEvents(), activeParcel));
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (mListener != null)
                            mListener.onThrowable(throwable);
                    }
                });
    }

    private void getFromPickUp(Parcel parcel) {
        final ActiveParcel activeParcel = convertParcelToActiveParcel(parcel);
        api.getPickupLocation(parcel.pickupLocationCode)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<GetPickupLocationResponse>() {
                    @Override
                    public void call(GetPickupLocationResponse response) {
                        if (response.hasErrors()) {
                            Log.e(TAG, "Parcel detail error: " + response.printErrors());
                            if (BuildConfig.LOG_ERRORS_TO_SERVER && !BuildConfig.DEBUG) {
                                Crashlytics.logException(new Exception(response.printErrors()));
                            }
                            if (mListener != null)
                                mListener.onLoadFailed(activeParcel, response.getErrors());
                            return;
                        }
                        addPickupLocationToActiveParcel(activeParcel, response.getPickupLocation());
                        if (mListener != null)
                            mListener.onLoadSuccess(activeParcel);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (mListener != null)
                            mListener.onThrowable(throwable);
                    }
                });
    }

    private void getFromPickUpWithPass(Parcel parcel) {
        final ActiveParcel activeParcel = convertParcelToActiveParcel(parcel);
        api.getPickupLocation(parcel.pickupLocationCode)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<GetPickupLocationResponse>() {
                    @Override
                    public void call(GetPickupLocationResponse response) {
                        if (response.hasErrors()) {
                            Log.e(TAG, "Parcel detail error: " + response.printErrors());
                            if (BuildConfig.LOG_ERRORS_TO_SERVER && !BuildConfig.DEBUG) {
                                Crashlytics.logException(new Exception(response.printErrors()));
                            }
                            if (mListener != null)
                                mListener.onLoadFailed(activeParcel, response.getErrors());
                            return;
                        }
                        addPickupLocationToActiveParcel(activeParcel, response.getPickupLocation());
                        if (mListener != null)
                            mListener.onLoadSuccess(activeParcel);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (mListener != null)
                            mListener.onThrowable(throwable);
                    }
                });
    }

    private void getCollectFromDepot(Parcel parcel) {
        final ActiveParcel activeParcel = convertParcelToActiveParcel(parcel);
        api.getDepot(parcel.deliveryDepot.depotCode)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<GetDepotResponse>() {
                    @Override
                    public void call(GetDepotResponse response) {
                        if (response.hasErrors()) {
                            Log.e(TAG, "Parcel detail error: " + response.printErrors());
                            if (BuildConfig.LOG_ERRORS_TO_SERVER && !BuildConfig.DEBUG) {
                                Crashlytics.logException(new Exception(response.printErrors()));
                            }
                            if (mListener != null)
                                mListener.onLoadFailed(activeParcel, response.getErrors());
                            return;
                        }
                        addCollectFromDepot(activeParcel, response.getDepot());
                        if (mListener != null)
                            mListener.onLoadSuccess(activeParcel);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (mListener != null)
                            mListener.onThrowable(throwable);
                    }
                });
    }

    private void getRouteView(Parcel parcel) {
        final ActiveParcel activeParcel = convertParcelToActiveParcel(parcel);
        String depotCode = parcel.deliveryDepot.depotCode;
        String routeKey = parcel.deliveryDepot.route.routeCode;
        api.getRouteViewOnRoute(depotCode, routeKey)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<GetRouteViewOnRouteResponse>() {
                               @Override
                               public void call(GetRouteViewOnRouteResponse response) {
                                   if (response.hasErrors()) {
                                       Log.e(TAG, "Parcel detail error: " + response.printErrors());
                                       if (BuildConfig.LOG_ERRORS_TO_SERVER && !BuildConfig.DEBUG) {
                                           Crashlytics.logException(new Exception(response.printErrors()));
                                       }
                                       if (mListener != null)
                                           mListener.onLoadFailed(activeParcel, response.getErrors());
                                       return;
                                   }
                                   addRouteInfoToActiveParcel(activeParcel, response.getRouteView());
                                   if (mListener != null)
                                       mListener.onLoadSuccess(activeParcel);
                               }
                           },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                if (mListener != null)
                                    mListener.onThrowable(throwable);
                            }
                        });
    }

    private void getEstimatedDeliveryDate(Parcel parcel) {
        final ActiveParcel activeParcel = convertParcelToActiveParcel(parcel);
        mListener.onLoadSuccess(activeParcel);
    }

    private void getIssues(Parcel parcel) {
        final ActiveParcel activeParcel = convertParcelToActiveParcel(parcel);
        mListener.onLoadSuccess(activeParcel);
    }

    private ActiveParcel convertParcelToActiveParcel(Parcel parcel) {
        ActiveParcel activeParcel = new ActiveParcel();
        activeParcel.organisation = parcel.getShipperName();
        activeParcel.parcelCode = parcel.parcelCode;
        activeParcel.parcelNumber = parcel.parcelNumber;
        activeParcel.parcelStatusType = Constants.ParcelStatusType.fromValue(parcel.parcelStatusType.ordinal());
        activeParcel.trackingStatusCurrent = parcel.trackingStatusCurrent;

        if (parcel.deliveryDepot != null
                && parcel.deliveryDepot.route != null
                && parcel.deliveryDepot.route.stop != null) {
            activeParcel.estimatedMinsToStop = parcel.deliveryDepot.route.stop.estimatedMinsToStop;
        }

        if (parcel.deliveryDetails != null
                && parcel.deliveryDetails.address != null
                && parcel.deliveryDetails.address.addressPoint != null) {
            activeParcel.deliveryLatitude = parcel.deliveryDetails.address.addressPoint.latitude;
            activeParcel.deliveryLongitude = parcel.deliveryDetails.address.addressPoint.longitude;
        }

        if (parcel.estimatedDeliveryDate != null) {
            final Calendar cal = Calendar.getInstance();
            Date date = DateUtils.getDate(parcel.estimatedDeliveryDate, "yyyy-MM-dd");
            cal.setTime(date);
            final String dateSuffix = DateUtils.getDayOfMonthSuffix(cal.get(Calendar.DATE));
            final SimpleDateFormat dateFormat = new SimpleDateFormat(String.format("d'%s' MMMM", dateSuffix), Locale.UK);
            activeParcel.estimatedDeliveryDate = dateFormat.format(date);
        }

        if (parcel.getIssueName() != null) {
            activeParcel.issueName = parcel.getIssueName().toLowerCase();
        }

        if (parcel.issueDetails != null) {
            activeParcel.issueCode = parcel.issueDetails.issueCode;
        }

        if (parcel.collectFromDepotDiaryDate != null) {
            final DateTimeFormatter dateFormat = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
            final DateTime dateTime1 = dateFormat.parseDateTime(parcel.collectFromDepotDiaryDate);
            final DateTime dateTime2 = dateTime1.withDurationAdded(1000 * 60 * 60, 1);
            activeParcel.depotDiaryDate = DateTimeFormat.forPattern("d").print(dateTime1) + DateUtils.getDayOfMonthSuffix(dateTime1.getDayOfMonth())
                    + DateTimeFormat.forPattern(" MMMM").print(dateTime1);
            activeParcel.depotStartTime = DateTimeFormat.forPattern("hh:mm").print(dateTime1);
            activeParcel.depotEndTime = DateTimeFormat.forPattern("hh:mm").print(dateTime2);
        }
        return activeParcel;
    }

    private void addRouteInfoToActiveParcel(ActiveParcel activeParcel, RouteView viewInfo) {
        activeParcel.driverName = viewInfo.driverName;
        activeParcel.routeLatitude = viewInfo.latitude;
        activeParcel.routeLongitude = viewInfo.longitude;
    }

    private void addPickupLocationToActiveParcel(ActiveParcel activeParcel, PickupLocation locationInfo) {
        activeParcel.shopAddress = locationInfo.address.toFormattedString();
        activeParcel.shopShortAddress = locationInfo.address.getFirstLine();
        activeParcel.shopLatitude = locationInfo.addressPoint.latitude;
        activeParcel.shopLongitude = locationInfo.addressPoint.longitude;
    }

    private void addCollectFromDepot(ActiveParcel activeParcel, Depot depotInfo) {
        activeParcel.depotName = depotInfo.address.organisation;
        activeParcel.shopAddress = depotInfo.address.toFormattedString();
        activeParcel.shopLatitude = depotInfo.addressPoint.latitude;
        activeParcel.shopLongitude = depotInfo.addressPoint.longitude;
    }

    public void setGetParcelDataListener(WearableDataProviderListener listener) {
        mListener = listener;
    }

    public interface WearableDataProviderListener {
        void onLoadedParcelHistory(ParcelHistory parcelHistory);

        void onLoadSuccess(ActiveParcel parcel);

        void onLoadFailed(Object o, List<APIError> errors);

        void onThrowable(Throwable throwable);
    }

    private Subscription getPhotos(List<ParcelEvent> parcelEvents, final ActiveParcel activeParcel) {
        final List<Photo> photos = new ArrayList<>(parcelEvents.size());
        for (ParcelEvent parcelEvent : parcelEvents) {
            if (parcelEvent.imageKey != null && parcelEvent.imageKey.length > 0) {
                for (int i = 0; i < parcelEvent.imageKey.length; i++) {
                    if (!TextUtils.isEmpty(parcelEvent.imageKey[i]) &&
                            parcelEvent.imageType.length > i &&
                            parcelEvent.imageType[i].equals(ImageType.SATURN_SAFE_PLACE)) {
                        final Photo photo = new Photo(
                                parcelEvent.parcelCode,
                                parcelEvent.imageKey[i],
                                parcelEvent.imageType[i],
                                parcelEvent.imageCaption[i]
                        );
                        photos.add(photo);
                    }
                }
            }
        }

        return Observable.from(photos)
                .subscribeOn(Schedulers.io())
                .flatMap(new Func1<Photo, Observable<Base64Photo>>() {
                    @Override
                    public Observable<Base64Photo> call(Photo photo) {
                        return Observable.zip(
                                Observable.just(photo),
                                api.getRackspaceImage(photo.imageKey, ImageType.SATURN_SAFE_PLACE),
                                new Func2<Photo, GetImageResponse, Base64Photo>() {
                                    @Override
                                    public Base64Photo call(Photo photo, GetImageResponse response) {
                                        if (response.hasErrors()) {
                                            return null;
                                        }
                                        return new Base64Photo(
                                                response.getImage(), // base64Data string
                                                photo.imageKey,
                                                TextUtils.isEmpty(photo.imageCaption) ? "" : photo.imageCaption
                                        );
                                    }
                                }
                        );
                    }
                })
                .toList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<List<Base64Photo>>() {
                               @Override
                               public void call(List<Base64Photo> photos) {
                                   createAssetFromBase64String(photos, activeParcel);
                               }
                           },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                if (mListener != null)
                                    mListener.onThrowable(throwable);
                            }
                        });
    }

    private void createAssetFromBase64String(List<Base64Photo> photos, final ActiveParcel activeParcel) {
        if (activeParcel.photos == null) {
            activeParcel.photos = new ArrayList<>();
        } else {
            activeParcel.photos.clear();
        }
        for (Base64Photo base64Photo : photos) {
            Asset photo = createAssetFromBitmap(PhotoUtils.base64ToBitmap(base64Photo.base64Data));
            DataMap item = new DataMap();
            item.putAsset(Constants.PARCEL_ASSET_PHOTOS, photo);
            item.putString(Constants.PARCEL_ASSET_PHOTO_IMAGE_NAME, base64Photo.caption);
            activeParcel.photos.add(item);
        }
        mListener.onLoadSuccess(activeParcel);
    }

    private Asset createAssetFromBitmap(Bitmap bitmap) {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        return Asset.createFromBytes(byteArrayOutputStream.toByteArray());
    }
}
