package com.dpd.yourdpd.model;

public class NotificationDetails {
    public String mobile;
    public String email;
    public String contactName;
}
