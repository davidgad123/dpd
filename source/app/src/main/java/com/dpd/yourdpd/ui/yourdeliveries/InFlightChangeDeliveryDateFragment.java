package com.dpd.yourdpd.ui.yourdeliveries;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;

import com.dpd.yourdpd.R;
import com.dpd.yourdpd.model.Parcel;
import com.dpd.yourdpd.model.ParcelActionCodes;
import com.dpd.yourdpd.net.request.ChangeParcelDateParams;
import com.dpd.yourdpd.net.response.ChangeParcelDateResponse;
import com.dpd.yourdpd.net.response.GetParcelUpdateDatesResponse;
import com.dpd.yourdpd.ui.BaseFragment;
import com.dpd.yourdpd.ui.BaseMenuActivity;
import com.dpd.yourdpd.util.CommonUtils;
import com.dpd.yourdpd.util.UIUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link InFlightChangeDeliveryDateFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link InFlightChangeDeliveryDateFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class InFlightChangeDeliveryDateFragment extends BaseFragment
        implements RadioGroup.OnCheckedChangeListener {

    private static final String TAG = InFlightChangeDeliveryDateFragment.class.getSimpleName();

    // the fragment initialization parameters
    private static final String ARG_PARCEL_CODE = "parcel_code";

    private OnFragmentInteractionListener listener;
    private Parcel parcel;
    private RadioGroup deliveryDatesRadioGroup;
    private Button confirmChangesButton;
    private ContentLoadingProgressBar progressBar;
    private ScrollView scrollView;

    private Subscription getDatesSubscription;
    private Subscription changeDateSubscription;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param parcelCode Parcel.parcelCode
     * @return A new instance of fragment InFlightChangeDeliveryDateFragment.
     */
    public static InFlightChangeDeliveryDateFragment newInstance(String parcelCode) {
        InFlightChangeDeliveryDateFragment fragment = new InFlightChangeDeliveryDateFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARCEL_CODE, parcelCode);
        fragment.setArguments(args);
        return fragment;
    }

    public InFlightChangeDeliveryDateFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            String parcelCode = getArguments().getString(ARG_PARCEL_CODE);
            parcel = dataStore.getParcelWithParcelCode(parcelCode);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.fragment_inflight_change_delivery_date, container, false);
        deliveryDatesRadioGroup = (RadioGroup) root.findViewById(R.id.delivery_dates_radiogroup);
        deliveryDatesRadioGroup.setOnCheckedChangeListener(this);
        confirmChangesButton = (Button) root.findViewById(R.id.confirm_changes_button);
        confirmChangesButton.setEnabled(false);
        confirmChangesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmChanges();
            }
        });
        progressBar = (ContentLoadingProgressBar) root.findViewById(R.id.progressBar);
        scrollView = (ScrollView) root.findViewById(R.id.scrollView);
        return root;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (OnFragmentInteractionListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initParcelStatusView();
        getDateOptions();
    }

    @Override
    public void onResume() {
        super.onResume();
        ((BaseMenuActivity) getActivity()).setupToolbar(getString(R.string.change_delivery_date_titlecase), true);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getDatesSubscription != null && !getDatesSubscription.isUnsubscribed()) {
            getDatesSubscription.unsubscribe();
            getDatesSubscription = null;
        }
        if (changeDateSubscription != null && !changeDateSubscription.isUnsubscribed()) {
            changeDateSubscription.unsubscribe();
            changeDateSubscription = null;
        }
        parcel = null;
    }

    private void getDateOptions() {
        showProgressBar();

        if (getDatesSubscription != null && !getDatesSubscription.isUnsubscribed()) {
            getDatesSubscription.unsubscribe();
        }
        getDatesSubscription = api.getParcelUpdateDates(parcel.parcelCode,
                ParcelActionCodes.CHANGE_DELIVERY_DATE.getValue())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<GetParcelUpdateDatesResponse>() {
                               @Override
                               public void call(GetParcelUpdateDatesResponse response) {
                                   hideProgressBar();
                                   if (response.hasErrors()) {
                                       Log.e(TAG, response.printErrors());
                                       showErrorAlertDialog(getString(R.string.problem_getting_dates));
                                   } else {
                                       final List<String> dates = response.getDiaryDates();
                                       showDateOptions(dates);
                                   }
                               }
                           },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                Log.e(TAG, "Problem getting dates", throwable);
                                hideProgressBar();
                                showErrorAlertDialog(getString(R.string.problem_getting_dates));
                            }
                        });
    }

    private void changeDate(String date) {
        showProgressBar();

        final ChangeParcelDateParams params = new ChangeParcelDateParams.Builder()
                .setParcelCode(parcel.parcelCode)
                .setEmail(user.getConsumerProfile().email)
                .setMobile(CommonUtils.addPrefixNumber(user.getConsumerProfile().mobile))
                .setContactName(user.getFullName())
                .setDiaryDate(date)
                .build();

        if (changeDateSubscription != null && !changeDateSubscription.isUnsubscribed()) {
            changeDateSubscription.unsubscribe();
        }
        changeDateSubscription = api.changeParcelDate(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<ChangeParcelDateResponse>() {
                               @Override
                               public void call(ChangeParcelDateResponse response) {
                                   hideProgressBar();
                                   if (response.hasErrors()) {
                                       Log.e(TAG, response.printErrors());
                                       showErrorAlertDialog(getString(R.string.parcel_status_not_changed), getString(R.string.error_occurred));
                                   } else {
                                       // all done
                                       showActionConfirmedDialog();
                                   }
                               }
                           },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                hideProgressBar();
                                Log.e(TAG, "Problem changing date", throwable);
                                showErrorAlertDialog(getString(R.string.parcel_status_not_changed), getString(R.string.error_occurred));
                            }
                        });
    }

    private void showActionConfirmedDialog() {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(getContext(), R.style.AlertDialogTheme);
        builder.setTitle(R.string.parcel_updated);
        builder.setMessage(R.string.parcel_delivered_date_confirmed);
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // all done - return from this Fragment
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
        builder.create().show();
    }

    private void initParcelStatusView() {
        final View view = getView();
        assert view != null;
        final Context context = getContext();
        UIUtils.populateParcelStatus(view, context, parcel);
    }

    private void showDateOptions(List<String> diaryDates) {
        final View view = getView();
        assert view != null;
        final LayoutInflater inflater = LayoutInflater.from(getActivity());
        final SimpleDateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd", Locale.UK);

        // remove previous
        deliveryDatesRadioGroup.removeAllViews();

        for (int i = 0; i < diaryDates.size(); i++) {
            try {
                final String diaryDate = diaryDates.get(i);
                final Date date = dateParser.parse(diaryDate);
                final Calendar cal = Calendar.getInstance();
                cal.setTime(date);
//                final String dateSuffix = DateUtils.getDayOfMonthSuffix(cal.get(Calendar.DATE));
//                final SimpleDateFormat dateFormat = new SimpleDateFormat(String.format("cccc d'%s' MMMM", dateSuffix), Locale.UK);
                final SimpleDateFormat dateFormat = new SimpleDateFormat("cccc d MMMM", Locale.UK);
                final RadioButton radioButton = (RadioButton) inflater.inflate(R.layout.item_radiobutton, deliveryDatesRadioGroup, false);
                radioButton.setId(i);
                radioButton.setText(dateFormat.format(date));
                radioButton.setTag(diaryDate);
                deliveryDatesRadioGroup.addView(radioButton);
            } catch (ParseException e) {
                Log.e(TAG, "Exception parsing diaryDate", e);
            }
        }

        if (deliveryDatesRadioGroup.getChildCount() > 0) {
            deliveryDatesRadioGroup.check(deliveryDatesRadioGroup.getChildAt(0).getId());
        }
    }

    private void showProgressBar() {
        progressBar.show();
        scrollView.setVisibility(View.GONE);
    }

    private void hideProgressBar() {
        progressBar.hide();
        scrollView.setVisibility(View.VISIBLE);
    }


    private void confirmChanges() {
        final View view = getView();
        assert view != null;

        final RadioButton radioButton = (RadioButton) view.findViewById(deliveryDatesRadioGroup.getCheckedRadioButtonId());
        if (radioButton == null) {
            // TODO: disable confirm button until
            return;
        }

        final String selectedDateStr = radioButton.getText().toString();
        final String selectedDate = (String) radioButton.getTag();
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(getActivity(), R.style.AlertDialogTheme);
        builder.setTitle(getString(R.string.change_delivery_dialog_title));
        builder.setMessage(getString(R.string.change_delivery_date_dialog_message, selectedDateStr));
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                changeDate(selectedDate);
                // TODO: show loading?
            }
        });
        builder.setNegativeButton(android.R.string.cancel, null);
        builder.show();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        confirmChangesButton.setEnabled(checkedId != -1);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
//        public void onInflightPickupFragmentInteraction(Uri uri);
    }
}
