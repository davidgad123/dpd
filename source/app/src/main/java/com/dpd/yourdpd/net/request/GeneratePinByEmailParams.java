package com.dpd.yourdpd.net.request;

/**
 * @see com.dpd.yourdpd.net.Api#generatePinByEmail(GeneratePinByEmailParams)
 */
public class GeneratePinByEmailParams {
    public String email;

    public GeneratePinByEmailParams(String email) {
        this.email = email;
    }
}
