package com.dpd.yourdpd.model;

import android.os.*;
import android.os.Parcel;

/**
 * Ties together relevant information about photos referenced within {@link ParcelEvent}s
 */
public class Photo implements Parcelable {
    public String parcelCode;
    public String imageKey;
    public String imageType;
    public String imageCaption;

    protected Photo(Parcel in) {
        parcelCode = in.readString();
        imageKey = in.readString();
        imageType = in.readString();
        imageCaption = in.readString();
    }

    public static final Creator<Photo> CREATOR = new Creator<Photo>() {
        @Override
        public Photo createFromParcel(Parcel in) {
            return new Photo(in);
        }

        @Override
        public Photo[] newArray(int size) {
            return new Photo[size];
        }
    };

    public Photo(String parcelCode, String imageKey, String imageType, String caption) {
        this.parcelCode = parcelCode;
        this.imageKey = imageKey;
        this.imageType = imageType;
        this.imageCaption = caption;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(parcelCode);
        dest.writeString(imageKey);
        dest.writeString(imageType);
        dest.writeString(imageCaption);
    }
}
