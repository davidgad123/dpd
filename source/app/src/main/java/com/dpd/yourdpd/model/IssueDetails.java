package com.dpd.yourdpd.model;

import android.util.Log;

import com.dpd.yourdpd.R;

import java.io.Serializable;

/**
 * @see Parcel#issueDetails
 */
public class IssueDetails implements Serializable {
    private static final String TAG = IssueDetails.class.getSimpleName();

    public String issueLogoUrl;
    public String issueName;
    public int issueDelayDays;
    public String issueCode;

    public int getIssueIconResId() {
        try {
//        0   Unexpected Delay     images/icon-exclamation.png
//        1   Fire                 images/icon-fire.png
//        2   Flood                images/icon-flood.png
//        3   Snow                 images/icon-snow.png
//        4   Civil Unrest         images/icon-civil.png
//        5   No Sailing           images/icon-sailing.png
//        6   Traffic Incident     images/icon-traffic.png
//        7   High Winds           images/icon-wind.png
//        8   Flight Delay         images/plane-icon.png
//        9   Olympic Delay        images/icon-olympics.png
//        10  Local Event          images/icon-exclamation.png
            if(issueCode.equals("0")) {
                return R.drawable.ic_exclamation;
            } else if(issueCode.equals("1")) {
                return R.drawable.ic_fire;
            } else if(issueCode.equals("2")) {
                return R.drawable.ic_flood;
            } else if(issueCode.equals("3")) {
                return R.drawable.ic_snow;
            } else if(issueCode.equals("4")) {
                return R.drawable.ic_civil_unrest;
            } else if(issueCode.equals("5")) {
                return R.drawable.ic_sailing;
            } else if(issueCode.equals("6")) {
                return R.drawable.ic_traffic;
            } else if(issueCode.equals("7")) {
                return R.drawable.ic_high_wind;
            } else if(issueCode.equals("8")) {
                return R.drawable.ic_plane;
            } else if(issueCode.equals("9")) {
                return R.drawable.ic_olympics;
            } else if(issueCode.equals("10")) {
                return R.drawable.ic_exclamation;
            }
        } catch (NullPointerException e) {
            Log.e(TAG, "issueCode null");
        }

        return R.drawable.ic_exclamation;
    }
}
