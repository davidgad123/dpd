package com.dpd.yourdpd.net.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * @see com.dpd.yourdpd.net.Api#getCollectParcelTimes(String, String)
 */
public class GetCollectParcelTimesResponse extends BaseResponse {
    @SerializedName("data")
    private TimePair pair;

    public List<TimePair> getTimes() {
        // TODO: I'm expecting the API to return an array, so let's hide that for now
        List<TimePair> list = new ArrayList<>();
        list.add(pair);
        return list;
    }

    public static class TimePair {
        /**
         * from hh:mm
         */
        public String startTime;
        /**
         * to hh:mm
         */
        public String endTime;
    }
}
