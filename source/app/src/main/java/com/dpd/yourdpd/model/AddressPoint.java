package com.dpd.yourdpd.model;

import com.google.android.gms.maps.model.LatLng;

public class AddressPoint {
    public float latitude;
    public float longitude;

    public LatLng toLatLng() {
        return new LatLng(latitude, longitude);
    }
}
