package com.dpd.yourdpd.ui.address;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.text.TextUtils;
import android.transitions.everywhere.TransitionManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.dpd.yourdpd.BuildConfig;
import com.dpd.yourdpd.R;
import com.dpd.yourdpd.model.Address;
import com.dpd.yourdpd.model.ConsumerProfile;
import com.dpd.yourdpd.model.ConsumerPropertyAddressInfo;
import com.dpd.yourdpd.model.ConsumerPropertyLabel;
import com.dpd.yourdpd.net.response.CheckPropertyResponse;
import com.dpd.yourdpd.net.response.LookupPostKeyResponse;
import com.dpd.yourdpd.net.response.SearchByAddressResponse;
import com.dpd.yourdpd.net.response.SearchByAddressResponse.AddressSearchResult;
import com.dpd.yourdpd.ui.BaseActivity;
import com.dpd.yourdpd.util.AlphaNumComparator;
import com.dpd.yourdpd.util.CommonUtils;
import com.dpd.yourdpd.util.ViewIdGenerator;

import java.util.Collections;
import java.util.List;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.functions.Func3;
import rx.schedulers.Schedulers;

/**
 * Find addresses by postcode and select from list
 */
public class AddAddressBasicDetailsFragment extends AddAddressStepFragmentBase {
    public static String TAG = AddAddressBasicDetailsFragment.class.getSimpleName();

    private ContentLoadingProgressBar postcodeProgressBar;
    private EditText postcodeEditText;
    private Button findPostcodeButton;
    private LinearLayout changeAddressContainer;
    private TextView addressTextView;
    private Button changeAddressButton;
    private LinearLayout addressesContainer;
    private LinearLayout addressOptionsContainer;
    private RadioButton homeRadioButton;
    private RadioButton workRadioButton;
    private RadioButton otherRadioButton;
    private EditText otherAddressNameEditText;
    private LinearLayout otherAddressDetailsContainer;
    private View noAdditionalPrefsMessage;
    private LinearLayout additionalPrefsContainer;
    private EditText addressNotesEditText;
    private ToggleButton avoidRushHourYesButton;
    private ToggleButton avoidRushHourNoButton;
    private ProgressBar progressBar;

    private Subscription searchByAddressSubscription;
    private Subscription lookupAddressDetailsSubscription;

    private List<AddressSearchResult> results;
    private Address selectedAddress;

    private TextView tooltipTextview;
    private View errorTooltip;

    public static AddAddressBasicDetailsFragment newInstance(String title, boolean isPrimary) {
        Bundle args = new Bundle();
        if (title != null) args.putString(ARG_TITLE, title);
        args.putBoolean(ARG_IS_PRIMARY, isPrimary);

        AddAddressBasicDetailsFragment fragment = new AddAddressBasicDetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public AddAddressBasicDetailsFragment() {
        // required empty public Fragment constructor
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_add_address_basic_details, container, false);

        root.findViewById(R.id.add_after_textview).setVisibility(isPrimary ? View.VISIBLE : View.GONE);
        homeRadioButton = (RadioButton) root.findViewById(R.id.home_radiobutton);
        workRadioButton = ((RadioButton) root.findViewById(R.id.work_radiobutton));
        otherRadioButton = ((RadioButton) root.findViewById(R.id.other_radiobutton));
        errorTooltip = root.findViewById(R.id.error_tooltip);
        errorTooltip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showToolTip(null, false);
            }
        });
        tooltipTextview = (TextView) root.findViewById(R.id.tooltip_textview);
        homeRadioButton.setText(getString(R.string.home));
        workRadioButton.setText(getString(R.string.work));
        otherRadioButton.setText(getString(R.string.other));
        homeRadioButton.setOnCheckedChangeListener(onChangeAddressType);
        workRadioButton.setOnCheckedChangeListener(onChangeAddressType);
        otherRadioButton.setOnCheckedChangeListener(onChangeAddressType);
        otherAddressNameEditText = (EditText) root.findViewById(R.id.other_address_name_edittext);

        postcodeEditText = (EditText) root.findViewById(R.id.postcode_edittext);
        postcodeEditText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                postcodeEditText.setFocusableInTouchMode(true);
                return false;
            }
        });
        postcodeProgressBar = (ContentLoadingProgressBar) root.findViewById(R.id.postcode_progressBar);
        findPostcodeButton = (Button) root.findViewById(R.id.find_postcode_button);
        findPostcodeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findAddressByPostcode();
            }
        });

        changeAddressContainer = (LinearLayout) root.findViewById(R.id.change_address_container);
        addressTextView = (TextView) root.findViewById(R.id.address_edittext);
        changeAddressButton = (Button) root.findViewById(R.id.change_address_button);
        changeAddressButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeAddress();
            }
        });

        addressesContainer = (LinearLayout) root.findViewById(R.id.addresses_container);
        addressesContainer.setVisibility(View.GONE);
        addressOptionsContainer = (LinearLayout) root.findViewById(R.id.address_options_container);
        addressOptionsContainer.setVisibility(View.GONE);
        otherAddressDetailsContainer = (LinearLayout) root.findViewById(R.id.other_address_details_container);

        noAdditionalPrefsMessage = root.findViewById(R.id.no_additional_prefs_message);
        additionalPrefsContainer = (LinearLayout) root.findViewById(R.id.additional_prefs_container);
        addressNotesEditText = (EditText) root.findViewById(R.id.address_notes_edittext);
        avoidRushHourYesButton = (ToggleButton) root.findViewById(R.id.avoid_rush_hour_yes_button);
        avoidRushHourYesButton.setOnCheckedChangeListener(onAvoidRushCheckChangeListener);
        avoidRushHourNoButton = (ToggleButton) root.findViewById(R.id.avoid_rush_hour_no_button);
        avoidRushHourNoButton.setOnCheckedChangeListener(onAvoidRushCheckChangeListener);
        progressBar = (ProgressBar) root.findViewById(R.id.progressBar);

        CommonUtils.setupUIForKeyboard(getActivity(), root);
        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        postcodeEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    findAddressByPostcode();
                }
                return false;
            }
        });
        disableNextButton();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (title == null) title = getString(R.string.add_new_address);
        setTitle(title);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (searchByAddressSubscription != null && !searchByAddressSubscription.isUnsubscribed()) {
            searchByAddressSubscription.unsubscribe();
        }
        searchByAddressSubscription = null;

        if (lookupAddressDetailsSubscription != null && !lookupAddressDetailsSubscription.isUnsubscribed()) {
            lookupAddressDetailsSubscription.unsubscribe();
        }
        lookupAddressDetailsSubscription = null;
    }

    private void findAddressByPostcode() {
        final String postcode = postcodeEditText.getText().toString().trim();
        if (TextUtils.isEmpty(postcode)) {
            return;
        }
        if (!postcode.matches(com.dpd.yourdpd.util.TextUtils.POSTCODE_REGEX)) {
            showToolTip(getString(R.string.please_enter_valid_postcode), true);
            return;
        }

        if (postcodeEditText.getText().toString().toUpperCase().replace(" ", "").equals(getResources().getString(R.string.address_has_no_options))) {
            ((BaseActivity) getActivity()).skipProgress = true;
        } else {
            ((BaseActivity) getActivity()).skipProgress = false;
        }

        postcodeEditText.setEnabled(false);
        findPostcodeButton.setEnabled(false);
        postcodeProgressBar.show();
        progressBar.setVisibility(View.GONE);
        disableNextButton();

        searchByAddressSubscription =
                api.searchByAddress(null, null, postcode, null, null, null, 1,
                        BuildConfig.SEARCH_BY_ADDRESS_LIMIT)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Action1<SearchByAddressResponse>() {
                                       @Override
                                       public void call(SearchByAddressResponse response) {
                                           if (response.hasErrors()) {
                                               Log.e(TAG, "Search by address error: " + response.printErrors());
                                           }
                                           onSearchAddressResults(response.getSearchResults());
                                       }
                                   },
                                new Action1<Throwable>() {
                                    @Override
                                    public void call(Throwable throwable) {
                                        Log.e(TAG, "Search by address error", throwable);
                                        onSearchAddressResults(null);
                                        showErrorAlertDialog(getString(R.string.a_problem_has_occurred_try_again));
                                    }
                                });
    }

    private void changeAddress() {
        addressesContainer.setVisibility(View.VISIBLE);
        addressOptionsContainer.setVisibility(View.GONE);
    }

    private void onSearchAddressResults(List<AddressSearchResult> results) {
        this.results = results;

        if (results != null)
            Collections.sort(this.results, new AlphaNumComparator<AddressSearchResult>());

        addressesContainer.setVisibility(View.VISIBLE);
        addressOptionsContainer.setVisibility(View.GONE);
        postcodeEditText.setEnabled(true);
        postcodeEditText.setFocusable(false);
        findPostcodeButton.setEnabled(true);
        postcodeProgressBar.hide();

        if (results != null)
            showAddresses();
    }

    private void showAddresses() {
        final View view = getView();
        assert view != null;
        final LayoutInflater inflater = LayoutInflater.from(getActivity());
        // final View addressContainerLabel = addressesContainer.getChildAt(0);

        addressesContainer.removeAllViews();
        // addressesContainer.addView(addressContainerLabel);
        if (results.size() > 0) {
            showToolTip(null, false);
        } else {
            showToolTip(getString(R.string.no_properties_found), true);
        }

        for (AddressSearchResult result : results) {
            final TextView itemView = (TextView) inflater.inflate(R.layout.item_list_item, addressesContainer, false);
            itemView.setText(result.quickviewAddress);
            itemView.setId(ViewIdGenerator.generateViewId());
            itemView.setOnClickListener(onClickAddress);
            itemView.setTag(result);
            addressesContainer.addView(itemView);
        }

        TransitionManager.beginDelayedTransition((ViewGroup) getView());
        addressesContainer.setVisibility(View.VISIBLE);
    }

    private View.OnClickListener onClickAddress = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            final AddressSearchResult addressSearchResult = (AddressSearchResult) v.getTag();
            loadAddressDetails(addressSearchResult);
        }
    };

    private void loadAddressDetails(final AddressSearchResult addressSearchResult) {
        addressesContainer.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        lookupAddressDetailsSubscription =
                api.lookupPostKey(addressSearchResult.addressKey)
                        .flatMap(new Func1<LookupPostKeyResponse, Observable<AddressDetails>>() {
                            @Override
                            public Observable<AddressDetails> call(LookupPostKeyResponse response) {
                                if (response.hasErrors()) {
                                    Log.e(TAG, "lookupPostKey error: " + response.printErrors());
                                    return Observable.error(new Exception("Unable to lookupPostKey"));
                                }
                                final Address address = response.getAddressLocation().address;

                                return Observable.zip(
                                        Observable.just(address),
                                        Observable.just(addressSearchResult),
                                        api.checkProperty(addressSearchResult.postcode),
                                        new Func3<Address, AddressSearchResult, CheckPropertyResponse, AddressDetails>() {
                                            @Override
                                            public AddressDetails call(Address address,
                                                                       AddressSearchResult addressSearchResult,
                                                                       CheckPropertyResponse response) {
                                                final AddressDetails addressDetails = new AddressDetails();
                                                addressDetails.address = address;
                                                addressDetails.hasDpdDepot = response.hasDpdDepot();
                                                return addressDetails;
                                            }
                                        });
                            }
                        })
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                new Action1<AddressDetails>() {
                                    @Override
                                    public void call(AddressDetails addressDetails) {
                                        onAddressDetailsResult(addressDetails.address, addressDetails.hasDpdDepot);
                                    }
                                },
                                new Action1<Throwable>() {
                                    @Override
                                    public void call(Throwable throwable) {
                                        Log.i(TAG, "Unable to load address details", throwable);
                                        onAddressDetailsResult(null, false);
                                    }
                                });
    }

    private void onAddressDetailsResult(Address address, boolean hasDpdDepot) {
        this.selectedAddress = address;

        TransitionManager.beginDelayedTransition((ViewGroup) getView());

        progressBar.setVisibility(View.GONE);
        addressesContainer.setVisibility(View.GONE);
        otherAddressDetailsContainer.setVisibility(View.GONE);
        addressOptionsContainer.setVisibility(View.VISIBLE);

        if (hasDpdDepot) {
            noAdditionalPrefsMessage.setVisibility(View.GONE);
            additionalPrefsContainer.setVisibility(View.VISIBLE);
        } else {
            // hide additional delivery options
            noAdditionalPrefsMessage.setVisibility(View.VISIBLE);
            additionalPrefsContainer.setVisibility(View.GONE);
        }

        if (selectedAddress != null) {
            addressTextView.setText(selectedAddress.toFormattedString());
            changeAddressContainer.setVisibility(View.VISIBLE);
        } else {
            changeAddressContainer.setVisibility(View.GONE);
        }
        homeRadioButton.setChecked(true);
        enableNextButton();
    }

    private CompoundButton.OnCheckedChangeListener onChangeAddressType =
            new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (buttonView.getId() == R.id.other_radiobutton) {
                        TransitionManager.beginDelayedTransition((ViewGroup) getView());
                        otherAddressDetailsContainer.setVisibility(isChecked ? View.VISIBLE : View.GONE);
                    }
                }
            };

    private CompoundButton.OnCheckedChangeListener onAvoidRushCheckChangeListener =
            new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (!isChecked) {
                        // prevent un-checking
                        buttonView.setChecked(true);
                        return;
                    }
                    switch (buttonView.getId()) {
                        case R.id.avoid_rush_hour_yes_button:
                            avoidRushHourNoButton.setOnCheckedChangeListener(null);
                            avoidRushHourNoButton.setChecked(false);
                            avoidRushHourNoButton.setOnCheckedChangeListener(this);
                            break;
                        case R.id.avoid_rush_hour_no_button:
                            avoidRushHourYesButton.setOnCheckedChangeListener(null);
                            avoidRushHourYesButton.setChecked(false);
                            avoidRushHourYesButton.setOnCheckedChangeListener(this);
                            break;
                    }
                }
            };

    @Override
    public boolean validate() {
        otherAddressNameEditText.setError(null);
        if (selectedAddress == null) {
            return false;
        }
        if (otherRadioButton.isChecked() && TextUtils.isEmpty(getPropertyLabel())) {
            otherAddressNameEditText.setError(getString(R.string.please_specify_other_address));
            otherAddressNameEditText.requestFocus();
            return false;
        }

        // Check added address.
        final ConsumerProfile consumerProfile = user.getConsumerProfile();
        final List<ConsumerPropertyAddressInfo> addressInfos = consumerProfile.getConsumerPropertyAddressInfos();
        if (addressInfos != null && addressInfos.size() > 0) {
            boolean isAdded = false;
            for (ConsumerPropertyAddressInfo address : addressInfos) {
                if (address.consumerProperty.consumerPropertyId.equals(selectedAddress.addressId)) {
                    isAdded = true;
                    break;
                }
            }

            if (isAdded) {
                showErrorAlertDialogWithOk(
                        getString(R.string.property_previous_added_message),
                        getString(R.string.property_previous_added_title),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                addressesContainer.setVisibility(View.GONE);
                                addressOptionsContainer.setVisibility(View.GONE);
                                postcodeEditText.setText("");
                                postcodeEditText.requestFocus();
                            }
                        }
                );
                return false;
            }
        }
        return true;
    }

    @Override
    public void applyChanges() {
        consumerProperty.selectedAddress = selectedAddress;
        consumerProperty.consumerPropertyId = selectedAddress.addressId;
        consumerProperty.accessNotes = addressNotesEditText.getText().toString().trim();
        consumerProperty.avoidRushHour = avoidRushHourYesButton.isChecked();
        consumerProperty.propertyLabel = getPropertyLabel();
    }

    /**
     * @return Work, Home or Other depending on selected radio button
     */
    private String getPropertyLabel() {
        if (homeRadioButton.isChecked()) return ConsumerPropertyLabel.HOME;
        else if (workRadioButton.isChecked()) return ConsumerPropertyLabel.WORK;
        else if (otherRadioButton.isChecked()) return otherAddressNameEditText.getText().toString();
        else return null;
    }

    /**
     * Container class tying together data relevant to the user's selected address
     * during API call re
     */
    private class AddressDetails {
        Address address;
        boolean hasDpdDepot;
    }

    private void showToolTip(CharSequence error, boolean isShow) {
        if (error != null) tooltipTextview.setText(error);
        TransitionManager.beginDelayedTransition((ViewGroup) getView());
        errorTooltip.setVisibility(isShow ? View.VISIBLE : View.GONE);
    }
}
