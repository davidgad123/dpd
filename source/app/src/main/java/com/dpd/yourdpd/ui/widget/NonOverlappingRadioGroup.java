package com.dpd.yourdpd.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RadioGroup;

/**
 * RadioGroup which returns false for {@link #hasOverlappingRendering}.
 * This prevents the GPU from creating too large textures during Transitions when the RadioGroup
 * contains too many items (e.g. taller than 4096px on a Nexus 5 would crash during the Transition
 * due to it exceeding the GPU texture size limit)
 */
public class NonOverlappingRadioGroup extends RadioGroup {
    public NonOverlappingRadioGroup(Context context) {
        this(context, null);
    }
    public NonOverlappingRadioGroup(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean hasOverlappingRendering() {
        return false;
    }
}
