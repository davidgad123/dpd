package com.dpd.yourdpd.model;

import java.io.Serializable;

/**
 * Created by richardleggett on 01/09/15.
 */
public class Service implements Serializable {
    /**
     * e.g. 1^1
     */
    public String serviceCode;
    /**
     * e.g. Next Day
     */
    public String serviceDescription;
}
