package com.dpd.yourdpd.net.response;

import com.dpd.yourdpd.model.Address;
import com.dpd.yourdpd.model.AddressPoint;
import com.dpd.yourdpd.net.Api;
import com.google.gson.annotations.SerializedName;

/**
 * @see Api#lookupPostKey(String)
 */
public class LookupPostKeyResponse extends BaseResponse {
    @SerializedName("data")
    private AddressLocationWrapper addressLocationWrapper;

    public AddressLocation getAddressLocation() {
        return addressLocationWrapper.addressLocation;
    }

    public static class AddressLocationWrapper {
        public AddressLocation addressLocation;
    }

    public static class AddressLocation {
        public Address address;
        public AddressPoint addressPoint;
        public String propertyType;
    }
}
