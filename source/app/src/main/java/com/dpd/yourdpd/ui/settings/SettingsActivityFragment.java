package com.dpd.yourdpd.ui.settings;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.UiThread;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dpd.yourdpd.R;
import com.dpd.yourdpd.model.ConsumerProfile;
import com.dpd.yourdpd.model.ConsumerPropertyAddressInfo;
import com.dpd.yourdpd.model.User;
import com.dpd.yourdpd.net.response.DeleteConsumerResponse;
import com.dpd.yourdpd.ui.BaseFragment;
import com.dpd.yourdpd.ui.address.AddAddressActivity;
import com.dpd.yourdpd.ui.address.EditAddressActivity;
import com.dpd.yourdpd.util.ViewIdGenerator;

import java.util.List;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Settings Activity main Fragment.
 * Displays a list of addresses (aka ConsumerProperties) that can be edited, or a new one added.
 */
public class SettingsActivityFragment extends BaseFragment implements User.Listener {

    private static final String TAG = SettingsActivityFragment.class.getSimpleName();

    private LinearLayout addressesContainer;
    private Subscription deleteConsumerSubscription;
    private ProgressDialog progressDialog;


    public static Fragment newInstance() {
        return new SettingsActivityFragment();
    }

    public SettingsActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.fragment_settings, container, false);
        root.findViewById(R.id.view_update_profile_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showYourProfileFragment();
            }
        });
        root.findViewById(R.id.add_address_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startAddNewAddressActivity();
            }
        });
        root.findViewById(R.id.delete_profile_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                promptDeleteProfile();
            }
        });
        addressesContainer = (LinearLayout) root.findViewById(R.id.addresses_container);
        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        displayAddresses();
        getFragmentManager().addOnBackStackChangedListener(onBackStackChangedListener);
        ((SettingsActivity) getActivity()).setupToolbar(getString(R.string.settings_titlecase), false);
    }

    @Override
    public void onPause() {
        super.onPause();
        user.removeListener(this);
        getFragmentManager().removeOnBackStackChangedListener(onBackStackChangedListener);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (deleteConsumerSubscription != null && !deleteConsumerSubscription.isUnsubscribed()) {
            deleteConsumerSubscription.unsubscribe();
        }
        deleteConsumerSubscription = null;
    }

    @UiThread
    public void displayAddresses() {
        addressesContainer.removeAllViews();

        final LayoutInflater inflater = LayoutInflater.from(getActivity());
        final ConsumerProfile consumerProfile = user.getConsumerProfile();
        final List<ConsumerPropertyAddressInfo> addressInfos = consumerProfile.getConsumerPropertyAddressInfos();
        if (addressInfos == null || addressInfos.size() == 0) {
            addressesContainer.setVisibility(View.GONE);
            return;
        }

        int addressCount = addressInfos.size();
        for (ConsumerPropertyAddressInfo address : addressInfos) {
            final TextView itemView = (TextView) inflater.inflate(R.layout.item_setting_list_item, addressesContainer, false);
            final StringBuilder label = new StringBuilder();
            if (!TextUtils.isEmpty(address.consumerProperty.propertyLabel)) {
                label.append(address.consumerProperty.propertyLabel);
                label.append(" - ");
            }
            label.append(address.addressFirstLineWithTown);
            itemView.setText(label);
            itemView.setId(ViewIdGenerator.generateViewId());
            itemView.setOnClickListener(onClickAddressLister);
            itemView.setTag(address.consumerProperty.consumerPropertyId);
//            itemView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_edit_grey_24dp, 0);
            addressesContainer.addView(itemView);
        }

        addressesContainer.setVisibility(View.VISIBLE);
    }

    private View.OnClickListener onClickAddressLister = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String consumerPropertyId = (String) v.getTag();
            int addressCount = user.getConsumerProfile().getConsumerPropertyAddressInfos().size();
            ConsumerProfile profile = user.getConsumerProfile();
            if (addressCount == 1) {
                startEditAddressActivity(consumerPropertyId, true);
            } else {
                startEditAddressActivity(consumerPropertyId, false);
            }
        }
    };

    private void showYourProfileFragment() {
        final Fragment fragment = SettingsYourProfileActivityFragment.newInstance();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment, SettingsYourProfileActivityFragment.class.getSimpleName());
        transaction.addToBackStack(null);
        transaction.commit();
    }

    /**
     * @param consumerPropertyId ID of item in {@link ConsumerProfile#consumerProperties}
     */
    private void startEditAddressActivity(String consumerPropertyId, boolean isSingleAddress) {
        final Intent intent = new Intent(getActivity(), EditAddressActivity.class);
        intent.putExtra(EditAddressActivity.EXTRA_CONSUMER_PROPERTY_ID, consumerPropertyId);
        intent.putExtra(EditAddressActivity.EXTRA_IS_SINGLE_ADDRESS, isSingleAddress);
        startActivity(intent);
    }

    private void startAddNewAddressActivity() {
        Intent intent = new Intent(getActivity(), AddAddressActivity.class);
        startActivity(intent);
    }

    private void promptDeleteProfile() {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(getActivity(), R.style.AlertDialogTheme);
        builder.setTitle(getString(R.string.delete_profile_dialog_title));
        builder.setMessage(getString(R.string.delete_profile_dialog_message));
        builder.setCancelable(true);
        builder.setNegativeButton(getString(R.string.cancel), null);
        builder.setPositiveButton(getString(R.string.delete), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                deleteConsumer();
            }
        });
        builder.create().show();
    }

    private void deleteConsumer() {
        showProgressDialog(getString(R.string.please_wait_));
        deleteConsumerSubscription =
                api.deleteConsumer()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Action1<DeleteConsumerResponse>() {
                                       @Override
                                       public void call(DeleteConsumerResponse response) {
                                           hideProgressDialog();
                                           if (response.hasErrors()) {
                                               Log.e(TAG, "Error deleting consumer: " + response.printErrors());
                                               showErrorDeletingConsumerDialog();
                                           } else {
                                               // wipe local data
                                               dataStore.clear();
                                               // log user out (causing listening Activity to finish)
                                               // also wipes user data
                                               user.logout();
                                           }
                                       }
                                   },
                                new Action1<Throwable>() {
                                    @Override
                                    public void call(Throwable throwable) {
                                        Log.e(TAG, "Error deleting consumer:", throwable);
                                        hideProgressDialog();
                                        showErrorDeletingConsumerDialog();
                                    }
                                });
    }

    private void showErrorDeletingConsumerDialog() {
        showErrorAlertWithRetryOption(
                getString(R.string.a_problem_has_occurred_try_again),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteConsumer();
                    }
                });
    }

    private void showProgressDialog(String message) {
        hideProgressDialog();
        progressDialog = ProgressDialog.show(getActivity(), null, message, true, false);
    }

    private void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }


    /**
     * {@link User} listener implemented by {@link com.dpd.yourdpd.ui.BaseActivity}
     *
     * @param success
     */
    @Override
    public void onConsumerProfileUpdated(boolean success) {
        if (success && isAdded()) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    displayAddresses();
                }
            });
        }
    }

    FragmentManager.OnBackStackChangedListener onBackStackChangedListener =
            new FragmentManager.OnBackStackChangedListener() {
                @Override
                public void onBackStackChanged() {
                    // reset selected ConsumerProperty and reload addresses, they may have changed
                    if (getFragmentManager().getBackStackEntryCount() == 0) {
                        user.selectedConsumerProperty = null;
                        displayAddresses();
                    }
                }
            };
}
