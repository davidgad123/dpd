package com.dpd.yourdpd.net.response;

import com.dpd.yourdpd.model.Parcel;
import com.google.gson.annotations.SerializedName;

/**
 * @see com.dpd.yourdpd.net.Api#getParcelDetails(String)
 */
public class GetParcelDetailsResponse extends BaseResponse {
    @SerializedName("data")
    private Parcel parcel;

    public Parcel getParcel() {
        return parcel;
    }
}
