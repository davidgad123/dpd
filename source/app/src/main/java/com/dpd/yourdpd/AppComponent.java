package com.dpd.yourdpd;

import com.dpd.yourdpd.analytics.AnalyticsManager;
import com.dpd.yourdpd.analytics.AnalyticsModule;
import com.dpd.yourdpd.datastore.DataStore;
import com.dpd.yourdpd.datastore.DataStoreModule;
import com.dpd.yourdpd.model.User;
import com.dpd.yourdpd.net.Api;
import com.dpd.yourdpd.net.ApiModule;
import com.paypal.android.sdk.payments.PayPalConfiguration;

import dagger.Component;

@PerApplication
@Component(modules = {
        AppModule.class,
        AnalyticsModule.class,
        ApiModule.class,
        DataStoreModule.class,
        UserModule.class,
        PayPalModule.class})
public interface AppComponent {
    void inject(YourDPDApp app);
    AnalyticsManager analyticsManager();
    Api api();
    DataStore dataStore();
    User user();
    PayPalConfiguration paypalConfig();
}
