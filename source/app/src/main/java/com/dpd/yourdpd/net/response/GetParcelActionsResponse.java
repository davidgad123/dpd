package com.dpd.yourdpd.net.response;

import com.dpd.yourdpd.model.ParcelAction;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @see com.dpd.yourdpd.net.Api#getParcelActions(String)
 */
public class GetParcelActionsResponse extends BaseResponse {
    @SerializedName("data")
    private List<ParcelAction> parcelActions;

    public List<ParcelAction> getParcelActions() {
        return parcelActions;
    }
}
