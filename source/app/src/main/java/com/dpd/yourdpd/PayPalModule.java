package com.dpd.yourdpd;

import com.paypal.android.sdk.payments.PayPalConfiguration;

import dagger.Module;
import dagger.Provides;

@Module
public class PayPalModule {
    private static PayPalConfiguration paypalConfig = new PayPalConfiguration()

            // Start with mock environment.  When ready, switch to sandbox (ENVIRONMENT_SANDBOX)
            // or live (ENVIRONMENT_PRODUCTION)
            .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
            .acceptCreditCards(false)
            .clientId(BuildConfig.PAYPAL_CLIENT_ID);

    @Provides
    @PerApplication
    PayPalConfiguration providePayPalConfiguration() {
        return paypalConfig;
    }
}
