package com.dpd.yourdpd.net;

/**
 * Created by richardleggett on 04/09/15.
 */
public class APIError {
    public enum ErrorCode {
        APPLICATION_ERROR(1000),
        MAX_LENGTH_EXCEEDED(1001),
        RESOURCE_NOT_FOUND(1004),
        RESOURCE_NOT_AVAILABLE(1006),
        MISSING_MANDATORY_FIELD(1007),
        INVALID_FIELD_VALUE(1018),
        INVALID_OBJECT_TYPE(1026),
        RESOURCE_ID_ALREADY_EXISTS(1027),
        FIELD_NOT_ALLOWED_FOR_RESOURCE_TYPE(1028),
        VALUE_PASSED_IS_NOT_A_VALID_TYPE(1029),
        SYSTEM_FAILED_TO_PASS_APPLICATION_RULE(1030),
        ERROR_MISSING_REQUIRED_FIELD(1031),
        WARNING_MISSING_REQUIRED_FIELD(1032),
        SECURITY_RELATED_EXCEPTION(1033),
        SERVER_SIDE_SESSION_USER_CONFIG_EXCEPTION(1034),
        NOTHING_TO_PROCESS(1035);

        private final int value;

        ErrorCode(int i) {
            this.value = i;
        }

        public String toString() {
            return String.valueOf(this.value);
        }
    }
    public String errorCode;
    public String errorType;
    public String errorAction;
    public String errorMessage;
    public String errorObj;

    @Override
    public String toString() {
        return "{" +
                "errorObj: " + errorObj + ", " +
                "errorCode: " + errorCode + ", " +
                "errorType: " + errorType + ", " +
                "errorMessage: " + errorMessage + ", " +
                "errorAction: " + errorAction + ", " +
                "}";
    }
}
