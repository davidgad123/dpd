package com.dpd.yourdpd.net.response;

import com.dpd.yourdpd.model.SafeLocation;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @see com.dpd.yourdpd.net.Api
 */
public class MobileStartupChecksResponse extends BaseResponse {
    public Data data;

    public class Data {
        public SiteContent siteContent;
        public ConsumerProfile consumerProfile;
        @SerializedName("safeLocation")
        public List<SafeLocation> safeLocations;
    }
    public class SiteContent {
        public String checksum;
    }
    public class ConsumerProfile {
        public String checksum;
    }
}
