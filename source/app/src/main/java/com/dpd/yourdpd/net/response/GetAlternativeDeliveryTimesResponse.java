package com.dpd.yourdpd.net.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @see com.dpd.yourdpd.net.Api#getAlternativeDeliveryTimes(String, String, String, String, String, String, String, String, String, String)
 */
public class GetAlternativeDeliveryTimesResponse extends BaseResponse {
    @SerializedName("data")
    private DeliveryTimes deliveryTimes;


    public List<DeliveryTime> getDeliveryTimes() {
        return (deliveryTimes==null) ? null : deliveryTimes.deliveryTimeList;
    }

    public static class DeliveryTimes {
        @SerializedName("deliveryTime")
        private List<DeliveryTime> deliveryTimeList;
    }

    public static class DeliveryTime {
        public String upgradeId;
        public String startTime;
        public String endTime;
        public boolean saturdayService;
        public boolean sundayService;
        public String upgradeCost;
        public String upgradeCostExcludeVAT;
        public String serviceCode;
    }
}
