package com.dpd.yourdpd.model;

import android.text.TextUtils;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.Serializable;
import java.util.List;


/**
 * Represents a Parcel within the system. Please note that not all fields will be populated as this
 * depends on which API service has returned the Parcel data.
 * <br/>
 * Question: Why are all dates stored as String in our models? Answer: Because the APIs return
 * around 5+ different formats, some dates are only partial, so we instead provide accessor methods
 * that return an actual Date object where needed, performing the parsing within the method.
 */
public class Parcel implements Serializable {

    private static final long serialVersionUID = 0L;

    /**
     * 0 = Unknown (this is NOT a valid server response)
     * 1 = RFI (Not applicable to app)
     * 2 = Show Delivery Image (delivered to shop or customer)
     * 3 = Collect from depot
     * 4 = Collect from pickup
     * 5 = Collect from pickup including pickup pass
     * 6 = Out for delivery (map available and "on route code")
     * 7 = Issue (parcel has issueDetails)
     * 8 = Show estimated delivery date (not out for delivery)
     */
    public enum ParcelStatusType {
        UNKNOWN(0),
        RFI(1),
        DELIVERED(2),
        COLLECT_FROM_DEPOT(3),
        COLLECT_FROM_PICKUP(4),
        COLLECT_FROM_PICKUP_WITH_PASS(5),
        OUT_FOR_DELIVERY(6),
        ISSUE(7),
        SHOW_ESTIMATED_DELIVERY_DATE(8);

        private final int value;

        ParcelStatusType(int value) {
            this.value = value;
        }

        public static ParcelStatusType fromValue(int value) {
            for (ParcelStatusType type : ParcelStatusType.values()) {
                if (type.value == value) {
                    return type;
                }
            }
            return UNKNOWN;
        }
    }

    public String parcelNumber;
    public String parcelCode;
    public ParcelStatusType parcelStatusType;

    public String organisation;
    /**
     * Base64 encoded logo
     */
    public String customerImageLogo;
    public String trackingStatusCurrent;

    // All other fields

    public String parcelNumberDisplay;

    // TODO: is this the inverse of {@link Address.agentIsDPD} field?
    public boolean transferToAgent;

    public List<Object> parcelsCollectedOnDelivery;
    public List<Object> parcelsDeliveredOnCollection;

    public RelatedParcels relatedParcels;
    public String alignedDepotCode;
    public String alignedDepotName;
    public int businessUnit;
    public CallingCardNumbers callingCardNumber;

    public boolean canCollectParcel;
    public String collectFromDepotDiaryDate;
    public String collectionCode;
    public String collectionCodeDisplay;
    public String collectionDate;
    public String collectionOnDeliveryCode;
    public String collectionOnDeliveryName;

    public String consignmentNumber;

    public List<Container> container;

    public int deliveryBusinessUnit;
    public Depot deliveryDepot;
    public ParcelDeliveryDetails deliveryDetails;
    /**
     * Formatted as 2015-07-26 00:00:00
     */
    public String estimatedDeliveryDate;

    public IssueDetails issueDetails;
    public String ivrStatusCode;

    public String lastConfirmDate;
    public String lastConfirmDepot;

    public OutForDeliveryDetails outForDeliveryDetails;

    public List<String> partnerRef;

    public Product product;

    public String reconsignedConsignmentCode;
    public String relabelledAsParcelCode;
    public String relabelledFromParcelCode;

    public Service service;

    public ShipperDetails shipperDetails;

    public boolean notificationFromCustomerData;

    public String customerTargetDate;
    public String depotTargetDate;

    public CollectionDetails collectionDetails;

    /**
     * e.g. DTN - deliver to neighbour
     *
     * @see ParcelActionCodes
     */
    public String estimatedDeliveryActionCode;
    public String estimatedDeliveryStartTime;
    public String estimatedDeliveryEndTime;

    public String eventLatestPosition;
    public String goodsDescription;
    public String packagingDescription;
    public String serialNumber;

    public float goodsValue;
    public String investigationNotes;
    public ClaimInformation claimInfomation;

    public String shipmentDate;

    public String relabelledFromDisplayParcelCode;
    public String relabelledAsDisplayParcelCode;

    public RfiReason rfiReason;

    public String pickupLocationCode;
    public int pickupType;
    public String pickupStatus;
    public String pickupExpiryDate;
    public boolean collectedFromShop;

    public String consumerPreferenceText;
    public boolean deliveredToConsumer;

    public String getIssueName() {
        return issueDetails != null && issueDetails.issueName != null ?
                issueDetails.issueName : null;
    }

    public DateTime getEstimatedDeliveryDate() {
        final DateTimeFormatter inputFormat = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        return DateTime.parse(estimatedDeliveryDate, inputFormat);
    }

    public String getEstimatedDeliveryStartTime() {
        if (estimatedDeliveryStartTime == null) return "?";
        else {
            // trim seconds
            return estimatedDeliveryStartTime.substring(0, 5);
        }
    }

    public String getEstimatedDeliveryEndTime() {
        if (estimatedDeliveryEndTime == null)
            return "?";
        else {
            // trim seconds
            return estimatedDeliveryEndTime.substring(0, 5);
        }
    }

    public String getParcelNumberDisplay() {
        return parcelNumberDisplay == null ? parcelNumber : parcelNumberDisplay;
    }

    public String getShipperName() {
//        String name = shipperDetails != null ? (shipperDetails.customerDisplayName != null ? shipperDetails.customerDisplayName : shipperDetails.organisation) : getOrganisation();
        String name = shipperDetails != null ? shipperDetails.customerDisplayName : getOrganisation();
        if (TextUtils.isEmpty(name)) name = "";
        return name;
    }

    public String getOrganisation() {
        return organisation;
    }

    public boolean isReAttemptingDelivery() {
        // TODO: implement isReAttemptingDelivery()
        return false;
    }

    public int getRelatedParcelCount() {
        return relatedParcels == null ? 0 : (relatedParcels.parcelCodes == null ? 0 : relatedParcels.parcelCodes.size());
    }

    public static class Container implements Serializable {
        public String containerType;
        public String containerNumber;
    }

//    public static class RelatedParcel implements Serializable {
//        @SerializedName("parcel_code")
//        public String parcelCode;
//        @SerializedName("user_type")
//        public String userType;
//        public String origin;
//    }

    public static class CallingCardNumbers implements Serializable {
        public List<String> numbers;
    }

    public static class RelatedParcels {
        public List<String> parcelCodes;
    }
}
