package com.dpd.yourdpd.model;

public class ParcelAction {
    /**
     * The human readable imageCaption of a possible parcel action
     * e.g. Collect from depot
     */
    public String parcelActionDescription;
    /**
     * The human readable imageCaption of a possible parcel action group
     * e.g. General
     */
    public String parcelActionGroupDescription;
    /**
     * An identifier for a possible parcel action group
     * e.g. GEN
     */
    public String parcelActionGroupId;
    /**
     * An identifier for a possible parcel action
     * e.g. TBC, SFP, DTN, RED, UPG, PKU
     * @see ParcelActionCodes
     */
    public String parcelActionId;

    /**
     * A unique identifier for a parcel
     */
    public String parcelCode;
}
