package com.dpd.yourdpd.model;

import java.util.Date;

/**
 * Created by richardleggett on 01/09/15.
 */
public class Route {
    public class Stop {
        /**
         * Date only (not time)
         */
        public Date deliveryWindowFrom;
        public Date deliveryWindowTo;
        public AddressPoint addressPoint;
        public int stopNumber;
        public int estimatedMinsToStop;
    }

    public String routeCode;
    public Stop stop;


}
