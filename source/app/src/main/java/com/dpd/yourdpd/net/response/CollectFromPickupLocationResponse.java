package com.dpd.yourdpd.net.response;

import com.dpd.yourdpd.net.request.CollectFromPickupLocationParams;

/**
 * @see com.dpd.yourdpd.net.Api#collectFromPickupLocation(CollectFromPickupLocationParams)
 */
public class CollectFromPickupLocationResponse extends BaseParcelActionResponse {
}
