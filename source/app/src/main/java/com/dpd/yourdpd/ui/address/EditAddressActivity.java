package com.dpd.yourdpd.ui.address;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.UiThread;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.MenuItem;

import com.dpd.yourdpd.R;
import com.dpd.yourdpd.net.response.PutConsumerResponse;
import com.dpd.yourdpd.ui.BaseMenuActivity;
import com.dpd.yourdpd.util.CommonUtils;

import rx.functions.Action1;

/**
 * Allows the user to edit a {@link com.dpd.yourdpd.model.ConsumerProperty} to their
 * {@link com.dpd.yourdpd.model.ConsumerProfile}
 * <p/>
 * TODO: a major refactoring would be to merge the Edit and Add Fragments passing in an "edit" flag
 * instead, which would dictate which layout to use and also different functionality around validation
 * This is possible as the code is 90% the same between them.
 */
public class EditAddressActivity extends BaseMenuActivity
        implements EditAddressFragmentBase.EditAddressFragmentListener {

    private static final String TAG = EditAddressActivity.class.getSimpleName();

    /**
     * The ID of the {@link com.dpd.yourdpd.model.ConsumerProperty} to edit
     */
    public static final String EXTRA_CONSUMER_PROPERTY_ID = "consumer_property_id";
    public static final String EXTRA_IS_SINGLE_ADDRESS = "single_address";

    private EditAddressActivityFragment editAddressFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_with_menu_fragment);
        setupToolbar(getTitle().toString(), true);

        // set the ConsumerProperty to edit
        if (!getIntent().hasExtra(EXTRA_CONSUMER_PROPERTY_ID)) {
            throw new IllegalStateException("EXTRA_CONSUMER_PROPERTY_ID must be specified");
        }
        final String consumerPropertyId = getIntent().getStringExtra(EXTRA_CONSUMER_PROPERTY_ID);
        final boolean isSingleAddress = getIntent().getBooleanExtra(EXTRA_IS_SINGLE_ADDRESS, false);
        user.selectedConsumerProperty = user.getConsumerProfile().getConsumerPropertyWithId(consumerPropertyId);

        if (editAddressFragment == null) {
            Bundle bundle = new Bundle();
            bundle.putBoolean(EditAddressActivityFragment.BUNDLE_SINGLE_ADDRESS, isSingleAddress);
            editAddressFragment = EditAddressActivityFragment.newInstance();
            editAddressFragment.setArguments(bundle);

            final FragmentManager fragmentManager = getSupportFragmentManager();
            final FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.add(R.id.container, editAddressFragment, EditAddressActivityFragment.class.getSimpleName());
            transaction.commit();
        }

        CommonUtils.setupUIForKeyboard(this, findViewById(android.R.id.content));
    }

    @Override
    public void onBackPressed() {
        if (editAddressFragment.validateAndApplyChanges()) {
            if (user.hasConsumerProfileChanged()) {
                putConsumerAndPopBackStack(true);
            } else {
                super.onBackPressed();
            }
        }
        // else hold them here until validation succeeds, onDiscardChanges() is called, or putConsumer() completes
    }

    @Override
    protected void onResume() {
        super.onResume();
        setupToolbar(getTitle().toString(), true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if (editAddressFragment.validateAndApplyChanges()) {
                if (user.hasConsumerProfileChanged()) {
                    putConsumerAndPopBackStack(true);
                } else {
                    return super.onOptionsItemSelected(item);
                }
            }
            // hold them here until validation succeeds, onDiscardChanges() is called, or putConsumer() completes
            return true;
        }
        return super.onOptionsItemSelected(item);

    }

    @UiThread
    private void showErrorSavingChangesDialog(final boolean popBackStackOnDismiss) {
        showErrorDialogWithRetryAndCancel(getString(R.string.problem_saving_changes_retry),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // attempt to PUT changes to server and then return from current Fragment
                        putConsumerAndPopBackStack(popBackStackOnDismiss);
                    }
                },
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (popBackStackOnDismiss) {
                            // return from current Fragment regardless of not saving changes
                            getSupportFragmentManager().popBackStack();
                        }
                    }
                });
    }

    @Override
    public void setTitle(CharSequence title) {
        this.setupToolbar(title.toString(), true);
    }

    public void putConsumerAndPopBackStack(final boolean popBackStackOnSuccess) {
        showProgressDialog(getString(R.string.saving_changes_));
        user.putConsumerProfile(api, true)
                .subscribe(
                        new Action1<PutConsumerResponse>() {
                            @Override
                            public void call(PutConsumerResponse response) {
                                hideProgressDialog();
                                if (response.hasErrors()) {
                                    Log.e(TAG, "Error saving changes: " + response.printErrors());
                                    showErrorSavingChangesDialog(popBackStackOnSuccess);
                                }
                                user.save();
                                if (popBackStackOnSuccess) {
                                    getSupportFragmentManager().popBackStack();
                                }
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                Log.e(TAG, "Error saving changes", throwable);
                                hideProgressDialog();
                                showErrorSavingChangesDialog(popBackStackOnSuccess);
                            }
                        });
    }

    /**
     * {@link com.dpd.yourdpd.ui.address.EditAddressFragmentBase.EditAddressFragmentListener} method
     * Invoked by currentFragment when the user wishes to discard their changes/input
     */
    @Override
    public void onDiscardChanges() {
        // simply return to the previous Fragment without applying any changes to ConsumerProperty
        getSupportFragmentManager().popBackStack();
    }
}
