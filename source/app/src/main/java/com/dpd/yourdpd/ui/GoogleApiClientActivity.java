package com.dpd.yourdpd.ui;

import com.google.android.gms.common.api.GoogleApiClient;

/**
 * Created by richardleggett on 17/06/15.
 */
public interface GoogleApiClientActivity {
    GoogleApiClient getGoogleApiClient();
    boolean hasGoogleApiClientConnected();
}
