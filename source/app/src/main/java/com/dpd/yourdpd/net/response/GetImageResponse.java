package com.dpd.yourdpd.net.response;

import com.google.gson.annotations.SerializedName;

/**
 * @see com.dpd.yourdpd.net.Api#getImage(String, String, String)
 * @see com.dpd.yourdpd.net.Api#getRackspaceImage(String, String)
 */
public class GetImageResponse extends BaseResponse {
    @SerializedName("data")
    public Data data;

    public String getImage() {
        return data == null ? null : data.image;
    }

    public class Data {
        /**
         * May be a base64Data string, or
         */
        public String image;
    }
}
