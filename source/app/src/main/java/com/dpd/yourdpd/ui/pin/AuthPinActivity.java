package com.dpd.yourdpd.ui.pin;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.common.Constants;
import com.dpd.yourdpd.BuildConfig;
import com.dpd.yourdpd.R;
import com.dpd.yourdpd.model.PinMethod;
import com.dpd.yourdpd.net.response.LoginResponse;
import com.dpd.yourdpd.net.response.PutConsumerResponse;
import com.dpd.yourdpd.ui.BaseActivity;
import com.dpd.yourdpd.ui.LoginActivity;
import com.dpd.yourdpd.ui.StartupChecksActivity;
import com.dpd.yourdpd.ui.yourdeliveries.YourDeliveriesActivity;
import com.dpd.yourdpd.util.AppConstant;
import com.util.WearableUtil;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class AuthPinActivity extends BaseActivity implements RequestPinFragment.OnPinRequestedListener,
        AuthPinFragment.FragmentInteractionListener {

    private static final String TAG = AuthPinActivity.class.getSimpleName();

    private RequestPinFragment requestPinFragment;
    private Subscription loginSubscription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth_pin);

        if (requestPinFragment == null) {
            final FragmentManager fragmentManager = getSupportFragmentManager();
            requestPinFragment = RequestPinFragment.newInstance();
            final FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.add(R.id.container, requestPinFragment, RequestPinFragment.class.getSimpleName());
            transaction.commit();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (loginSubscription != null && !loginSubscription.isUnsubscribed()) {
            loginSubscription.unsubscribe();
        }
        loginSubscription = null;
    }

    private void showEnterPinFragment(PinMethod pinMethod, int requestPinType, String requestStr) {
        final FragmentManager fragmentManager = getSupportFragmentManager();
        final AuthPinFragment authPinFragment = AuthPinFragment.newInstance(pinMethod);
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstant.BUNDLE_PIN_TYPE, requestPinType);
        bundle.putString(AppConstant.BUNDLE_REQUEST_STRING, requestStr);
        authPinFragment.setArguments(bundle);

        final FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.container, authPinFragment, AuthPinFragment.class.getSimpleName());
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void startMobileStartupChecksActivity() {
        final Intent intent = new Intent(this, StartupChecksActivity.class);
        startActivity(intent);
        finish();
    }

    private void putConsumerAndStartYourDeliveriesActivity() {
        user.putConsumerProfile(api, true)
                .subscribe(
                        new Action1<PutConsumerResponse>() {
                            @Override
                            public void call(PutConsumerResponse response) {
                                if (response.hasErrors()) {
                                    Log.e(TAG, "Unable to put Consumer" + response.printErrors());
                                    showErrorDialog(getString(R.string.a_problem_has_occurred_try_again),
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    putConsumerAndStartYourDeliveriesActivity();
                                                }
                                            });
                                    return;
                                }
                                final Intent intent = new Intent(AuthPinActivity.this, YourDeliveriesActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                showErrorDialog(getString(R.string.a_problem_has_occurred_try_again),
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                putConsumerAndStartYourDeliveriesActivity();
                                            }
                                        });
                            }
                        });
    }

    @Override
    public void onPinRequested(PinMethod pinMethod, int requestPinType, String requestStr) {
        user.setPinMethod(pinMethod);
        user.save();
        showEnterPinFragment(pinMethod, requestPinType, requestStr);
    }

    @Override
    public void onRequestNewPin() {
        getSupportFragmentManager().popBackStack();
    }

    @Override
    public void onPinVerified(String consumerId, boolean deviceConflict) {
        // persist consumerId to allow for authenticated login
        user.setConsumerId(consumerId);
        user.setHasSeenCoachScreens(true);
        user.setHasAuthenticatedPin(true);
        user.save();

        if (deviceConflict) {
            // before logging in with an auditUser, let them choose if they wish to use this new
            // device, removing support for the previous on the server
            /*final Intent intent = new Intent(this, DeviceConflictActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
            return;*/

            final Intent intent = new Intent(AuthPinActivity.this, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra(LoginActivity.IS_SIGN_IN, true);
            startActivity(intent);
            finish();
        }
        loginWithAuditUser();
    }

    private void loginWithAuditUser() {
        Log.i(TAG, "Logging in with auditUser...");
        if (loginSubscription != null && !loginSubscription.isUnsubscribed()) {
            loginSubscription.unsubscribe();
        }
        loginSubscription =
                api.loginWithAuditUser(user.getConsumerId(), BuildConfig.AUTH_HEADER)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                new Action1<LoginResponse>() {
                                    @Override
                                    public void call(LoginResponse response) {
                                        if (response.hasErrors()) {
                                            Log.e(TAG, "login error: " + response.printErrors());
                                            showErrorDialog(getString(R.string.problem_logging_in));
                                        } else {
                                            onLoginComplete(response.getDpdSession());
                                        }
                                    }
                                },
                                new Action1<Throwable>() {
                                    @Override
                                    public void call(Throwable throwable) {
                                        Log.e(TAG, "login error", throwable);
                                        showErrorDialog(getString(R.string.problem_logging_in));
                                    }
                                });
    }

    private void onLoginComplete(String dpdSession) {
        if (dpdSession != null) {
            user.setDpdSession(dpdSession);
            user.setHasLoggedInWithAuditUser(true);
            user.save();
        }

        // if user has a ConsumerProperty already we can simply PUT the Consumer (inc. deviceId)
        // and skip ProfileSetupActivity entirely
        if (user.getConsumerProfile().hasConsumerProperties()) {
            putConsumerAndStartYourDeliveriesActivity();

        } else {
            // re-run startup checks now that we've authenticated the pin
            startMobileStartupChecksActivity();

        }
    }
}
