package com.dpd.yourdpd.model;

/**
 * Provides simple address info related to a {@link ConsumerProperty} preventing the need to perform
 * further API lookups to get this simple information.
 * Used in {@link com.dpd.yourdpd.ui.settings.SettingsActivity} to display list of addresses
 */
public class ConsumerPropertyAddressInfo {
    public ConsumerProperty consumerProperty;
    public String addressFirstLine;
    public String addressFirstLineWithTown;
}
