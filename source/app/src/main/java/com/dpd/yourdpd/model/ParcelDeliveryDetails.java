package com.dpd.yourdpd.model;

/**
 * @see Parcel
 */
public class ParcelDeliveryDetails {
    public Address address;
    public NotificationDetails notificationDetails;
    public PodDetails podDetails;
    public float totalWeight;
    public float verifiedWeight;
    public int numberOfParcels;
    public boolean liability;
    public float deliveredLatitude;
    public float deliveredLongitude;

    ParcelDeliveryDetails() {
        deliveredLatitude = Float.MAX_VALUE;
        deliveredLongitude = Float.MAX_VALUE;
    }
}
