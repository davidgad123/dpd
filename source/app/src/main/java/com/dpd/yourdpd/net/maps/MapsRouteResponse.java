package com.dpd.yourdpd.net.maps;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import org.json.JSONArray;

import java.util.List;

/**
 * @see MapsRouteRequest
 */
public class MapsRouteResponse {
    public String distance;
    public String duration;
    public LatLng start;
    public LatLng end;
    public JSONArray steps;
    public LatLngBounds boundsLatLng;
    public List<LatLng> points;
}
