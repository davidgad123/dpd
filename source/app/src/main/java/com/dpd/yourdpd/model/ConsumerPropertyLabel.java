package com.dpd.yourdpd.model;

/**
 * Values used for {@link ConsumerProperty#propertyLabel}.
 * Note that "other" values are specified by user.
 */
public class ConsumerPropertyLabel {
    public static final String HOME = "Home";
    public static final String WORK = "Work";
}
