package com.dpd.yourdpd.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by richardleggett on 17/06/15.
 */
public class PlaceholderPickupShop implements Parcelable {
    public final String name;
    public final String address;
    public final LatLng latLng;

    public PlaceholderPickupShop(String name, String address, LatLng latLng) {
        this.name = name;
        this.address = address;
        this.latLng = latLng;
    }

    protected PlaceholderPickupShop(Parcel in) {
        name = in.readString();
        address = in.readString();
        latLng = in.readParcelable(LatLng.class.getClassLoader());
    }

    public static final Creator<PlaceholderPickupShop> CREATOR = new Creator<PlaceholderPickupShop>() {
        @Override
        public PlaceholderPickupShop createFromParcel(Parcel in) {
            return new PlaceholderPickupShop(in);
        }

        @Override
        public PlaceholderPickupShop[] newArray(int size) {
            return new PlaceholderPickupShop[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(address);
        dest.writeParcelable(latLng, flags);
    }
}
