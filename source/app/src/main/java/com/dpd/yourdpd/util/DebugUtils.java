package com.dpd.yourdpd.util;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.widget.ArrayAdapter;

import com.dpd.yourdpd.ui.ExperienceRatingActivity;

/**
 * Developer utilities to force various states
 */
public class DebugUtils {
    public static String forcedParcelId = null;

    private static String[] parcelIDs = {
            "15976604602604*17441", // status 2
            "15505969010179*17327", // status 3
            "15505749005682*17295", // status 4
            "15505749000850*17284", // status 5
            "15501086396192*17452", // status 6
            "01455088667718*17459", // status 7
            "15505411068682*17452", // status 8
    };

    public static void showDebugMenu(final Activity activity) {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(activity);
        builderSingle.setTitle("Debug:");
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(activity, android.R.layout.select_dialog_item);
        arrayAdapter.add("Go to Experience Rating");
        arrayAdapter.add("Reset parcel status");
        arrayAdapter.add("Set parcel to status 2");
        arrayAdapter.add("Set parcel to status 3");
        arrayAdapter.add("Set parcel to status 4");
        arrayAdapter.add("Set parcel to status 5");
        arrayAdapter.add("Set parcel to status 6");
        arrayAdapter.add("Set parcel to status 7");
        arrayAdapter.add("Set parcel to status 8");
        builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builderSingle.setAdapter(arrayAdapter,
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
//                        String strName = arrayAdapter.getItem(which);
                        switch (which) {
                            case 0: {
                                Intent intent = new Intent(activity, ExperienceRatingActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.putExtra(ExperienceRatingActivity.EXTRA_PARCEL_CODE, "15501564661572*17568");
                                activity.startActivity(intent);
                                break;
                            }
                            default: {
                                if (which == 1) {
                                    forcedParcelId = null;
                                } else {
                                    forcedParcelId = parcelIDs[which - 2];
                                }
                            }
//                            case 1: {
//                                PlaceholderParcel parcel = new PlaceholderParcel();
//                                parcel.status = PlaceholderParcel.Status.REATTEMPTNG_DELIVERY;
//                                NotificationCompat.Builder builder = new NotificationCompat.Builder(activity);
//                                Intent intent = new Intent(activity, ParcelDetailActivity.class);
//                                intent.putExtra(ParcelDetailActivity.EXTRA_PARCEL, parcel);
//                                PendingIntent pi = PendingIntent.getActivity(activity, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
//                                builder.setSmallIcon(R.drawable.ic_notification_small);
//                                builder.setAutoCancel(true);
//                                builder.setContentIntent(pi);
//                                builder.setContentTitle(activity.getString(R.string.app_name));
//                                builder.setContentText("We see you are now at your delivery address. Your driver Brian will be back to deliver within 90 minutes");
//                                NotificationManagerCompat.from(activity).notify(0, builder.build());
//                                break;
//
//                            }
//                            case 2: {
//                                PlaceholderParcel parcel = new PlaceholderParcel();
//                                parcel.status = PlaceholderParcel.Status.DELAYED;
//                                Intent intent = new Intent(activity, ParcelDetailActivity.class);
//                                intent.putExtra(ParcelDetailActivity.EXTRA_PARCEL, parcel);
//                                activity.startActivity(intent);
//                                break;
//                            }
                        }
                    }
                });
        builderSingle.show();
    }
}
