package com.dpd.yourdpd.ui.address;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.transitions.everywhere.TransitionManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.dpd.yourdpd.R;
import com.dpd.yourdpd.model.SafeLocation;
import com.dpd.yourdpd.util.CommonUtils;
import com.dpd.yourdpd.util.ImagePickerUtils;
import com.dpd.yourdpd.util.PhotoUtils;
import com.dpd.yourdpd.util.ViewIdGenerator;

import java.io.File;
import java.util.List;

/**
 * Allow user to specify a safe place to leave parcels
 */
public class AddAddressSafePlaceFragment extends AddAddressStepFragmentBase {
    public static String TAG = AddAddressSafePlaceFragment.class.getSimpleName();

    private ToggleButton safePlaceYesButton;
    private ToggleButton safePlaceNoButton;
    private View safePlaceOptionsView;
    private RadioGroup safePlaceRadioGroup;
    private Button safePlacePhotoButton;
    private ImageView safePlacePhotoImageView;
    private String safePlacePhotoBase64Data;
    private CheckBox safePlaceCheckbox;
    private View safePlaceTooltip;
    private EditText driverNotesEditText;

    public static AddAddressSafePlaceFragment newInstance() {
        Bundle args = new Bundle();
        AddAddressSafePlaceFragment fragment = new AddAddressSafePlaceFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public AddAddressSafePlaceFragment() {
        // required empty Fragment constructor
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_add_address_safe_place, container, false);
        safePlaceYesButton = (ToggleButton) root.findViewById(R.id.safe_place_yes_button);
        safePlaceNoButton = (ToggleButton) root.findViewById(R.id.safe_place_no_button);
        safePlaceYesButton.setOnCheckedChangeListener(onSafePlaceCheckChange);
        safePlaceNoButton.setOnCheckedChangeListener(onSafePlaceCheckChange);
        safePlaceOptionsView = root.findViewById(R.id.safe_place_options);
        safePlaceOptionsView.setVisibility(View.GONE);
        safePlaceRadioGroup = (RadioGroup) root.findViewById(R.id.safe_places_radiogroup);
        safePlacePhotoImageView = (ImageView) root.findViewById(R.id.safe_place_photo_imageview);
        safePlacePhotoButton = (Button) root.findViewById(R.id.safe_place_photo_button);
        safePlacePhotoButton.setOnClickListener(onSafePlaceImageClick);
        safePlacePhotoImageView = (ImageView) root.findViewById(R.id.safe_place_photo_imageview);
        safePlacePhotoImageView.setOnClickListener(onSafePlaceImageClick);
        safePlaceCheckbox = (CheckBox) root.findViewById(R.id.safe_place_accept_checkbox);
        safePlaceCheckbox.setOnCheckedChangeListener(onAcceptTermsCheckChangeListener);
        safePlaceTooltip = root.findViewById(R.id.safe_place_tooltip);
        driverNotesEditText = (EditText) root.findViewById(R.id.driver_notes_edittext);
        driverNotesEditText.setHorizontallyScrolling(false);
        driverNotesEditText.setMaxLines(Integer.MAX_VALUE);

        CommonUtils.setupUIForKeyboard(getActivity(), root);

        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        disableNextButton();
        addSafePlaces();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            File file = new File(getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES), "temp.jpg");
            if (requestCode == ImagePickerUtils.REQUEST_CAMERA) {
                ImagePickerUtils.loadAndDisplayBitmap(file,
                        safePlacePhotoImageView,
                        new ImagePickerUtils.LoadImageCallback() {
                            @Override
                            public void onSuccess(Bitmap bitmap) {
                                onImageLoaded(bitmap);
                            }

                            @Override
                            public void onProgress(int progress) {
                            }

                            @Override
                            public void onError(int code) {
                                Toast.makeText(getActivity(),
                                        getString(R.string.problem_loading_image),
                                        Toast.LENGTH_SHORT).show();
                            }
                        });

            } else if (requestCode == ImagePickerUtils.REQUEST_GALLERY) {
                ImagePickerUtils.loadAndDisplayBitmap(data,
                        getActivity(),
                        safePlacePhotoImageView,
                        new ImagePickerUtils.LoadImageCallback() {
                            @Override
                            public void onSuccess(Bitmap bitmap) {
                                onImageLoaded(bitmap);
                            }

                            @Override
                            public void onProgress(int progress) {
                            }

                            @Override
                            public void onError(int code) {
                                Toast.makeText(getActivity(),
                                        getString(R.string.problem_loading_image),
                                        Toast.LENGTH_SHORT).show();
                            }
                        });
            }

            file.delete();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        ImagePickerUtils.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
    }

    private void onImageLoaded(Bitmap bitmap) {
        safePlacePhotoBase64Data = PhotoUtils.bitmapToBase64(bitmap);
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitle(getString(R.string.your_safe_place_titlecase));
    }

    private void addSafePlaces() {
        final LayoutInflater inflater = LayoutInflater.from(getActivity());
        List<SafeLocation> safeLocations = dataStore.getSafeLocations();
        if (safeLocations != null) {
            for (SafeLocation safeLocation : safeLocations) {
                final RadioButton radioButton = (RadioButton) inflater.inflate(R.layout.item_radiobutton, safePlaceRadioGroup, false);
                radioButton.setText(safeLocation.getSafeLocationDescription());
                radioButton.setTag(safeLocation.getSafeLocationId());
                radioButton.setId(ViewIdGenerator.generateViewId());
                safePlaceRadioGroup.addView(radioButton);
            }

            // automatically select the first option to start
            safePlaceRadioGroup.check(safePlaceRadioGroup.getChildAt(0).getId());
        }
    }

    private void pickPhoto() {
        ImagePickerUtils.showPickerDialog(this);
    }

    private void hideSafePlaceOptions() {
        safePlaceOptionsView.setVisibility(View.GONE);
        validate();
    }

    private void showSafePlaceOptions() {
        TransitionManager.beginDelayedTransition((ViewGroup) getView());
        safePlaceOptionsView.setVisibility(View.VISIBLE);
        validate();
    }

    private View.OnClickListener onSafePlaceImageClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            pickPhoto();
        }
    };

    private CompoundButton.OnCheckedChangeListener onSafePlaceCheckChange =
            new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (!isChecked) {
                        // prevent un-checking
                        buttonView.setChecked(true);
                        return;
                    }
                    switch (buttonView.getId()) {
                        case R.id.safe_place_yes_button:
                            safePlaceNoButton.setOnCheckedChangeListener(null);
                            safePlaceNoButton.setChecked(false);
                            safePlaceNoButton.setOnCheckedChangeListener(this);
                            showSafePlaceOptions();
                            break;
                        case R.id.safe_place_no_button:
                            safePlaceYesButton.setOnCheckedChangeListener(null);
                            safePlaceYesButton.setChecked(false);
                            safePlaceYesButton.setOnCheckedChangeListener(this);
                            hideSafePlaceOptions();
                            break;
                    }
                }
            };

    private CompoundButton.OnCheckedChangeListener onAcceptTermsCheckChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            safePlaceTooltip.setVisibility(isChecked ? View.INVISIBLE : View.VISIBLE);
            validate();
        }
    };

    @Override
    public boolean validate() {
        boolean isValid = safePlaceCheckbox.isChecked() || safePlaceNoButton.isChecked();
        if (isValid) {
            enableNextButton();
        } else {
            disableNextButton();
        }
        return isValid;
    }

    @Override
    public void applyChanges() {
        final int checkedRadioButtonId = safePlaceRadioGroup.getCheckedRadioButtonId();
        final RadioButton checkedRadioButton = (checkedRadioButtonId == -1) ? null : (RadioButton) safePlaceRadioGroup.findViewById(checkedRadioButtonId);
        consumerProperty.safeLocationAllow = safePlaceYesButton.isChecked();
        consumerProperty.safeLocationPhotograph = safePlacePhotoBase64Data;
        consumerProperty.safeLocationDescription = driverNotesEditText.getText().toString();
        consumerProperty.safeLocationId = (checkedRadioButton == null) ? null : (String) checkedRadioButton.getTag();
    }
}
