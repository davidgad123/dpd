package com.dpd.yourdpd.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

// TODO: remove fields we are never going to need (unused should be highlighted in grey)
public class Depot {
    // TODO: these first three fields were returned as part of Parcel detail, other fields from GET depot
    public String depotCode;
    public Route route;
    public float distanceToDelivery;

    public String depotDescription;
    public String depotTypeCode;     // e.g. Y
    public String depotName;
    public String location;
    public Address address;
    public boolean isHub;

    @SerializedName("openingTimes")
    public List<DepotOpeningTimes> openingTimes;

    public static class DepotOpeningTimes {
        public int weekDay;
        public String openTime;
        public String closeTime;
    }

    public AddressPoint addressPoint;
    public String telephone;
    public String telephoneShort;
    public String fax;
    public boolean geopostOwned;
    public String depotDualCode;
    public String depotDualDesc;

    public DepotRoutePanning depotRoutePanning;

    public static class DepotRoutePanning {
        public boolean autoBulkAllocate;
        public int autoBulkAllocateMax;
        public String defaultShiftManager;
        public String routeMatrixMenu;
        public boolean routeOptimiseActive;
        public boolean saturnLockdown;
    }

    public String countryCodeAltCode;
    public String coutnryCodeAltDesc;

    public int businessUnitPrimary;
    public int[] businessUnitSupported;

    // probably going to be removed from API due to security
//    public List<DepotContact> contact;
}
