package com.dpd.yourdpd.ui.yourdeliveries;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.dpd.yourdpd.R;
import com.dpd.yourdpd.model.Base64Photo;
import com.dpd.yourdpd.ui.PhotosActivity;
import com.dpd.yourdpd.util.PhotoUtils;

import java.io.Serializable;
import java.util.List;

/**
 * Displays one or more delivery photos
 */
public class DeliveryPhotosView extends FrameLayout {
    private static final String TAG = DeliveryPhotosView.class.getSimpleName();

    private ViewGroup photosContainer;

    public DeliveryPhotosView(Context context) {
        this(context, null);
    }

    public DeliveryPhotosView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DeliveryPhotosView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater.from(getContext()).inflate(R.layout.delivery_photos, this, true);

        photosContainer = (ViewGroup) findViewById(R.id.photos_container);
    }

    public void setPhotos(final List<Base64Photo> photos, final String dpdSession) {
        final Context context = getContext();
        findViewById(R.id.top_image_label_textview).setVisibility(photos.size() > 0 ? View.VISIBLE : View.GONE);
        findViewById(R.id.bottom_image_label_textview).setVisibility(photos.size() > 0 ? View.GONE : View.VISIBLE);

        for (int i = 0; i < photos.size(); i++) {
            final Base64Photo photo = photos.get(i);
            final int photoIndex = i;

            if (i < photosContainer.getChildCount()) {
                final ImageView imageView = (ImageView) photosContainer.getChildAt(i);
                imageView.setVisibility(View.VISIBLE);
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Intent intent = new Intent(context, PhotosActivity.class);
                        intent.putExtra(PhotosActivity.EXTRA_TITLE, context.getString(R.string.delivered));
                        intent.putExtra(PhotosActivity.EXTRA_PHOTOS, (Serializable) photos);
                        intent.putExtra(PhotosActivity.EXTRA_STARTING_POSITION, photoIndex);
                        intent.putExtra(PhotosActivity.EXTRA_DPD_SESSION, dpdSession);
                        context.startActivity(intent);
                    }
                });

                try {
                    PhotoUtils.loadBase64ImageIntoImageView(photo.base64Data, imageView);
                } catch (Exception e) {
                    Log.e(TAG, "Unable to decode image");
                }
            }
        }
    }


}