package com.dpd.yourdpd.model;

import com.dpd.yourdpd.net.response.GetCollectParcelTimesResponse;

import java.util.List;

public class DateAndTimes {
    public String diaryDate;
    public List<GetCollectParcelTimesResponse.TimePair> timePairs;
}
