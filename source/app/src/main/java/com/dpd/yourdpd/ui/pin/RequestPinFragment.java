package com.dpd.yourdpd.ui.pin;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.transitions.everywhere.TransitionManager;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.dpd.yourdpd.R;
import com.dpd.yourdpd.model.PinMethod;
import com.dpd.yourdpd.net.request.GeneratePinByEmailParams;
import com.dpd.yourdpd.net.request.GeneratePinBySmsParams;
import com.dpd.yourdpd.net.response.GeneratePinResponse;
import com.dpd.yourdpd.ui.BaseFragment;
import com.dpd.yourdpd.util.AppConstant;
import com.dpd.yourdpd.util.CommonUtils;

import retrofit.RetrofitError;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Request PIN via SMS (mobile number) or email
 */
public class RequestPinFragment extends BaseFragment {

    private static final String TAG = RequestPinFragment.class.getSimpleName();

    private ToggleButton smsButton;
    private ToggleButton emailButton;
    private View phoneEditTextContainer;
    private EditText phonePrefixEditText;
    private EditText phoneEditText;
    private EditText emailEditText;
    private View errorTooltip;
    private Button requestPinButton;
    private ProgressBar progressBar;
    private InputMethodManager inputMethodManager;

    private OnPinRequestedListener listener;
    private PinMethod method;
    private Subscription generatePinSubscription;
    private TextView textView;

    public interface OnPinRequestedListener {
        void onPinRequested(PinMethod pinMethod, int requestPinType, String requestStr);
    }

    public static RequestPinFragment newInstance() {
        return new RequestPinFragment();
    }

    public RequestPinFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (OnPinRequestedListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() +
                    " must implement " + OnPinRequestedListener.class.getName());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.fragment_request_pin, container, false);
        smsButton = (ToggleButton) root.findViewById(R.id.sms_button);
        smsButton.setChecked(true);
        smsButton.setOnCheckedChangeListener(onChangeRequestPinMethod);
        emailButton = (ToggleButton) root.findViewById(R.id.email_button);
        emailButton.setOnCheckedChangeListener(onChangeRequestPinMethod);
        phoneEditTextContainer = root.findViewById(R.id.phone_edittext_container);
        phonePrefixEditText = (EditText) root.findViewById(R.id.phone_prefix_edittext);
        phoneEditText = (EditText) root.findViewById(R.id.phone_edittext);
        emailEditText = (EditText) root.findViewById(R.id.email_edittext);
        errorTooltip = root.findViewById(R.id.error_tooltip);
        requestPinButton = (Button) root.findViewById(R.id.request_pin_button);
        requestPinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestPin();
            }
        });
        progressBar = (ProgressBar) root.findViewById(R.id.progressBar);

        textView = (TextView) errorTooltip.findViewById(R.id.tooltip_textview);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideError();
            }
        });

        CommonUtils.setupUIForKeyboard(getActivity(), root);

        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        method = PinMethod.SMS;
        inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
    }

    private CompoundButton.OnCheckedChangeListener onChangeRequestPinMethod =
            new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (!isChecked) {
                        // prevent un-checking
                        buttonView.setChecked(true);
                        return;
                    }
                    switch (buttonView.getId()) {
                        case R.id.email_button:
                            method = PinMethod.EMAIL;
                            smsButton.setOnCheckedChangeListener(null);
                            smsButton.setChecked(false);
                            smsButton.setOnCheckedChangeListener(this);
                            TransitionManager.beginDelayedTransition((ViewGroup) getView());
                            emailEditText.setVisibility(View.VISIBLE);
                            phoneEditTextContainer.setVisibility(View.INVISIBLE);
                            break;
                        case R.id.sms_button:
                            method = PinMethod.SMS;
                            emailButton.setOnCheckedChangeListener(null);
                            emailButton.setChecked(false);
                            emailButton.setOnCheckedChangeListener(this);
                            TransitionManager.beginDelayedTransition((ViewGroup) getView());
                            phoneEditTextContainer.setVisibility(View.VISIBLE);
                            emailEditText.setVisibility(View.INVISIBLE);
                            break;
                    }
                    hideError();
                    closeKeyboard();
                }
            };

    private void closeKeyboard() {
        final View view = getView();
        if (view != null) {
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_IMPLICIT_ONLY);
        }
    }

    private void requestPin() {
        hideError();
        if (emailButton.isChecked()) {
            final String email = emailEditText.getText().toString();
            if (TextUtils.isEmpty(email)) {
                showErrorText(getString(R.string.invalid_empty_email_tooltip));
            } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                showErrorText(getString(R.string.invalid_email_tooltip_retry));
            } else {
                performRequestPinAPICall(AppConstant.REQUEST_PIN_MAIL);
            }

        } else {
            final String phone = phoneEditText.getText().toString();
            if (TextUtils.isEmpty(phone)) {
                showErrorText(getString(R.string.invalid_empty_phone_tooltip));
            } else if (phone.length() < 7 || !(Patterns.PHONE.matcher(phone).matches())) {
                showErrorText(getString(R.string.invalid_phone_tooltip_retry));
            } else {
                performRequestPinAPICall(AppConstant.REQUEST_PIN_PHONE);
            }
        }
    }

    private void showErrorText(CharSequence error) {
        if (phoneEditTextContainer.getVisibility() == View.VISIBLE) {
            phoneEditText.setActivated(true);
            phonePrefixEditText.setActivated(true);
        } else
            emailEditText.setActivated(true);

        textView.setText(error);
        TransitionManager.beginDelayedTransition((ViewGroup) getView());
        errorTooltip.setVisibility(View.VISIBLE);
    }

    private void hideError() {
        if (phoneEditTextContainer.getVisibility() == View.VISIBLE) {
            phoneEditText.setActivated(false);
            phonePrefixEditText.setActivated(false);
        } else
            emailEditText.setActivated(false);

        TransitionManager.beginDelayedTransition((ViewGroup) getView());
        errorTooltip.setVisibility(View.GONE);
    }

    private void performRequestPinAPICall(final int requestPinType) {
        final boolean isSMSPin = method.equals(PinMethod.SMS);
        String email = emailEditText.getText().toString();
        String mobile = CommonUtils.addPrefixNumber(phoneEditText.getText().toString());

        showLoading();

        final Observable<GeneratePinResponse> observable = isSMSPin ?
                api.generatePinBySMS(new GeneratePinBySmsParams(mobile)) :
                api.generatePinByEmail(new GeneratePinByEmailParams(email));
        final String requestStr = method.equals(PinMethod.SMS) ? mobile : email;

        if (generatePinSubscription != null && !generatePinSubscription.isUnsubscribed()) {
            generatePinSubscription.unsubscribe();
        }
        generatePinSubscription = observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<GeneratePinResponse>() {
                               @Override
                               public void call(GeneratePinResponse response) {
                                   hideLoading();
                                   if (response.hasErrors()) {
                                       Log.e(TAG, "Generate pin error: " + response.printErrors());
                                       showErrorText(getString(isSMSPin ? R.string.problem_invalid_mobile : R.string.problem_invalid_mail));
                                   } else {
                                       listener.onPinRequested(method, requestPinType, requestStr);
                                   }
                               }
                           },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                hideLoading();
                                if (throwable instanceof RetrofitError) {
                                    RetrofitError retrofitError = (RetrofitError) throwable;
                                    if (retrofitError.getResponse().getStatus() == 429) {
                                        showErrorText(getString(R.string.too_many_requests));
                                        return;
                                    }
                                }
                                Log.e(TAG, "Generate pin error", throwable);
                                showErrorText(getString(isSMSPin ? R.string.problem_invalid_mobile : R.string.problem_invalid_mail));
                            }
                        });
    }

    private void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
        requestPinButton.setText("");
        emailButton.setClickable(false);
        smsButton.setClickable(false);
    }

    private void hideLoading() {
        progressBar.setVisibility(View.GONE);
        requestPinButton.setText(R.string.request_pin);
        emailButton.setClickable(true);
        smsButton.setClickable(true);
    }
}
