package com.dpd.yourdpd.net.response;

import com.dpd.yourdpd.model.UDPRNAddress;
import com.google.gson.annotations.SerializedName;

/**
 * @see com.dpd.yourdpd.net.Api#getAddressByUDPRN(String)
 */
public class GetAddressByUDPRNResponse extends BaseResponse {
    @SerializedName("data")
    public UDPRNAddress address;

    // e.g. response
//    {
//        "error": null,
//            "data": {
//                "postcode": "B45 0LD",
//                "street": "Frankley Industrial Park, Tay Road",
//                "locality": "Rubery, Rednal                                            ",
//                "town": "Birmingham",
//                "county": "",
//                "organisation": "Jordan Motor Services",
//                "property": "Unit 13",
//                "udprnLongitude": -2.0073,
//                "udprnLatitude": 52.4045,
//                "occupancy": 2
//    }
//    }
}
