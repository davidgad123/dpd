package com.dpd.yourdpd.ui.address;

import android.content.Context;
import android.os.Bundle;

import com.dpd.yourdpd.model.ConsumerProperty;
import com.dpd.yourdpd.ui.BaseFragment;
import com.dpd.yourdpd.ui.Validating;

/**
 * Base class for Fragments that appear in the add address (aka ConsumerProperty) steps UI flow
 * e.g. in {@link AddAddressActivity} and {@link com.dpd.yourdpd.ui.profile.ProfileSetupActivity}
 */
public abstract class AddAddressStepFragmentBase extends BaseFragment implements Validating {

    public static final String ARG_TITLE = "title";
    public static final String ARG_IS_PRIMARY = "is_primary";

    protected String title;
    protected boolean isPrimary;
    protected boolean wasNextButtonEnabled;
    protected ConsumerProperty consumerProperty;
    protected AddAddressStepFragmentListener listener;

    /**
     * Interface which must be implemented by any Activities hosting a
     * Fragment of this base class
     */
    public interface AddAddressStepFragmentListener {
        void enableNextButton();

        void disableNextButton();

        boolean isNextButtonEnabled();

        void setTitle(CharSequence title);

        void scrollToTop();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        title = getArguments().getString(ARG_TITLE, null);
        isPrimary = getArguments().getBoolean(ARG_IS_PRIMARY, false);

        // store reference to the ConsumerProperty we are adding/editing
        consumerProperty = user.selectedConsumerProperty;
        if (consumerProperty == null) {
            throw new IllegalStateException("ConsumerProfile.selectedConsumerProperty must not be null");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (AddAddressStepFragmentListener) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity() + " must implement " + AddAddressStepFragmentListener.class.getSimpleName());
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        consumerProperty = null;
    }

    protected void setTitle(String title) {
        this.title = title;
        listener.setTitle(title);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (hidden) {
            // keep track of the Next button state for when we return to this Fragment through
            // the history back stack
            wasNextButtonEnabled = listener.isNextButtonEnabled();
        } else {
            // restore app bar title
            setTitle(title);

            // restore next button state
            if (wasNextButtonEnabled) {
                enableNextButton();
            } else {
                disableNextButton();
            }
        }
    }

    protected void scrollToTop() {
        listener.scrollToTop();
    }

    protected void enableNextButton() {
        listener.enableNextButton();
    }

    protected void disableNextButton() {
        listener.disableNextButton();
    }
}
