package com.dpd.yourdpd.ui.help;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.MediaController;
import android.widget.VideoView;

import com.dpd.yourdpd.R;

public class HelpVideoActivity extends AppCompatActivity {
    /**
     * TODO: Set the path variable to a streaming video URL or a local media
     * file path.
     */
    private VideoView videoView;

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_help_video);
        videoView = (VideoView) findViewById(R.id.videoView);
        /*
         * Alternatively, you can use videoView.setVideoPath(<path>);
         */
//        videoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() +
//                "/" + R.raw.videoviewdemo));
        videoView.setMediaController(new MediaController(this));
        videoView.requestFocus();
    }
}
