package com.dpd.yourdpd.model;

public class PickupLocationResult {
    public PickupLocation pickupLocation;
    public float distance;
    /**
     *
     */
    public String pickupDate;
}
