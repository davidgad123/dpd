package com.dpd.yourdpd.net.request;

/**
 * @see com.dpd.yourdpd.net.Api#collectFromDepot(CollectFromDepotParams)
 */
public class DeliverToNeighbourParams {
    public static String ANY_NEIGHBOUR_ADDRESS_ID = ""; // intentionally left blank ;)

    private String addressId;
    //    private String deliveryInstructions;
    private String diaryDate;
    private String contactName;
    private String deliveryInstructions;
    private String email;
    private String mobile;
    private String houseNumber;
    private String[] parcelCode;

    public static class Builder {
        private DeliverToNeighbourParams params = new DeliverToNeighbourParams();

        public Builder() {
            // setup defaults
//            params.deliveryInstructions = "Deliver to neighbour";
            params.deliveryInstructions = null;
            params.contactName = null;
            params.houseNumber = null;
            params.diaryDate = null; // only required for carded parcels
        }

        public Builder setHouseNumber(String houseNumber) {
            params.houseNumber = houseNumber;
            return this;
        }

        public Builder setAddressId(String addressId) {
            params.addressId = addressId;
            return this;
        }

        public Builder setContactName(String contactName) {
            params.contactName = contactName;
            return this;
        }

        public Builder setDiaryDate(String diaryDate) {
            params.diaryDate = diaryDate;
            return this;
        }

        public Builder setEmail(String email) {
            params.email = email;
            return this;
        }

        public Builder setMobile(String mobile) {
            params.mobile = mobile;
            return this;
        }

        public Builder setParcelCode(String parcelCode) {
            params.parcelCode = new String[]{parcelCode};
            return this;
        }

        public DeliverToNeighbourParams build() {
            //if (params.addressId == null) {
            //    throw new IllegalStateException("addressId cannot be null");
            //}
            if (params.parcelCode == null) {
                throw new IllegalStateException("parcelCode cannot be null");
            }
            return params;
        }
    }
}
