package com.dpd.yourdpd.ui.address;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.UiThread;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.TextView;

import com.dpd.yourdpd.R;
import com.dpd.yourdpd.model.ConsumerProperty;
import com.dpd.yourdpd.model.ConsumerPropertyAddressInfo;
import com.dpd.yourdpd.ui.BaseFragment;
import com.dpd.yourdpd.ui.Validating;

/**
 * Base class for all of the address editing Fragments, similar to {@link AddAddressStepFragmentBase}
 */
abstract class EditAddressFragmentBase extends BaseFragment implements Validating {

    public interface EditAddressFragmentListener {
        void onDiscardChanges();
    }

    private EditAddressFragmentListener listener;
    protected ConsumerProperty consumerProperty;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (EditAddressFragmentListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement EditAddressFragmentBase.EditAddressFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // store reference to the ConsumerProperty we are editing
        consumerProperty = user.selectedConsumerProperty;
        if (consumerProperty == null) {
            throw new IllegalStateException("ConsumerProfile.selectedConsumerProperty must not be null");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        consumerProperty = null;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        View root = getView();
        assert root != null;

        final ConsumerPropertyAddressInfo addressInfo = user.getConsumerProfile().getConsumerPropertyAddressInfoWithId(consumerProperty.consumerPropertyId);
        final TextView addressNameTextView = (TextView) root.findViewById(R.id.address_name_textview);
        addressNameTextView.setText(getString(R.string.address_name_preferences, addressInfo.addressFirstLine));
    }

    @UiThread
    protected void showErrorAlertWithDiscardOption(String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(getActivity(), R.style.AlertDialogTheme);
        builder.setMessage(message);
        builder.setPositiveButton(android.R.string.ok, null);
        builder.setNegativeButton("Discard Changes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                listener.onDiscardChanges();
            }
        });
        builder.create().show();
    }
}