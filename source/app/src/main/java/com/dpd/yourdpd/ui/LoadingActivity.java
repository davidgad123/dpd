package com.dpd.yourdpd.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;

import com.dpd.yourdpd.BuildConfig;
import com.dpd.yourdpd.R;
import com.dpd.yourdpd.util.AppConstant;
import com.dpd.yourdpd.util.CommonUtils;
import com.newrelic.agent.android.NewRelic;
import com.newrelic.agent.android.logging.AgentLog;

/**
 * The launcher Activity, if this is the first run, displays an animation introducing the brands
 * and moves onto the CoachActivity
 */
public class LoadingActivity extends BaseActivity {

    private ImageView logoImageView;
    private Handler handler;
    private int currentLogo = 0;

    private boolean is_connection = true;
    private boolean is_authorized = true;

    private int[] logos = new int[]{
            R.drawable.dpd_group_logo,
            R.drawable.dpd_logo,
            R.drawable.interlink_logo
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);

        NewRelic.withApplicationToken(BuildConfig.NEWRELIC_TOKEN)
                .withLogLevel(AgentLog.DEBUG)
                .start(this.getApplication());

        logoImageView = (ImageView) findViewById(R.id.logo);

        if (getIntent() != null) {
            is_connection = getIntent().getBooleanExtra(AppConstant.BUNDLE_NO_CONNECTION_RESPONSE, true);
            is_authorized = getIntent().getBooleanExtra(AppConstant.BUNDLE_NO_AUTHORIZED_RESPONSE, true);
        }

        // determine whether we can skip this Activity and CoachActivity
        if (!is_connection) {
            showNoConnectionDialog();
        } else if (!is_authorized) {
            showNoAuthorizedDialog();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (is_connection && is_authorized) {
            startLogoAnimation();
        }
    }

    private void showNoConnectionDialog() {
        showErrorAlertDialogWithOk(getString(R.string.no_connection_body), getString(R.string.no_connection_title), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (CommonUtils.isNetworkAvailable(LoadingActivity.this)) {
                    startLogoAnimation();
                } else {
                    showNoConnectionDialog();
                }
            }
        });
    }

    private void showNoAuthorizedDialog() {
        showErrorAlertDialogWithOk(getString(R.string.please_sign_in_again), getString(R.string.an_error_has_occured), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                user.logout();
            }
        });
    }

    private void startLogoAnimation() {
        ((View) logoImageView).setAlpha(0);
        currentLogo = 0;
        handler = new Handler();
        handler.postDelayed(showNextLogo, 250);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (handler != null) {
            handler.removeCallbacks(showNextLogo);
            if (fadeInAndOut != null) {
                fadeInAndOut.removeAllListeners();
                fadeInAndOut.cancel();
            }
        }
    }

    private AnimatorSet fadeInAndOut;
    private Runnable showNextLogo = new Runnable() {
        @Override
        public void run() {
            ObjectAnimator fadeIn = ObjectAnimator.ofFloat(logoImageView, View.ALPHA, 0, 1);
            fadeIn.setDuration(3000);
            fadeIn.setInterpolator(new DecelerateInterpolator());
            ObjectAnimator fadeOut = ObjectAnimator.ofFloat(logoImageView, View.ALPHA, 1, 0);
            fadeIn.setDuration(1000);
            fadeIn.setStartDelay(250);
            fadeInAndOut = new AnimatorSet();
            fadeInAndOut.playSequentially(fadeIn, fadeOut);
            fadeInAndOut.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    currentLogo++;
                    if (currentLogo == logos.length) {
                        // move on
                        handler.postDelayed(startCoachActivityRunnable, 1000);
                        return;
                    }
                    if (currentLogo > logos.length - 1) {
                        currentLogo = 0;
                    }
                    logoImageView.setImageResource(logos[currentLogo]);
                    handler.postDelayed(showNextLogo, 750);
                }
            });
            fadeInAndOut.start();
        }
    };

    private Runnable startCoachActivityRunnable = new Runnable() {
        @Override
        public void run() {
            if (user.hasSeenCoachScreens()) {
                final Intent intent = new Intent(LoadingActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            } else {
                startCoachActivity();
            }
        }
    };

    private void startCoachActivity() {
        Intent intent = new Intent(this, CoachActivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }
}
