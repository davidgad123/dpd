package com.dpd.yourdpd.net.request;

import android.text.TextUtils;

/**
 * @see com.dpd.yourdpd.net.Api#leaveInSafePlace(LeaveInSafeLocationParams)
 */
public class LeaveInSafeLocationParams {
    private String callingCardLocation;
    private String safeLocationId;
    private String safeLocationInstruction;
    private String diaryDate;
    private String contactName;
    private String email;
    private String mobile;
    private String[] parcelCode;

    public static class Builder {
        private LeaveInSafeLocationParams params = new LeaveInSafeLocationParams();

        public Builder() {
            // setup defaults
            params.callingCardLocation = null;
            params.contactName = "";
            params.diaryDate = ""; // only required for carded parcels
            params.safeLocationInstruction = ""; // only required for "Other" safe place
        }

        public Builder setCallingCardLocation(String callingCardLocation) {
            params.callingCardLocation = callingCardLocation;
            return this;
        }

        public Builder setSafeLocationId(String safeLocationId) {
            params.safeLocationId = safeLocationId;
            return this;
        }

        public Builder setSafeLocationInstruction(String safeLocationInstruction) {
            params.safeLocationInstruction = safeLocationInstruction;
            return this;
        }

        public Builder setContactName(String contactName) {
            params.contactName = contactName;
            return this;
        }

        public Builder setDiaryDate(String diaryDate) {
            params.diaryDate = diaryDate;
            return this;
        }

        public Builder setEmail(String email) {
            params.email = !TextUtils.isEmpty(email) ? email : null;
            return this;
        }

        public Builder setMobile(String mobile) {
            params.mobile = !TextUtils.isEmpty(mobile) ? mobile : null;
            return this;
        }

        public Builder setParcelCode(String parcelCode) {
            params.parcelCode = new String[]{parcelCode};
            return this;
        }

        public LeaveInSafeLocationParams build() {
            if (params.safeLocationId == null) {
                throw new IllegalStateException("safeLocationId cannot be null");
            }
            //if (params.callingCardLocation == null) {
            //    throw new IllegalStateException("callingCardLocation cannot be null");
            //}
            if (params.parcelCode == null) {
                throw new IllegalStateException("parcelCode cannot be null");
            }
            return params;
        }
    }
}
