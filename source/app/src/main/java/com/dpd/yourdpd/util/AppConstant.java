package com.dpd.yourdpd.util;

public class AppConstant {
    public final static int REQUEST_PIN_PHONE = 1;
    public final static int REQUEST_PIN_MAIL = 2;
    public static String BUNDLE_PIN_TYPE = "pin_type";
    public static String BUNDLE_REQUEST_STRING = "request_string";
    public final static String PROFILE_ERR_CODE = "Duplicate Email";

    public static String BUNDLE_NO_CONNECTION_RESPONSE = "no_connection";
    public static String BUNDLE_NO_AUTHORIZED_RESPONSE = "no_authorized";
}
