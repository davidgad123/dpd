package com.dpd.yourdpd.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.dpd.yourdpd.R;

/**
 * Displays a message allowing the user to continue using the app with this device,
 * or cancel and reset all data
 */
public class DeviceConflictActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_confict);

        findViewById(R.id.use_this_device_button)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // persist user
                        user.save();

                        // move to LoginActivity to perform an audit user login as user.consumerId is set
                        final Intent intent = new Intent(DeviceConflictActivity.this, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    }
                });

        findViewById(R.id.use_previous_device_button)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dataStore.clear();
                        user.logout();
                        final Intent intent = new Intent(DeviceConflictActivity.this, LoadingActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    }
                });
    }

    @Override
    public void onBackPressed() {
        dataStore.clear();
        user.logout();

        final Intent intent = new Intent(this, LoadingActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    public void onUserLoggedOut() {
        // no-op
    }
}
