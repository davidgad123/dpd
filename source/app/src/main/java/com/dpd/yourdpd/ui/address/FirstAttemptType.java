package com.dpd.yourdpd.ui.address;

/**
 * @see com.dpd.yourdpd.model.ConsumerProperty#firstAttemptType
 */
public class FirstAttemptType {
    public static String HOME = "H";
    public static String SHOP = "S";
}
