package com.dpd.yourdpd.model;

import android.text.TextUtils;

import java.util.List;

/**
 * Represents a pickup shop where consumers can go to collect parcels
 */
public class PickupLocation {
    public String pickupLocationCode;
    public ContactDetails contactDetails;
    public Address address;
    /**
     * e.g. 100
     */
    public String pickupLocationType;
    public String shortName;
    public AddressPoint addressPoint;
    /**
     * e.g. en
     */
    public String languageSpoken;
    public boolean disabledAccess;
    public boolean parkingAvailable;
    public List<String> geoServiceCode;
    public String pickupLocationDirections;
    public ParcelLocationAvailability pickupLocationAvailability;
    public boolean openLate;
    public boolean openSaturday;
    public boolean openSunday;

    public String getName() {
        if (!TextUtils.isEmpty(shortName)) {
            return shortName;
        } else if (address != null && !TextUtils.isEmpty(address.organisation)) {
            return address.organisation;
        }
        return null;
    }

    public String getNameWithTown() {
        return getName() + (address.town == null ? "" : " - " + address.town);
    }

    public static class ParcelLocationAvailability {
        public String pickupLocationActiveStart;
        public String pickupLocationActiveEnd;
        public List<PickupLocationOpenWindow> pickupLocationOpenWindow;
        public String pickupLocationDeliveryStart;
        public String pickupLocationDeliveryClosed;
        public List<PickupLocationDowntime> pickupLocationDownTime;
        public String pickupLocationOpeningExceptions;
    }

    public static class PickupLocationOpenWindow {
        public String pickupLocationOpenWindowStartTime;
        public String pickupLocationOpenWindowEndTime;
        public int pickupLocationOpenWindowDay;
    }

    public static class PickupLocationDowntime {
        public String pickupLocationDownTimeFrom;
        public String pickupLocationDownTimeTo;
    }

    public static class ContactDetails {
        public String contactName;
        public String telephone;
        public String email;
    }
}