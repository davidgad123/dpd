package com.dpd.yourdpd.net.response;

import com.dpd.yourdpd.model.PickupLocationResult;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetParcelPickupLocationsResponse extends BaseResponse {
    @SerializedName("data")
    private PickupLocations pickupLocations;

    private int totalResults;

    public List<PickupLocationResult> getPickupLocationResults() {
        return pickupLocations.results;
    }

    public int getTotalResults() {
        return totalResults;
    }


    private class PickupLocations {
        public List<PickupLocationResult> results;
    }
}
