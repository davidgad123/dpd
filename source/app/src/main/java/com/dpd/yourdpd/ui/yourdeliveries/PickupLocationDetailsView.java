package com.dpd.yourdpd.ui.yourdeliveries;

import android.annotation.SuppressLint;
import android.content.Context;
import android.transitions.everywhere.AutoTransition;
import android.transitions.everywhere.TransitionManager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dpd.yourdpd.R;
import com.dpd.yourdpd.model.PickupLocation;

import java.util.ArrayList;
import java.util.List;

/**
 * Displayed in {@link ParcelDetailActivityFragment} and displays the details for the given
 * {@link com.dpd.yourdpd.model.PickupLocation}
 */
public class PickupLocationDetailsView extends FrameLayout {
    private static final String TAG = PickupLocationDetailsView.class.getSimpleName();
    private final TextView titleTextView;
    private final TextView addressTextView;
    private final Button openingHoursButton;
    private final LinearLayout openingHoursContainer;

    public PickupLocationDetailsView(Context context) {
        this(context, null);
    }

    public PickupLocationDetailsView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PickupLocationDetailsView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater.from(getContext()).inflate(R.layout.pickup_location_details, this, true);

        titleTextView = (TextView) findViewById(R.id.title_textview);
        addressTextView = (TextView) findViewById(R.id.address_textview);
        openingHoursContainer = (LinearLayout) findViewById(R.id.opening_hours_container);

        openingHoursButton = (Button) findViewById(R.id.opening_hours_button);
        openingHoursButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleShowOpeningHours();
            }
        });
    }

    @SuppressLint("SetTextI18n")
    public void setPickupLocation(PickupLocation location) {
        final LayoutInflater inflater = LayoutInflater.from(getContext());

        titleTextView.setText(location.getName());
        addressTextView.setText(location.address.toFormattedString());

        // add opening hours
        openingHoursContainer.removeAllViews();

        try {
            final PickupLocation.ParcelLocationAvailability availability = location.pickupLocationAvailability;
            final List<PickupLocation.PickupLocationOpenWindow> openWindows = availability.pickupLocationOpenWindow;

            final String[] daysOfWeek = getResources().getStringArray(R.array.days_of_week);
            final List<String> days = new ArrayList<>(7);
            final List<String> hours = new ArrayList<>(7);
            int i = 0;
            int prevDay = -1;
            PickupLocation.PickupLocationOpenWindow prevWindow = null;

            for (PickupLocation.PickupLocationOpenWindow window : openWindows) {
                final int day = window.pickupLocationOpenWindowDay;
                final String slot = window.pickupLocationOpenWindowStartTime + " - " + window.pickupLocationOpenWindowEndTime;
                if (day != prevDay) {
                    // new day
                    final String dayOfWeek = daysOfWeek[day - 1];
                    days.add(dayOfWeek);
                    hours.add(slot);
                    i++;
                } else {
                    // see if window start time matches prev window close time and merge the hours
                    if (prevWindow != null && prevWindow.pickupLocationOpenWindowEndTime.equals(window.pickupLocationOpenWindowStartTime)) {
                        // merge OpenWindows together into a single slot
                        // so 10:00-12:00 and 12:00-22:00 -> 10:00-22:00
                        hours.set(i - 1, prevWindow.pickupLocationOpenWindowStartTime + " - " + window.pickupLocationOpenWindowEndTime);
                    } else {
                        // append second slot to end of hours
                        hours.set(i - 1, hours.get(i - 1) + "/" + slot);
                    }
                }

                prevWindow = window;
                prevDay = day;
            }

            for (int j = 0; j < days.size(); j++) {
                final String dayOfWeek = days.get(j);
                final String openingHours = hours.get(j);
                final View row = inflater.inflate(R.layout.item_opening_hour, openingHoursContainer, false);
                final TextView dayTextView = (TextView) row.findViewById(R.id.day_textview);
                final TextView hoursTextView = (TextView) row.findViewById(R.id.hours_textview);

                dayTextView.setText(dayOfWeek);
                hoursTextView.setText(openingHours);

                // insert row before divider
                openingHoursContainer.addView(row, new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT));
            }
        } catch (Exception e) {
            Log.e(TAG, "Problem formatting opening times", e);
        }
    }

    private void toggleShowOpeningHours() {
        final AutoTransition transition = new AutoTransition(getContext(), null);
        TransitionManager.beginDelayedTransition(this, transition);
        openingHoursContainer.setVisibility(openingHoursContainer.getVisibility() == VISIBLE ? GONE : VISIBLE);
        findViewById(R.id.opening_hours_divider).setVisibility(openingHoursContainer.getVisibility() == VISIBLE ? VISIBLE : GONE);
        openingHoursButton.setText(openingHoursContainer.getVisibility() == VISIBLE ?
                getResources().getString(R.string.hide_opening_hours) :
                getResources().getString(R.string.show_opening_hours));
    }
}
