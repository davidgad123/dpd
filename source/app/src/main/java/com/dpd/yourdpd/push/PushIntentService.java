package com.dpd.yourdpd.push;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.dpd.yourdpd.BuildConfig;
import com.dpd.yourdpd.R;
import com.dpd.yourdpd.ui.ExperienceRatingActivity;
import com.dpd.yourdpd.ui.yourdeliveries.ParcelDetailActivity;
import com.localz.spotzpush.sdk.service.AbstractGcmIntentService;
import com.util.WearableUtil;

import java.util.Date;

public class PushIntentService extends AbstractGcmIntentService {
    private static final String TAG = PushIntentService.class.getSimpleName();

    public PushIntentService() {
        super("PushIntentService");
    }

    @Override
    public void handleNotification(Bundle extras, String title, String message, String sound, String imageUrl) {
        if (BuildConfig.DEBUG) {
            Log.i(TAG, "Push received: " + extras.toString());
        }

        PendingIntent pendingIntent = null;

        final String parcelCode = extras.getString("parcelCode");
        final String notificationType = extras.getString("notificationType");
        if (notificationType != null && notificationType.equals("rate")) {
            final Intent resultIntent = new Intent(this, ExperienceRatingActivity.class);
            resultIntent.putExtra(ExperienceRatingActivity.EXTRA_PARCEL_CODE, parcelCode);
            resultIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            pendingIntent =
                    PendingIntent.getActivity(
                            this,
                            0,
                            resultIntent,
                            PendingIntent.FLAG_UPDATE_CURRENT
                    );
        } else {
            final Intent resultIntent = new Intent(this, ParcelDetailActivity.class);
            resultIntent.putExtra(ParcelDetailActivity.EXTRA_PARCEL_CODE, parcelCode);
            resultIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            pendingIntent =
                    PendingIntent.getActivity(
                            this,
                            0,
                            resultIntent,
                            PendingIntent.FLAG_UPDATE_CURRENT
                    );
        }

        final NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);

        builder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
        if (title == null || title.equals("")) {
            //;title = getResources().getString(R.string.app_name);
            title = "Your DPD";
        }
        if (message == null) {
            builder.setContentTitle(title);
        } else {
            builder.setContentTitle(title).setContentText(message);
        }
        if (sound != null) {
            builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
        }
        builder.setContentIntent(pendingIntent);
        builder.setSmallIcon(R.drawable.ic_notification_small);
        builder.setAutoCancel(true);
        builder.setLocalOnly(true);
        // ID unique for 68 years
        int id = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);

        notificationManager.notify(id, builder.build());
        sendNotificationToWearable(id, title, message, parcelCode);
    }


    public void sendNotificationToWearable(final int id, final String title, final String message, final String parcelCode) {
        final WearableUtil wearableUtil = new WearableUtil(getApplicationContext());
        wearableUtil.setWearableConnectionListener(new WearableUtil.WearableConnectListener() {
            @Override
            public void connectionSuccess() {
                Log.d(TAG, "Send notification to wearable with parcel code : " + parcelCode);
                wearableUtil.sendNotificationToWearable(id, title, message, parcelCode);
            }

            @Override
            public void connectionFailed() {
                Log.e(TAG, "Wearable connection failed");
            }
        });

        wearableUtil.startConnection();
    }
}
