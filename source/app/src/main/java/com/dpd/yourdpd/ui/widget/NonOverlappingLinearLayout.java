package com.dpd.yourdpd.ui.widget;

import android.content.Context;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.AttributeSet;

/**
 * LinearLayout which returns false for {@link #hasOverlappingRendering}.
 * This prevents the GPU from creating too large textures during Transitions when the LinearLayout
 * contains too many items (e.g. taller than 4096px on a Nexus 5 would crash during the Transition
 * due to it exceeding the GPU texture size limit)
 */
public class NonOverlappingLinearLayout extends LinearLayoutCompat {
    public NonOverlappingLinearLayout(Context context) {
        this(context, null);
    }
    public NonOverlappingLinearLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }
    public NonOverlappingLinearLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean hasOverlappingRendering() {
        return false;
    }
}
