package com.dpd.yourdpd.model;

/**
 * @see ParcelAction
 */
public enum ParcelActionCodes {
    COLLECT_FROM_DEPOT("TBC"),
    LEAVE_IN_SAFE_PLACE("SFP"),
    DELIVER_TO_NEIGHBOUR("DTN"),
    CHANGE_DELIVERY_DATE("RED"),
    UPGRADE_DELIVERY("UPG"),
    COLLECT_FROM_PICKUP_SHOP("PKU");

    private String value;

    ParcelActionCodes(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
