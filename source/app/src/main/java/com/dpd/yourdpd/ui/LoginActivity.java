package com.dpd.yourdpd.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.dpd.yourdpd.BuildConfig;
import com.dpd.yourdpd.R;
import com.dpd.yourdpd.net.response.LoginResponse;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Performs loginAnonymously() or loginWithAuditUser() api calls depending on user state.
 * <p/>
 * NOTE: on launching the app we login anonymously to gain a dpdsession for use in API calls,
 * upon authenticating via pin we login again, this time passing consumerId (aka "auditId")
 * to the login service to authenticate the ongoing session.
 */
public class LoginActivity extends BaseActivity {

    private static final String TAG = LoginActivity.class.getSimpleName();

    private View progressBar;
    private View errorContainer;
    private TextView errorTextView;
    private CompositeSubscription subscriptions;
    private LinearLayout linear_signing;
    private Boolean is_sign_in;
    public final static String IS_SIGN_IN = "signing";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_startup_checks);

        progressBar = findViewById(R.id.progressBar);
        errorContainer = findViewById(R.id.error_container);
        errorTextView = (TextView) errorContainer.findViewById(R.id.error_textview);
        linear_signing = (LinearLayout) findViewById(R.id.linear_signing);
        final Button retryButton = (Button) errorContainer.findViewById(R.id.retry_button);
        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                retryLogin();
            }
        });

        subscriptions = new CompositeSubscription();
        is_sign_in = getIntent().getBooleanExtra(IS_SIGN_IN, false);
        if (is_sign_in)
            linear_signing.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        retryLogin();
    }

    @Override
    protected void onPause() {
        super.onPause();
        subscriptions.unsubscribe();
    }

    private void retryLogin() {
        login();
    }

    private void login() {
        // log the user in if necessary, else skip to the startup checks
        if (user.getDpdSession() == null || !user.hasAuthenticatedSession()) {
            if (user.getConsumerId() != null) {
                // attempt to re-authenticate with existing consumerId
                loginWithAuditUser();
            } else {
                // start a new session
                loginAnonymously();
            }
        } else {
            // user already logged in, run startup checks
            launchStartupChecksActivity();
        }
    }

    private void loginAnonymously() {
        Log.i(TAG, "Logging in anonymously...");

        showProgressBar();
        subscriptions.add(
                api.login(BuildConfig.AUTH_HEADER)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                new Action1<LoginResponse>() {
                                    @Override
                                    public void call(LoginResponse response) {
                                        hideProgressBar();
                                        if (response.hasErrors()) {
                                            Log.e(TAG, "login error: " + response.printErrors());
                                            if (BuildConfig.LOG_ERRORS_TO_SERVER && !BuildConfig.DEBUG) {
                                                Crashlytics.log(response.printErrors());
                                                Crashlytics.logException(new Exception("LoginError"));
                                            }
                                            showError(getString(R.string.problem_logging_in));
                                        } else {
                                            onLoginComplete(response.getDpdSession(), false);
                                        }
                                    }
                                },
                                new Action1<Throwable>() {
                                    @Override
                                    public void call(Throwable throwable) {
                                        hideProgressBar();
                                        Log.e(TAG, "login error", throwable);
                                        if (BuildConfig.LOG_ERRORS_TO_SERVER && !BuildConfig.DEBUG) {
                                            Crashlytics.logException(throwable);
                                        }
                                        showError(getString(R.string.problem_logging_in));
                                    }
                                })
        );

    }

    private void loginWithAuditUser() {
        Log.i(TAG, "Logging in with auditUser...");

        showProgressBar();
        subscriptions.add(
                api.loginWithAuditUser(user.getConsumerId(), BuildConfig.AUTH_HEADER)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                new Action1<LoginResponse>() {
                                    @Override
                                    public void call(LoginResponse response) {
                                        hideProgressBar();
                                        if (response.hasErrors()) {
                                            Log.e(TAG, "login error: " + response.printErrors());
                                            if (BuildConfig.LOG_ERRORS_TO_SERVER && !BuildConfig.DEBUG) {
                                                Crashlytics.log(response.printErrors());
                                                Crashlytics.logException(new Exception("LoginAuditError"));
                                            }
                                            showError(getString(R.string.problem_logging_in));
                                        } else {
                                            onLoginComplete(response.getDpdSession(), true);
                                        }
                                    }
                                },
                                new Action1<Throwable>() {
                                    @Override
                                    public void call(Throwable throwable) {
                                        hideProgressBar();
                                        Log.e(TAG, "login error", throwable);
                                        if (BuildConfig.LOG_ERRORS_TO_SERVER && !BuildConfig.DEBUG) {
                                            Crashlytics.logException(throwable);
                                        }
                                        showError(getString(R.string.problem_logging_in));
                                    }
                                })
        );
    }

    private void onLoginComplete(String dpdSession, boolean isAuditUser) {
        analyticsManager.setUserId(user.getConsumerId());

        user.setDpdSession(dpdSession);
        user.setHasLoggedInWithAuditUser(isAuditUser);
        user.save();

        if (user.getDpdSession() != null) {
            Log.i(TAG, "login complete with dpdsession: " + user.getDpdSession());
            launchStartupChecksActivity();
        } else {
            Log.e(TAG, "login returned no session data");
            showErrorDialog(getString(R.string.problem_logging_in));
        }
    }

    private void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    private void showProgressBar() {
        hideError();
        progressBar.setVisibility(View.VISIBLE);
    }

    private void showError(String error) {
        hideProgressBar();
        errorTextView.setText(error);
        errorContainer.setVisibility(View.VISIBLE);
    }

    private void hideError() {
        errorContainer.setVisibility(View.GONE);
    }

    private void launchStartupChecksActivity() {
        final Intent intent = new Intent(this, StartupChecksActivity.class);
        intent.putExtra(IS_SIGN_IN, is_sign_in);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    public void onUserLoggedOut() {
        // no-op base method to prevent infinite redirect to this very Activity
    }
}
