package com.dpd.yourdpd.ui.address;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ScrollView;

import com.dpd.yourdpd.R;
import com.dpd.yourdpd.model.ConsumerProfile;
import com.dpd.yourdpd.model.ConsumerProperty;
import com.dpd.yourdpd.model.ConsumerPropertyAddressInfo;
import com.dpd.yourdpd.model.UDPRNAddress;
import com.dpd.yourdpd.net.response.PutConsumerResponse;
import com.dpd.yourdpd.ui.BaseMenuActivity;
import com.dpd.yourdpd.ui.Validating;
import com.dpd.yourdpd.ui.widget.StepsView;
import com.dpd.yourdpd.util.CommonUtils;

import rx.functions.Action1;

import static com.dpd.yourdpd.ui.address.AddAddressStepFragmentBase.AddAddressStepFragmentListener;

/**
 * Allows the user to add a {@link com.dpd.yourdpd.model.ConsumerProperty} to their
 * {@link com.dpd.yourdpd.model.ConsumerProfile}
 */
public class AddAddressActivity extends BaseMenuActivity
        implements AddAddressStepFragmentListener {

    private static final String TAG = AddAddressActivity.class.getSimpleName();

    private static final int TOTAL_STEPS = Step.values().length;
    private ConsumerProfile originalProfile;
    private InputMethodManager inputMethodManager;
    private UDPRNAddress homeAddress;

    private enum Step {
        BASIC_ADDRESS_DETAILS,
        SAFE_PLACE,
        NEIGHBOURS,
        SHOPS,
        FIRST_ATTEMPT_PREFS;

        Step() {
        }

        public Step next() {
            return fromValue(ordinal() + 1);
        }

        static Step fromValue(int i) {
            for (Step step : Step.values()) {
                if (i == step.ordinal()) return step;
            }
            return null;
        }
    }

    private Step currentStep;
    private ScrollView scrollView;
    private StepsView stepsView;
    private Button nextButton;
    private Fragment currentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_address);

        // set a new ConsumerProperty to be added
        user.selectedConsumerProperty = new ConsumerProperty();

        scrollView = (ScrollView) findViewById(R.id.scrollView);
        stepsView = (StepsView) findViewById(R.id.steps);
        nextButton = (Button) findViewById(R.id.next_button);
        nextButton.setOnClickListener(onClickNext);

        originalProfile = new ConsumerProfile(user.getConsumerProfile());
        addSteps();
        setCurrentStep(Step.BASIC_ADDRESS_DETAILS);

        CommonUtils.setupUIForKeyboard(this, findViewById(android.R.id.content));
    }

    @Override
    protected void onResume() {
        super.onResume();
        setupToolbar(getTitle().toString(), true);
        getSupportFragmentManager().addOnBackStackChangedListener(onBackStackChangedListener);
    }

    @Override
    public void onPause() {
        super.onPause();
        getSupportFragmentManager().removeOnBackStackChangedListener(onBackStackChangedListener);
    }

    @Override
    public void onBackPressed() {
        // AddNewAddressStepsFragment uses a child FragmentManager,
        // propagate back key presses down to the child FragmentManager first
        final Fragment addStepsFragment = getSupportFragmentManager().findFragmentByTag(AddAddressBasicDetailsFragment.class.getSimpleName());
        if (addStepsFragment != null && addStepsFragment.isVisible()) {
            final FragmentManager childFragmentManager = addStepsFragment.getChildFragmentManager();
            if (childFragmentManager.getBackStackEntryCount() == 0) {
                super.onBackPressed();
            } else {
                childFragmentManager.popBackStackImmediate();
            }
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // pass Activity result on to child Fragment
        if (currentFragment != null) {
            currentFragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void addSteps() {
        stepsView.addStep(new StepsView.Step("1", getString(R.string.address)));
        stepsView.addStep(new StepsView.Step("2", getString(R.string.safe_place)));
        stepsView.addStep(new StepsView.Step("3", getString(R.string.neighbours)));
        stepsView.addStep(new StepsView.Step("4", getString(R.string.shops)));
    }

    private void moveBackToStep(Step step) {
        currentStep = step;
        stepsView.setActiveStep(step.ordinal());
        scrollToTop();
    }

    private void setCurrentStep(Step step) {
        stepsView.setActiveStep(step.ordinal());
        currentStep = step;

        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//        transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right);

        final String tag = String.valueOf(step);
        boolean addToBackStack = step.ordinal() > 0;
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);
        if (fragment == null) {
            switch (step) {
                case BASIC_ADDRESS_DETAILS:
                    fragment = AddAddressBasicDetailsFragment.newInstance(getString(R.string.add_new_address), false);
                    break;
                case SAFE_PLACE:
                    fragment = AddAddressSafePlaceFragment.newInstance();
                    break;
                case NEIGHBOURS:
                    fragment = AddAddressNeighboursFragment.newInstance();
                    break;
                case SHOPS:
                    fragment = AddAddressPickupShopFragment.newInstance();
                    break;
                case FIRST_ATTEMPT_PREFS:
                    fragment = AddFirstAttemptPreferenceFragment.newInstance();
                    break;
            }
            transaction.add(R.id.fragment_container, fragment, tag);
        } else {
            transaction.show(fragment);
        }
        if (currentFragment != null) {
            transaction.hide(currentFragment);
        }

        if (addToBackStack) {
            transaction.addToBackStack(null);
        }
        transaction.commit();

        currentFragment = fragment;
        scrollToTop();
    }

    private View.OnClickListener onClickNext = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // if we are showing a Fragment that needs validation, do that before moving on
            final boolean isValidatingFragment = (currentFragment instanceof Validating);
            final Validating validatingFragment = isValidatingFragment ? ((Validating) currentFragment) : null;

            if (!isValidatingFragment || validatingFragment.validate()) {
                assert validatingFragment != null;
                validatingFragment.applyChanges();

                if (currentStep.equals(Step.SHOPS) && !skipProgress) {
                    if (user.selectedConsumerProperty.pickupLocationCode == null) {
                        putConsumerProfile();
                    } else {
                        setCurrentStep(currentStep.next());
                    }
                } else if (!currentStep.equals(Step.FIRST_ATTEMPT_PREFS) && !skipProgress) {
                    setCurrentStep(currentStep.next());
                } else {
                    // all steps done, add new ConsumerProperty and PUT to server
                    putConsumerProfile();
                }
            }
            closeKeyboard();
        }
    };

    private void closeKeyboard() {
        inputMethodManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (this.getCurrentFocus() != null)
            inputMethodManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
    }

    private FragmentManager.OnBackStackChangedListener onBackStackChangedListener =
            new FragmentManager.OnBackStackChangedListener() {
                @Override
                public void onBackStackChanged() {
                    // handle back button presses
                    final FragmentManager fragmentManager = getSupportFragmentManager();
                    final int step = fragmentManager.getBackStackEntryCount();
                    final boolean isLastStep = step == TOTAL_STEPS - 1;
                    if (step < currentStep.ordinal()) { // moved before current step
                        final String tag = Step.fromValue(step).name();
                        currentFragment = fragmentManager.findFragmentByTag(tag);
                        moveBackToStep(Step.fromValue(step));
                    }
                    nextButton.setText(step == TOTAL_STEPS - 1 ? getString(R.string.done) : getString(R.string.next));
                    stepsView.setVisibility(isLastStep ? View.GONE : View.VISIBLE);
                }
            };

    /**
     * Save ConsumerProfile to server and continue to Your Deliveries
     */
    private void putConsumerProfile() {
        showProgressDialog(getString(R.string.please_wait_));
        user.putConsumerProfile(api, false)
                .subscribe(
                        new Action1<PutConsumerResponse>() {
                            @Override
                            public void call(PutConsumerResponse response) {
                                hideProgressDialog();
                                if (response.hasErrors()) {
                                    Log.e(TAG, "Error updating Consumer: " + response.printErrors());
                                } else {
                                    Log.e(TAG, "Consumer put success.");
                                    onPutConsumerProfileComplete();
                                }
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                hideProgressDialog();
                                Log.e(TAG, "Error putting Consumer", throwable);
                                showErrorDialogWithRetryAndCancel(
                                        getString(R.string.problem_creating_profile_retry),
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                putConsumerProfile();
                                            }
                                        });
                            }
                        });
    }

    public void setHomeAddress(UDPRNAddress address) {
        this.homeAddress = address;
    }

    private void onPutConsumerProfileComplete() {
        user.getConsumerProfile().getConsumerProperties().add(user.selectedConsumerProperty);

        // add new ConsumerPropertyAddressInfo to allow Settings to immediately display address
        // prior to update/sync
        // TODO: I'm not keen on having ConsumerPropertyAddressInfo at all, look at an alternative way to immediately allow "address" details to be looked up without this
        final ConsumerProfile consumerProfile = user.getConsumerProfile();
        final ConsumerPropertyAddressInfo addressInfo = new ConsumerPropertyAddressInfo();
        addressInfo.consumerProperty = user.selectedConsumerProperty;
        addressInfo.addressFirstLine = homeAddress.getFirstLine();
        addressInfo.addressFirstLineWithTown = homeAddress.getFirstLineWithTown();
        consumerProfile.getConsumerPropertyAddressInfos().add(addressInfo);

        // ConsumerProfile saved on server
        finish();
    }

    @Override
    public void setTitle(CharSequence title) {
        this.setupToolbar(title.toString(), true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (Step.BASIC_ADDRESS_DETAILS.ordinal() == currentStep.ordinal()) {
            user.setConsumerProfile(originalProfile);
            user.save();
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * {@link AddAddressStepFragmentListener} methods
     */

    public void enableNextButton() {
        nextButton.setEnabled(true);
    }

    public void disableNextButton() {
        nextButton.setEnabled(false);
    }

    public boolean isNextButtonEnabled() {
        return nextButton.isEnabled();
    }

    public void scrollToTop() {
        scrollView.scrollTo(0, 0);
    }
}