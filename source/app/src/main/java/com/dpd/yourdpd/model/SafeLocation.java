package com.dpd.yourdpd.model;

public class SafeLocation {
    private String safeLocationId;
    /**
     * Mobile Startup Checks returns safeLocationCode in place of safeLocationId but it's the same value
     */
    private String safeLocationCode;

    private String safeLocationDescription; // aka callingCardLocation

    public String getSafeLocationId() {
        return (safeLocationId==null) ? safeLocationCode : safeLocationId;
    }

    public String getSafeLocationDescription() {
        return safeLocationDescription;
    }
}
