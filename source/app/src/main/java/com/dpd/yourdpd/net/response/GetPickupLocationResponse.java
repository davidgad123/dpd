package com.dpd.yourdpd.net.response;

import com.dpd.yourdpd.model.PickupLocation;
import com.google.gson.annotations.SerializedName;

/**
 * @see com.dpd.yourdpd.net.Api#getPickupLocation(String)
 */
public class GetPickupLocationResponse extends BaseResponse {

    @SerializedName("data")
    public PickupLocationWrapper pickupLocationWrapper;

    public PickupLocation getPickupLocation() {
        return pickupLocationWrapper==null ? null : pickupLocationWrapper.pickupLocation;
    }

    private class PickupLocationWrapper {
        PickupLocation pickupLocation;
    }
}
