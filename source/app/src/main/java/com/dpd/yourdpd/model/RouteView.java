package com.dpd.yourdpd.model;

import android.content.Context;

import com.dpd.yourdpd.R;

/**
 * @see com.dpd.yourdpd.net.response.GetRouteViewOnRouteResponse
 */
public class RouteView {
    public String driverName;
    public String driverPhotoId;
    public int completedCollectionStops;
    public int completedDeliveryStops;
    public float latitude;
    public float longitude;

    public String getDriverName(Context context) {
        return driverName == null ? context.getString(R.string.your_driver) : driverName;
    }
}
