package com.dpd.yourdpd.ui.settings;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.MenuItem;

import com.dpd.yourdpd.R;
import com.dpd.yourdpd.net.response.PutConsumerResponse;
import com.dpd.yourdpd.ui.BaseFragment;
import com.dpd.yourdpd.ui.BaseMenuActivity;

import rx.functions.Action1;

public class SettingsActivity extends BaseMenuActivity {
    private static final String TAG = SettingsActivity.class.getSimpleName();

    public static final String EDIT_PROFILE_ONLY = "edit_profile_only";
    private static final String FRAGMENT_SETTINGS = "settings";

    private boolean isProfileOnly;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_with_menu_fragment);

        setupToolbar(getTitle().toString(), false);
        setSelectedMenuButton(R.id.settings_button);
        getSupportFragmentManager().addOnBackStackChangedListener(onBackStackChangedListener);

        isProfileOnly = getIntent().getBooleanExtra(EDIT_PROFILE_ONLY, false);
        if (isProfileOnly) {
            showFragment(SettingsYourProfileActivityFragment.newInstance());
        } else {
            showFragment(SettingsActivityFragment.newInstance());
            updateConsumerProfile();
        }
    }

    private void showFragment(Fragment fragment) {
        final FragmentManager fragmentManager = getSupportFragmentManager();
        final FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.container, fragment, FRAGMENT_SETTINGS);
        transaction.commit();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            BaseFragment fragment = (BaseFragment) getSupportFragmentManager().findFragmentByTag(SettingsYourProfileActivityFragment.class.getSimpleName());
            if (fragment != null) {
                if (fragment.validate()) {
                    fragment.applyChanges();
                    getSupportFragmentManager().popBackStack();
                }
            } else {
                getSupportFragmentManager().popBackStack();
            }

        } else {
            if (isProfileOnly) {
                BaseFragment fragment = (BaseFragment) getSupportFragmentManager().findFragmentByTag(FRAGMENT_SETTINGS);
                if (fragment.validate()) {
                    fragment.applyChanges();
                    if (user.hasConsumerProfileChanged())
                        saveAndPutConsumerProfile(true);
                    else
                        super.onBackPressed();
                }
            } else
                super.onBackPressed();
        }
    }

    public void showBeforeFragment() {
        if (isProfileOnly)
            super.onBackPressed();
        else
            getSupportFragmentManager().popBackStack();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getSupportFragmentManager().removeOnBackStackChangedListener(onBackStackChangedListener);
    }

    @Override
    protected void onResume() {
        super.onResume();
        user.addListener(this);
        syncConsumerProfileChanges();
    }

    @Override
    protected void onPause() {
        super.onPause();
        user.removeListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (isProfileOnly && item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private FragmentManager.OnBackStackChangedListener onBackStackChangedListener =
            new FragmentManager.OnBackStackChangedListener() {
                @Override
                public void onBackStackChanged() {
                    // if we have come back to the Settings home screen, sync ConsumerProfile
                    // changes
                    if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
                        syncConsumerProfileChanges();
                    }
                }
            };

    /**
     * If ConsumerProfile has been changed locally, put changes *to* the server, otherwise refresh
     * ConsumerProfile *from* server
     */
    private void syncConsumerProfileChanges() {
        if (user.hasConsumerProfileChanged()) {
            saveAndPutConsumerProfile(false);
        }
    }

    private void saveAndPutConsumerProfile(final boolean shouldBackAfterSave) {
        showProgressDialog(getString(R.string.saving_changes_));
        user.save();
        user.putConsumerProfile(api, false) // false, do not omit photograph from json
                .subscribe(
                        new Action1<PutConsumerResponse>() {
                            @Override
                            public void call(PutConsumerResponse response) {
                                hideProgressDialog();
                                if (response.hasErrors()) {
                                    Log.e(TAG, "Problem updating ConsumerProfile: " + response.printErrors());
                                    showProblemSavingChangesDialog(shouldBackAfterSave);
                                } else {
                                    if (shouldBackAfterSave) SettingsActivity.this.onBackPressed();
                                }
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                Log.e(TAG, "Problem updating ConsumerProfile", throwable);
                                hideProgressDialog();
                                showProblemSavingChangesDialog(shouldBackAfterSave);
                            }
                        });
    }

    private void updateConsumerProfile() {
        //showProgressDialog(getString(R.string.please_wait_));
        user.updateConsumerProfile(api, this);
    }

    @Override
    public void onConsumerProfileUpdated(boolean success) {
        Log.e(TAG, "HIDE Progress Bar");
        hideProgressDialog();
        super.onConsumerProfileUpdated(success);
    }

    private void showProblemSavingChangesDialog(final boolean shouldBackAfterSave) {
        if (!isFinishing()) {
            showErrorDialogWithRetryAndCancel(getString(R.string.problem_saving_changes_retry),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            // allow user to try again
                            saveAndPutConsumerProfile(shouldBackAfterSave);
                        }
                    });
        }
    }
}
