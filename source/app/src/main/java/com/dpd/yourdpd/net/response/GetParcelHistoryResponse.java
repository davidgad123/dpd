package com.dpd.yourdpd.net.response;

import com.dpd.yourdpd.model.ParcelHistory;
import com.google.gson.annotations.SerializedName;

/**
 * @see com.dpd.yourdpd.net.Api#getParcelEvents(String)
 */
public class GetParcelHistoryResponse extends BaseResponse {
    @SerializedName("data")
    private ParcelHistory parcelHistory;

    public ParcelHistory getParcelHistory() {
        return parcelHistory;
    }
}
