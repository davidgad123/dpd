package com.dpd.yourdpd.ui.yourdeliveries;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.app.AlertDialog;
import android.transitions.everywhere.AutoTransition;
import android.transitions.everywhere.TransitionManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dpd.yourdpd.R;
import com.dpd.yourdpd.model.AddressPoint;
import com.dpd.yourdpd.model.Parcel;
import com.dpd.yourdpd.model.PickupLocation;
import com.dpd.yourdpd.model.PickupLocationResult;
import com.dpd.yourdpd.net.request.CollectFromPickupLocationParams;
import com.dpd.yourdpd.net.response.CollectFromPickupLocationResponse;
import com.dpd.yourdpd.net.response.GetParcelPickupLocationsResponse;
import com.dpd.yourdpd.ui.BaseFragment;
import com.dpd.yourdpd.ui.BaseMenuActivity;
import com.dpd.yourdpd.ui.GoogleApiClientActivity;
import com.dpd.yourdpd.ui.widget.CheckableImageView;
import com.dpd.yourdpd.util.CommonUtils;
import com.dpd.yourdpd.util.DateUtils;
import com.dpd.yourdpd.util.GMapsUtils;
import com.dpd.yourdpd.util.MapMarkerUtils;
import com.dpd.yourdpd.util.UIUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

import static com.dpd.yourdpd.model.PickupLocation.ParcelLocationAvailability;
import static com.dpd.yourdpd.model.PickupLocation.PickupLocationOpenWindow;


/**
 * NOTE: Currently although we are obtaining the user's last known location we are not using this
 * to calculate distances to the various pickup locations, instead the distances come from the API
 * server. We may use actual location to improve accuracy in future. TBD with DPD.
 * <p/>
 * Activities that contain this fragment must implement the
 * {@link InFlightCollectFromPickupShopFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link InFlightCollectFromPickupShopFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class InFlightCollectFromPickupShopFragment extends BaseFragment {

    private static final String TAG = InFlightCollectFromPickupShopFragment.class.getSimpleName();

    // the fragment initialization parameters
    private static final String ARG_PARCEL_CODE = "delivery";

    private static final int PAGE_SIZE = 10;
    private static final int NOT_INCLUDE = 100;

    private OnFragmentInteractionListener listener;

    private Parcel parcel;
    private Date pickedUpDate;

    private List<PickupLocationResult> pickupLocationResults;
    private PickupLocationResult selectedLocationResult;
    private View noShopsMessage;
    private View loadMoreButton;
    private View parcelStatusView;
    private LinearLayout shopsContainer;
    private ArrayList<Object> pickupLocationViews;
    private ContentLoadingProgressBar progressBar;
    private View mapContainer;
    private SupportMapFragment mapFragment;
    private boolean isMapReady;
    private Location lastKnownLocation; // TODO: check if we actually need the user's physical location here any more

    private Subscription getParcelPickupLocationsSubscription;
    private Subscription collectFromPickupLocationSubscription;
    private int page = 1;

    private Date pickupDate;
    private String pickupString = "";

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param parcelCode Parcel.parcelCode
     * @return A new instance of fragment InFlightCollectFromPickupShopFragment.
     */
    public static InFlightCollectFromPickupShopFragment newInstance(String parcelCode) {
        final InFlightCollectFromPickupShopFragment fragment = new InFlightCollectFromPickupShopFragment();
        final Bundle args = new Bundle();
        args.putString(ARG_PARCEL_CODE, parcelCode);
        fragment.setArguments(args);
        return fragment;
    }

    public InFlightCollectFromPickupShopFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            String parcelCode = getArguments().getString(ARG_PARCEL_CODE);
            parcel = dataStore.getParcelWithParcelCode(parcelCode);
        }

        pickupLocationResults = new ArrayList<>();
        pickupLocationViews = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_inflight_collect_from_pickup_shop, container, false);
        parcelStatusView = root.findViewById(R.id.parcel_status);
        mapContainer = root.findViewById(R.id.map_container);
        noShopsMessage = root.findViewById(R.id.no_shops_message);
        loadMoreButton = root.findViewById(R.id.load_more_shops_button);
        loadMoreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadMoreShops();
            }
        });
        shopsContainer = (LinearLayout) root.findViewById(R.id.shops_container);
        progressBar = (ContentLoadingProgressBar) root.findViewById(R.id.progressBar);
        root.findViewById(R.id.confirm_changes_button).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        confirmChanges();
                    }
                }
        );
        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getLastKnownLocation();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (OnFragmentInteractionListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ((BaseMenuActivity) getActivity()).setupToolbar(getString(R.string.collect_from_pickup_shop), true);
        initParcelStatusView();

        // YD-785: Only load shops at first.
        if (pickupLocationResults != null && pickupLocationResults.size() > 0)
            initMapView();
        else
            getPickupLocations();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (getParcelPickupLocationsSubscription != null && !getParcelPickupLocationsSubscription.isUnsubscribed()) {
            getParcelPickupLocationsSubscription.unsubscribe();
            getParcelPickupLocationsSubscription = null;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;

        final GoogleApiClientActivity activity = (GoogleApiClientActivity) getActivity();
        final GoogleApiClient googleApiClient = activity.getGoogleApiClient();
        googleApiClient.unregisterConnectionCallbacks(googleApiClientConnectionCallbacks);
        googleApiClient.unregisterConnectionFailedListener(googleApiClientConnectionFailedCallbacks);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (collectFromPickupLocationSubscription != null && !collectFromPickupLocationSubscription.isUnsubscribed()) {
            collectFromPickupLocationSubscription.unsubscribe();
            collectFromPickupLocationSubscription = null;
        }
    }

    private void getPickupLocations() {
        progressBar.setVisibility(View.VISIBLE);

        final int maxDist = 30;
        getParcelPickupLocationsSubscription =
                api.getParcelPickupLocations(parcel.parcelCode, page, PAGE_SIZE, maxDist)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Action1<GetParcelPickupLocationsResponse>() {
                                       @Override
                                       public void call(GetParcelPickupLocationsResponse response) {
                                           Log.i(TAG, response.toString());
                                           addPickupLocationResults(response.getPickupLocationResults());
                                       }
                                   },
                                new Action1<Throwable>() {
                                    @Override
                                    public void call(Throwable throwable) {
                                        Log.e(TAG, "Error loading pickup locations", throwable);
                                    }
                                });
    }

    private void collectFromPickupLocation(PickupLocation location, Date pickupDate) {
        final CollectFromPickupLocationParams params = new CollectFromPickupLocationParams.Builder()
                .setParcelCode(parcel.parcelCode)
                .setEmail(user.getConsumerProfile().email)
                .setMobile(CommonUtils.addPrefixNumber(user.getConsumerProfile().mobile))
                .setPickupDate(pickupDate)
                .setContactName(user.getFullName())
                .setPickupLocationCode(location.pickupLocationCode)
                .build();
        collectFromPickupLocationSubscription = api.collectFromPickupLocation(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<CollectFromPickupLocationResponse>() {
                               @Override
                               public void call(CollectFromPickupLocationResponse response) {
                                   if (response.hasErrors()) {
                                       Log.e(TAG, String.format("Get depot error %s", response.printErrors()));
                                       showErrorAlertDialog(getString(R.string.parcel_status_not_changed), getString(R.string.error_occurred));
                                       return;
                                   }
                                   showActionConfirmed();
                               }
                           },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                Log.e(TAG, "Problem actioning collect from pickup", throwable);
                                showErrorAlertDialog(getString(R.string.parcel_status_not_changed), getString(R.string.error_occurred));
                            }
                        });
    }

    private void showActionConfirmed() {
        final String organisation = parcel.deliveryDetails.address.organisation;
        final DateTime dateTime = new DateTime(selectedLocationResult.pickupDate);
        final Date date = dateTime.toDate();
        final Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        String day_of_week = cal.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.UK);

        final String dateSuffix = DateUtils.getDayOfMonthSuffix(cal.get(Calendar.DATE));
        final SimpleDateFormat dateFormat = new SimpleDateFormat(String.format("d'%s' MMMM", dateSuffix), Locale.UK);
        final String formattedPickupDate = dateFormat.format(date);

        final AlertDialog.Builder builder =
                new AlertDialog.Builder(getContext(), R.style.AlertDialogTheme);
        builder.setTitle(R.string.parcel_updated);
//        builder.setMessage(UIUtils.formatText(
//                getString(R.string.collection_from_pickup_location_confirmed,
//                        day_of_week,
//                        formattedPickupDate,
//                        "12:00"), // (sic), it's always 12:00
//                getContext()
//        ));
        builder.setMessage(R.string.parcel_collection_pickup_shop_confirmed);
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // all done - return from this Fragment
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
        builder.create().show();
    }

    private void getLastKnownLocation() {
        final GoogleApiClientActivity activity = (GoogleApiClientActivity) getActivity();
        final GoogleApiClient googleApiClient = activity.getGoogleApiClient();

        if (googleApiClient.hasConnectedApi(LocationServices.API)) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            lastKnownLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        } else {
            // wait for GoogleApiClient to connect or fail before continuing
            googleApiClient.registerConnectionCallbacks(googleApiClientConnectionCallbacks);
            googleApiClient.registerConnectionFailedListener(googleApiClientConnectionFailedCallbacks);
        }
    }

    private void initParcelStatusView() {
        final Context context = getContext();
        UIUtils.populateParcelStatus(parcelStatusView, context, parcel);
    }

    private void initMapView() {
        // add MapFragment
        if (mapFragment == null) {
            mapFragment = SupportMapFragment.newInstance();
        }
        if (!mapFragment.isAdded()) {
            final FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.map_container, mapFragment);
            fragmentTransaction.commit();
            mapFragment.getMapAsync(onMapReadyCallback);
        }
    }

    private void addPickupLocationResults(List<PickupLocationResult> results) {
        if (results == null) {
            loadMoreButton.setVisibility(View.GONE);
            return;
        }
        final int previousLocationCount = pickupLocationResults.size();
        for (PickupLocationResult result : results) {
            if (result.distance <= 30.0f)
                addPickupLocationResult(result);
        }
        if (isMapReady) {
            // show new markers
            addMapMarkers(results, previousLocationCount);
        } else {
            initMapView();
        }
        progressBar.hide();

        /*if(results.size()<PAGE_SIZE) {
            loadMoreButton.setVisibility(View.GONE);
        } else {
            loadMoreButton.setVisibility(View.VISIBLE);
        }*/

        if (results.size() == 0) {
            mapContainer.setVisibility(View.GONE);
            noShopsMessage.setVisibility(View.VISIBLE);
            shopsContainer.setVisibility(View.GONE);
            setSelectedLocationResult(null);
        } else {
            mapContainer.setVisibility(View.VISIBLE);
            noShopsMessage.setVisibility(View.GONE);
            shopsContainer.setVisibility(View.VISIBLE);
            setSelectedLocationResult(results.get(0));
        }
    }

    private void loadMoreShops() {
        page++;
        getPickupLocations();
    }

    private View.OnClickListener shopViewOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            final PickupLocationResult result = (PickupLocationResult) v.getTag();
            setSelectedLocationResult(result);
        }
    };

    @SuppressLint("SetTextI18n")
    private void addPickupLocationResult(final PickupLocationResult pickupLocationResult) {
        final PickupLocation location = pickupLocationResult.pickupLocation;
        final int index = pickupLocationViews.size();
        final Context context = getContext();
        final LayoutInflater inflater = LayoutInflater.from(context);
        final ViewGroup view = (ViewGroup) inflater.inflate(R.layout.item_pickup_location, shopsContainer, false);
        final TextView indexTextView = (TextView) view.findViewById(R.id.index_textview);
        final TextView titleTextView = (TextView) view.findViewById(R.id.title_textview);
        final TextView addressTextView = (TextView) view.findViewById(R.id.address_textview);
        final TextView distanceTextView = (TextView) view.findViewById(R.id.distance_textview);
        final TextView availableDateTextView = (TextView) view.findViewById(R.id.available_date_textview);
        final Button openingHoursButton = (Button) view.findViewById(R.id.opening_hours_button);
        // TODO: do we not want to calculate this based on user's current location?
        // discussion was that we could move to using GPS for greater accuracy, but for now keep it
        // the same as the website (i.e. using pickupLocation.distance)
//        final float distance = GMapsUtils.getDistanceFromLocation(locationDetails.latLng, lastKnownLocation);

        final String addressText = location.address.getStreetAndTown();
        final String distanceText = (pickupLocationResult.distance > -1 ? getString(R.string.x_miles_away_single_digit, pickupLocationResult.distance) : "");
        indexTextView.setText(String.valueOf(index + 1));
        titleTextView.setText(location.getName());
        addressTextView.setText(addressText);
        distanceTextView.setText(distanceText);

        // show relevant icons for this location
        view.findViewById(R.id.ic_disabled_access).setVisibility(location.disabledAccess ? View.VISIBLE : View.GONE);
        view.findViewById(R.id.ic_parking).setVisibility(location.parkingAvailable ? View.VISIBLE : View.GONE);
        view.findViewById(R.id.ic_open_late).setVisibility(location.openLate ? View.VISIBLE : View.GONE);
        view.findViewById(R.id.ic_open_saturday).setVisibility(location.openSaturday ? View.VISIBLE : View.GONE);
        view.findViewById(R.id.ic_open_sunday).setVisibility(location.openSunday ? View.VISIBLE : View.GONE);

        // add opening hours
        final ViewGroup openingHoursContainer = (ViewGroup) view.findViewById(R.id.opening_hours_container);
        try {
            final ParcelLocationAvailability availability = location.pickupLocationAvailability;
            final List<PickupLocationOpenWindow> openWindows = availability.pickupLocationOpenWindow;

            final String[] daysOfWeek = getResources().getStringArray(R.array.days_of_week);
            final List<String> days = new ArrayList<>(7);
            final List<String> hours = new ArrayList<>(7);
            int i = 0;
            int prevDay = -1;
            PickupLocationOpenWindow prevWindow = null;

            for (PickupLocationOpenWindow window : openWindows) {
                final int day = window.pickupLocationOpenWindowDay;
                final String slot = window.pickupLocationOpenWindowStartTime + " - " + window.pickupLocationOpenWindowEndTime;
                if (day != prevDay) {
                    // new day
                    final String dayOfWeek = daysOfWeek[day - 1];
                    days.add(dayOfWeek);
                    hours.add(slot);
                    i++;
                } else {
                    // see if window start time matches prev window close time and merge the hours
                    if (prevWindow != null && prevWindow.pickupLocationOpenWindowEndTime.equals(window.pickupLocationOpenWindowStartTime)) {
                        // merge OpenWindows together into a single slot
                        // so 10:00-12:00 and 12:00-22:00 -> 10:00-22:00
                        hours.set(i - 1, prevWindow.pickupLocationOpenWindowStartTime + " - " + window.pickupLocationOpenWindowEndTime);
                    } else {
                        // append second slot to end of hours
                        hours.set(i - 1, hours.get(i - 1) + "/" + slot);
                    }
                }

                prevWindow = window;
                prevDay = day;
            }

            int nextAvailableDayFromToday = 0;
            String nextAvailableDayString = "";
            String pickupDateString = pickupLocationResult.pickupDate;
            String pickupDateDay = pickupDateString.substring(0, 9);

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.UK);
            Date pickupDate = format.parse(pickupDateString);

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(pickupDate);
            String[] day_s = new String[] { "None", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
            nextAvailableDayString = day_s[calendar.get(Calendar.DAY_OF_WEEK)];

//            for (int ind = 1; ind < 7; ind++) {
//                if (days.contains(DateUtils.getWeekDayStringFromToday(ind))) {
//                    nextAvailableDayString = DateUtils.getWeekDayStringFromToday(ind);
//                    //if the available day is weekend then get another available day
//                    //because weekend day is a rest day
//
//                    if (nextAvailableDayString.equals("Sunday") || nextAvailableDayString.equals("Saturday")) {
//                        continue;
//                    }
//                    break;
//                }
//            }

            int k = -1;
            for (int j = 0; j < 7; j++) {
                String dayOfWeek = DateUtils.getDayStringFromMonday(j);
                String openingHours = getString(R.string.closed);
                int indexOfArray = getIndexOfList(dayOfWeek, days);

                if (indexOfArray != NOT_INCLUDE) {
                    openingHours = hours.get(indexOfArray);
                    k++;
                }

                final View row = inflater.inflate(R.layout.item_opening_hour, openingHoursContainer, false);
                final TextView dayTextView = (TextView) row.findViewById(R.id.day_textview);
                final TextView hoursTextView = (TextView) row.findViewById(R.id.hours_textview);

                dayTextView.setText(dayOfWeek);
                hoursTextView.setText(openingHours);
                if (dayOfWeek.equals(nextAvailableDayString)) {
                    nextAvailableDayFromToday = k;
                }
                // insert row before divider
                final int childCount = openingHoursContainer.getChildCount();
                openingHoursContainer.addView(row, childCount - 1, new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT));
            }

            String availableDate = String.format(getString(R.string.available_date_and_time), nextAvailableDayString, UIUtils.getEndTime(hours.get(nextAvailableDayFromToday)));
            availableDateTextView.setText(availableDate);
            availableDateTextView.setVisibility(View.VISIBLE);
            //pickupLocationResult.pickupDate = DateUtils.getDateFromNextWeekDay(nextAvailableDayFromToday);
        } catch (Exception e) {
            Log.e(TAG, "Problem formatting opening times", e);
        }

        openingHoursButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AutoTransition transition = new AutoTransition(getContext(), null);
                TransitionManager.beginDelayedTransition(view, transition);
                openingHoursContainer.setVisibility(openingHoursContainer.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
                openingHoursButton.setText(openingHoursContainer.getVisibility() == View.VISIBLE ? getResources().getString(R.string.hide_opening_hours) : getResources().getString(R.string.show_opening_hours));
            }
        });

        view.setOnClickListener(shopViewOnClickListener);
        view.setTag(pickupLocationResult);

        shopsContainer.addView(view, shopsContainer.getChildCount() - 1);
        pickupLocationViews.add(view);
        pickupLocationResults.add(pickupLocationResult);
    }

    private int getIndexOfList(String dayOfWeek, List<String> days) {
        for (int i = 0; i < days.size(); i++) {
            if (days.get(i).equals(dayOfWeek)) {
                return i;
            }
        }
        return NOT_INCLUDE;
    }

    private void setSelectedLocationResult(PickupLocationResult locationResult) {
        // uncheck previous
        if (selectedLocationResult != null && !selectedLocationResult.equals(locationResult)) {
            final View view = shopsContainer.findViewWithTag(selectedLocationResult);
            final CheckableImageView radioImageView = (CheckableImageView) view.findViewById(R.id.radio);
            radioImageView.setChecked(false);
        }
        if (locationResult != null) {
            selectedLocationResult = locationResult;
            final View view = shopsContainer.findViewWithTag(locationResult);
            final CheckableImageView radioImageView = (CheckableImageView) view.findViewById(R.id.radio);
            radioImageView.setChecked(true);
        }
    }

    private OnMapReadyCallback onMapReadyCallback = new OnMapReadyCallback() {
        @Override
        public void onMapReady(GoogleMap map) {
            isMapReady = true;
            addMapMarkers(pickupLocationResults, 0);
        }
    };

    private void addMapMarkers(List<PickupLocationResult> results, int indexOffset) {
        if (pickupLocationResults == null) {
            return;
        }

        final Context context = getContext();
        final GoogleMap map = mapFragment.getMap();
        final LatLng[] latLngs = new LatLng[results.size() + 1];

        int markerWidth = 0, markerHeight = 0;
        for (int i = 0; i < results.size(); i++) {
            PickupLocationResult result = results.get(i);
            final PickupLocation location = result.pickupLocation;
            final Bitmap markerBitmap =
                    MapMarkerUtils.getBitmapForMarker(context,
                            R.drawable.ic_location_marker,
                            String.valueOf(i + indexOffset + 1),
                            0xffffffff);

            if (markerHeight == 0 && markerBitmap != null) {
                markerWidth = markerBitmap.getWidth();
                markerHeight = markerBitmap.getHeight();
            }

            final AddressPoint addressPoint = location.addressPoint;
            final LatLng latLng = addressPoint.toLatLng();
            map.addMarker(new MarkerOptions()
                            .icon(BitmapDescriptorFactory.fromBitmap(markerBitmap))
                            .position(latLng)
                            .anchor(0.5f, 0.75f)
                            .title(location.getName())
            );
            latLngs[i] = latLng;
        }

        // Add home marker
        if (indexOffset == 0 && parcel.deliveryDetails != null && parcel.deliveryDetails.address.addressPoint != null) {
            try {
                final LatLng houseLatLng = new LatLng(
                        parcel.deliveryDetails.address.addressPoint.latitude,
                        parcel.deliveryDetails.address.addressPoint.longitude);
                map.addMarker(new MarkerOptions()
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin_house))
                        .position(houseLatLng)
                        .alpha(1f)
                        .anchor(0.5f, 1.0f)
                        .title(getString(R.string.your_address)));
                latLngs[latLngs.length - 1] = houseLatLng;
            } catch (Exception ignored) {
            }
        }

        final int markerWidth1 = markerWidth;
        final int markerHeight1 = markerHeight;
        map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                GMapsUtils.zoomWithPadding(mapFragment, latLngs, 20, markerWidth1, markerHeight1);
            }
        });
        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                GMapsUtils.zoomWithPadding(mapFragment, latLngs, 20, markerWidth1, markerHeight1, true);
            }
        });
        map.getUiSettings().setAllGesturesEnabled(false);
    }

    private void confirmChanges() {
        final View view = getView();
        assert view != null;
        final PickupLocationResult result = getSelectedLocationResult();
        if (result == null) {
            return;
        }
        final PickupLocation location = result.pickupLocation;
        if (location == null) {
            return;
        }
        final Date pickupDate = DateUtils.parseISO8601DateString(result.pickupDate);

        final AlertDialog.Builder builder =
                new AlertDialog.Builder(getActivity(), R.style.AlertDialogTheme);
        builder.setTitle(getString(R.string.change_delivery_dialog_title));
        builder.setMessage(getString(R.string.change_delivery_pickup_shop_dialog_message, location.getName()));
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                collectFromPickupLocation(location, pickupDate);
                // TODO: show loader
            }
        });
        builder.setNegativeButton(android.R.string.cancel, null);
        builder.show();
    }

    private GoogleApiClient.ConnectionCallbacks googleApiClientConnectionCallbacks =
            new GoogleApiClient.ConnectionCallbacks() {
                @Override
                public void onConnected(Bundle bundle) {
                    // try getting last known location again
                    getLastKnownLocation();
                }

                @Override
                public void onConnectionSuspended(int i) {
                }
            };
    private GoogleApiClient.OnConnectionFailedListener googleApiClientConnectionFailedCallbacks =
            new GoogleApiClient.OnConnectionFailedListener() {

                @Override
                public void onConnectionFailed(ConnectionResult connectionResult) {
                    // continue without knowing user's location
//                    init();
                }
            };

    private PickupLocationResult getSelectedLocationResult() {
        View view = shopsContainer.findViewWithTag(selectedLocationResult);
        if (view != null) {
            return (PickupLocationResult) view.getTag();
        }
        return null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
//        public void onInFlightPickupFragmentInteraction(Uri uri);
    }
}
