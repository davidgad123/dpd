package com.dpd.yourdpd.net.response;

/**
 * Use internally by other response classes
 */
public class SuccessResponse {
    public boolean success;
}
