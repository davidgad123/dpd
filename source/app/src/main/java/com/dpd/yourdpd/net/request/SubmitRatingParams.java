package com.dpd.yourdpd.net.request;

/**
 * @see com.dpd.yourdpd.net.Api#submitRating(String, SubmitRatingParams)
 */
public class SubmitRatingParams {
    public int experienceRating;
    public String notes;
}
