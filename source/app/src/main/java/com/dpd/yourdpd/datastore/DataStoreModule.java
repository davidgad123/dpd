package com.dpd.yourdpd.datastore;

import android.content.Context;

import com.dpd.yourdpd.PerApplication;

import dagger.Module;
import dagger.Provides;

@Module
public class DataStoreModule {
    private DataStore dataStore;

    public DataStoreModule(Context context) {
        dataStore = new DataStore(context);
    }

    @Provides @PerApplication DataStore provideDataStore() {
        return dataStore;
    }
}
