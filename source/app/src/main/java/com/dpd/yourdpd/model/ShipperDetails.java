package com.dpd.yourdpd.model;

import java.io.Serializable;

/**
 * Created by richardleggett on 01/09/15.
 */
public class ShipperDetails implements Serializable {
    public String organisation;
    public String accountNumber;
    public String customerDisplayName;
    public String customerDisplayShortName;
    public String shippingRef1;
    public String shippingRef2;
    public String shippingRef3;
}
