package com.dpd.yourdpd.net.response;

import com.google.gson.annotations.SerializedName;

/**
 * @see com.dpd.yourdpd.net.Api#getPickupPass(String)
 */
public class GetPickupPassResponse extends BaseResponse {
    @SerializedName("data")
    private ImageData imageData;

    private class ImageData {
        public String image;
    }

    public String getImageBase64() {
        return (imageData==null) ? null : imageData.image;
    }
}
