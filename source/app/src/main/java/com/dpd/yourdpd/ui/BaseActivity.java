package com.dpd.yourdpd.ui;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.UiThread;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.dpd.yourdpd.ActivityComponent;
import com.dpd.yourdpd.ActivityModule;
import com.dpd.yourdpd.BuildConfig;
import com.dpd.yourdpd.DaggerActivityComponent;
import com.dpd.yourdpd.R;
import com.dpd.yourdpd.YourDPDApp;
import com.dpd.yourdpd.analytics.AnalyticsManager;
import com.dpd.yourdpd.datastore.DataStore;
import com.dpd.yourdpd.model.User;
import com.dpd.yourdpd.net.Api;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.localz.spotzpush.sdk.model.response.BaseJsonResponse;
import com.localz.spotzpush.sdk.model.response.DeviceJsonResponse;
import com.localz.spotzpush.sdk.service.SpotzPushService;
import com.localz.spotzpush.sdk.task.BaseDeviceRegisterOrUpdateTask;
import com.localz.spotzpush.sdk.util.Common;
import com.paypal.android.sdk.payments.PayPalConfiguration;

import javax.inject.Inject;

/**
 * Base Activity class that provides dependency injection common through {@link ActivityComponent}
 */
public class BaseActivity extends AppCompatActivity implements User.Listener {
    private String TAG = BaseActivity.class.getSimpleName();

    public static final int PERMISSION_REQUEST_ACCESS_FINE_LOCATION = 2;
    public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    protected ActivityComponent component;

    protected @Inject DataStore dataStore;
    protected @Inject Api api;
    protected @Inject User user;
    protected @Inject PayPalConfiguration paypalConfig;
    protected @Inject AnalyticsManager analyticsManager;

    public boolean skipProgress = false;

    protected ProgressDialog progressDialog;
    protected boolean isRegisteringPushService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // inject dependencies
        component = DaggerActivityComponent.builder()
                .appComponent(YourDPDApp.getAppComponent())
                .activityModule(new ActivityModule())
                .build();
        component.inject(this);

/*
        // SpotzPushService is initialized onCreate of every BaseActivity to ensure we always have
        // an updated deviceId, and that the Service is running to receive push notifications
        initSpotzPushService();
*/
    }

    public void initSpotzPushService() {
        final User thisUser = user;
        if (checkPlayServices()) {
            isRegisteringPushService = true;
            SpotzPushService.init(this,
                    BuildConfig.PLAY_PROJECT_ID,
                    BuildConfig.SPOTZ_PUSH_PROJECT_ID,
                    BuildConfig.SPOTZ_PUSH_PROJECT_KEY,
                    null,
                    BuildConfig.SPOTZ_PRODUCTION_ENV ? Common.ENV_PROD : Common.ENV_DEV,
                    new BaseDeviceRegisterOrUpdateTask.Callback() {
                        @Override
                        public void onCompleted(DeviceJsonResponse deviceJsonResponse) {
                            if (BuildConfig.DEBUG) {
                                Log.i(TAG, "Spotz init complete: " + deviceJsonResponse.deviceId);
                            }
                            onSpotzPushServiceInitCallback(thisUser, true, deviceJsonResponse.deviceId);
                        }

                        @Override
                        public void onError(BaseJsonResponse baseJsonResponse) {
                            Log.e(TAG, "Spotz init error: " + baseJsonResponse);
                            onSpotzPushServiceInitCallback(thisUser, false, null);
                        }
                    });
        } else {
            Log.w(TAG, "Play services not available, push will be unavailable");
        }
    }

    protected void onSpotzPushServiceInitCallback(User user, boolean success, String spotzDeviceId) {
        if (success && user != null) {
            user.setSpotzDeviceId(spotzDeviceId);
            user.save();
        }
        isRegisteringPushService = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        user.addListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        user.removeListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        component = null;
        dataStore = null;
        api = null;
        user = null;
        paypalConfig = null;
        analyticsManager = null;
    }

    @UiThread
    protected AlertDialog showErrorDialog(String message) {
        AlertDialog.Builder builder =
                new AlertDialog.Builder(this, R.style.AlertDialogTheme);
        builder.setMessage(message);
        builder.setPositiveButton(android.R.string.ok, null);
        AlertDialog dialog = builder.create();
        dialog.show();
        return dialog;
    }

    @UiThread
	protected void showErrorAlertDialogWithOk(String message, String title, DialogInterface.OnClickListener callback) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this, R.style.AlertDialogTheme);
        if (title != null) builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(android.R.string.ok, callback);
        builder.setCancelable(false);
        builder.create().show();
    }

    @UiThread
    protected AlertDialog showErrorDialog(String message, String title) {
        AlertDialog.Builder builder =
                new AlertDialog.Builder(this, R.style.AlertDialogTheme);
        builder.setMessage(message);
        builder.setTitle(title);
        builder.setPositiveButton(android.R.string.ok, null);
        AlertDialog dialog = builder.create();
        dialog.show();
        return dialog;
    }

    @UiThread
    protected AlertDialog showErrorDialog(String message, DialogInterface.OnClickListener callback) {
        AlertDialog.Builder builder =
                new AlertDialog.Builder(this, R.style.AlertDialogTheme);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(android.R.string.ok, callback);
        AlertDialog dialog = builder.create();
        dialog.show();
        return dialog;
    }

    @UiThread
    protected void showErrorDialogWithRetryAndCancel(
            String message,
            @NonNull DialogInterface.OnClickListener retryCallback) {
        showErrorDialogWithRetryAndCancel(message, retryCallback, null);
    }

    @UiThread
    protected AlertDialog showErrorDialogWithRetryAndCancel(
            String message,
            @NonNull DialogInterface.OnClickListener retryCallback,
            DialogInterface.OnClickListener cancelCallback) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this, R.style.AlertDialogTheme);
        builder.setMessage(message);
        builder.setPositiveButton(getString(R.string.retry), retryCallback);
        builder.setNegativeButton(android.R.string.cancel, cancelCallback);
        AlertDialog dialog = builder.create();
        dialog.show();
        return dialog;
    }

    protected void showProgressDialog(String message) {
        hideProgressDialog();
        progressDialog = ProgressDialog.show(this, null, message, true, false);
    }

    protected void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    /**
     * Helper method to ensure that Google Play services are available and updated on the device.
     * This is a prerequisite to be able to use push notifications.
     */
    protected boolean checkPlayServices() {
        int result = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);

        if (result != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(result)) {
                GooglePlayServicesUtil.getErrorDialog(result, this, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
            }
            return false;
        }
        return true;
    }

    @Override
    public void onUserLoggedOut() {
//        final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogTheme);
//        builder.setCancelable(false);
//        builder.setTitle(getString(R.string.signed_out_alert_title));
//        builder.setMessage(getString(R.string.signed_out_alert_message));
//        builder.setPositiveButton(android.R.string.ok,
//                new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.dismiss();
                        final Intent intent = new Intent(BaseActivity.this, LoadingActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
//                    }
//                });
//        builder.create().show();
    }

    @Override
    public void onConsumerProfileUpdated(boolean success) {
    }
}
