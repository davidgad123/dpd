package com.dpd.yourdpd;

import com.dpd.yourdpd.analytics.AnalyticsManager;
import com.dpd.yourdpd.datastore.DataStore;
import com.dpd.yourdpd.model.User;
import com.dpd.yourdpd.net.Api;
import com.dpd.yourdpd.ui.BaseFragment;
import com.dpd.yourdpd.ui.yourdeliveries.YourDeliveriesActivityFragment;

import dagger.Component;

/**
 * Module scoped to a Fragment that depends on {@link AppComponent}
 * TODO: we could make this a base class to have different injects for different Fragments
 */
@PerFragment
@Component(dependencies = AppComponent.class, modules = FragmentModule.class)
public interface FragmentComponent {
    Api api();
    DataStore dataStore();
    AnalyticsManager analyticsManager();
    User user();

    void inject(BaseFragment fragment);
}
