package com.dpd.yourdpd.net.response;

import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * @see com.dpd.yourdpd.net.Api#getParcelUpdateDates(String, String)
 */
public class GetParcelUpdateDatesResponse extends BaseResponse {
    @SerializedName("data")
    private List<DiaryDate> diaryDates;

    public List<String> getDiaryDates() {
        final List<String> dates = new ArrayList<>();
        for (int i = 0; i < diaryDates.size(); i++) {
            String dateStr = diaryDates.get(i).date;
            dates.add(dateStr);
        }
        return dates;
    }

    public static class DiaryDate {
        @SerializedName("diaryDate")
        public String date;
    }
}
