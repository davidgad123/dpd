package com.dpd.yourdpd.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v4.widget.ScrollerCompat;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.dpd.yourdpd.R;
import com.dpd.yourdpd.ui.help.HelpActivity;
import com.dpd.yourdpd.ui.settings.SettingsActivity;
import com.dpd.yourdpd.ui.settings.SettingsYourProfileActivityFragment;
import com.dpd.yourdpd.ui.yourdeliveries.YourDeliveriesActivity;

/**
 * Base class for Activities displaying the primary navigation menu at the top
 */
public class BaseMenuActivity extends GoogleApiActivity
        implements View.OnClickListener {

    @SuppressWarnings("unused")
    private static final String TAG = BaseMenuActivity.class.getSimpleName();

    protected boolean isMenuExpanded = false;
    protected int selectedMenuButtonId = -1;
    protected Toolbar toolbar;
    protected boolean showUpIndicator;
    private View mainContainer;
    private GestureDetectorCompat gestureDetector;
    private ScrollerCompat scroller;
    private ValueAnimator scrollAnimator;
    private View touchInterceptor;
    private View menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawable(null);
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);

        if (findViewById(R.id.menu) != null) {
            // listen to nav menu buttons
            findViewById(R.id.your_deliveries_button).setOnClickListener(this);
            findViewById(R.id.settings_button).setOnClickListener(this);
            findViewById(R.id.help_button).setOnClickListener(this);
            menu = findViewById(R.id.menu);
            mainContainer = findViewById(R.id.main_container);
            touchInterceptor = findViewById(R.id.touchInterceptor);
            touchInterceptor.setVisibility(View.GONE);
            gestureDetector =
                    new GestureDetectorCompat(this, new GestureDetector.OnGestureListener() {
                        @Override
                        public boolean onDown(MotionEvent e) {
                            return true;
                        }

                        @Override
                        public void onShowPress(MotionEvent e) {
                        }

                        @Override
                        public boolean onSingleTapUp(MotionEvent e) {
                            closeMenu();
                            return true;
                        }

                        @Override
                        public boolean onScroll(MotionEvent e1, MotionEvent e2,
                                                float distanceX, float distanceY) {
                            int scroll = (int) (mainContainer.getTranslationY() - distanceY);
                            int limit = menu.getHeight();
                            scroll = Math.max(0, Math.min(scroll, limit));
                            mainContainer.setTranslationY(scroll);
                            return false;
                        }

                        @Override
                        public void onLongPress(MotionEvent e) {
                        }

                        @Override
                        public boolean onFling(MotionEvent e1, MotionEvent e2,
                                               float velocityX, float velocityY) {
                            int translationY = (int) mainContainer.getTranslationY();
                            int height = menu.getHeight();
                            final int minY = 0;
                            final int maxY = height;
                            scroller = ScrollerCompat.create(BaseMenuActivity.this);
                            scrollAnimator = ValueAnimator.ofFloat(0, 1);
                            scrollAnimator.addListener(new AnimatorListenerAdapter() {
                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    super.onAnimationEnd(animation);
                                    closeMenu(150);
                                }
                            });
                            scrollAnimator.addUpdateListener(
                                    new ValueAnimator.AnimatorUpdateListener() {
                                        @Override
                                        public void onAnimationUpdate(ValueAnimator valueAnimator) {
                                            if (!scroller.isFinished()) {
                                                scroller.computeScrollOffset();
                                                mainContainer.setTranslationY(scroller.getCurrY());
                                            } else {
                                                scrollAnimator.cancel();
                                                closeMenu(150);
                                            }
                                        }
                                    });
                            scroller.fling(0, translationY,
                                    0, (int) (velocityY),
                                    0, 0,
                                    minY, maxY);
                            scrollAnimator.start();
                            return true;
                        }
                    });
        }
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (selectedMenuButtonId != -1) {
            Button menuButton = (Button) findViewById(selectedMenuButtonId);
            menuButton.setTypeface(Typeface.DEFAULT_BOLD);
        }
    }

    public void setupToolbar(String title, boolean showUpIndicator) {
        this.showUpIndicator = showUpIndicator;
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        ImageView imgSameIntent = (ImageView) toolbar.findViewById(R.id.to_get_sameIndent);
        imgSameIntent.setVisibility(View.GONE);
        TextView txtTitle = (TextView) toolbar.findViewById(R.id.txt_title);
        txtTitle.setText(title);
        toolbar.setTitle(title);
        if (showUpIndicator) {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_grey600_24dp);
            toolbar.setNavigationContentDescription(R.string.back);
        } else {
            toolbar.setNavigationIcon(R.drawable.ic_menu_grey600_24dp);
            toolbar.setNavigationContentDescription(R.string.menu);
        }
        setSupportActionBar(toolbar);
    }

    @Override
    public void setTitle(CharSequence title) {
        //super.setTitle(title);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView txtTitle = (TextView) toolbar.findViewById(R.id.txt_title);
        (toolbar.findViewById(R.id.to_get_sameIndent)).setVisibility(View.INVISIBLE);
        txtTitle.setText(title);
    }

    protected void setSelectedMenuButton(@IdRes int buttonId) {
        selectedMenuButtonId = buttonId;
    }

    protected void toggleShowMenu() {
        if (isMenuExpanded) {
            closeMenu();
        } else {
            openMenu();
        }
    }

    protected void openMenu() {
        final View menu = findViewById(R.id.menu);
        menu.setVisibility(View.VISIBLE);
        menu.getViewTreeObserver().addOnPreDrawListener(
                new ViewTreeObserver.OnPreDrawListener() {
                    @Override
                    public boolean onPreDraw() {
                        int height = menu.getHeight();
                        mainContainer.animate().translationY(height);
                        touchInterceptor.setVisibility(View.VISIBLE);
                        touchInterceptor.setOnTouchListener(new View.OnTouchListener() {
                            @Override
                            public boolean onTouch(View v, MotionEvent event) {
                                if (!gestureDetector.onTouchEvent(event)) {
                                    if (event.getAction() == MotionEvent.ACTION_UP) {
                                        closeMenu();
                                    }
                                }
                                return true;
                            }
                        });
                        FrameLayout.LayoutParams touchInterceptorParams =
                                new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                        touchInterceptorParams.topMargin = height;
                        touchInterceptor.setLayoutParams(touchInterceptorParams);
                        menu.getViewTreeObserver().removeOnPreDrawListener(this);
                        return true;
                    }
                });
        isMenuExpanded = true;
    }

    protected void closeMenu() {
        closeMenu(250);
    }

    protected void closeMenu(int duration) {
        final View menu = findViewById(R.id.menu);
        final ViewPropertyAnimator animator = findViewById(R.id.main_container).animate();
        final Animator.AnimatorListener animatorListener = new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                touchInterceptor.setOnTouchListener(null);
                touchInterceptor.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                menu.setVisibility(View.GONE);
                animator.setListener(null);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                animator.setListener(null);
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        };
        animator.translationY(0).setDuration(duration).setListener(animatorListener);
        isMenuExpanded = false;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a listener activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home: {
                if (showUpIndicator) {
                    if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                        BaseFragment fragment = (BaseFragment) getSupportFragmentManager().findFragmentByTag(SettingsYourProfileActivityFragment.class.getSimpleName());
                        if (fragment != null) {
                            if (fragment.validate()) {
                                fragment.applyChanges();
                                getSupportFragmentManager().popBackStack();
                            }
                        } else {
                            getSupportFragmentManager().popBackStack();
                        }

                    } else {
                        final Activity activity = BaseMenuActivity.this;
                        final Intent upIntent = NavUtils.getParentActivityIntent(activity);
                        try {
                            if (NavUtils.shouldUpRecreateTask(activity, upIntent)) {
                                // This activity is NOT part of this app's task, so create a new task
                                // when navigating up, with a synthesized back stack.
                                TaskStackBuilder.create(activity)
                                        // Add all of this activity's parents to the back stack
                                        .addNextIntentWithParentStack(upIntent)
                                        // Navigate up to the closest listener
                                        .startActivities();
                            } else {
                                // This activity is part of this app's task, so simply
                                // navigate up to the logical listener activity.
                                NavUtils.navigateUpTo(activity, upIntent);
                            }
                        } catch (Exception e) {
                            // no listener Activity specified
                            finish();
                        }

                    }
                } else {
                    toggleShowMenu();
                }
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == selectedMenuButtonId) {
            return;
        }
        switch (v.getId()) {
            case R.id.your_deliveries_button: {
                final Intent intent = new Intent(this, YourDeliveriesActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            }
            case R.id.settings_button: {
                final Intent intent = new Intent(this, SettingsActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            }
            case R.id.help_button: {
                final Intent intent = new Intent(this, HelpActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            }
        }
        closeMenu();
    }
}
