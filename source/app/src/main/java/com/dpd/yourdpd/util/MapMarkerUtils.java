package com.dpd.yourdpd.util;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.text.*;
import android.view.View;

import com.dpd.yourdpd.R;

/**
 * Utility methods for generating Google Map markers
 */
public class MapMarkerUtils {

    /**
     * Returns a bitmap with the given drawable resource image + label
     * @param context Context
     * @param bmpResourceId A drawable resource ID
     * @param label Text label to center on bitmap
     * @param labelColor 32bit label color
     * @return Bitmap compositing bmpResourceId and text label
     */
    public static Bitmap getBitmapForMarker(Context context,
                                            @DrawableRes int bmpResourceId,
                                            String label,
                                            int labelColor) {
        try {
            Resources resources = context.getResources();
            final float scale = resources.getDisplayMetrics().density;
            Bitmap bitmap = BitmapFactory.decodeResource(resources, bmpResourceId);
            Config bitmapConfig = bitmap.getConfig();

            // set default bitmap config if none
            if (bitmapConfig == null)
                bitmapConfig = Config.ARGB_8888;

            bitmap = bitmap.copy(bitmapConfig, true);

            final Canvas canvas = new Canvas(bitmap);
            final Paint textPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
            textPaint.setColor(labelColor);
            textPaint.setTextAlign(Paint.Align.CENTER);
            textPaint.setTextSize((int) (14 * scale));
            textPaint.setTypeface(Typeface.DEFAULT);
            textPaint.setStyle(Paint.Style.FILL);
//            textPaint.setShadowLayer(1f, 0f, 1f, Color.DKGRAY);

            // draw text to the Canvas center
            if(!android.text.TextUtils.isEmpty(label)) {
                final Rect bounds = new Rect();
                textPaint.getTextBounds(label, 0, label.length(), bounds);
                int x = bitmap.getWidth() / 2;// - bounds.width()/2);
                int y = (int) (bounds.height() / 2 + scale * 15f);

    //            canvas.drawText(label, x * scale, y * scale, textPaint);
                canvas.drawText(label, x, y, textPaint);
    //            canvas.drawText("1234", 0, 20, textPaint);
            }
            return bitmap;

        } catch (Exception e) {
            return null;
        }
    }

    public static Bitmap getPinWithImageOverlay(Context context,
                                            Bitmap bitmap) {
        try {
            Resources resources = context.getResources();
            final float scale = resources.getDisplayMetrics().density;
            Config bitmapConfig = bitmap.getConfig();

            BitmapDrawable pinDrawable = (BitmapDrawable) resources.getDrawable(R.drawable.ic_pin);
            Bitmap pinBitmap = pinDrawable.getBitmap();


            Bitmap bmOverlay = Bitmap.createBitmap(
                    pinBitmap.getWidth(),
                    pinBitmap.getHeight(),
                    pinBitmap.getConfig());
            Canvas canvas = new Canvas(bmOverlay);
            canvas.drawBitmap(pinBitmap, new Matrix(), null);
            canvas.drawBitmap(bitmap, 0, 0, null);

            // set default bitmap config if none
//            if(bitmapConfig == null)
//                bitmapConfig = Config.ARGB_8888;
//
//            bitmap = bitmap.copy(bitmapConfig, true);

            return bmOverlay;

        } catch (Exception e) {
            return null;
        }
    }

    public static Bitmap getBitmapForMarker(View view) {
        try {
            Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(),
                    Bitmap.Config.ARGB_8888);
            Canvas c = new Canvas(bitmap);
            view.draw(c);

            Config bitmapConfig = bitmap.getConfig();

            // set default bitmap config if none
            if(bitmapConfig == null)
                bitmapConfig = Config.ARGB_8888;

            bitmap = bitmap.copy(bitmapConfig, true);

            return bitmap;

        } catch (Exception e) {
            return null;
        }
    }
}
