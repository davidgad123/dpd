package com.dpd.yourdpd;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Defines a per-Fragment scope (one that lives as long as the Fragment)
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface PerFragment {
}