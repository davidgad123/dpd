package com.dpd.yourdpd.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.dpd.yourdpd.BuildConfig;
import com.dpd.yourdpd.R;
import com.dpd.yourdpd.model.ParcelHistory;
import com.dpd.yourdpd.net.response.GetConsumerResponse;
import com.dpd.yourdpd.net.response.GetContentResponse;
import com.dpd.yourdpd.net.response.GetParcelHistoryResponse;
import com.dpd.yourdpd.net.response.MobileStartupChecksResponse;
import com.dpd.yourdpd.ui.pin.AuthPinActivity;
import com.dpd.yourdpd.ui.profile.ProfileSetupActivity;
import com.dpd.yourdpd.ui.yourdeliveries.YourDeliveriesActivity;
import com.dpd.yourdpd.util.DebugUtils;
import com.dpd.yourdpd.util.DeviceIdUtil;
import com.util.WearableUtil;

import java.util.List;

import retrofit.RetrofitError;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * App Startup checks follow login and pin auth.
 * Here we ensure we get Consumer updates, and Mobile Content updates.
 */
public class StartupChecksActivity extends BaseActivity {

    private static final String TAG = StartupChecksActivity.class.getSimpleName();
    private static final int MAX_RETRIES = 3;

    private View progressBar;
    private View errorContainer;
    private TextView errorTextView;
    private CompositeSubscription subscriptions;

    private boolean hasContentUpdate = false;
    private boolean hasConsumerUpdate = false;
    private int retryAttempts = 0;
    private boolean isSignIn;
    private Subscription parcelHistorySubscription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_startup_checks);

        progressBar = findViewById(R.id.progressBar);
        errorContainer = findViewById(R.id.error_container);
        errorTextView = (TextView) errorContainer.findViewById(R.id.error_textview);
        final Button retryButton = (Button) errorContainer.findViewById(R.id.retry_button);
        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                retry();
            }
        });

        // persist unique device ID to store in Consumer
        user.getConsumerProfile().deviceId = DeviceIdUtil.getDeviceId();
        user.save();

        isSignIn = getIntent().getBooleanExtra(LoginActivity.IS_SIGN_IN, false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        subscriptions = new CompositeSubscription();
        showProgressBar();
        doMobileStartupChecks();
    }

    @Override
    protected void onPause() {
        super.onPause();

        subscriptions.unsubscribe();
    }

    private void doMobileStartupChecks() {
        Log.i(TAG, "Performing mobile startup checks...");

        final String apiVersion = BuildConfig.API_VERSION;
        final String deviceId = user.getConsumerProfile().deviceId;
        final Subscription startupChecksSubscription =
                api.mobileStartupChecks(apiVersion, deviceId)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Action1<MobileStartupChecksResponse>() {
                                       @Override
                                       public void call(MobileStartupChecksResponse response) {
                                           hideProgressBar();
                                           if (response.hasErrors()) {
                                               Log.e(TAG, "MobileStartupCheck error " + response.printErrors());
                                               if (BuildConfig.LOG_ERRORS_TO_SERVER && !BuildConfig.DEBUG) {
                                                   Crashlytics.log(response.printErrors());
                                                   Crashlytics.logException(new Exception("MobileStartupChecks"));
                                               }
                                               showError(getString(R.string.a_problem_has_occurred));
                                               return;
                                           }
                                           processStartupChecksData(response.data);
                                       }
                                   },
                                new Action1<Throwable>() {
                                    @Override
                                    public void call(Throwable throwable) {
                                        Log.e(TAG, "MobileStartupCheck error", throwable);
                                        if (BuildConfig.LOG_ERRORS_TO_SERVER && !BuildConfig.DEBUG) {
                                            Crashlytics.logException(throwable);
                                        }
                                        hideProgressBar();
                                        showError(getString(R.string.a_problem_has_occurred));
                                    }
                                });

        subscriptions.add(startupChecksSubscription);
    }

    private void retry() {
//        if (retryAttempts++ > MAX_RETRIES) {
//            // unrecoverable or unknown problem
//            AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogTheme);
//            builder.setMessage(getString(R.string.unrecoverable_error_dialog));
//            builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    dialog.dismiss();
//                    user.logout();
//                    finish();
//                }
//            });
//            builder.create().show();
//        } else {
            showProgressBar();
            doMobileStartupChecks();
//        }
    }

    private void processStartupChecksData(MobileStartupChecksResponse.Data data) {
        final String siteContentChecksum = dataStore.getSiteContentChecksum();
        final String consumerProfileChecksum = user.getConsumerProfileChecksum();

        // update SafeLocations
        dataStore.setSafeLocations(data.safeLocations);

        // if there are any changes to the site/app content, set flag to download and update it
        if (siteContentChecksum == null || !siteContentChecksum.equals(data.siteContent.checksum)) {
            hasContentUpdate = true;
        }

        // if we have a consumerId and there are any changes to ConsumerProfile (or we haven't
        // downloaded it yet), set the flag to GET ConsumerProfile
        if (user.getConsumerId() != null &&
                (consumerProfileChecksum == null || !consumerProfileChecksum.equals(data.consumerProfile.checksum))) {
            // NOTE: we are not saving the user until we have downloaded the ConsumerProfile
            user.setConsumerProfileChecksum(data.consumerProfile.checksum);
            hasConsumerUpdate = true;
        }

        if (hasContentUpdate) {
            getContent();

        } else if (hasConsumerUpdate) {
            getConsumerProfile();

        } else {
            onStartupChecksComplete();
        }
    }

    private void getContent() {
        Log.i(TAG, "Getting content updates...");

        showProgressBar();

        hasContentUpdate = false;

        Subscription subscription =
                api.getContent(BuildConfig.API_VERSION)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                new Action1<GetContentResponse>() {
                                    @Override
                                    public void call(GetContentResponse response) {
                                        if (response.hasErrors()) {
                                            Log.e(TAG, "Get Content error: " + response.printErrors());
                                            if (BuildConfig.LOG_ERRORS_TO_SERVER) {
                                                Crashlytics.log(response.printErrors());
                                                Crashlytics.logException(new Exception("StartupChecks - GetContent"));
                                            }
                                            showError(getString(R.string.a_problem_has_occurred));
                                        } else {
                                            // update checksum
                                            dataStore.setSiteContentChecksum(response.getChecksum());

                                            // pull out Terms and Conditions
                                            final List<GetContentResponse.AppContent> appContent = response.getAppContent();
                                            for (GetContentResponse.AppContent content : appContent) {
                                                // QA used "1", client would not confirm whether it will be "1" or "TERMS" :(
                                                if ("1".equals(content.contentKey.toLowerCase())) {
                                                    dataStore.getSiteContent().termsAndConditions = content.contentText;
                                                    dataStore.saveSiteContent();
                                                }
                                                if ("terms".equals(content.contentKey.toLowerCase())) {
                                                    dataStore.getSiteContent().termsAndConditions = content.contentText;
                                                    dataStore.saveSiteContent();
                                                }
                                            }

                                            if (hasConsumerUpdate) {
                                                getConsumerProfile();
                                            } else {
                                                onStartupChecksComplete();
                                            }
                                        }
                                    }
                                },
                                new Action1<Throwable>() {
                                    @Override
                                    public void call(Throwable throwable) {
                                        if (BuildConfig.LOG_ERRORS_TO_SERVER) {
                                            Crashlytics.log("Get content error");
                                            Crashlytics.logException(throwable);
                                        }
                                        showError(getString(R.string.a_problem_has_occurred));
                                    }
                                });
        subscriptions.add(subscription);
    }

    private void getConsumerProfile() {
        Log.i(TAG, "Getting Consumer profile...");

        if (TextUtils.isEmpty(user.getConsumerId())) {
            Log.e(TAG, "Cannot get consumer profile, user.consumerId is null");
            showError(getString(R.string.a_problem_has_occurred));
            return;
        }

        showProgressBar();

        hasConsumerUpdate = false;

        final Subscription subscription =
                api.getConsumer()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Action1<GetConsumerResponse>() {
                                       @Override
                                       public void call(GetConsumerResponse response) {
                                           if (response.hasErrors()) {
                                               Log.e(TAG, "Get Consumer error: " + response.printErrors());
                                               if (BuildConfig.LOG_ERRORS_TO_SERVER && !BuildConfig.DEBUG) {
                                                   Crashlytics.log(response.printErrors());
                                                   Crashlytics.logException(new Exception("StartupChecks - GetProfile"));
                                               }
                                               showError(getString(R.string.a_problem_has_occurred));
                                           } else {
                                               user.setConsumerProfile(response.consumerProfile);
                                               user.save(); // saves new checksum also
                                               user.updateNotificationId(api, user.getSpotzDeviceId(), DeviceIdUtil.getDeviceId());
                                               onStartupChecksComplete();
                                           }
                                       }
                                   },
                                new Action1<Throwable>() {
                                    @Override
                                    public void call(Throwable throwable) {
                                        if (BuildConfig.LOG_ERRORS_TO_SERVER && !BuildConfig.DEBUG) {
                                            Crashlytics.log("Get consumer error");
                                            Crashlytics.logException(throwable);
                                        }
                                        if (throwable instanceof RetrofitError) {
                                            RetrofitError retrofitError = (RetrofitError) throwable;
                                            if (retrofitError.getResponse().getStatus() == 404) {
                                                logout();
                                                return;
                                            }
                                        }
                                        showError(getString(R.string.a_problem_has_occurred));
                                    }
                                });
        subscriptions.add(subscription);
    }

    private void logout() {
        user.logout();
        finish();
    }

    private void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    private void showProgressBar() {
        hideError();
        progressBar.setVisibility(View.VISIBLE);
    }

    private void showError(String error) {
        hideProgressBar();
        errorTextView.setText(error);
        errorContainer.setVisibility(View.VISIBLE);
    }

    private void hideError() {
        errorContainer.setVisibility(View.GONE);
    }

    /**
     * Called when all startup checks have completed (successfully or otherwise).
     * Move the user onto a relevant next Activity depending on their current status.
     */
    private void onStartupChecksComplete() {
        Intent intent = null;
        if (TextUtils.isEmpty(user.getConsumerId()) || !user.hasAuthenticatedPin()) {
            // user is yet to authenticate a pin, take them to pin entry
            intent = new Intent(this, AuthPinActivity.class);

        } else if (user.getConsumerProfile() == null ||
                TextUtils.isEmpty(user.getConsumerProfile().firstname) ||
                TextUtils.isEmpty(user.getConsumerProfile().surname) ||
                !user.getConsumerProfile().hasConsumerProperties()) {
            // user hasn't setup a profile or property
            intent = new Intent(this, ProfileSetupActivity.class);

        } else if (user.hasAuthenticatedSession()) {
            if (isSignIn) {
                loadParcelHistory();
            } else {
                dataStore.clear();
                intent = new Intent(this, YourDeliveriesActivity.class);
            }

        } else {
            // user should not be in this state, reset dpdsession and get them to login again
            Log.e(TAG, "User does not have authenticated session and is in an inconsistent state, kicking to login...");
            logout();
            return;
        }
        if (intent != null) {
            startActivity(intent);
            finish();
        }
    }

    private void startYourDeliveryActivity() {
        startActivity(new Intent(this, YourDeliveriesActivity.class));
        finish();
    }

    //this is for loading at sign up only
    private void loadParcelHistory() {
        if (parcelHistorySubscription != null && !parcelHistorySubscription.isUnsubscribed()) {
            parcelHistorySubscription.unsubscribe();
        }

        parcelHistorySubscription =
                api.getParcelHistory(1, 15)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                new Action1<GetParcelHistoryResponse>() {
                                    @Override
                                    public void call(GetParcelHistoryResponse response) {
                                        if (response.hasErrors()) {
                                            Log.e(TAG, response.getErrors().toString());
                                            showErrorDialog(getString(R.string.unable_to_load_parcels));
                                            return;
                                        }

                                        final ParcelHistory parcelHistory = response.getParcelHistory();
                                        // force certain parcel to be first in list in debug mode
                                        // if specified via the secret menu
                                        if (BuildConfig.DEBUG && DebugUtils.forcedParcelId != null) {
                                            parcelHistory.getActiveParcels().get(0).parcelCode = DebugUtils.forcedParcelId;
                                        }
                                        dataStore.setParcelHistory(parcelHistory);

                                        // For wearable
                                        WearableUtil wearableUtil = new WearableUtil(getApplicationContext());
                                        wearableUtil.refreshParcelHistory();

                                        startYourDeliveryActivity();
                                    }
                                },
                                new Action1<Throwable>() {
                                    @Override
                                    public void call(Throwable throwable) {
                                        showErrorDialog(getString(R.string.unable_to_load_parcels));
                                    }
                                });
    }
}
