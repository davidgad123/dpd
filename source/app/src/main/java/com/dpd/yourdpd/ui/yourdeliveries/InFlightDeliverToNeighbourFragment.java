package com.dpd.yourdpd.ui.yourdeliveries;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;

import com.dpd.yourdpd.BuildConfig;
import com.dpd.yourdpd.R;
import com.dpd.yourdpd.model.Address;
import com.dpd.yourdpd.model.Parcel;
import com.dpd.yourdpd.model.ParcelActionCodes;
import com.dpd.yourdpd.net.request.DeliverToNeighbourParams;
import com.dpd.yourdpd.net.response.DeliverToNeighbourResponse;
import com.dpd.yourdpd.net.response.GetParcelUpdateDatesResponse;
import com.dpd.yourdpd.net.response.SearchByAddressResponse;
import com.dpd.yourdpd.ui.BaseFragment;
import com.dpd.yourdpd.ui.BaseMenuActivity;
import com.dpd.yourdpd.util.AlphaNumComparator;
import com.dpd.yourdpd.util.CommonUtils;
import com.dpd.yourdpd.util.DateUtils;
import com.dpd.yourdpd.util.UIUtils;
import com.dpd.yourdpd.util.ViewIdGenerator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link InFlightDeliverToNeighbourFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link InFlightDeliverToNeighbourFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class InFlightDeliverToNeighbourFragment extends BaseFragment {
    // the fragment initialization parameters
    private static final String ARG_PARCEL_CODE = "parcel_code";

    private static final String TAG = InFlightDeliverToNeighbourFragment.class.getSimpleName();

    private OnFragmentInteractionListener listener;
    private Parcel parcel;
    private RadioGroup deliveryDatesRadioGroup;
    private RadioGroup neighbourAddressRadioGroup;
    private Button confirmChangesButton;
    private ContentLoadingProgressBar progressBar;
    private ScrollView scrollView;

    private Subscription loadDataSubscription;
    private Subscription deliverToNeighbourSubscription;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param parcelCode Parcel.parcelCode
     * @return A new instance of fragment InFlightDeliverToNeighbourFragment.
     */
    public static InFlightDeliverToNeighbourFragment newInstance(String parcelCode) {
        InFlightDeliverToNeighbourFragment fragment = new InFlightDeliverToNeighbourFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARCEL_CODE, parcelCode);
        fragment.setArguments(args);
        return fragment;
    }

    public InFlightDeliverToNeighbourFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            final String parcelCode = getArguments().getString(ARG_PARCEL_CODE);
            parcel = dataStore.getParcelWithParcelCode(parcelCode);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.fragment_inflight_deliver_to_neighbour, container, false);

        final RadioGroup.OnCheckedChangeListener radioCheckChangeListener =
                new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        if ((deliveryDatesRadioGroup.getCheckedRadioButtonId() != -1 ||
                                root.findViewById(R.id.dates_container).getVisibility() == View.GONE)
                                && neighbourAddressRadioGroup.getCheckedRadioButtonId() != -1) {
                            confirmChangesButton.setEnabled(true);
                        } else {
                            confirmChangesButton.setEnabled(false);
                        }
                    }
                };
        deliveryDatesRadioGroup = (RadioGroup) root.findViewById(R.id.delivery_dates_radiogroup);
        deliveryDatesRadioGroup.setOnCheckedChangeListener(radioCheckChangeListener);
        neighbourAddressRadioGroup = (RadioGroup) root.findViewById(R.id.neighbour_addresses_radiogroup);
        neighbourAddressRadioGroup.setOnCheckedChangeListener(radioCheckChangeListener);
        confirmChangesButton = (Button) root.findViewById(R.id.confirm_changes_button);
        confirmChangesButton.setEnabled(false);
        confirmChangesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showConfirmChangesDialog();
            }
        });
        progressBar = (ContentLoadingProgressBar) root.findViewById(R.id.progressBar);
        scrollView = (ScrollView) root.findViewById(R.id.scrollView);
        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        populateParcelStatusView();
        loadData();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (OnFragmentInteractionListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        /*final ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if(actionBar != null) {
            actionBar.setTitle(R.string.deliver_to_neighbour_titlecase);
        }*/
        ((BaseMenuActivity) getActivity())
                .setupToolbar(getString(R.string.deliver_to_neighbour_titlecase), true);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (loadDataSubscription != null && !loadDataSubscription.isUnsubscribed()) {
            loadDataSubscription.unsubscribe();
            loadDataSubscription = null;
        }
        if (deliverToNeighbourSubscription != null && !deliverToNeighbourSubscription.isUnsubscribed()) {
            deliverToNeighbourSubscription.unsubscribe();
            deliverToNeighbourSubscription = null;
        }
    }

    private void loadData() {
        if (parcel.deliveryDetails == null || parcel.deliveryDetails.address == null) {
            showErrorAlertDialog("Unknown address");
            getActivity().getSupportFragmentManager().popBackStack();
            return;
        }
        final Address targetAddress = parcel.deliveryDetails.address;

        showProgressBar();

        if (loadDataSubscription != null && !loadDataSubscription.isUnsubscribed()) {
            loadDataSubscription.unsubscribe();
            loadDataSubscription = null;
        }
        loadDataSubscription =
                // get list of available delivery dates for this action
                getDeliveryDates()
                        // then getSearchResults()
                        .flatMap(new Func1<GetParcelUpdateDatesResponse, Observable<SearchByAddressResponse>>() {
                            @Override
                            public Observable<SearchByAddressResponse> call(final GetParcelUpdateDatesResponse response) {
                                if (response.hasErrors()) {
                                    Log.e(TAG, "Problem getting dates: " + response.printErrors());
                                    return Observable.error(new Exception(
                                            getString(R.string.problem_getting_delivery_dates)));
                                }
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        setDeliveryDates(response.getDiaryDates());
                                    }
                                });
                                return getAddresses(targetAddress);
                            }
                        })
                                // build a list of SearchAddresses to perform further API calls on
                        .flatMapIterable(new Func1<SearchByAddressResponse, Iterable<SearchByAddressResponse.AddressSearchResult>>() {
                            @Override
                            public Iterable<SearchByAddressResponse.AddressSearchResult> call(SearchByAddressResponse response) {
                                if (response.hasErrors()) {
                                    Log.e(TAG, "Problem getting dates: " + response.printErrors());
                                    return null;
                                }
                                final List<SearchByAddressResponse.AddressSearchResult> addressSearchResults = new ArrayList<>();
                                addressSearchResults.addAll(response.getSearchResults());
                                return addressSearchResults;
                            }
                        })
                                // for each AddressSearchResult look up Address (specifically addressId) via addressKey
//                .flatMap(new Func1<AddressSearchResult, Observable<LookupPostKeyResponse>>() {
//                    @Override
//                    public Observable<LookupPostKeyResponse> call(AddressSearchResult address) {
//                        return api.lookupPostKey(address.addressKey);
//                    }
//                })
//                .toList()
//                .flatMapIterable(new Func1<List<LookupPostKeyResponse>, Iterable<Address>>() {
//                    @Override
//                    public Iterable<Address> call(List<LookupPostKeyResponse> responses) {
//                        final List<Address> addresses = new ArrayList<>();
//                        for (LookupPostKeyResponse response : responses) {
//                            if (response.hasErrors()) {
//                                Log.e(TAG, "lookupPostKey error: " + response.printErrors());
//                                continue;
//                            }
//                            addresses.add(response.getAddressLocation().address);
//                        }
//                        return addresses;
//                    }
//                })
                        .toList()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Action1<List<SearchByAddressResponse.AddressSearchResult>>() {
                                       @Override
                                       public void call(List<SearchByAddressResponse.AddressSearchResult> addressSearchResults) {
                                           hideProgressBar();
                                           if (addressSearchResults == null || addressSearchResults.size() == 0) {
                                               showErrorAlertDialog(getString(R.string.a_problem_has_occurred_try_again));
                                               return;
                                           }
                                           Collections.sort(addressSearchResults, new AlphaNumComparator<SearchByAddressResponse.AddressSearchResult>());
                                           setAddresses(addressSearchResults);
                                       }
                                   },
                                new Action1<Throwable>() {
                                    @Override
                                    public void call(Throwable throwable) {
                                        hideProgressBar();
                                        Log.e(TAG, "Problem getting data: ", throwable);
                                        showErrorAlertDialog(getString(R.string.a_problem_has_occurred_try_again));
                                    }
                                });
//                .subscribe(new Action1<List<Address>>() {
//                               @Override
//                               public void call(List<Address> addresses) {
//                                   hideProgressBar();
//                                   if (addresses == null || addresses.size() == 0) {
//                                       showErrorDialog(getString(R.string.a_problem_has_occurred_try_again));
//                                       return;
//                                   }
//                                   setAddresses(addresses);
//                               }
//                           },
//                        new Action1<Throwable>() {
//                            @Override
//                            public void call(Throwable throwable) {
//                                hideProgressBar();
//                                Log.e(TAG, "Problem getting data: ", throwable);
//                                showErrorDialog(getString(R.string.a_problem_has_occurred_try_again));
//                            }
//                        });
    }

    private Observable<SearchByAddressResponse> getAddresses(Address targetAddress) {
        return api.searchByAddress(
                targetAddress.locality,
                targetAddress.organisation,
                targetAddress.postcode,
                targetAddress.street,
                targetAddress.town,
                targetAddress.county,
                1, // page
                BuildConfig.SEARCH_BY_ADDRESS_LIMIT)  // pageSize
                .retry(3);
    }

    private Observable<GetParcelUpdateDatesResponse> getDeliveryDates() {
        return api.getParcelUpdateDates(
                parcel.parcelCode,
                ParcelActionCodes.DELIVER_TO_NEIGHBOUR.getValue())
                .subscribeOn(Schedulers.io())
                .retry(3);
    }

    /**
     * Perform the Deliver To Neighbour action on the server
     *
     * @param date    May be null if no date is required else yyyy-MM-dd
     * @param address AddressSearchResult to send parcel to
     */
    private void deliverToNeighbour(String date, SearchByAddressResponse.AddressSearchResult address) {
        showProgressBar();
        confirmChangesButton.setEnabled(false);

        final DeliverToNeighbourParams params =
                new DeliverToNeighbourParams.Builder()
                        .setContactName(user.getFullName())
                        .setEmail(user.getConsumerProfile().email)
                        .setMobile(CommonUtils.addPrefixNumber(user.getConsumerProfile().mobile))
                        .setHouseNumber(address == null ? "" : null)
                        .setAddressId(address == null ?
                                null /*DeliverToNeighbourParams.ANY_NEIGHBOUR_ADDRESS_ID*/ :
                                address.addressKey)
                        .setDiaryDate(date)
                        .setParcelCode(parcel.parcelCode)
                        .build();

        if (deliverToNeighbourSubscription != null && !deliverToNeighbourSubscription.isUnsubscribed()) {
            deliverToNeighbourSubscription.unsubscribe();
            deliverToNeighbourSubscription = null;
        }
        deliverToNeighbourSubscription = api.deliverToNeighbour(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<DeliverToNeighbourResponse>() {
                               @Override
                               public void call(DeliverToNeighbourResponse response) {
                                   hideProgressBar();
                                   confirmChangesButton.setEnabled(true);

                                   if (response.hasErrors()) {
                                       Log.e(TAG, "Problem calling deliverToNeighbour() " + response.printErrors());
                                       showErrorAlertDialog(getString(R.string.parcel_status_not_changed), getString(R.string.error_occurred));
                                       return;
                                   }

                                   showActionConfirmedDialog();
                               }
                           },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                Log.e(TAG, "Problem calling deliverToNeighbour()", throwable);
                                hideProgressBar();
                                confirmChangesButton.setEnabled(true);
                                showErrorAlertDialog(getString(R.string.parcel_status_not_changed), getString(R.string.error_occurred));
                            }
                        });
    }

    private void showActionConfirmedDialog() {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(getContext(), R.style.AlertDialogTheme);
        builder.setTitle(R.string.parcel_updated);
        builder.setMessage(getString(R.string.parcel_delivered_to_neighbour_confirmed));
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // all done - return from this Fragment
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
        builder.create().show();
    }

    private void setDeliveryDates(List<String> diaryDates) {
        final View view = getView();
        assert view != null;

        if (diaryDates == null || diaryDates.size() == 0) {
            // no date selection is required
            view.findViewById(R.id.dates_container).setVisibility(View.GONE);
            return;
        } else {
            view.findViewById(R.id.dates_container).setVisibility(View.VISIBLE);
        }

        final LayoutInflater inflater = LayoutInflater.from(getActivity());
        final SimpleDateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd", Locale.UK);

        deliveryDatesRadioGroup.removeAllViews();

        for (String diaryDate : diaryDates) {
            try {
                final Date date = dateParser.parse(diaryDate);
                final Calendar cal = Calendar.getInstance();
                cal.setTime(date);
                final String dateSuffix = DateUtils.getDayOfMonthSuffix(cal.get(Calendar.DATE));
                final SimpleDateFormat dateFormat = new SimpleDateFormat(String.format("cccc d'%s' MMMM", dateSuffix), Locale.UK);
                final RadioButton radioButton = (RadioButton) inflater.inflate(R.layout.item_radiobutton, deliveryDatesRadioGroup, false);
                radioButton.setId(ViewIdGenerator.generateViewId());
                radioButton.setText(dateFormat.format(date));
                radioButton.setTag(diaryDate);
                deliveryDatesRadioGroup.addView(radioButton);
            } catch (ParseException e) {
                Log.e(TAG, "Exception parsing diaryDates for UI display", e);
            }

        }
        deliveryDatesRadioGroup.clearCheck();
        deliveryDatesRadioGroup.check(deliveryDatesRadioGroup.getChildAt(0).getId());
    }

    private void setAddresses(List<SearchByAddressResponse.AddressSearchResult> addresses) {
        final View view = getView();
        assert view != null;
        final LayoutInflater inflater = LayoutInflater.from(getActivity());

        neighbourAddressRadioGroup.removeAllViews();

        // add "Any Neighbour" option
        final RadioButton radioButton = (RadioButton) inflater.inflate(R.layout.item_radiobutton, neighbourAddressRadioGroup, false);
        radioButton.setText(R.string.any_neighbour);
        radioButton.setId(ViewIdGenerator.generateViewId());
        neighbourAddressRadioGroup.addView(radioButton);

        // add other addresses
        for (SearchByAddressResponse.AddressSearchResult address : addresses) {
            final RadioButton radio = (RadioButton) inflater.inflate(R.layout.item_radiobutton, neighbourAddressRadioGroup, false);
            radio.setText(address.quickviewAddress);
            radio.setId(ViewIdGenerator.generateViewId());
            radio.setTag(address);
            neighbourAddressRadioGroup.addView(radio);
        }
        neighbourAddressRadioGroup.clearCheck();
        neighbourAddressRadioGroup.check(neighbourAddressRadioGroup.getChildAt(0).getId());
    }

    private void showProgressBar() {
        progressBar.show();
        scrollView.setVisibility(View.GONE);
    }

    private void hideProgressBar() {
        progressBar.hide();
        scrollView.setVisibility(View.VISIBLE);
    }

    private void populateParcelStatusView() {
        final View view = getView();
        assert view != null;
        final Context context = getContext();
        UIUtils.populateParcelStatus(view, context, parcel);
    }

    private void showConfirmChangesDialog() {
        final View view = getView();
        assert view != null;
        final RadioButton dateButton = (RadioButton) view.findViewById(deliveryDatesRadioGroup.getCheckedRadioButtonId());
        final RadioButton addressButton = (RadioButton) view.findViewById(neighbourAddressRadioGroup.getCheckedRadioButtonId());

        // format selected diaryDate into something a little nicer for the prompt
        final String formattedDate;
        final String selectedDate = (dateButton == null) ? null : (String) dateButton.getTag();
        if (selectedDate != null) {
            final SimpleDateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd", Locale.UK);
            Date date = null;
            try {
                date = dateParser.parse(selectedDate);
            } catch (ParseException e) {
                Log.e(TAG, "Unable to format diaryDate", e);
            }
            if (date != null) {
                final Calendar cal = Calendar.getInstance();
                cal.setTime(date);
                final String dateSuffix = DateUtils.getDayOfMonthSuffix(cal.get(Calendar.DATE));
                final SimpleDateFormat dateFormat = new SimpleDateFormat(String.format("cccc d'%s' MMMM", dateSuffix), Locale.UK);
                formattedDate = dateFormat.format(date);
            } else {
                formattedDate = selectedDate;
            }
        } else {
            // TODO: just remove this part of the formatted text
            formattedDate = getString(R.string.next_available_day);
        }

        // grab the selected AddressSearchResult
        final SearchByAddressResponse.AddressSearchResult selectedAddress = (SearchByAddressResponse.AddressSearchResult) addressButton.getTag();

        final AlertDialog.Builder builder =
                new AlertDialog.Builder(getActivity(), R.style.AlertDialogTheme);
        builder.setTitle(getString(R.string.change_delivery_dialog_title));
        if (selectedAddress == null) {
//            builder.setMessage(getString(R.string.change_delivery_any_neighbour_dialog_message,
//                    formattedDate));
            builder.setMessage(getString(R.string.change_delivery_any_neighbour_dialog_message));
        } else {
//            builder.setMessage(getString(R.string.change_delivery_neighbour_dialog_message,
//                    selectedAddress.quickviewAddress,
//                    formattedDate));
            builder.setMessage(getString(R.string.change_delivery_neighbour_dialog_message,
                    selectedAddress.quickviewAddress));
        }
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                deliverToNeighbour(selectedDate, selectedAddress);
            }
        });
        builder.setNegativeButton(android.R.string.cancel, null);
        builder.show();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
//        public void onInflightPickupFragmentInteraction(Uri uri);
    }
}
