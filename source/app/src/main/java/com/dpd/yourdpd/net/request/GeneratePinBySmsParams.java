package com.dpd.yourdpd.net.request;

/**
 * @see com.dpd.yourdpd.net.Api#generatePinBySMS(GeneratePinBySmsParams)
 */
public class GeneratePinBySmsParams {
    public String mobile;

    public GeneratePinBySmsParams(String mobile) {
        this.mobile = mobile;
    }
}
