package com.dpd.yourdpd.net.response;

import com.dpd.yourdpd.model.PickupLocationResult;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @see com.dpd.yourdpd.net.Api#getPickupLocationsByAddress(String, int, int, int)
 */
public class GetPickupLocationsByAddressResponse extends BaseResponse {
    private Data data;

    private class Data {
        public int totalResults;
        @SerializedName("results")
        public List<PickupLocationResult> pickupLocationResults;
    }

    public List<PickupLocationResult> getPickupLocationResults() {
        return (data == null) ? null : data.pickupLocationResults;
    }
}
