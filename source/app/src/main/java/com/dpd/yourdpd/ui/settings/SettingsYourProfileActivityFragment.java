package com.dpd.yourdpd.ui.settings;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.transitions.everywhere.TransitionManager;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.dpd.yourdpd.BuildConfig;
import com.dpd.yourdpd.R;
import com.dpd.yourdpd.model.ConsumerProfile;
import com.dpd.yourdpd.model.PinMethod;
import com.dpd.yourdpd.net.APIError;
import com.dpd.yourdpd.net.request.GeneratePinByEmailParams;
import com.dpd.yourdpd.net.request.GeneratePinBySmsParams;
import com.dpd.yourdpd.net.response.AuthenticatePinParams;
import com.dpd.yourdpd.net.response.AuthenticatePinResponse;
import com.dpd.yourdpd.net.response.GeneratePinResponse;
import com.dpd.yourdpd.ui.BaseFragment;
import com.dpd.yourdpd.util.CommonUtils;
import com.dpd.yourdpd.util.ImagePickerUtils;
import com.dpd.yourdpd.util.PhotoUtils;

import java.io.File;

import br.com.sapereaude.maskedEditText.MaskedEditText;
import retrofit.RetrofitError;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * A Fragment which provides the user with a mechanism to edit their Profile information
 */
public class SettingsYourProfileActivityFragment extends BaseFragment {
    private static final String TAG = SettingsYourProfileActivityFragment.class.getSimpleName();

    private EditText firstNameEditText;
    private EditText lastNameEditText;
    private ImageView profilePhotoImageView;
    private Button photoEditButton;

    private String szPreviousEmail = "", szPreviousPhoneNumber = "";

    private EditText emailEditText;
    private View emailTooltip;
    private Button editEmailButton;
    private Button emailRequestPinButton;
    private MaskedEditText emailPinEditText;
    private View emailPinContainer;

    private EditText phonePrefixEditText;
    private EditText phoneEditText;
    private View phoneTooltip;
    private Button editPhoneButton;
    private Button phoneRequestPinButton;
    private MaskedEditText phonePinEditText;
    private View phonePinContainer;

    private String selectedPhotoBase64Data;
    private ProgressDialog progressDialog;
    private Subscription generatePinSubscription;
    private Subscription authPinSubscription;

    private final int MAIL_CONFIRM_PROGRESSING = 1;
    private final int MOBILE_CONFIRM_PROGRESSING = 2;
    private final int NONE_CONFIRM_PROGRESSING = 0;
    private int confirm_mail_progressing = NONE_CONFIRM_PROGRESSING;
    private int confirm_phone_progressing = NONE_CONFIRM_PROGRESSING;

    public static SettingsYourProfileActivityFragment newInstance() {
        return new SettingsYourProfileActivityFragment();
    }

    public SettingsYourProfileActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_settings_your_profile, container, false);
        firstNameEditText = (EditText) root.findViewById(R.id.first_name_edittext);
        lastNameEditText = (EditText) root.findViewById(R.id.last_name_edittext);
        profilePhotoImageView = (ImageView) root.findViewById(R.id.profile_photo_imageview);

        emailEditText = (EditText) root.findViewById(R.id.email_edittext);
        emailEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().isEmpty() || s.toString().equals(szPreviousEmail)) {
                    emailRequestPinButton.setVisibility(View.GONE);
                    confirm_mail_progressing = NONE_CONFIRM_PROGRESSING;
                } else {
                    emailRequestPinButton.setVisibility(View.VISIBLE);
                    confirm_mail_progressing = MAIL_CONFIRM_PROGRESSING;
                }
            }
        });
        emailEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus && emailRequestPinButton.getVisibility() == View.VISIBLE)
                    emailRequestPinButton.performClick();
            }
        });
        emailTooltip = root.findViewById(R.id.email_tooltip);
        emailTooltip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTooltip(emailTooltip, false, null);
            }
        });
        editEmailButton = (Button) root.findViewById(R.id.edit_email_button);
        editEmailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirm_mail_progressing = MAIL_CONFIRM_PROGRESSING;
                emailRequestPinButton.setVisibility(View.GONE);
                editEmail();
            }
        });
        emailRequestPinButton = (Button) root.findViewById(R.id.email_request_pin_button);
        emailRequestPinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestEmailPin();
            }
        });
        emailPinContainer = root.findViewById(R.id.email_pin_container);
        emailPinEditText = (MaskedEditText) root.findViewById(R.id.email_pin_edittext);
        emailPinEditText.setTypeface(Typeface.MONOSPACE);
        emailPinEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (confirm_phone_progressing == MOBILE_CONFIRM_PROGRESSING)
                    protectEditEmail();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!emailPinEditText.isEditingAfter()) return;

                final int pinLength = getResources().getInteger(R.integer.pin_length);
                String pin = com.dpd.yourdpd.util.TextUtils.getRealTextFromPIN(s.toString(), emailPinEditText.getMask(), emailPinEditText.getViewHolderChar());
                if (pin.length() == pinLength) {
                    authEmailPin();
                }
            }
        });

        phonePrefixEditText = (EditText) root.findViewById(R.id.phone_prefix_edittext);
        phoneEditText = (EditText) root.findViewById(R.id.phone_edittext);
        phoneEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (confirm_mail_progressing == MAIL_CONFIRM_PROGRESSING)
                    protectEditEmail();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().isEmpty() || s.toString().equals(szPreviousPhoneNumber)) {
                    phoneRequestPinButton.setVisibility(View.GONE);
                } else {
                    phoneRequestPinButton.setVisibility(View.VISIBLE);
                }
            }
        });
        phoneEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus && phoneRequestPinButton.getVisibility() == View.VISIBLE)
                    phoneRequestPinButton.performClick();
            }
        });
        phoneRequestPinButton = (Button) root.findViewById(R.id.phone_request_pin_button);
        phoneRequestPinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestPhonePin();
            }
        });
        phonePinContainer = root.findViewById(R.id.phone_pin_container);
        phonePinEditText = (MaskedEditText) root.findViewById(R.id.phone_pin_edittext);
        phonePinEditText.setTypeface(Typeface.MONOSPACE);
        phonePinEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!phonePinEditText.isEditingAfter()) return;

                final int pinLength = getResources().getInteger(R.integer.pin_length);
                String pin = com.dpd.yourdpd.util.TextUtils.getRealTextFromPIN(s.toString(), phonePinEditText.getMask(), phonePinEditText.getViewHolderChar());
                if (pin.length() == pinLength) {
                    authPhonePin();
                }
            }
        });
        phoneTooltip = root.findViewById(R.id.phone_tooltip);
        phoneTooltip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTooltip(phoneTooltip, false, null);
            }
        });
        editPhoneButton = (Button) root.findViewById(R.id.edit_phone_button);
        editPhoneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirm_phone_progressing = MOBILE_CONFIRM_PROGRESSING;
                phoneRequestPinButton.setVisibility(View.GONE);
                editPhone();
            }
        });

        photoEditButton = (Button) root.findViewById(R.id.edit_photo_button);
        photoEditButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editPhoto();
            }
        });

        root.findViewById(R.id.resend_email_pin_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestEmailPin();
            }
        });
        root.findViewById(R.id.resend_phone_pin_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestPhonePin();
            }
        });
        root.findViewById(R.id.profile_photo_frame).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editPhoto();
            }
        });

        CommonUtils.setupUIForKeyboard(getActivity(), root);
        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final ConsumerProfile consumerProfile = user.getConsumerProfile();

        firstNameEditText.setText(consumerProfile.firstname);
        lastNameEditText.setText(consumerProfile.surname);

        szPreviousEmail = consumerProfile.email == null ? "" : consumerProfile.email;
        szPreviousPhoneNumber = consumerProfile.mobile == null ? "" : CommonUtils.removePrefixNumber(consumerProfile.mobile);
        emailEditText.setText(szPreviousEmail);
        phoneEditText.setText(szPreviousPhoneNumber);

        selectedPhotoBase64Data = user.getConsumerProfile().photograph;
        if (!TextUtils.isEmpty(selectedPhotoBase64Data)) {
            PhotoUtils.loadUnknownImageDataIntoImageView(getContext(), selectedPhotoBase64Data, profilePhotoImageView, 0);
            profilePhotoImageView.setVisibility(View.VISIBLE);
            photoEditButton.setVisibility(View.VISIBLE);
        } else {
            profilePhotoImageView.setImageBitmap(null);
            photoEditButton.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (generatePinSubscription != null && !generatePinSubscription.isUnsubscribed()) {
            generatePinSubscription.unsubscribe();
        }
        generatePinSubscription = null;

        if (authPinSubscription != null && !authPinSubscription.isUnsubscribed()) {
            authPinSubscription.unsubscribe();
        }
        authPinSubscription = null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            final File file = new File(getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES), "temp.jpg");
            if (requestCode == ImagePickerUtils.REQUEST_CAMERA) {
                // allow user to pick image and display image in target ImageView
                // callback includes Bitmap of image
                ImagePickerUtils.loadAndDisplayBitmap(file, profilePhotoImageView,
                        new ImagePickerUtils.LoadImageCallback() {
                            @Override
                            public void onSuccess(Bitmap bitmap) {
                                onImageLoaded(bitmap);
                            }

                            @Override
                            public void onProgress(int progress) {
                            }

                            @Override
                            public void onError(int code) {
                                Toast.makeText(getActivity(),
                                        getString(R.string.problem_loading_image),
                                        Toast.LENGTH_SHORT).show();
                            }
                        });

            } else if (requestCode == ImagePickerUtils.REQUEST_GALLERY) {
                // allow user to pick image and display image in target ImageView
                // callback includes Bitmap of image
                ImagePickerUtils.loadAndDisplayBitmap(data, getActivity(), profilePhotoImageView,
                        new ImagePickerUtils.LoadImageCallback() {
                            @Override
                            public void onSuccess(Bitmap bitmap) {
                                onImageLoaded(bitmap);
                            }

                            @Override
                            public void onProgress(int progress) {
                            }

                            @Override
                            public void onError(int code) {
                                Toast.makeText(getActivity(),
                                        getString(R.string.problem_loading_image),
                                        Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        }
    }

    private void onImageLoaded(Bitmap bitmap) {
        selectedPhotoBase64Data = PhotoUtils.bitmapToBase64(bitmap);
        if (!TextUtils.isEmpty(selectedPhotoBase64Data))
            photoEditButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void onResume() {
        super.onResume();
        // restore Fragment title in App Bar
        ((SettingsActivity) getActivity()).setupToolbar(getString(R.string.your_profile_titlecase), true);
    }

    @Override
    public boolean validate() {
        if (firstNameEditText.getText().toString().trim().isEmpty() || lastNameEditText.getText().toString().trim().isEmpty()) {
            showNameEmptyWarning();
        } else if (confirm_phone_progressing == MOBILE_CONFIRM_PROGRESSING) {
            promptGoToPreviousScreen(getString(R.string.mobile_unconfirmed), getString(R.string.mobile_unconfirmed_message));
        } else if (confirm_mail_progressing == MAIL_CONFIRM_PROGRESSING) {
            promptGoToPreviousScreen(getString(R.string.email_unconfirmed), getString(R.string.email_unconfirmed_message));
        } else {
            CommonUtils.hideSoftKeyboard(getActivity());
            return true;
        }
        return false;
    }

    @Override
    public void onStop() {
        super.onStop();
        CommonUtils.hideSoftKeyboard(getActivity());
    }

    private void promptGoToPreviousScreen(String title, String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(getActivity(), R.style.AlertDialogTheme);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(true);
        builder.setNegativeButton(getString(android.R.string.cancel), null);
        builder.setPositiveButton(getString(android.R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                ((SettingsActivity) getActivity()).showBeforeFragment();
            }
        });
        builder.create().show();

        CommonUtils.hideSoftKeyboard(getActivity());
    }

    private void showNameEmptyWarning() {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(getActivity(), R.style.AlertDialogTheme);
        builder.setTitle(getString(R.string.name_empty_warning));
        builder.setMessage(getString(R.string.name_empty_warning_message));
        builder.setCancelable(true);
        builder.setNegativeButton(getString(android.R.string.cancel), null);
        builder.setPositiveButton(getString(android.R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

                // Revert changes.
                final ConsumerProfile consumerProfile = user.getConsumerProfile();
                firstNameEditText.setText(consumerProfile.firstname);
                lastNameEditText.setText(consumerProfile.surname);

                ((SettingsActivity) getActivity()).showBeforeFragment();
                CommonUtils.hideSoftKeyboard(getActivity());
            }
        });
        builder.create().show();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    public void applyChanges() {
        final ConsumerProfile consumerProfile = user.getConsumerProfile();
        consumerProfile.photograph = selectedPhotoBase64Data;
        consumerProfile.firstname = firstNameEditText.getText().toString().trim();
        consumerProfile.surname = lastNameEditText.getText().toString().trim();
        consumerProfile.email = emailEditText.getText().toString().trim();
        consumerProfile.mobile = phoneEditText.getText().toString().trim();
    }

    private void requestEmailPin() {
        CommonUtils.hideSoftKeyboard(getActivity());
        requestPin(PinMethod.EMAIL);
    }

    private void requestPhonePin() {
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(phoneEditText.getWindowToken(), 0);
        }
        requestPin(PinMethod.SMS);
    }

    private void requestPin(PinMethod method) {
        // NOTE: RequestPinFragment contains similar validations
        if (method.equals(PinMethod.EMAIL)) {
            final String email = emailEditText.getText().toString();
            if (TextUtils.isEmpty(email) || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                showTooltip(emailTooltip, true, getString(R.string.invalid_email_tooltip));
                return;
            }

        } else {
            final String phone = phoneEditText.getText().toString();
            if (TextUtils.isEmpty(phone) || phone.length() < 7 || !(Patterns.PHONE.matcher(phone).matches())) {
                showTooltip(phoneTooltip, true, getString(R.string.invalid_phone_tooltip));
                return;
            }
        }
        performRequestPinAPICall(method);
    }

    private void showTooltip(View tooltip, boolean isShow, CharSequence error) {
        if (error != null) {
            TextView tooltipTextView = (TextView) tooltip.findViewById(tooltip.getId() == R.id.phone_tooltip ? R.id.phone_tooltip_textview : R.id.email_tooltip_textview);
            tooltipTextView.setText(error);
        }

        TransitionManager.beginDelayedTransition((ViewGroup) getActivity().findViewById(android.R.id.content));
        tooltip.setVisibility(isShow ? View.VISIBLE : View.GONE);
    }

    private void performRequestPinAPICall(final PinMethod method) {
        final boolean isSMSPin = method.equals(PinMethod.SMS);
        final String email = emailEditText.getText().toString();
        final String mobile = CommonUtils.addPrefixNumber(phoneEditText.getText().toString());

        showLoadingDialog(null);

        final Observable<GeneratePinResponse> observable = isSMSPin ?
                api.generatePinBySMS(new GeneratePinBySmsParams(mobile)) :
                api.generatePinByEmail(new GeneratePinByEmailParams(email));

        if (generatePinSubscription != null && !generatePinSubscription.isUnsubscribed()) {
            generatePinSubscription.unsubscribe();
        }
        generatePinSubscription = observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        new Action1<GeneratePinResponse>() {
                            @Override
                            public void call(GeneratePinResponse response) {
                                progressDialog.hide();
                                if (response.hasErrors()) {
                                    Log.e(TAG, "Generate pin error: " + response.printErrors());
                                    if (BuildConfig.LOG_ERRORS_TO_SERVER && !BuildConfig.DEBUG) {
                                        Crashlytics.log(response.printErrors());
                                        Crashlytics.logException(new Exception("SettingsGeneratePin"));
                                    }
                                    showTooltip(isSMSPin ? phoneTooltip : emailTooltip, true,
                                            getString(isSMSPin ? R.string.problem_invalid_mobile : R.string.problem_invalid_mail));
                                } else {
                                    onPinRequested(method);
                                }
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                progressDialog.hide();
                                if (BuildConfig.LOG_ERRORS_TO_SERVER && !BuildConfig.DEBUG) {
                                    Crashlytics.logException(throwable);
                                }
                                if (throwable instanceof RetrofitError) {
                                    RetrofitError retrofitError = (RetrofitError) throwable;
                                    if (retrofitError.getResponse().getStatus() == 429) {
                                        showTooltip(phoneTooltip, true, getString(R.string.too_many_requests));
                                        return;
                                    }
                                }
                                Log.e(TAG, "Generate pin error", throwable);
                                showTooltip(isSMSPin ? phoneTooltip : emailTooltip, true,
                                        getString(isSMSPin ? R.string.problem_invalid_mobile : R.string.problem_invalid_mail));
                            }
                        });
    }

    private void onPinRequested(PinMethod method) {
        // show email or SMS pin entry field
        TransitionManager.beginDelayedTransition((ViewGroup) getView());
        if (method.equals(PinMethod.EMAIL)) {
            emailEditText.setEnabled(false);
            editEmailButton.setVisibility(View.VISIBLE);
            emailTooltip.setVisibility(View.GONE);
            emailRequestPinButton.setVisibility(View.GONE);
            emailPinContainer.setVisibility(View.VISIBLE);
            emailEditText.post(new Runnable() {
                @Override
                public void run() {
                    View.OnFocusChangeListener listener = emailEditText.getOnFocusChangeListener();
                    emailEditText.setOnFocusChangeListener(null);
                    emailPinEditText.setText("");
                    emailPinEditText.requestFocus();
                    emailEditText.setOnFocusChangeListener(listener);
                }
            });
        } else {
            phoneEditText.setEnabled(false);
            phonePrefixEditText.setEnabled(false);
            editPhoneButton.setVisibility(View.VISIBLE);
            phoneTooltip.setVisibility(View.GONE);
            phoneRequestPinButton.setVisibility(View.GONE);
            phonePinContainer.setVisibility(View.VISIBLE);
            phoneEditText.post(new Runnable() {
                @Override
                public void run() {
                    View.OnFocusChangeListener listener = phoneEditText.getOnFocusChangeListener();
                    phoneEditText.setOnFocusChangeListener(null);
                    phonePinEditText.setText("");
                    phonePinEditText.requestFocus();
                    phoneEditText.setOnFocusChangeListener(listener);
                }
            });
        }
    }

    private void authEmailPin() {
        authenticatePin(PinMethod.EMAIL, com.dpd.yourdpd.util.TextUtils.getRealTextFromPIN(emailPinEditText.getText().toString(), emailPinEditText.getMask(), emailPinEditText.getViewHolderChar()));
        CommonUtils.hideSoftKeyboard(getActivity());
    }

    private void authPhonePin() {
        authenticatePin(PinMethod.SMS, com.dpd.yourdpd.util.TextUtils.getRealTextFromPIN(phonePinEditText.getText().toString(), phonePinEditText.getMask(), phonePinEditText.getViewHolderChar()));
        CommonUtils.hideSoftKeyboard(getActivity());
    }

    private void authenticatePin(final PinMethod method, String pin) {
        final String email = emailEditText.getText().toString();
        final String mobile = CommonUtils.addPrefixNumber(phoneEditText.getText().toString());

        final AuthenticatePinParams params = new AuthenticatePinParams();
        params.deviceId = user.getConsumerProfile().deviceId;
        params.pinNumber = pin;
        params.mediumValue = method.equals(PinMethod.SMS) ? mobile : email;
        params.medium = method.equals(PinMethod.SMS) ? "sms" : "email";

        showLoadingDialog(getString(R.string.verifying_pin));

        if (authPinSubscription != null && !authPinSubscription.isUnsubscribed()) {
            authPinSubscription.unsubscribe();
        }
        authPinSubscription = api.authenticatePin(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<AuthenticatePinResponse>() {
                               @Override
                               public void call(AuthenticatePinResponse response) {
                                   hideLoadingDialog();
                                   if (response.hasErrors()) {
                                       final APIError error = response.getErrors().get(0);
//                                       if (APIError.ErrorCode.INVALID_FIELD_VALUE.toString().equals(error.errorCode)) {
                                           showErrorAlertDialog(getString(R.string.pin_incorrect_message), getString(R.string.pin_incorrect));
//                                       } else {
//                                           showErrorAlertDialog(getString(R.string.a_problem_has_occurred_try_again));
//                                       }
                                       onPinVerifyFailed(method);
                                   } else {
                                       onPinVerified(method);
                                   }
                               }
                           },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                hideLoadingDialog();
                                showErrorAlertDialog(getString(R.string.a_problem_has_occurred_try_again));
                                onPinVerifyFailed(method);
                            }
                        });
    }

    private void onPinVerified(PinMethod method) {
        final ConsumerProfile consumerProfile = user.getConsumerProfile();
        if (method.equals(PinMethod.EMAIL)) {
            consumerProfile.email = emailEditText.getText().toString().trim();
            confirm_mail_progressing = NONE_CONFIRM_PROGRESSING;
            szPreviousEmail = emailEditText.getText().toString();

            // Reset and Hide pin container.
            emailPinEditText.setText("");
            TransitionManager.beginDelayedTransition((ViewGroup) getView());
            emailPinContainer.setVisibility(View.GONE);

            // Show verified dialog.
            showEmailVerifiedDialog();

        } else {
            consumerProfile.mobile = phoneEditText.getText().toString().trim();
            confirm_phone_progressing = NONE_CONFIRM_PROGRESSING;
            szPreviousPhoneNumber = phoneEditText.getText().toString();

            // Reset and Hide pin container.
            phonePinEditText.setText("");
            TransitionManager.beginDelayedTransition((ViewGroup) getView());
            phonePinContainer.setVisibility(View.GONE);

            // Show verified dialog.
            showMobileVerifiedDialog();
        }
    }

    private void onPinVerifyFailed(PinMethod method) {
        if (method.equals(PinMethod.EMAIL)) {
            // Reset and Hide pin container.
            emailPinEditText.setText("");
        } else {
            // Reset and Hide pin container.
            phonePinEditText.setText("");
        }
    }

    private void showEmailVerifiedDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AlertDialogTheme);
        builder.setTitle(getString(R.string.email_verified_title));
        builder.setMessage(getString(R.string.email_verified_message));
        builder.setPositiveButton(getString(android.R.string.ok), null);
        builder.create().show();
    }

    private void showMobileVerifiedDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AlertDialogTheme);
        builder.setTitle(getString(R.string.phone_pin_verified_title));
        builder.setMessage(getString(R.string.phone_pin_verified_message));
        builder.setPositiveButton(getString(android.R.string.ok), null);
        builder.create().show();
    }

    private void editEmail() {
        protectEditPhone();
        emailPinContainer.setVisibility(View.GONE);
        emailEditText.setEnabled(true);
        emailEditText.setSelection(0, emailEditText.length());
        emailEditText.requestFocus();
        TransitionManager.beginDelayedTransition((ViewGroup) getView());
        editEmailButton.setVisibility(View.INVISIBLE);
        emailTooltip.setVisibility(View.GONE);
        phoneTooltip.setVisibility(View.GONE);
        emailRequestPinButton.setVisibility(View.VISIBLE);

        CommonUtils.hideSoftKeyboard(getActivity());
    }

    private void protectEditEmail() {
        emailPinContainer.setVisibility(View.GONE);
        emailEditText.setEnabled(false);
        TransitionManager.beginDelayedTransition((ViewGroup) getView());
        editEmailButton.setVisibility(View.VISIBLE);
        emailTooltip.setVisibility(View.GONE);
        emailRequestPinButton.setVisibility(View.GONE);
        emailEditText.setText(szPreviousEmail);
    }

    private void protectEditPhone() {
        phonePinContainer.setVisibility(View.GONE);
        phonePrefixEditText.setEnabled(false);
        phoneEditText.setEnabled(false);
        TransitionManager.beginDelayedTransition((ViewGroup) getView());
        editPhoneButton.setVisibility(View.VISIBLE);
        phoneTooltip.setVisibility(View.GONE);
        phoneRequestPinButton.setVisibility(View.INVISIBLE);
        phoneEditText.setText(szPreviousPhoneNumber);
    }

    private void editPhone() {
        protectEditEmail();
        phonePinContainer.setVisibility(View.GONE);
        phonePrefixEditText.setEnabled(true);
        phoneEditText.setEnabled(true);
        phoneEditText.setSelection(0, phoneEditText.length());
        phoneEditText.requestFocus();
        TransitionManager.beginDelayedTransition((ViewGroup) getView());
        editPhoneButton.setVisibility(View.INVISIBLE);
        emailTooltip.setVisibility(View.GONE);
        phoneTooltip.setVisibility(View.GONE);
        phoneRequestPinButton.setVisibility(View.VISIBLE);

        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.showSoftInput(phoneEditText, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        ImagePickerUtils.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
    }

    private void editPhoto() {
        ImagePickerUtils.showPickerDialog(this);
    }

    /**
     * @param title (optional)
     */
    private void showLoadingDialog(String title) {
        if (title == null) {
            title = getString(R.string.please_wait_);
        }
        hideLoadingDialog();
        progressDialog = ProgressDialog.show(getActivity(),
                null,
                title,
                true,
                false);
    }

    private void hideLoadingDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }
}
