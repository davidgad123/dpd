package com.dpd.yourdpd.net.request;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * @see com.dpd.yourdpd.net.Api#collectFromDepot(CollectFromDepotParams)
 */
public class CollectFromDepotParams {
    private String collectFromDepotInstructions;
    private String diaryDate;
    private String contactName;
    private String email;
    private String mobile;
    private String[] parcelCode;

    public static class Builder {
        private CollectFromDepotParams params = new CollectFromDepotParams();

        public Builder() {
            // setup defaults
            params.collectFromDepotInstructions = "";
            params.contactName = "";
            params.diaryDate = ""; // only required for carded parcels
            params.parcelCode = new String[] {};
        }
        public Builder setContactName(String contactName) {
            params.contactName = contactName;
            return this;
        }
        public Builder setCollectFromDepotInstructions(String collectFromDepotInstructions) {
            params.collectFromDepotInstructions = collectFromDepotInstructions;
            return this;
        }
        public Builder setDiaryDate(Date date) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.UK);
            params.diaryDate = dateFormat.format(date);
            return this;
        }
        public Builder setEmail(String email) {
            params.email = email;
            return this;
        }
        public Builder setMobile(String mobile) {
            params.mobile = mobile;
            return this;
        }
        public Builder setParcelCode(String parcelCode) {
            params.parcelCode = new String[] { parcelCode };
            return this;
        }

        public CollectFromDepotParams build() {
            return params;
        }
    }

}
