package com.dpd.yourdpd.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

public class ParcelHistory implements Serializable {
    private static final long serialVersionUID = 0L;

    @SerializedName("parcelsActive")
    private ParcelListWrapper activeParcels;
    @SerializedName("parcelHistory")
    private ParcelListWrapper previousParcels;

    public List<Parcel> getActiveParcels() {
        return activeParcels != null ? activeParcels.parcels : Collections.<Parcel>emptyList();
    }

    public List<Parcel> getPreviousParcels() {
        return previousParcels != null ? previousParcels.parcels : Collections.<Parcel>emptyList();
    }

    public void removeParcel(String parcelCode) {
        for (int i = 0; i < getActiveParcels().size(); i++) {
            Parcel p = getActiveParcels().get(i);
            if (p.parcelCode != null && p.parcelCode.equals(parcelCode)) {
                getActiveParcels().remove(i);
                return;
            }
        }
        for (int i = 0; i < getPreviousParcels().size(); i++) {
            Parcel p = getPreviousParcels().get(i);
            if (p.parcelCode != null && p.parcelCode.equals(parcelCode)) {
                getPreviousParcels().remove(i);
                return;
            }
        }
    }

    /**
     * Replaces a matching Parcel (via parcelCode) with the given Parcel. We do this when
     * fetching a Parcel's detail vs the lightweight Parcels found in the parcelHistory list.
     *
     * @param parcel Parcel containing the additional detail
     */
    public void replaceParcel(String parcelCode, Parcel parcel) {
        for (int i = 0; i < getActiveParcels().size(); i++) {
            Parcel p = getActiveParcels().get(i);
            if (p.parcelCode != null && p.parcelCode.equals(parcelCode)) {
                mergeMissingDetails(parcel, p);
                getActiveParcels().set(i, parcel);
            }
        }
        for (int i = 0; i < getPreviousParcels().size(); i++) {
            Parcel p = getPreviousParcels().get(i);
            if (p.parcelCode != null && p.parcelCode.equals(parcelCode)) {
                mergeMissingDetails(parcel, p);
                getPreviousParcels().set(i, parcel);
            }
        }
    }

    /**
     * Merge details found in historyParcel into given Parcel (e.g. customerImageLogo)
     *
     * @param parcel        The Parcel from parcelDetails
     * @param historyParcel The Parcel from ParcelHistory
     */
    private void mergeMissingDetails(Parcel parcel, Parcel historyParcel) {
        parcel.parcelCode = historyParcel.parcelCode;
        parcel.customerImageLogo = historyParcel.customerImageLogo;
    }

    public Parcel getParcelWithParcelCode(String parcelCode) {
        for (int i = 0; i < getActiveParcels().size(); i++) {
            Parcel p = getActiveParcels().get(i);
            if (p.parcelCode != null && p.parcelCode.equals(parcelCode)) {
                return p;
            }
        }
        for (int i = 0; i < getPreviousParcels().size(); i++) {
            Parcel p = getPreviousParcels().get(i);
            if (p.parcelCode != null && p.parcelCode.equals(parcelCode)) {
                return p;
            }
        }
        return null;
    }
}
