package com.dpd.yourdpd.net.response;

import java.util.List;

/**
 * @see com.dpd.yourdpd.net.Api#getContent(String)
 */
public class GetContentResponse extends BaseResponse {
    private Data data;

    private class Data {
        public List<AppContent> appContent;
        public String checksum;
    }
    public class AppContent {
        public String contentKey;
        public String contentText;
    }

    public List<AppContent> getAppContent() {
        return (data==null) ? null : data.appContent;
    }

    public String getChecksum() {
        return data.checksum;
    }
}
