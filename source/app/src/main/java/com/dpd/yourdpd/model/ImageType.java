package com.dpd.yourdpd.model;

/**
 * @see com.dpd.yourdpd.net.Api#getImage(String, String, String)
 */
public class ImageType {
    public static final String SATURN_SAFE_PLACE = "S";
    public static final String CONSUMER_SAFE_PLACE = "PROPERTY";
    public static final String DRIVER = "DRIVER";
    public static final String CUSTOMER_LOGO = "TBC";
}
