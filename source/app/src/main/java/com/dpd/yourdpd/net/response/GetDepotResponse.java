package com.dpd.yourdpd.net.response;

import com.dpd.yourdpd.model.Depot;
import com.google.gson.annotations.SerializedName;

/**
 * @see com.dpd.yourdpd.net.Api#getDepot(String)
 */
public class GetDepotResponse extends BaseResponse {
    @SerializedName("data")
    private Depot depot;

    public Depot getDepot() {
        return depot;
    }
}