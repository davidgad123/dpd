package com.dpd.yourdpd.net.request;

import com.dpd.yourdpd.util.DateUtils;

import java.util.Date;

/**
 * @see com.dpd.yourdpd.net.Api#collectFromPickupLocation(CollectFromPickupLocationParams)
 */
public class CollectFromPickupLocationParams {
    private String pickupLocationCode;
    private String contactName;
    private String pickupDate;
    private String email;
    private String mobile;
    private String[] parcelCode;

    public static class Builder {
        private CollectFromPickupLocationParams params;

        public Builder() {
            // set up defaults
            params = new CollectFromPickupLocationParams();
            params.pickupDate = ""; // when parcel is not carded we don't need to set this
        }

        public Builder setPickupLocationCode(String pickupLocationCode) {
            params.pickupLocationCode = pickupLocationCode;
            return this;
        }

        /**
         * @param contactName TBC: optional
         */
        public Builder setContactName(String contactName) {
            params.contactName= contactName;
            return this;
        }

        /**
         * @param pickupDate (optional) only required if Parcel is carded
         */
        public Builder setPickupDate(Date pickupDate) {
//            params.pickupDate = DateUtils.dateToUTCISO8601String(pickupDate, true);
            params.pickupDate = DateUtils.dateToDateTimeFormat(pickupDate);
            return this;
        }

        public Builder setEmail(String email) {
            params.email = email;
            return this;
        }

        public Builder setMobile(String mobile) {
            params.mobile = mobile;
            return this;
        }

        public Builder setParcelCode(String parcelCode) {
            params.parcelCode = new String[] { parcelCode };
            return this;
        }

        public CollectFromPickupLocationParams build() {
            return params;
        }
    }
}