package com.dpd.yourdpd.util;

import android.content.Context;
import android.content.SharedPreferences;

public class PermissionUtil {
    private static final String PERMISSION_PREFS = "permission";
    public static final int REQUEST_CODE = 200;

    public static SharedPreferences get(Context context) {
        return context.getSharedPreferences(PERMISSION_PREFS, Context.MODE_PRIVATE);
    }

    public static boolean shouldWeAsk(Context context, String permission) {
        return (get(context).getBoolean(permission, true));
    }

    public static void markAsAsked(Context context, String permission) {
        get(context).edit().putBoolean(permission, false).apply();
    }
}
