package com.dpd.yourdpd.ui.yourdeliveries;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.transitions.everywhere.AutoTransition;
import android.transitions.everywhere.Transition;
import android.transitions.everywhere.TransitionManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.dpd.yourdpd.BuildConfig;
import com.dpd.yourdpd.R;
import com.dpd.yourdpd.model.Base64Photo;
import com.dpd.yourdpd.model.Depot;
import com.dpd.yourdpd.model.ImageType;
import com.dpd.yourdpd.model.Parcel;
import com.dpd.yourdpd.model.ParcelAction;
import com.dpd.yourdpd.model.ParcelActionCodes;
import com.dpd.yourdpd.model.ParcelEvent;
import com.dpd.yourdpd.model.Photo;
import com.dpd.yourdpd.model.PickupLocation;
import com.dpd.yourdpd.model.RouteView;
import com.dpd.yourdpd.net.APIError;
import com.dpd.yourdpd.net.response.GetDepotResponse;
import com.dpd.yourdpd.net.response.GetImageResponse;
import com.dpd.yourdpd.net.response.GetParcelActionsResponse;
import com.dpd.yourdpd.net.response.GetParcelDetailsResponse;
import com.dpd.yourdpd.net.response.GetParcelEventsResponse;
import com.dpd.yourdpd.net.response.GetPickupLocationResponse;
import com.dpd.yourdpd.net.response.GetPickupPassResponse;
import com.dpd.yourdpd.net.response.GetRouteViewOnRouteResponse;
import com.dpd.yourdpd.ui.BaseFragment;
import com.dpd.yourdpd.ui.GoogleApiClientActivity;
import com.dpd.yourdpd.ui.widget.BottomTooltip;
import com.dpd.yourdpd.util.UIUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.functions.Func2;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

import static com.dpd.yourdpd.model.Parcel.ParcelStatusType;

/**
 * Displays the details of an active {@link Parcel}
 */
public class ParcelDetailActivityFragment extends BaseFragment
        implements View.OnClickListener {
    private static final String TAG = ParcelDetailActivityFragment.class.getSimpleName();

    public static String ARG_PARCEL_CODE = "parcel_code";

    private String parcelCode;
    private Subscription getParcelSubscription;
    private Parcel parcel;
    private List<ParcelEvent> parcelEvents;

    /**
     * Required when a Parcel is {@link ParcelStatusType#COLLECT_FROM_DEPOT}
     * along with parcel to display the collect from depot state
     */
    private Depot depot;

    /**
     * Required when a Parcel is {@link ParcelStatusType#COLLECT_FROM_DEPOT}
     * along with parcel to display the on-route information such as driver name/no. stops
     */
    private RouteView routeView;
    private String routeDriverImage;

    /**
     * Required when a Parcel is {@link ParcelStatusType#COLLECT_FROM_PICKUP}
     */
    private PickupLocation pickupLocation;

    /**
     * Available from Parcel Events when the Parcel is {@link ParcelStatusType#DELIVERED}
     */
    private List<Photo> photos;
    private Location lastKnownLocation;

    private SwipeRefreshLayout swipeRefreshLayout;
    private ScrollView scrollView;
    private View parcelStatusView;
    private BottomTooltip parcelIssueTooltip;
    private DeliveryPhotosView deliveryPhotosView;
    private ParcelLocationView parcelLocationView;
    private PickupPassView pickupPassView;
    private PickupLocationDetailsView pickupLocationDetailsView;
    private ViewGroup userMessagesView;
    private View parcelActionsView;
    private Button expandParcelActionsButton;
    private Button expandParcelEventsButton;

    private final DateTimeFormatter inFormatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
    private final DateTimeFormatter outFormatter = DateTimeFormat.forPattern("dd MMM yyyy'\n'HH:mm");

    private CompositeSubscription subscriptions;

    /**
     * Parent Activity must implement this interface
     */
    public interface OnFragmentInteractionListener {
        void onShowProgressBar();

        void onHideProgressBar();
    }

    private OnFragmentInteractionListener listener;

    public ParcelDetailActivityFragment() {
    }

    public static ParcelDetailActivityFragment newInstance(String parcelCode) {
        final Bundle args = new Bundle();
        args.putString(ARG_PARCEL_CODE, parcelCode);
        final ParcelDetailActivityFragment fragment = new ParcelDetailActivityFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        parcelCode = getArguments().getString(ARG_PARCEL_CODE);
        if (parcelCode == null) {
            showParcelNotFoundDialog();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.fragment_parcel_detail, container, false);
        swipeRefreshLayout = (SwipeRefreshLayout) root.findViewById(R.id.swipe_refresh_layout);
        scrollView = (ScrollView) root.findViewById(R.id.scrollView);
        parcelStatusView = root.findViewById(R.id.parcel_status);
        userMessagesView = (ViewGroup) root.findViewById(R.id.user_messages_container);
        deliveryPhotosView = (DeliveryPhotosView) root.findViewById(R.id.parcel_delivery_photos);
        pickupPassView = (PickupPassView) root.findViewById(R.id.pickup_pass);
        pickupLocationDetailsView = (PickupLocationDetailsView) root.findViewById(R.id.pickup_location_details);
        parcelLocationView = (ParcelLocationView) root.findViewById(R.id.parcel_location);
        parcelLocationView.setVisibility(View.GONE);
        parcelIssueTooltip = (BottomTooltip) root.findViewById(R.id.parcel_issue_tooltip);
        parcelActionsView = root.findViewById(R.id.parcel_actions);
        expandParcelActionsButton = (Button) root.findViewById(R.id.expand_parcel_actions_button);
        expandParcelActionsButton.setOnClickListener(this);
        expandParcelActionsButton.setEnabled(false);
        expandParcelEventsButton = (Button) root.findViewById(R.id.expand_parcel_events_button);
        expandParcelEventsButton.setOnClickListener(this);
        expandParcelEventsButton.setEnabled(false);
        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getLastKnownLocation();
                reloadParcel(true);
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (OnFragmentInteractionListener) context;
        } catch (ClassCastException e) {
            Log.e(TAG, "Context must implement OnFragmentInteractionListener");
            throw e;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        // cleanup Views
        parcelLocationView.destroy();
        listener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        getLastKnownLocation();
        reloadParcel(false);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (getParcelSubscription != null && !getParcelSubscription.isUnsubscribed()) {
            getParcelSubscription.unsubscribe();
            getParcelSubscription = null;
        }
        if (subscriptions != null && subscriptions.hasSubscriptions()) {
            subscriptions.unsubscribe();
            subscriptions = null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        parcel = null;
    }

    public void setAndReloadParcel(String parcelCode) {
        if (getActivity().isFinishing() || parcelCode == null) {
            return;
        }

        this.parcelCode = parcelCode;
        reloadParcel(false);
    }

    public void reloadParcel(boolean isReload) {
        if (getActivity().isFinishing() || parcelCode == null) {
            return;
        }

        if (isReload) {
            swipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    swipeRefreshLayout.setRefreshing(true);
                }
            });
        } else
            listener.onShowProgressBar();

        // hide or reset any state-dependent views
        hideParcelIssue();
        hidePickupPass();
        hidePickupLocationDetails();
        hideDeliveryPhotos();

        getParcel();
    }

    private void getAdditionalParcelData() {
        if (subscriptions != null && !subscriptions.isUnsubscribed()) {
            subscriptions.unsubscribe();
        }

        showParcelStatus();
        showUserMessages();

        boolean showParcelLocationView = true;

        // switch new parcelStatusType and for some states, await response from multiple APIs
        // before updating the UI
        switch (parcel.parcelStatusType) {
            case OUT_FOR_DELIVERY: {
                subscriptions = new CompositeSubscription(
                        getParcelActions(),
                        getRouteView(),
                        getParcelEvents()
                );
                parcelLocationView.setIsLoading(true);
                break;
            }
            case COLLECT_FROM_DEPOT: {
                subscriptions = new CompositeSubscription(
                        getParcelActions(),
                        getDepot(),
                        getParcelEvents()
                );
                parcelLocationView.setIsLoading(true);
                break;
            }
            case COLLECT_FROM_PICKUP: {
                subscriptions = new CompositeSubscription(
                        getParcelActions(),
                        getPickupLocation(),
                        getParcelEvents()
                );
                parcelLocationView.setIsLoading(true);
                break;
            }
            case COLLECT_FROM_PICKUP_WITH_PASS: {
                subscriptions = new CompositeSubscription(
                        getParcelActions(),
                        getPickupLocation(),
                        getParcelEvents(),
                        getPickupPass()
                );
                parcelLocationView.setIsLoading(true);
                showPickupPass();
                break;
            }
            case SHOW_ESTIMATED_DELIVERY_DATE: {
                subscriptions = new CompositeSubscription(
                        getParcelActions(),
                        getParcelEvents()
                );
                // immediately show parcel location, no extra data download needed
                showParcelLocation();
                break;
            }
            case ISSUE: {
                subscriptions = new CompositeSubscription(
                        getParcelActions(),
                        getParcelEvents()
                );
                showParcelIssue();
                showParcelLocationView = false;
                break;
            }
            case DELIVERED: {
                subscriptions = new CompositeSubscription(
                        getParcelActions(),
                        getParcelEvents()
                );
                showParcelLocation();
                break;
            }
            default: {
                subscriptions = new CompositeSubscription(
                        getParcelActions(),
                        getParcelEvents()
                );
                showParcelLocationView = false;
                break;
            }
        }
        parcelLocationView.setVisibility(showParcelLocationView ? View.VISIBLE : View.GONE);
    }

    private void getParcel() {
        getParcelSubscription = api.getParcelDetails(parcelCode)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<GetParcelDetailsResponse>() {
                               @Override
                               public void call(GetParcelDetailsResponse response) {
                                   // Hide progress.
                                   if (swipeRefreshLayout.isRefreshing())
                                       swipeRefreshLayout.setRefreshing(false);
                                   else
                                       listener.onHideProgressBar();

                                   if (response.hasErrors()) {
                                       Log.e(TAG, "Parcel detail error: " + response.printErrors());
                                       if (BuildConfig.LOG_ERRORS_TO_SERVER && !BuildConfig.DEBUG) {
                                           Crashlytics.logException(new Exception(response.printErrors()));
                                       }
                                       final APIError error = response.getErrors().get(0);
                                       if (APIError.ErrorCode.RESOURCE_NOT_FOUND.toString().equals(error.errorCode)) {
                                           showParcelNotFoundDialog();
                                       } else {
                                           showUnableToLoadParcelDialog();
                                       }
                                       return;
                                   }

                                   parcel = response.getParcel();
                                   // ensure fully-populated parcel data available for further action Fragments
                                   dataStore.getParcelHistory().replaceParcel(parcelCode, parcel);
                                   getAdditionalParcelData();
                               }
                           },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                // Hide progress
                                if (swipeRefreshLayout.isRefreshing())
                                    swipeRefreshLayout.setRefreshing(false);
                                else
                                    listener.onHideProgressBar();

                                Log.e(TAG, "Parcel detail error", throwable);
                                if (BuildConfig.LOG_ERRORS_TO_SERVER && !BuildConfig.DEBUG) {
                                    Crashlytics.log("parcel detail error");
                                    Crashlytics.logException(throwable);
                                }
                                showUnableToLoadParcelDialog();
                            }
                        });
    }

    private Subscription getParcelActions() {
        return api
                .getParcelActions(parcel.parcelCode)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<GetParcelActionsResponse>() {
                               @Override
                               public void call(GetParcelActionsResponse parcelActionsResponse) {
                                   if (parcelActionsResponse.hasErrors()) {
                                       Log.e(TAG, "Error loading parcel actions: " + parcelActionsResponse.printErrors());
                                       if (BuildConfig.LOG_ERRORS_TO_SERVER && !BuildConfig.DEBUG) {
                                           Crashlytics.log(parcelActionsResponse.printErrors());
                                           Crashlytics.logException(new Exception("LoadingParcelActions"));
                                       }
                                       showParcelActionsErrorState(true);
                                   } else {
                                       showParcelActionsErrorState(false);
                                       setParcelActions(parcelActionsResponse.getParcelActions());
                                   }
                               }
                           },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                Log.e(TAG, "Error loading parcel actions: ", throwable);
                                if (BuildConfig.LOG_ERRORS_TO_SERVER && !BuildConfig.DEBUG) {
                                    Crashlytics.log("Unable to get parcel actions");
                                    Crashlytics.logException(throwable);
                                }
                                showParcelActionsErrorState(true);
                            }
                        });
    }

    private Subscription getPickupLocation() {
        return api
                .getPickupLocation(parcel.pickupLocationCode)
                .retry(3)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        new Action1<GetPickupLocationResponse>() {
                            @Override
                            public void call(GetPickupLocationResponse response) {
                                if (response.hasErrors()) {
                                    Log.e(TAG, String.format("Get pickup location error %s", response.printErrors()));
                                    if (BuildConfig.LOG_ERRORS_TO_SERVER && !BuildConfig.DEBUG) {
                                        Crashlytics.log(response.printErrors());
                                        Crashlytics.logException(new Exception("GetPickupLocation"));
                                    }
                                    showErrorAlertDialog(getString(R.string.problem_getting_pickup_location));
                                    return;
                                }
                                setPickupLocation(response.getPickupLocation());
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                Log.e(TAG, "Problem getting pickup location", throwable);
                                if (BuildConfig.LOG_ERRORS_TO_SERVER && !BuildConfig.DEBUG) {
                                    Crashlytics.log("Unable to get pickup location");
                                    Crashlytics.logException(throwable);
                                }
                                showErrorAlertDialog(getString(R.string.problem_getting_pickup_location));
                            }
                        });
    }

    private Subscription getPickupPass() {
        return api
                .getPickupPass(parcel.parcelCode)
                .retry(3)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        new Action1<GetPickupPassResponse>() {
                            @Override
                            public void call(GetPickupPassResponse response) {
                                if (response.hasErrors()) {
                                    Log.e(TAG, String.format("Get pickup pass error %s", response.printErrors()));
                                    if (BuildConfig.LOG_ERRORS_TO_SERVER && !BuildConfig.DEBUG) {
                                        Crashlytics.log(response.printErrors());
                                        Crashlytics.logException(new Exception("GetPickupPass"));
                                    }
                                    showErrorAlertDialog(getString(R.string.problem_getting_pickup_pass));
                                    return;
                                }
                                setPickupPassImage(response.getImageBase64());
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                Log.e(TAG, "Unable to get/parse pickup pass", throwable);
                                if (BuildConfig.LOG_ERRORS_TO_SERVER && !BuildConfig.DEBUG) {
                                    Crashlytics.log("Unable to get pickup pass");
                                    Crashlytics.logException(throwable);
                                }
                                showErrorAlertDialog(getString(R.string.problem_getting_pickup_pass));
                            }
                        });
    }

    private Subscription getDepot() {
        return api
                .getDepot(parcel.deliveryDepot.depotCode)
                .retry(3)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<GetDepotResponse>() {
                               @Override
                               public void call(GetDepotResponse response) {
                                   if (response.hasErrors()) {
                                       Log.e(TAG, String.format("Get depot error %s", response.printErrors()));
                                       showErrorAlertDialog(getString(R.string.problem_getting_depot_directions));
                                       return;
                                   }
                                   setDepot(response.getDepot());
                               }
                           },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                Log.e(TAG, "Unable to get/parse depot", throwable);
                                if (BuildConfig.LOG_ERRORS_TO_SERVER && !BuildConfig.DEBUG) {
                                    Crashlytics.log("Unable to get depot");
                                    Crashlytics.logException(throwable);
                                }
                                showErrorAlertDialog(getString(R.string.problem_getting_depot_directions));
                            }
                        });
    }

    private Subscription getRouteView() {
        final String depotCode = parcel.deliveryDepot.depotCode;
        final String routeKey = parcel.deliveryDepot.route.routeCode;
        return api.getRouteViewOnRoute(depotCode, routeKey)
                .retry(3)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<GetRouteViewOnRouteResponse>() {
                               @Override
                               public void call(GetRouteViewOnRouteResponse response) {
                                   if (response.hasErrors()) {
                                       Log.e(TAG, "Error loading route view: " + response.printErrors());
                                       showRouteViewError();
                                       return;
                                   }

                                   RouteView routeView = response.getRouteView();
                                   if (routeView.driverPhotoId != null) {
                                       setRouteView(routeView, false);
                                       subscriptions.add(getDriverPhoto(routeView.driverPhotoId));
                                   } else {
                                       setRouteView(routeView, true);
                                   }
                               }
                           },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                Log.e(TAG, "Error loading route view", throwable);
                                showRouteViewError();
                            }
                        });
    }

    private Subscription getParcelEvents() {
        return api
                .getParcelEvents(parcel.parcelCode)
                .retry(3)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<GetParcelEventsResponse>() {
                               @Override
                               public void call(GetParcelEventsResponse response) {
                                   if (response.hasErrors()) {
                                       Log.e(TAG, "Error loading parcel events: " + response.printErrors());
                                       if (BuildConfig.LOG_ERRORS_TO_SERVER && !BuildConfig.DEBUG) {
                                           Crashlytics.log(response.printErrors());
                                           Crashlytics.logException(new Exception("GetParcelEvents"));
                                       }
                                       showParcelEventsErrorState(true);
                                   } else {
                                       showParcelEventsErrorState(false);
                                       setParcelEvents(response.getParcelEvents());
                                       if (parcel.parcelStatusType.equals(ParcelStatusType.DELIVERED)) {
                                           subscriptions.add(getPhotos());
                                       }
                                   }
                               }
                           },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                Log.e(TAG, "Error loading parcel events: ", throwable);
                                showParcelEventsErrorState(true);
                            }
                        });
    }

    private Subscription getDriverPhoto(String driverPhotoId) {
        return api
                .getRackspaceImage(driverPhotoId, ImageType.DRIVER)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<GetImageResponse>() {
                               @Override
                               public void call(GetImageResponse response) {
                                   if (response.hasErrors()) {
                                       if (BuildConfig.LOG_ERRORS_TO_SERVER && !BuildConfig.DEBUG) {
                                           Crashlytics.log(response.printErrors());
                                           Crashlytics.logException(new Exception("LoadDriverPhoto"));
                                       }
                                       setRouteViewWithDriver(null);
                                   } else
                                       setRouteViewWithDriver(response.getImage());
                               }
                           },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                Log.e(TAG, "Error loading driver photo", throwable);
                                if (BuildConfig.LOG_ERRORS_TO_SERVER && !BuildConfig.DEBUG) {
                                    Crashlytics.logException(throwable);
                                }
                                setRouteViewWithDriver(null);
                            }
                        });
    }

    private Subscription getPhotos() {
        final List<Photo> photos = new ArrayList<>(parcelEvents.size());
        for (ParcelEvent parcelEvent : parcelEvents) {
            if (parcelEvent.imageKey != null && parcelEvent.imageKey.length > 0) {
                for (int i = 0; i < parcelEvent.imageKey.length; i++) {
                    if (!TextUtils.isEmpty(parcelEvent.imageKey[i]) &&
                            parcelEvent.imageType.length > i &&
                            parcelEvent.imageType[i].equals(ImageType.SATURN_SAFE_PLACE)) {
                        final Photo photo = new Photo(
                                parcelEvent.parcelCode,
                                parcelEvent.imageKey[i],
                                parcelEvent.imageType[i],
                                parcelEvent.imageCaption[i]
                        );

                        photos.add(photo);
                    }
                }
            }
        }

        return Observable.from(photos)
                .subscribeOn(Schedulers.io())
                .flatMap(new Func1<Photo, Observable<Base64Photo>>() {
                    @Override
                    public Observable<Base64Photo> call(Photo photo) {
                        return Observable.zip(
                                Observable.just(photo),
                                api.getRackspaceImage(photo.imageKey, ImageType.SATURN_SAFE_PLACE),
                                new Func2<Photo, GetImageResponse, Base64Photo>() {
                                    @Override
                                    public Base64Photo call(Photo photo, GetImageResponse response) {
                                        if (response.hasErrors()) {
                                            if (BuildConfig.LOG_ERRORS_TO_SERVER && !BuildConfig.DEBUG) {
                                                Crashlytics.log(response.printErrors());
                                                Crashlytics.logException(new Exception("LoadDeliveryImage"));
                                            }
                                            return null;
                                        }
                                        return new Base64Photo(
                                                response.getImage(), // base64Data string
                                                photo.imageKey,
                                                TextUtils.isEmpty(photo.imageCaption) ? "" : photo.imageCaption
                                        );
                                    }
                                }
                        );
                    }
                })
                .toList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<List<Base64Photo>>() {
                               @Override
                               public void call(List<Base64Photo> photos) {
                                   setDeliveryPhotos(photos);
                               }
                           },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                Log.e(TAG, "Error loading delivery images", throwable);
                                if (BuildConfig.LOG_ERRORS_TO_SERVER && !BuildConfig.DEBUG) {
                                    Crashlytics.logException(throwable);
                                }
                                setDeliveryPhotos(null);
                            }
                        });
    }


    private void getLastKnownLocation() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.w(TAG, "Unable to get lastKnownLocation, user has disabled permission");
            setLastKnownLocation(null);
            return;
        }

        final GoogleApiClientActivity activity = (GoogleApiClientActivity) getActivity();
        final GoogleApiClient googleApiClient = activity.getGoogleApiClient();

        if (googleApiClient.hasConnectedApi(LocationServices.API)) {
            setLastKnownLocation(LocationServices.FusedLocationApi.getLastLocation(googleApiClient));
        } else {
            // wait for GoogleApiClient to connect or fail before continuing
            googleApiClient.registerConnectionCallbacks(googleApiClientConnectionCallbacks);
            googleApiClient.registerConnectionFailedListener(googleApiClientConnectionFailedCallbacks);
        }
    }

    private GoogleApiClient.ConnectionCallbacks googleApiClientConnectionCallbacks =
            new GoogleApiClient.ConnectionCallbacks() {
                @Override
                public void onConnected(Bundle bundle) {
                    // try getting last known location again
                    getLastKnownLocation();
                }

                @Override
                public void onConnectionSuspended(int i) {
                }
            };
    private GoogleApiClient.OnConnectionFailedListener googleApiClientConnectionFailedCallbacks =
            new GoogleApiClient.OnConnectionFailedListener() {
                @Override
                public void onConnectionFailed(ConnectionResult connectionResult) {
                    // continue without knowing user's location
                    Log.w(TAG, "Unable to connect to Google API Client, errorCode: " + connectionResult.getErrorCode());
                }
            };

    // TODO: enhancement - it's currently awkward to require X pieces of data to be loaded before initializing the parcel location view
    // we need to co-ordinate the response to loading async data better
    private void setLastKnownLocation(Location lastLocation) {
        lastKnownLocation = lastLocation;
        if (this.routeView != null) {
            showParcelLocation();
        }
    }

    private void setRouteView(RouteView routeView, boolean justShow) {
        this.routeView = routeView;
        if (parcel.parcelStatusType.equals(ParcelStatusType.OUT_FOR_DELIVERY) || this.depot != null) {
            if (justShow)
                showParcelLocation();
        }
    }

    private void setRouteViewWithDriver(String image) {
        this.routeDriverImage = image;
        if (this.routeView != null) {
            showParcelLocation();
        }
    }

    private void setDepot(Depot depot) {
        this.depot = depot;
        if (parcel.parcelStatusType.equals(ParcelStatusType.COLLECT_FROM_DEPOT) || this.routeView != null) {
            showParcelLocation();
        }
    }

    private void setPickupLocation(PickupLocation pickupLocation) {
        this.pickupLocation = pickupLocation;
        showPickupLocationDetails();
        showParcelLocation();
    }

    private void setPickupPassImage(String imageBase64) {
        pickupPassView.setBarcodeImage(imageBase64);
    }

    private void setDeliveryPhotos(List<Base64Photo> photos) {
        if (photos != null) {
            Collections.sort(photos, new Comparator<Base64Photo>() {
                @Override
                public int compare(Base64Photo lhs, Base64Photo rhs) {
                    if (lhs == null || rhs == null || lhs.imageKey == null || rhs.imageKey == null)
                        return 0;
                    return lhs.imageKey.compareToIgnoreCase(rhs.imageKey);
                }
            });
        }
        showDeliveryPhotos(photos);
    }

    private void showParcelStatus() {
        final Context context = getContext();
        UIUtils.populateParcelStatus(parcelStatusView, context, parcel);
    }

    /**
     * Add messages such as proof of ID prompts where required
     */
    private void showUserMessages() {
        final LayoutInflater inflater = LayoutInflater.from(getContext());
        userMessagesView.removeAllViews();
        if (parcel.parcelStatusType.equals(ParcelStatusType.COLLECT_FROM_DEPOT)) {
            inflater.inflate(R.layout.user_message_proof_of_id_depot, userMessagesView, true);

        } else if (parcel.parcelStatusType.equals(ParcelStatusType.COLLECT_FROM_PICKUP)) {
            inflater.inflate(R.layout.user_message_pickup_pass_not_ready, userMessagesView, true);
            inflater.inflate(R.layout.user_message_photo_also_id, userMessagesView, true);

        } else if (parcel.parcelStatusType.equals(ParcelStatusType.COLLECT_FROM_PICKUP_WITH_PASS)) {
            inflater.inflate(R.layout.user_message_photo_id, userMessagesView, true);
        }

        userMessagesView.setVisibility(userMessagesView.getChildCount() > 0 ? View.VISIBLE : View.GONE);
    }

    private void hideParcelIssue() {
        parcelIssueTooltip.setVisibility(View.GONE);
    }

    private void showParcelIssue() {
        // show delayed tooltip and hide location
        String formattedDate = "";
        try {
            final DateTimeFormatter dateFormat = DateTimeFormat.forPattern("dd/MM/yyyy");
            formattedDate = dateFormat.print(parcel.getEstimatedDeliveryDate());
        } catch (Exception e) {
            Log.e(TAG, "Unable to parse parcel.estimatedDeliveryDate", e);
        } finally {
            if (!formattedDate.equals("")) {
                parcelIssueTooltip.setText(getString(R.string.parcel_delayed_message,
                        parcel.getIssueName().toLowerCase(),
                        formattedDate));
            } else {
                parcelIssueTooltip.setText(getString(R.string.parcel_delayed_message_no_date,
                        parcel.getIssueName().toLowerCase()));
            }
        }

        try {
            parcelIssueTooltip.setIcon(parcel.issueDetails.getIssueIconResId());
        } catch (Exception e) {
            Log.e(TAG, "Unable to set issue icon", e);
        }

        parcelIssueTooltip.setVisibility(View.VISIBLE);
    }

    private void hidePickupPass() {
        pickupPassView.setVisibility(View.GONE);
    }

    private void showPickupPass() {
        pickupPassView.setParcel(parcel);
        pickupPassView.setVisibility(View.VISIBLE);
    }

    private void hidePickupLocationDetails() {
        pickupLocationDetailsView.setVisibility(View.GONE);
    }

    private void showPickupLocationDetails() {
        pickupLocationDetailsView.setPickupLocation(pickupLocation);
        pickupLocationDetailsView.setVisibility(View.VISIBLE);
    }

    private void hideDeliveryPhotos() {
        deliveryPhotosView.setVisibility(View.GONE);
    }

    private void showDeliveryPhotos(List<Base64Photo> photos) {
        if (photos != null && photos.size() > 0) {
            deliveryPhotosView.setVisibility(View.VISIBLE);
            deliveryPhotosView.setPhotos(photos, user.getDpdSession());
        } else {
            deliveryPhotosView.setVisibility(View.GONE);
        }
    }

    /**
     * This is called once we have loaded both the Depot and RouteView data
     *
     * @see #setDepot(Depot)
     * @see #setRouteView(RouteView, boolean)
     */
    private void showParcelLocation() {
        parcelLocationView.setVisibility(View.VISIBLE);
        parcelLocationView.setUser(user);
        parcelLocationView.setIsLoading(false);
        parcelLocationView.setFragmentManager(getChildFragmentManager());
        parcelLocationView.setUserLocation(lastKnownLocation);

        switch (parcel.parcelStatusType) {
            case DELIVERED: {
                parcelLocationView.showDelivered(parcel);
                break;
            }
            case OUT_FOR_DELIVERY: {
                parcelLocationView.showOutForDelivery(parcel, routeView, routeDriverImage);
                break;
            }
            case COLLECT_FROM_DEPOT: {
                parcelLocationView.showDepot(parcel, depot, routeView);
                break;
            }
            case COLLECT_FROM_PICKUP:
            case COLLECT_FROM_PICKUP_WITH_PASS: {
                parcelLocationView.showPickupLocation(parcel, pickupLocation);
                break;
            }
            case SHOW_ESTIMATED_DELIVERY_DATE: {
                parcelLocationView.showEstimatedDeliveryDate(parcel);
                break;
            }
        }
    }

    private void setParcelActions(List<ParcelAction> actions) {
        // map action ID string to action button view
        final HashMap<String, View> actionButtons = new HashMap<>();
        actionButtons.put(ParcelActionCodes.CHANGE_DELIVERY_DATE.getValue(), parcelActionsView.findViewById(R.id.action_change_date_button));
        actionButtons.put(ParcelActionCodes.COLLECT_FROM_PICKUP_SHOP.getValue(), parcelActionsView.findViewById(R.id.action_collect_from_pickup_shop_button));
        actionButtons.put(ParcelActionCodes.COLLECT_FROM_DEPOT.getValue(), parcelActionsView.findViewById(R.id.action_collect_from_depot_button));
        actionButtons.put(ParcelActionCodes.LEAVE_IN_SAFE_PLACE.getValue(), parcelActionsView.findViewById(R.id.action_delivery_to_safe_place));
        actionButtons.put(ParcelActionCodes.DELIVER_TO_NEIGHBOUR.getValue(), parcelActionsView.findViewById(R.id.action_delivery_to_neighbour_button));
        actionButtons.put(ParcelActionCodes.UPGRADE_DELIVERY.getValue(), parcelActionsView.findViewById(R.id.action_upgrade_button));

        // remove all actions
        final ViewGroup buttons = (ViewGroup) parcelActionsView.findViewById(R.id.parcel_action_buttons);
        for (Map.Entry entry : actionButtons.entrySet()) {
            View button = (View) entry.getValue();
            button.setVisibility(View.GONE);
            button.setOnClickListener(this);
        }

        // show available actions
        for (ParcelAction action : actions) {
            for (Map.Entry entry : actionButtons.entrySet()) {
                if (entry.getKey().equals(action.parcelActionId)) {
                    View button = (View) entry.getValue();
                    button.setVisibility(View.VISIBLE);
                    buttons.removeView(button);
                    buttons.addView(button, buttons.getChildCount() - 1);
                }
            }
        }

        // if there are no actions, hide entire view
        parcelActionsView.setVisibility(actions.size() > 0 ? View.VISIBLE : View.GONE);
    }

    private void setParcelEvents(List<ParcelEvent> events) {
        parcelEvents = events;
        showParcelEvents();
    }

    private void showParcelEvents() {
        final Context context = getActivity();
        final View view = getView();
        assert view != null;
        final LayoutInflater inflater = LayoutInflater.from(context);
        final TableLayout tableLayout = (TableLayout) view.findViewById(R.id.parcel_events_tablelayout);

        tableLayout.removeAllViews();

        if (parcelEvents == null) {
            Log.w(TAG, "Parcel events null, cannot be displayed");
            return;
        } else {
            TableRow tableTitle = (TableRow) inflater.inflate(R.layout.delivery_parcel_history_row, tableLayout, false);
            TextView date_time = (TextView) tableTitle.findViewById(R.id.parcel_history_date_and_time_textview);
            date_time.setTypeface(null, Typeface.BOLD);
            TextView location = (TextView) tableTitle.findViewById(R.id.parcel_history_location_textview);
            location.setTypeface(null, Typeface.BOLD);
            TextView description = (TextView) tableTitle.findViewById(R.id.parcel_history_description_textview);
            description.setTypeface(null, Typeface.BOLD);
            tableLayout.addView(tableTitle);
        }

        for (int i = 0; i < parcelEvents.size(); i++) {
            ParcelEvent event = parcelEvents.get(i);
            final TableRow row = (TableRow) inflater.inflate(R.layout.delivery_parcel_history_row, tableLayout, false);
            TextView dateTimeTextView = (TextView) row.findViewById(R.id.parcel_history_date_and_time_textview);
            TextView locationTextView = (TextView) row.findViewById(R.id.parcel_history_location_textview);
            TextView descriptionTextView = (TextView) row.findViewById(R.id.parcel_history_description_textview);
            try {
                dateTimeTextView.setText(outFormatter.print(inFormatter.parseDateTime(event.eventDate)));
            } catch (Exception e) {
                Log.e(TAG, "Unable to parse Date in Parcel events", e);
                Crashlytics.logException(e);
            }
            locationTextView.setText(event.eventLocation);
            descriptionTextView.setText(UIUtils.formatText(event.eventText, getActivity()));
            tableLayout.addView(row);
        }
    }

    @Override
    public void onClick(View v) {
        final String parcelCode = parcel.parcelCode;
        switch (v.getId()) {
            case R.id.expand_parcel_actions_button: {
                final View view = getView();
                assert view != null;
                final ViewGroup buttons = (ViewGroup) view.findViewById(R.id.parcel_action_buttons);
                final boolean isVisible = buttons.getVisibility() == View.VISIBLE;
                final AutoTransition transition = new AutoTransition(view.getContext(), null);
                transition.addListener(new Transition.TransitionListenerAdapter() {
                    @Override
                    public void onTransitionEnd(Transition transition) {
                        expandParcelActionsButton.setOnClickListener(ParcelDetailActivityFragment.this);
                    }
                });
                expandParcelActionsButton.setOnClickListener(null);
                TransitionManager.beginDelayedTransition((ViewGroup) view, transition);
                buttons.setVisibility(isVisible ? View.GONE : View.VISIBLE);
                expandParcelActionsButton.setCompoundDrawablesWithIntrinsicBounds(0, 0, isVisible ? R.drawable.ic_plus : R.drawable.ic_minus, 0);
                break;
            }
            case R.id.expand_parcel_events_button: {
                final View view = getView();
                assert view != null;
                final ViewGroup eventsView = (ViewGroup) view.findViewById(R.id.parcel_events_container);
                final boolean isVisible = eventsView.getVisibility() == View.VISIBLE;
//                final AutoTransition transition = new AutoTransition(view.getContext(), null);
                if (!isVisible) {
//                    UIUtils.smoothScrollToViewAfterTransition(scrollView, eventsView, transition);
                }
//                TransitionManager.beginDelayedTransition((ViewGroup) view, transition);
                eventsView.setVisibility(isVisible ? View.GONE : View.VISIBLE);
                expandParcelEventsButton.setCompoundDrawablesWithIntrinsicBounds(
                        0, 0, isVisible ? R.drawable.ic_plus : R.drawable.ic_minus, 0);
                break;
            }
            case R.id.action_collect_from_pickup_shop_button: {
                // NOTE: we are adding the Fragment atop the previous to prevent re-creation of
                // the detail Fragment
                final FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.add(R.id.container, InFlightCollectFromPickupShopFragment.newInstance(parcelCode));
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                transaction.addToBackStack(null);
                transaction.commit();
                break;
            }
            case R.id.action_collect_from_depot_button: {
                // NOTE: we are adding the Fragment atop the previous to prevent re-creation of
                // the detail Fragment
                final FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.add(R.id.container, InFlightCollectFromDepotFragment.newInstance(parcelCode));
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                transaction.addToBackStack(null);
                transaction.commit();
                break;
            }
            case R.id.action_delivery_to_neighbour_button: {
                final FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.add(R.id.container, InFlightDeliverToNeighbourFragment.newInstance(parcelCode));
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                transaction.addToBackStack(null);
                transaction.commit();
                break;
            }
            case R.id.action_delivery_to_safe_place: {
                final FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.add(R.id.container, InFlightLeaveInSafePlaceFragment.newInstance(parcelCode));
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                transaction.addToBackStack(null);
                transaction.commit();
                break;
            }
            case R.id.action_change_date_button: {
                final FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.add(R.id.container, InFlightChangeDeliveryDateFragment.newInstance(parcelCode));
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                transaction.addToBackStack(null);
                transaction.commit();
                break;
            }
            case R.id.action_upgrade_button: {
                final FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.add(R.id.container, InFlightUpgradeDeliveryFragment.newInstance(parcelCode));
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                transaction.addToBackStack(null);
                transaction.commit();
                break;
            }
        }
    }

    private void showUnableToLoadParcelDialog() {
        showErrorAlertWithRetryOption(
                getString(R.string.a_problem_has_occurred_try_again),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        reloadParcel(false);
                    }
                });
    }

    private void showParcelNotFoundDialog() {
        /*final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AlertDialogTheme);
        builder.setTitle(getString(R.string.parcel_not_found));
        builder.setMessage(getString(R.string.parcel_not_found_message, parcelCode));
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getActivity().finish();
            }
        });
        builder.setCancelable(false);
        builder.create().show();*/

        final Intent intent = new Intent(getActivity(), YourDeliveriesActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("parcelCode", parcelCode != null ? parcelCode : "");
        startActivity(intent);
        getActivity().finish();
    }

    private void showParcelActionsErrorState(boolean flag) {
        // hide all the buttons and display the error message
        final ViewGroup buttons = (ViewGroup) parcelActionsView.findViewById(R.id.parcel_action_buttons);
        int childCount = buttons.getChildCount();
        for (int i = 0; i < childCount; i++) {
            buttons.getChildAt(i).setVisibility(flag ? View.GONE : View.VISIBLE);
        }
        parcelActionsView.findViewById(R.id.problem_loading_actions_textview).setVisibility(flag ? View.VISIBLE : View.GONE);
        expandParcelActionsButton.setEnabled(true);
    }

    private void showRouteViewError() {
        // TODO: enhancement - better implementation than a dialog
        showErrorAlertDialog("There was a problem loading your parcel's route information.");
    }

    private void showParcelEventsErrorState(boolean flag) {
        View view = getView();
        assert view != null;
        view.findViewById(R.id.parcel_events_tablelayout).setVisibility(flag ? View.GONE : View.VISIBLE);
        view.findViewById(R.id.problem_loading_events_textview).setVisibility(flag ? View.VISIBLE : View.GONE);
        expandParcelEventsButton.setEnabled(true);
    }
}
