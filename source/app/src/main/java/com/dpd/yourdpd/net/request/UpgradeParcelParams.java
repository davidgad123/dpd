package com.dpd.yourdpd.net.request;

import android.text.TextUtils;

/**
 * @see com.dpd.yourdpd.net.Api#upgradeParcel(UpgradeParcelParams)
 */
public class UpgradeParcelParams {
    public String contactName;
    public String email;
    public String mobile;
    public String parcelCode[];
    public String orderId;

    public String diaryDate;
    public String upgradeId;
    public int paymentVendor;
    public String vendorRef;

    private UpgradeParcelParams() {
        // use Builder to construct
    }

    public static class Builder {
        private UpgradeParcelParams params = new UpgradeParcelParams();

        public Builder setContactName(String contactName) {
            if (contactName == null) {
                throw new IllegalArgumentException("contactName cannot be null");
            }
            params.contactName = contactName.substring(0, Math.min(35, contactName.length())); // limit to 35 chars
            return this;
        }

        public Builder setUpgradeId(String upgradeId) {
            params.upgradeId = upgradeId;
            return this;
        }

        public Builder setOrderId(String orderId) {
            params.orderId = orderId;
            return this;
        }

        /**
         * @param vendorRef e.g. PayPal's paymentTransactionId
         */
        public Builder setVendorRef(String vendorRef) {
            params.vendorRef = vendorRef;
            return this;
        }

        public Builder setPaymentVendor(int paymentVendor) {
            params.paymentVendor = paymentVendor;
            return this;
        }

        public Builder setDiaryDate(String diaryDate) {
            params.diaryDate = diaryDate;
            return this;
        }

        public Builder setEmail(String email) {
            params.email = email;
            return this;
        }

        public Builder setMobile(String mobile) {
            params.mobile = mobile;
            return this;
        }

        public Builder setParcelCode(String parcelCode) {
            params.parcelCode = new String[]{parcelCode};
            return this;
        }

        public UpgradeParcelParams build() {
            if (TextUtils.isEmpty(params.upgradeId)) {
                throw new IllegalStateException("upgradeId cannot be null or empty");
            }
            if (params.parcelCode == null || TextUtils.isEmpty(params.parcelCode[0])) {
                throw new IllegalStateException("parcelCode cannot be null or empty");
            }
            if (TextUtils.isEmpty(params.vendorRef)) {
                throw new IllegalStateException("vendorRef cannot be null or empty");
            }
            if (TextUtils.isEmpty(params.upgradeId)) {
                throw new IllegalStateException("upgradeId cannot be null or empty");
            }
//            if(TextUtils.isEmpty(params.paymentVendor)) {
//                throw new IllegalStateException("paymentVendor cannot be null or empty");
//            }
            if (TextUtils.isEmpty(params.diaryDate)) {
                throw new IllegalStateException("diaryDate cannot be null or empty");
            }
            if (TextUtils.isEmpty(params.orderId)) {
                throw new IllegalStateException("orderId cannot be null or empty");
            }
            return params;
        }
    }
}
