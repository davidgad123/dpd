package com.dpd.yourdpd.model;

import java.io.Serializable;

/**
 * Represents a photo that is stored as a base64Data string
 */
public class Base64Photo implements Serializable {
    public String imageKey;
    public String base64Data;
    public String caption;

    public Base64Photo(String base64Data, String imageKey, String caption) {
        this.base64Data = base64Data;
        this.imageKey = imageKey;
        this.caption = caption;
    }

//    protected Base64Photo(Parcel in) {
//        imageKey = in.readString();
//        base64Data = in.readString();
//        caption = in.readString();
//    }
//
//    public static final Creator<Base64Photo> CREATOR = new Creator<Base64Photo>() {
//        @Override
//        public Base64Photo createFromParcel(Parcel in) {
//            return new Base64Photo(in);
//        }
//
//        @Override
//        public Base64Photo[] newArray(int size) {
//            return new Base64Photo[size];
//        }
//    };
//
//    @Override
//    public int describeContents() {
//        return 0;
//    }
//
//    @Override
//    public void writeToParcel(Parcel dest, int flags) {
//        dest.writeString(imageKey);
//        dest.writeString(base64Data);
//        dest.writeString(caption);
//    }
}
