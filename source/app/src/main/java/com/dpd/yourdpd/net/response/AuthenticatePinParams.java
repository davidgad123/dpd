package com.dpd.yourdpd.net.response;

import com.google.gson.annotations.SerializedName;

/**
 * @see com.dpd.yourdpd.net.Api#authenticatePin(AuthenticatePinParams)
 */
public class AuthenticatePinParams {
    public String pinNumber;
    public String deviceId;
    public String medium;
    public String mediumValue;
}
