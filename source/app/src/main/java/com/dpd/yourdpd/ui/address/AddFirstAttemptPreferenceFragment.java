package com.dpd.yourdpd.ui.address;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.transitions.everywhere.TransitionManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dpd.yourdpd.R;
import com.dpd.yourdpd.model.ConsumerProperty;
import com.dpd.yourdpd.model.PickupLocation;
import com.dpd.yourdpd.model.UDPRNAddress;
import com.dpd.yourdpd.net.response.GetAddressByUDPRNResponse;
import com.dpd.yourdpd.net.response.GetPickupLocationResponse;
import com.dpd.yourdpd.ui.BaseFragment;
import com.dpd.yourdpd.ui.BaseMenuActivity;
import com.dpd.yourdpd.ui.Validating;
import com.dpd.yourdpd.ui.widget.CheckableImageView;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func2;
import rx.schedulers.Schedulers;

/**
 * Allow user to specify whether deliveries should go first to their home or their chosen shop
 */
public class AddFirstAttemptPreferenceFragment extends BaseFragment
        implements Validating {
    private static final String TAG = AddFirstAttemptPreferenceFragment.class.getSimpleName();

    private View deliveryPrefsContentView;
    private View homeView;
    private View shopView;
    private CheckableImageView homeRadioButton;
    private CheckableImageView shopRadioButton;
    private ContentLoadingProgressBar progressBar;

    private ConsumerProperty consumerProperty;
    private Subscription getDataSubscription;
    private UDPRNAddress homeAddress;
    private PickupLocation pickupLocation;

    public static AddFirstAttemptPreferenceFragment newInstance() {
        final Bundle args = new Bundle();
        final AddFirstAttemptPreferenceFragment fragment = new AddFirstAttemptPreferenceFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public AddFirstAttemptPreferenceFragment() {
        // Required empty constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        consumerProperty = user.selectedConsumerProperty;
        if (consumerProperty == null) {
            throw new IllegalStateException("ConsumerProfile.selectedConsumerProperty must not be null");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.fragment_add_first_attempt_preference, container, false);
        deliveryPrefsContentView = root.findViewById(R.id.delivery_prefs_content);
        homeView = root.findViewById(R.id.home);
        shopView = root.findViewById(R.id.shop);
        homeRadioButton = (CheckableImageView) homeView.findViewById(R.id.home_radio);
        shopRadioButton = (CheckableImageView) shopView.findViewById(R.id.shop_radio);
        homeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeRadioButton.setChecked(true);
                shopRadioButton.setChecked(false);
                homeView.findViewById(R.id.home_additional_info).setVisibility(View.VISIBLE);
                shopView.findViewById(R.id.pickup_shop_additional_info).setVisibility(View.GONE);
            }
        });
        shopView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shopRadioButton.setChecked(true);
                homeRadioButton.setChecked(false);
                shopView.findViewById(R.id.pickup_shop_additional_info).setVisibility(View.VISIBLE);
                homeView.findViewById(R.id.home_additional_info).setVisibility(View.GONE);
            }
        });
        progressBar = (ContentLoadingProgressBar) root.findViewById(R.id.progressBar);
        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(getString(R.string.first_attempt_preference_titlecase));

        deliveryPrefsContentView.setVisibility(View.GONE);
        progressBar.show();

        getData();
    }


    @Override
    public void onPause() {
        super.onPause();
        if (getDataSubscription != null && !getDataSubscription.isUnsubscribed()) {
            getDataSubscription.unsubscribe();
        }
        getDataSubscription = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        consumerProperty = null;
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            ((BaseMenuActivity) getActivity()).setupToolbar(getString(R.string.first_attempt_preference_titlecase), true);
        }
    }

    private void getData() {
        getDataSubscription =
                Observable.zip(api.getAddressByUDPRN(consumerProperty.consumerPropertyId),
                        api.getPickupLocation(consumerProperty.pickupLocationCode),
                        new Func2<GetAddressByUDPRNResponse, GetPickupLocationResponse, Throwable>() {
                            @Override
                            public Throwable call(GetAddressByUDPRNResponse response,
                                                  GetPickupLocationResponse response2) {
                                if (response.hasErrors()) {
                                    Log.e(TAG, "getAddressByUDPRN error: " + response.printErrors());
                                    return new Throwable(response.printErrors());
                                } else {
                                    setHomeAddress(response.address);
                                }
                                if (response2.hasErrors()) {
                                    Log.e(TAG, "getPickupLocation error: " + response2.printErrors());
                                    return new Throwable(response2.printErrors());
                                } else {
                                    setPickupLocation(response2.getPickupLocation());
                                }
                                // all OK, return no error
                                return null;
                            }
                        })
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                new Action1<Throwable>() {
                                    @Override
                                    public void call(Throwable error) {
                                        onDataLoaded(error);
                                    }
                                },
                                new Action1<Throwable>() {
                                    @Override
                                    public void call(Throwable error) {
                                        onDataLoaded(error);
                                    }
                                });
    }

    private void setHomeAddress(UDPRNAddress address) {
        this.homeAddress = address;
    }

    private void setPickupLocation(PickupLocation pickupLocation) {
        this.pickupLocation = pickupLocation;
    }

    private void onDataLoaded(Throwable error) {
        if (error != null) {
            Log.e(TAG, "Problem loading data", error);
        }

        progressBar.hide();

        if (homeAddress != null) {
            showHomeAddressDetails(homeAddress);
        } else {
            homeView.setVisibility(View.GONE);
            showErrorAlertWithRetryOption(getString(R.string.problem_getting_address_retry),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            getData();
                        }
                    });
        }

        if (pickupLocation != null) {
            showPickupLocationDetails(pickupLocation);
        } else {
            shopView.setVisibility(View.GONE);
            showErrorAlertWithRetryOption(getString(R.string.problem_getting_pickup_location_retry),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            getData();
                        }
                    });
        }

        // select user's choice (default to Home if not set)
        if (FirstAttemptType.HOME.equals(consumerProperty.firstAttemptType) && homeView.getVisibility() == View.VISIBLE) {
            homeView.performClick();
        } else if (FirstAttemptType.SHOP.equals(consumerProperty.firstAttemptType) && shopView.getVisibility() == View.VISIBLE) {
            shopView.performClick();
        } else {
            homeView.performClick();
        }

        TransitionManager.beginDelayedTransition((ViewGroup) deliveryPrefsContentView);
        deliveryPrefsContentView.setVisibility(View.VISIBLE);
    }

    private void showHomeAddressDetails(UDPRNAddress address) {
        ((TextView) homeView.findViewById(R.id.home_name_textview)).setText(consumerProperty.propertyLabel);
        ((TextView) homeView.findViewById(R.id.home_address_textview)).setText(address.getFirstLineWithTown());
    }

    private void showPickupLocationDetails(PickupLocation pickupLocation) {
        ((TextView) shopView.findViewById(R.id.shop_name_textview)).setText(pickupLocation.getName());
        ((TextView) shopView.findViewById(R.id.shop_address_textview)).setText(pickupLocation.address.getStreetAndTown());
    }

    @Override
    public boolean validate() {
        if (shopRadioButton.isChecked() || homeRadioButton.isChecked()) {
            return true;
        }
        showErrorAlertDialog(getString(R.string.please_select_address_or_pickup_shop));
        return false;
    }

    @Override
    public void applyChanges() {
        consumerProperty.firstAttemptType = shopRadioButton.isChecked() ? FirstAttemptType.SHOP : FirstAttemptType.HOME;
        if (getActivity() instanceof AddAddressActivity)
            ((AddAddressActivity) getActivity()).setHomeAddress(homeAddress);
    }
}
