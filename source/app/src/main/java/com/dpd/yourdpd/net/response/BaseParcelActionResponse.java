package com.dpd.yourdpd.net.response;

import com.google.gson.annotations.SerializedName;

/**
 * Base class for Parcel action API responses
 */
public abstract class BaseParcelActionResponse extends BaseResponse {
    @SerializedName("data")
    protected ParcelCodeWrapper parcelCodeWrapper;

    public String getParcelCode() {
        if(parcelCodeWrapper.parcelCode != null && parcelCodeWrapper.parcelCode.length>0) {
            return parcelCodeWrapper.parcelCode[0];
        }
        return null;
    }

    protected class ParcelCodeWrapper {
        private String[] parcelCode;
    }
}
