package com.dpd.yourdpd.net.response;

import com.dpd.yourdpd.net.request.ChangeParcelDateParams;

/**
 * @see com.dpd.yourdpd.net.Api#changeParcelDate(ChangeParcelDateParams)
 */
public class ChangeParcelDateResponse extends BaseParcelActionResponse {
}
