package com.dpd.yourdpd.ui.address;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.dpd.yourdpd.BuildConfig;
import com.dpd.yourdpd.R;
import com.dpd.yourdpd.model.Address;
import com.dpd.yourdpd.net.response.GetAddressByUDPRNResponse;
import com.dpd.yourdpd.net.response.LookupPostKeyResponse;
import com.dpd.yourdpd.net.response.SearchByAddressResponse;
import com.dpd.yourdpd.net.response.SearchByAddressResponse.AddressSearchResult;
import com.dpd.yourdpd.util.AlphaNumComparator;
import com.dpd.yourdpd.util.ViewIdGenerator;

import java.util.Collections;
import java.util.List;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.exceptions.OnErrorThrowable;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Allow user to specify neighbour delivery settings
 */
public class AddAddressNeighboursFragment extends AddAddressStepFragmentBase {
    public static String TAG = AddAddressNeighboursFragment.class.getSimpleName();

    private View userInputContainer;
    private ToggleButton anyNeighbourButton;
    private ToggleButton chooseNeighbourButton;
    private ToggleButton avoidNeighbourYesButton;
    private ToggleButton avoidNeighbourNoButton;
    private RadioGroup neighbourAddressRadioGroup;
    private RadioGroup avoidAddressRadioGroup;
    private TextView noPreferredAddressesTextView;
    private TextView noAvoidAddressesTextView;
    private ProgressBar progressBar;

    private List<Address> addresses;

    private Subscription getNeighboursSubscription;

    public static AddAddressNeighboursFragment newInstance() {
        Bundle args = new Bundle();
        AddAddressNeighboursFragment fragment = new AddAddressNeighboursFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public AddAddressNeighboursFragment() {
        // required empty constructor
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_add_address_neighbours, container, false);
        userInputContainer = root.findViewById(R.id.user_input_container);
        anyNeighbourButton = (ToggleButton) root.findViewById(R.id.preferred_neighbour_any_button);
        chooseNeighbourButton = (ToggleButton) root.findViewById(R.id.preferred_neighbour_choose_button);
        anyNeighbourButton.setOnCheckedChangeListener(onChangePreferredNeighbour);
        chooseNeighbourButton.setOnCheckedChangeListener(onChangePreferredNeighbour);

        noPreferredAddressesTextView = (TextView) root.findViewById(R.id.no_preferred_addresses_found_textview);
        noAvoidAddressesTextView = (TextView) root.findViewById(R.id.no_avoid_addresses_found_textview);

        avoidNeighbourYesButton = (ToggleButton) root.findViewById(R.id.avoid_neighbour_yes_button);
        avoidNeighbourNoButton = (ToggleButton) root.findViewById(R.id.avoid_neighbour_no_button);
        avoidNeighbourYesButton.setOnCheckedChangeListener(onChangeAvoidNeighbour);
        avoidNeighbourNoButton.setOnCheckedChangeListener(onChangeAvoidNeighbour);

        neighbourAddressRadioGroup = (RadioGroup) root.findViewById(R.id.neighbour_addresses_radiogroup);
        avoidAddressRadioGroup = (RadioGroup) root.findViewById(R.id.avoid_addresses_radiogroup);

        progressBar = (ProgressBar) root.findViewById(R.id.progressBar);
        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        enableNextButton();
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitle(getString(R.string.your_neighbours_titlecase));
        getNeighbours();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getNeighboursSubscription != null && !getNeighboursSubscription.isUnsubscribed()) {
            getNeighboursSubscription.unsubscribe();
        }
        getNeighboursSubscription = null;
    }

    private void getNeighbours() {
        disableNextButton();
        userInputContainer.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        noPreferredAddressesTextView.setVisibility(View.GONE);
        noAvoidAddressesTextView.setVisibility(View.GONE);

        // prepare yourself, the APIs make you jump through a LOT of hoops to get your data...

        // NOTE: we could store postcode from user input on Profile Setup step 2 to remove the first
        // few flatMaps below which simply obtain postcode from consumerPropertyId, but this would
        // not work for editing addresses where the user is not entering a postcode

        getNeighboursSubscription =
                // find Address (via AddressLocation) for consumerPropertyId
                api.getAddressByUDPRN(consumerProperty.consumerPropertyId)
                        // grab user Address postcode to use for search
                        .flatMap(new Func1<GetAddressByUDPRNResponse, Observable<String>>() {
                            @Override
                            public Observable<String> call(GetAddressByUDPRNResponse response) {
                                if (response.hasErrors()) {
                                    Log.e(TAG, "getAddressByUDPRN error: " + response.printErrors());
                                    throw OnErrorThrowable.from(new Exception("getAddressByUDPRN error"));
                                }
                                return Observable.just(response.address.postcode);
                            }
                        })
                                // search for addresses by postcode
                        .flatMap(new Func1<String, Observable<SearchByAddressResponse>>() {
                            @Override
                            public Observable<SearchByAddressResponse> call(String postcode) {
                                return api.searchByAddress(null, null, postcode, null, null, null, 1, BuildConfig.SEARCH_BY_ADDRESS_LIMIT);
                            }
                        })
                                // for each AddressSearchResult...
                        .flatMapIterable(new Func1<SearchByAddressResponse, Iterable<AddressSearchResult>>() {
                            @Override
                            public Iterable<AddressSearchResult> call(SearchByAddressResponse response) {
                                if (response.hasErrors()) {
                                    Log.e(TAG, "searchByAddress error: " + response.printErrors());
                                    throw OnErrorThrowable.from(new Exception("searchByAddress error"));
                                }
                                return response.getSearchResults();
                            }
                        })
                                // call lookupPostKey to get Addresses from their addressKey
                        .flatMap(new Func1<AddressSearchResult, Observable<LookupPostKeyResponse>>() {
                            @Override
                            public Observable<LookupPostKeyResponse> call(AddressSearchResult searchResult) {
                                return api.lookupPostKey(searchResult.addressKey);
                            }
                        })
                                // and pull out the Address property
                        .map(new Func1<LookupPostKeyResponse, Address>() {
                            @Override
                            public Address call(LookupPostKeyResponse response) {
                                if (response.hasErrors()) {
                                    Log.e(TAG, "lookupPostKey error: " + response.printErrors());
                                    throw OnErrorThrowable.from(new Exception("lookupPostKey error"));
                                }
                                return response.getAddressLocation().address;
                            }
                        })
                                // removing any that match the user's Address
                        .filter(new Func1<Address, Boolean>() {
                            @Override
                            public Boolean call(Address address) {
                                return !consumerProperty.consumerPropertyId.equals(address.addressId);
                            }
                        })
                        .toList()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                new Action1<List<Address>>() {
                                    @Override
                                    public void call(List<Address> addresses) {
                                        onAddressResults(addresses);
                                    }
                                },
                                new Action1<Throwable>() {
                                    @Override
                                    public void call(Throwable throwable) {
                                        Log.e(TAG, "get by address error", throwable);
                                        onAddressResults(null);
                                        showErrorAlertDialog(getString(R.string.a_problem_has_occurred_try_again));
                                    }
                                });
    }

    private void onAddressResults(List<Address> addresses) {
        this.addresses = addresses;

        userInputContainer.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);

        if (addresses != null) {
            showNeighbourAddresses();
            showAvoidNeighbourAddresses();
            enableNextButton();
        }
    }

    private void showNeighbourAddresses() {
        final LayoutInflater inflater = LayoutInflater.from(getActivity());
        neighbourAddressRadioGroup.removeAllViews();

        if (addresses != null) {
            Collections.sort(addresses, new AlphaNumComparator<Address>());
            for (Address address : addresses) {
                final RadioButton radioButton = (RadioButton) inflater.inflate(R.layout.item_radiobutton, neighbourAddressRadioGroup, false);
                radioButton.setText(address.toFormattedString());
                radioButton.setTag(address.addressId);
                radioButton.setId(ViewIdGenerator.generateViewId());
                neighbourAddressRadioGroup.addView(radioButton);
            }
        }

        if (neighbourAddressRadioGroup.getChildCount() > 0) {
            neighbourAddressRadioGroup.check(neighbourAddressRadioGroup.getChildAt(0).getId());
        }
    }

    private void showAvoidNeighbourAddresses() {
        final LayoutInflater inflater = LayoutInflater.from(getActivity());
        avoidAddressRadioGroup.removeAllViews();

        if (addresses != null) {
            Collections.sort(addresses, new AlphaNumComparator<Address>());
            for (Address address : addresses) {
                final RadioButton radioButton = (RadioButton) inflater.inflate(R.layout.item_radiobutton, avoidAddressRadioGroup, false);
                radioButton.setText(address.toFormattedString());
                radioButton.setTag(address.addressId);
                radioButton.setId(ViewIdGenerator.generateViewId());
                avoidAddressRadioGroup.addView(radioButton);
            }
        }

        if (avoidAddressRadioGroup.getChildCount() > 0) {
            avoidAddressRadioGroup.check(avoidAddressRadioGroup.getChildAt(0).getId());
        }
    }

    private CompoundButton.OnCheckedChangeListener onChangePreferredNeighbour =
            new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (!isChecked) {
                        // prevent un-checking
                        buttonView.setChecked(true);
                        return;
                    }
                    switch (buttonView.getId()) {
                        case R.id.preferred_neighbour_any_button:
                            chooseNeighbourButton.setOnCheckedChangeListener(null);
                            chooseNeighbourButton.setChecked(false);
                            chooseNeighbourButton.setOnCheckedChangeListener(this);
                            neighbourAddressRadioGroup.setVisibility(View.GONE);
                            noPreferredAddressesTextView.setVisibility(View.GONE);
                            break;
                        case R.id.preferred_neighbour_choose_button:
                            anyNeighbourButton.setOnCheckedChangeListener(null);
                            anyNeighbourButton.setChecked(false);
                            anyNeighbourButton.setOnCheckedChangeListener(this);
                            if (addresses == null || addresses.size() == 0) {
                                noPreferredAddressesTextView.setVisibility(View.VISIBLE);
                            } else {
                                neighbourAddressRadioGroup.setVisibility(View.VISIBLE);
                            }
                            break;
                    }
                }
            };

    private CompoundButton.OnCheckedChangeListener onChangeAvoidNeighbour =
            new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (!isChecked) {
                        // prevent un-checking
                        buttonView.setChecked(true);
                        return;
                    }
                    switch (buttonView.getId()) {
                        case R.id.avoid_neighbour_yes_button:
                            avoidNeighbourNoButton.setOnCheckedChangeListener(null);
                            avoidNeighbourNoButton.setChecked(false);
                            avoidNeighbourNoButton.setOnCheckedChangeListener(this);
                            if (addresses == null || addresses.size() == 0) {
                                noAvoidAddressesTextView.setVisibility(View.VISIBLE);
                            } else {
                                avoidAddressRadioGroup.setVisibility(View.VISIBLE);
                            }
                            break;
                        case R.id.avoid_neighbour_no_button:
                            avoidNeighbourYesButton.setOnCheckedChangeListener(null);
                            avoidNeighbourYesButton.setChecked(false);
                            avoidNeighbourYesButton.setOnCheckedChangeListener(this);
                            avoidAddressRadioGroup.setVisibility(View.GONE);
                            noAvoidAddressesTextView.setVisibility(View.GONE);
                            break;
                    }
                }
            };

    private boolean getAllowAnyNeighbourChecked() {
        return anyNeighbourButton.isChecked();
    }

    private String getAvoidNeighbourId() {
        if (avoidNeighbourNoButton.isChecked()) {
            return null;
        } else {
            final int checkedRadioButtonId = avoidAddressRadioGroup.getCheckedRadioButtonId();
            if (checkedRadioButtonId == -1) {
                return null;
            }
            return (String) avoidAddressRadioGroup.findViewById(checkedRadioButtonId).getTag();
        }
    }

    private String getPreferredNeighbourId() {
        if (anyNeighbourButton.isChecked()) {
            return null;
        } else {
            final int checkedRadioButtonId = neighbourAddressRadioGroup.getCheckedRadioButtonId();
            if (checkedRadioButtonId == -1) {
                return null;
            }
            return (String) neighbourAddressRadioGroup.findViewById(checkedRadioButtonId).getTag();
        }
    }

    @Override
    public boolean validate() {
        if (avoidNeighbourYesButton.isChecked() && getAvoidNeighbourId() == null) {
            showErrorAlertDialog(getString(R.string.please_select_neighbour_to_avoid));
            return false;
        }
        if (chooseNeighbourButton.isChecked() && getPreferredNeighbourId() == null) {
            showErrorAlertDialog(getString(R.string.please_select_preferred_neighbour));
            return false;
        }
        if (chooseNeighbourButton.isChecked() && avoidNeighbourYesButton.isChecked()) {
            final String preferredNeighbourId = getPreferredNeighbourId();
            final String avoidNeighbourId = getAvoidNeighbourId();
            if (preferredNeighbourId.equals(avoidNeighbourId)) {
                showErrorAlertDialog(getString(R.string.please_select_different_neighbours));
                return false;
            }
        }
        return true;
    }

    @Override
    public void applyChanges() {
        consumerProperty.neighbourAllow = getAllowAnyNeighbourChecked();
        consumerProperty.neighbourAvoid = getAvoidNeighbourId();
        consumerProperty.neighbourPreferred = getPreferredNeighbourId();
    }
}
