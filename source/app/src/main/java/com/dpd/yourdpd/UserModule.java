package com.dpd.yourdpd;

import android.content.Context;

import com.dpd.yourdpd.model.User;

import dagger.Module;
import dagger.Provides;

@Module
public class UserModule {
    private static User user;

    public UserModule(Context context) {
        user = new User(context);
        user.load();
    }

    @Provides @PerApplication User provideUser() {
        return user;
    }
}
