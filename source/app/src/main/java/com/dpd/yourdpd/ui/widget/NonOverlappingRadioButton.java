package com.dpd.yourdpd.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RadioButton;
import android.widget.RadioGroup;

/**
 * RadioButton which returns false for {@link #hasOverlappingRendering}.
 * This prevents the GPU from creating too large textures during Transitions when the RadioButton's
 * parent contains too many items (e.g. taller than 4096px on a Nexus 5 would crash during the
 * Transition due to it exceeding the GPU texture size limit)
 */
public class NonOverlappingRadioButton extends RadioButton {
    public NonOverlappingRadioButton(Context context) {
        this(context, null);
    }
    public NonOverlappingRadioButton(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }
    public NonOverlappingRadioButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean hasOverlappingRendering() {
        return false;
    }
}
