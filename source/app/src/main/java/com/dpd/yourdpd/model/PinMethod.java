package com.dpd.yourdpd.model;

/**
 * The method used to request an application PIN
 */
public enum PinMethod {
    SMS, EMAIL;

    public static PinMethod fromOrdinal(int anInt) {
        return SMS.ordinal()==anInt ? SMS : EMAIL;
    }
}
