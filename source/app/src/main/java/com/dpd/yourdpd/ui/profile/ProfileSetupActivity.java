package com.dpd.yourdpd.ui.profile;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ScrollView;

import com.dpd.yourdpd.R;
import com.dpd.yourdpd.model.ConsumerProperty;
import com.dpd.yourdpd.net.response.PutConsumerResponse;
import com.dpd.yourdpd.ui.BaseMenuActivity;
import com.dpd.yourdpd.ui.Validating;
import com.dpd.yourdpd.ui.address.AddAddressBasicDetailsFragment;
import com.dpd.yourdpd.ui.address.AddAddressNeighboursFragment;
import com.dpd.yourdpd.ui.address.AddAddressPickupShopFragment;
import com.dpd.yourdpd.ui.address.AddAddressSafePlaceFragment;
import com.dpd.yourdpd.ui.address.AddAddressStepFragmentBase.AddAddressStepFragmentListener;
import com.dpd.yourdpd.ui.address.AddFirstAttemptPreferenceFragment;
import com.dpd.yourdpd.ui.address.FirstAttemptType;
import com.dpd.yourdpd.ui.widget.StepsView;
import com.dpd.yourdpd.ui.yourdeliveries.YourDeliveriesActivity;
import com.dpd.yourdpd.util.AppConstant;
import com.dpd.yourdpd.util.CommonUtils;

import rx.functions.Action1;

public class ProfileSetupActivity extends BaseMenuActivity implements
        ProfileDetailsFragment.ProfileDetailsFragmentListener,
        AddAddressStepFragmentListener {

    private static final String TAG = ProfileSetupActivity.class.getSimpleName();

    private static final int TOTAL_STEPS = 6;

    private enum Step {
        PROFILE_DETAILS,
        BASIC_ADDRESS_DETAILS,
        SAFE_PLACE,
        NEIGHBOURS,
        SHOPS,
        FIRST_ATTEMPT_PREFS;

        Step() {
        }

        public Step next() {
            return fromValue(ordinal() + 1);
        }

        static Step fromValue(int i) {
            for (Step step : Step.values()) {
                if (i == step.ordinal()) return step;
            }
            return null;
        }
    }

    private ScrollView scrollView;
    private StepsView stepsView;
    private Button nextButton;
    private ProgressDialog progressDialog;
    private Fragment currentFragment;
    private Step currentStep;
    private ConsumerProperty consumerProperty;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_setup);

        // setup the new ConsumerProperty to edit
        consumerProperty = new ConsumerProperty();
        user.selectedConsumerProperty = consumerProperty;

        /**/
        scrollView = (ScrollView) findViewById(R.id.scrollView);
        stepsView = (StepsView) findViewById(R.id.steps);
        nextButton = (Button) findViewById(R.id.next_button);
        nextButton.setOnClickListener(onClickNext);

        getSupportFragmentManager().addOnBackStackChangedListener(onBackStackChangedListener);
        addSteps();
        setCurrentStep(Step.PROFILE_DETAILS);

        CommonUtils.setupUIForKeyboard(this, findViewById(android.R.id.content));

        initSpotzPushService();
    }

    private View.OnClickListener onClickNext = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // if we are showing a Fragment that needs validation, do that before moving on
            final boolean isValidatingFragment = (currentFragment instanceof Validating);
            final Validating validatingFragment = isValidatingFragment ? ((Validating) currentFragment) : null;

            if (!isValidatingFragment || validatingFragment.validate()) {
                assert validatingFragment != null;
                validatingFragment.applyChanges();

                final boolean isLastStep = currentStep.equals(Step.FIRST_ATTEMPT_PREFS);

                if (!isLastStep && !skipProgress) {
                    final Step nextStep = currentStep.next();

                    if (nextStep.equals(Step.FIRST_ATTEMPT_PREFS)) {
                        // see if we can move onto First Attempt Preferences...
                        if (TextUtils.isEmpty(consumerProperty.pickupLocationCode)) {
                            // no pickup shop available, choose Home and move on
                            consumerProperty.firstAttemptType = FirstAttemptType.HOME;
                            onProfileSetupComplete();
                            return;
                        }
                    }
                    setCurrentStep(nextStep);
                } else {
                    // all steps done
                    onProfileSetupComplete();
                }
            }
        }
    };

    /**
     * Invoked when user has completed profile setup, add ConsumerProperty to ConsumerProfile
     * and save to server
     */
    private void onProfileSetupComplete() {
        user.getConsumerProfile().getConsumerProperties().add(user.selectedConsumerProperty);
        putConsumerProfile();
    }

    private FragmentManager.OnBackStackChangedListener onBackStackChangedListener =
            new FragmentManager.OnBackStackChangedListener() {
                @Override
                public void onBackStackChanged() {
                    // handle back button presses
                    final FragmentManager fragmentManager = getSupportFragmentManager();
                    final int step = fragmentManager.getBackStackEntryCount();
                    final boolean isLastStep = step == TOTAL_STEPS - 1;
                    if (step < currentStep.ordinal()) { // moved before current step
                        // remove last Fragment
                        fragmentManager.beginTransaction().remove(currentFragment).commit();

                        // update display for previous Step/Fragment
                        final String tag = Step.fromValue(step).name();
                        currentFragment = fragmentManager.findFragmentByTag(tag);
                        moveBackToStep(Step.fromValue(step));
                    }
                    nextButton.setText(isLastStep ? getString(R.string.view_your_deliveries_titlecase) : getString(R.string.next));
                    stepsView.setVisibility(isLastStep ? View.GONE : View.VISIBLE);

                }
            };

    private void addSteps() {
        stepsView.addStep(new StepsView.Step("1", getString(R.string.profile)));
        stepsView.addStep(new StepsView.Step("2", getString(R.string.address)));
        stepsView.addStep(new StepsView.Step("3", getString(R.string.safe_place)));
        stepsView.addStep(new StepsView.Step("4", getString(R.string.neighbours)));
        stepsView.addStep(new StepsView.Step("5", getString(R.string.shops)));
    }

    private void moveBackToStep(Step step) {
        currentStep = step;
        stepsView.setActiveStep(step.ordinal());
        scrollToTop();
    }

    private void setCurrentStep(Step step) {
        stepsView.setActiveStep(step.ordinal());
        currentStep = step;

        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        // TODO: re-enable transitions by solving the GPU overflow issue (caused by extremely long views)
//        transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right);
//        transaction.setCustomAnimations(0,0,0,0);
        transaction.setTransition(FragmentTransaction.TRANSIT_NONE);

        final String tag = String.valueOf(step);
        boolean addToBackStack = step.ordinal() > 0;
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);
        if (fragment == null) {
            switch (step) {
                case PROFILE_DETAILS:
                    fragment = ProfileDetailsFragment.newInstance();
                    break;
                case BASIC_ADDRESS_DETAILS:
                    fragment = AddAddressBasicDetailsFragment.newInstance(getString(R.string.your_primary_address), true);
                    break;
                case SAFE_PLACE:
                    fragment = AddAddressSafePlaceFragment.newInstance();
                    break;
                case NEIGHBOURS:
                    fragment = AddAddressNeighboursFragment.newInstance();
                    break;
                case SHOPS:
                    fragment = AddAddressPickupShopFragment.newInstance();
                    break;
                case FIRST_ATTEMPT_PREFS:
                    fragment = AddFirstAttemptPreferenceFragment.newInstance();
                    break;
            }
            transaction.add(R.id.fragment_container, fragment, tag);
        } else {
            transaction.show(fragment);
        }
        if (currentFragment != null) {
            transaction.hide(currentFragment);
        }

        if (addToBackStack) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
        getSupportFragmentManager().executePendingTransactions();

        currentFragment = fragment;
        scrollToTop();
    }

    /**
     * Save ConsumerProfile to server and continue to Your Deliveries
     */
    private void putConsumerProfile() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
        progressDialog = ProgressDialog.show(this, null, getString(R.string.please_wait_), true, false);

        user.putConsumerProfile(api, false)
                .subscribe(
                        new Action1<PutConsumerResponse>() {
                            @Override
                            public void call(PutConsumerResponse response) {
                                progressDialog.dismiss();
                                if (response.hasErrors()) {
                                    Log.e(TAG, "Error putting Consumer: " + response.printErrors());
                                    String strErr = response.printErrors();
                                    if (strErr.contains(AppConstant.PROFILE_ERR_CODE)) {
                                        showErrorDialog(getResources().getString(R.string.mail_duplicate_error));
                                    } else if (strErr.contains("email") && strErr.contains("Invalid field")) {
                                        showErrorDialog(getResources().getString(R.string.mail_invalid_error));
                                    } else if (strErr.contains("email") && strErr.contains("Missing")) {
                                        showErrorDialog(getResources().getString(R.string.mail_input_address_error));
                                        //onConsumerProfilePutComplete();
                                    }
                                } else {
                                    Log.e(TAG, "Consumer put");
                                    onConsumerProfilePutComplete();
                                }
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                progressDialog.dismiss();
                                Log.e(TAG, "Error updating Consumer", throwable);
                                showErrorDialogWithRetryAndCancel(
                                        getString(R.string.problem_creating_profile_retry),
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                putConsumerProfile();
                                            }
                                        });
                            }
                        });
    }

    private void onConsumerProfilePutComplete() {
        // finally save the consumer information so that we can skip ProfileSetupActivity in future
        user.save();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                startYourDeliveriesActivity();
            }
        });
    }

    private void startYourDeliveriesActivity() {
        final Intent intent = new Intent(this, YourDeliveriesActivity.class);
        //intent.putExtra("showWelcome", true);
        startActivity(intent);
        finish();
    }

    /**
     * {@link ProfileDetailsFragment.ProfileDetailsFragmentListener} and
     * {@link AddAddressStepFragmentListener}
     */
    @Override
    public void enableNextButton() {
        nextButton.setEnabled(true);
    }

    @Override
    public void disableNextButton() {
        nextButton.setEnabled(false);
    }

    @Override
    public void doNext() {
        nextButton.performClick();
    }

    @Override
    public boolean isNextButtonEnabled() {
        return nextButton.isEnabled();
    }

    public void scrollToTop() {
        scrollView.scrollTo(0, 0);
    }

    @Override
    public void setTitle(CharSequence title) {
        /*super.setTitle(title);
        toolbar.setTitle(title);*/
        this.setupToolbar(title.toString(), true);
    }
}
