package com.dpd.yourdpd.ui.yourdeliveries;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.transitions.everywhere.TransitionManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Toast;

import com.dpd.yourdpd.R;
import com.dpd.yourdpd.model.Parcel;
import com.dpd.yourdpd.model.ParcelActionCodes;
import com.dpd.yourdpd.model.SafeLocation;
import com.dpd.yourdpd.net.request.LeaveInSafeLocationParams;
import com.dpd.yourdpd.net.response.GetParcelUpdateDatesResponse;
import com.dpd.yourdpd.net.response.LeaveInSafePlaceResponse;
import com.dpd.yourdpd.net.response.SafeLocationsResponse;
import com.dpd.yourdpd.ui.BaseFragment;
import com.dpd.yourdpd.ui.BaseMenuActivity;
import com.dpd.yourdpd.util.CommonUtils;
import com.dpd.yourdpd.util.DateUtils;
import com.dpd.yourdpd.util.ImagePickerUtils;
import com.dpd.yourdpd.util.UIUtils;
import com.dpd.yourdpd.util.ViewIdGenerator;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link InFlightLeaveInSafePlaceFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class InFlightLeaveInSafePlaceFragment extends BaseFragment {

    private static final String TAG = InFlightLeaveInSafePlaceFragment.class.getSimpleName();

    // the fragment initialization parameters
    private static final String ARG_PARCEL_CODE = "parcel_code";

    private OnFragmentInteractionListener listener;
    private Parcel parcel;
    private RadioGroup deliveryDatesRadioGroup;
    private RadioGroup safePlaceRadioGroup;
    private EditText driverNotesEditText;
    private ImageView safePlacePhotoImageView;
    private CheckBox acceptCheckbox;
    private Button confirmChangesButton;
    private ContentLoadingProgressBar progressBar;
    private ScrollView scrollView;
    private FrameLayout frameLayout;
    private boolean isOtherSelected = false;

    private Subscription loadDataSubscription;
    private Subscription leaveInSafePlaceSubscription;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param parcelCode Parcel.parcelCode
     * @return A new instance of fragment InFlightLeaveInSafePlaceFragment.
     */
    public static InFlightLeaveInSafePlaceFragment newInstance(String parcelCode) {
        final InFlightLeaveInSafePlaceFragment fragment = new InFlightLeaveInSafePlaceFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARCEL_CODE, parcelCode);
        fragment.setArguments(args);
        return fragment;
    }

    public InFlightLeaveInSafePlaceFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            String parcelCode = getArguments().getString(ARG_PARCEL_CODE);
            parcel = dataStore.getParcelWithParcelCode(parcelCode);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.fragment_inflight_leave_in_safe_place, container, false);
//        root.findViewById(R.id.safe_place_photo_button).setOnClickListener(
//                new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        pickPhoto();
//                    }
//                });
//        safePlacePhotoImageView = (ImageView) root.findViewById(R.id.safe_place_photo_imageview);
//        safePlacePhotoImageView.setOnClickListener(
//                new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        pickPhoto();
//                    }
//                });
        final RadioGroup.OnCheckedChangeListener radioCheckChangeListener =
                new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        if ((deliveryDatesRadioGroup.getCheckedRadioButtonId() != -1 || root.findViewById(R.id.dates_container).getVisibility() == View.GONE)
                                && safePlaceRadioGroup.getCheckedRadioButtonId() != -1) {
                            confirmChangesButton.setEnabled(true);
                        } else {
                            confirmChangesButton.setEnabled(false);
                        }

                        RadioButton safePlaceRadioButton = (RadioButton) safePlaceRadioGroup.findViewById(safePlaceRadioGroup.getCheckedRadioButtonId());
                        if (safePlaceRadioButton != null) {
                            String safePlaceName = safePlaceRadioButton.getText().toString();
                            isOtherSelected = safePlaceName.equals("Other");
                        }
                    }
                };
        deliveryDatesRadioGroup = (RadioGroup) root.findViewById(R.id.delivery_dates_radiogroup);
        deliveryDatesRadioGroup.setOnCheckedChangeListener(radioCheckChangeListener);
        safePlaceRadioGroup = (RadioGroup) root.findViewById(R.id.safe_places_radiogroup);
        safePlaceRadioGroup.setOnCheckedChangeListener(radioCheckChangeListener);
        driverNotesEditText = (EditText) root.findViewById(R.id.driver_notes_edittext);
        driverNotesEditText.setHorizontallyScrolling(false);
        driverNotesEditText.setMaxLines(Integer.MAX_VALUE);
        acceptCheckbox = (CheckBox) root.findViewById(R.id.safe_place_accept_checkbox);
        confirmChangesButton = (Button) root.findViewById(R.id.confirm_changes_button);
        frameLayout = (FrameLayout) root.findViewById(R.id.frame_err_tooltip);
        frameLayout.setVisibility(View.INVISIBLE);
        confirmChangesButton.setEnabled(false);
        confirmChangesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmChanges();
            }
        });
        progressBar = (ContentLoadingProgressBar) root.findViewById(R.id.progressBar);
        scrollView = (ScrollView) root.findViewById(R.id.scrollView);

        CommonUtils.setupUIForKeyboard(getActivity(), root);

        return root;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (OnFragmentInteractionListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initParcelStatusView();
        loadData();
    }

    @Override
    public void onResume() {
        super.onResume();
        /*final ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if(actionBar != null) {
            actionBar.setTitle(R.string.change_delivery_date_titlecase);
        }*/
        ((BaseMenuActivity) getActivity()).setupToolbar(getString(R.string.deliver_to_safe_place), true);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            final File file = new File(getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES), "temp.jpg");
            if (requestCode == ImagePickerUtils.REQUEST_CAMERA) {
                ImagePickerUtils.loadAndDisplayBitmap(file, safePlacePhotoImageView,
                        new ImagePickerUtils.LoadImageCallback() {
                            @Override
                            public void onSuccess(Bitmap bitmap) {
                                // all good
                                // TODO: convert to base64?
                                Toast.makeText(getActivity(),
                                        "No API to upload file to (ignoring image)",
                                        Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onProgress(int progress) {
                            }

                            @Override
                            public void onError(int code) {
                                Toast.makeText(getActivity(),
                                        getString(R.string.problem_loading_image),
                                        Toast.LENGTH_SHORT).show();
                            }
                        });

            } else if (requestCode == ImagePickerUtils.REQUEST_GALLERY) {
                ImagePickerUtils.loadAndDisplayBitmap(data, getActivity(), safePlacePhotoImageView,
                        new ImagePickerUtils.LoadImageCallback() {
                            @Override
                            public void onSuccess(Bitmap bitmap) {
                                // all good
                                // TODO: convert to base64?
                                Toast.makeText(getActivity(),
                                        "No API to upload file to (ignoring image)",
                                        Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onProgress(int progress) {
                                // TODO: update progress dialog?
                            }

                            @Override
                            public void onError(int code) {
                                Toast.makeText(getActivity(),
                                        getString(R.string.problem_loading_image),
                                        Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        ImagePickerUtils.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (loadDataSubscription != null && !loadDataSubscription.isUnsubscribed()) {
            loadDataSubscription.unsubscribe();
            loadDataSubscription = null;
        }
        if (leaveInSafePlaceSubscription != null && !leaveInSafePlaceSubscription.isUnsubscribed()) {
            leaveInSafePlaceSubscription.unsubscribe();
            leaveInSafePlaceSubscription = null;
        }
    }

    private void loadData() {
        showProgressBar();

        if (loadDataSubscription != null && !loadDataSubscription.isUnsubscribed()) {
            loadDataSubscription.unsubscribe();
            loadDataSubscription = null;
        }
        // TODO: could run these instead with zip()?
        loadDataSubscription =
                // get list of available delivery dates for this action
                getDeliveryDates()
                        // then chain getSafeLocations()
                        .flatMap(new Func1<GetParcelUpdateDatesResponse, Observable<SafeLocationsResponse>>() {
                            @Override
                            public Observable<SafeLocationsResponse> call(final GetParcelUpdateDatesResponse response) {
                                if (response.hasErrors()) {
                                    Log.e(TAG, "Problem getting dates: " + response.printErrors());
                                    return Observable.error(new Exception(getString(R.string.problem_getting_delivery_dates)));
                                }
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        setDeliveryDates(response.getDiaryDates());
                                    }
                                });
                                return getSafeLocations();
                            }
                        })
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                new Action1<SafeLocationsResponse>() {
                                    @Override
                                    public void call(SafeLocationsResponse response) {
                                        hideProgressBar();
                                        if (response.hasErrors()) {
                                            Log.e(TAG, "Problem getting safe locations: " + response.printErrors());
                                            showErrorAlertDialog(getString(R.string.a_problem_has_occurred_try_again));
                                            return;
                                        }
                                        setSafeLocations(response.getSafeLocations());
                                    }
                                },
                                new Action1<Throwable>() {
                                    @Override
                                    public void call(Throwable throwable) {
                                        hideProgressBar();
                                        Log.e(TAG, "Problem getting data: ", throwable);
                                        showErrorAlertDialog(getString(R.string.a_problem_has_occurred_try_again));
                                    }
                                });
    }

    private Observable<SafeLocationsResponse> getSafeLocations() {
        return api.getSafeLocations()
                .retry(3);
    }

    private Observable<GetParcelUpdateDatesResponse> getDeliveryDates() {
        return api.getParcelUpdateDates(
                parcel.parcelCode,
                ParcelActionCodes.LEAVE_IN_SAFE_PLACE.getValue())
                .subscribeOn(Schedulers.io())
                .retry(3);
    }

    private void initParcelStatusView() {
        final View view = getView();
        assert view != null;
        final Context context = getContext();
        UIUtils.populateParcelStatus(view, context, parcel);
    }

    private void setDeliveryDates(List<String> diaryDates) {
        final View view = getView();
        assert view != null;

        if (diaryDates == null || diaryDates.size() == 0) {
            // no date selection is required
            view.findViewById(R.id.dates_container).setVisibility(View.GONE);
            return;
        } else {
            view.findViewById(R.id.dates_container).setVisibility(View.VISIBLE);
        }

        final LayoutInflater inflater = LayoutInflater.from(getActivity());
        final SimpleDateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd", Locale.UK);

        deliveryDatesRadioGroup.removeAllViews();

        for (String diaryDate : diaryDates) {
            try {
                final Date date = dateParser.parse(diaryDate);
                final Calendar cal = Calendar.getInstance();
                cal.setTime(date);
                final String dateSuffix = DateUtils.getDayOfMonthSuffix(cal.get(Calendar.DATE));
                final SimpleDateFormat dateFormat = new SimpleDateFormat(String.format("cccc d'%s' MMMM", dateSuffix), Locale.UK);
                final RadioButton radioButton = (RadioButton) inflater.inflate(R.layout.item_radiobutton, deliveryDatesRadioGroup, false);
                radioButton.setId(ViewIdGenerator.generateViewId());
                radioButton.setText(dateFormat.format(date));
                radioButton.setTag(diaryDate);
                deliveryDatesRadioGroup.addView(radioButton);
            } catch (ParseException e) {
                Log.e(TAG, "Exception parsing diaryDates for display", e);
            }

        }
        deliveryDatesRadioGroup.clearCheck();
        deliveryDatesRadioGroup.check(deliveryDatesRadioGroup.getChildAt(0).getId());
    }

    private void setSafeLocations(List<SafeLocation> safeLocations) {
        final View view = getView();
        assert view != null;
        final LayoutInflater inflater = LayoutInflater.from(getActivity());

        safePlaceRadioGroup.removeAllViews();

        for (SafeLocation safeLocation : safeLocations) {
            final RadioButton radioButton = (RadioButton) inflater.inflate(R.layout.item_radiobutton, safePlaceRadioGroup, false);
            radioButton.setText(safeLocation.getSafeLocationDescription());
            radioButton.setTag(safeLocation);
            radioButton.setId(ViewIdGenerator.generateViewId());
            safePlaceRadioGroup.addView(radioButton);
        }

        safePlaceRadioGroup.clearCheck();
        safePlaceRadioGroup.check(safePlaceRadioGroup.getChildAt(0).getId());
    }

    private void leaveInSafeLocation(SafeLocation safeLocation,
                                     String safeLocationInstruction,
                                     String selectedDate) {
        showProgressBar();
        confirmChangesButton.setEnabled(false);

        final LeaveInSafeLocationParams params =
                new LeaveInSafeLocationParams.Builder()
                        //.setCallingCardLocation(safeLocation.getSafeLocationDescription())
                        .setSafeLocationId(safeLocation.getSafeLocationId())
                        .setSafeLocationInstruction(TextUtils.isEmpty(safeLocationInstruction) ? null : safeLocationInstruction)
                        .setContactName(user.getFullName())
                        .setEmail(user.getConsumerProfile().email)
                        .setMobile(CommonUtils.addPrefixNumber(user.getConsumerProfile().mobile))
                        .setDiaryDate(selectedDate)
                        .setParcelCode(parcel.parcelCode)
                        .build();

        if (leaveInSafePlaceSubscription != null && !leaveInSafePlaceSubscription.isUnsubscribed()) {
            leaveInSafePlaceSubscription.unsubscribe();
            leaveInSafePlaceSubscription = null;
        }
        leaveInSafePlaceSubscription = api.leaveInSafePlace(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<LeaveInSafePlaceResponse>() {
                               @Override
                               public void call(LeaveInSafePlaceResponse response) {
                                   hideProgressBar();
                                   confirmChangesButton.setEnabled(true);

                                   if (response.hasErrors()) {
                                       Log.e(TAG, "Problem calling leaveInSafeLocation() " + response.printErrors());
                                       showErrorAlertDialog(getString(R.string.parcel_status_not_changed), getString(R.string.error_occurred));
                                       return;
                                   }

                                   showActionConfirmedDialog();
                               }
                           },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                Log.e(TAG, "Problem calling leaveInSafeLocation()", throwable);
                                hideProgressBar();
                                confirmChangesButton.setEnabled(true);
                                showErrorAlertDialog(getString(R.string.parcel_status_not_changed), getString(R.string.error_occurred));
                            }
                        });
    }

    /**
     * No longer used but may come back
     */
    private void pickPhoto() {
        ImagePickerUtils.showPickerDialog(this);
    }

    private void showProgressBar() {
        progressBar.show();
        scrollView.setVisibility(View.GONE);
    }

    private void hideProgressBar() {
        progressBar.hide();
        scrollView.setVisibility(View.VISIBLE);
    }

    private void confirmChanges() {
        final View view = getView();
        assert view != null;

        if (!acceptCheckbox.isChecked()) {
            TransitionManager.beginDelayedTransition((ViewGroup) getView());
            frameLayout.setVisibility(View.VISIBLE);
            scrollView.post(new Runnable() {
                @Override
                public void run() {
                    scrollView.fullScroll(ScrollView.FOCUS_DOWN);
                }
            });

            return;
        }

        if (isOtherSelected) {
            if (driverNotesEditText.getText().toString().isEmpty()) {
                final AlertDialog.Builder builder =
                        new AlertDialog.Builder(getActivity(), R.style.AlertDialogTheme);
                builder.setMessage(getString(R.string.enter_driver_instruction));
                builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.show();
                return;
            }
        }

        final RadioButton safePlaceRadioButton = (RadioButton) safePlaceRadioGroup.findViewById(safePlaceRadioGroup.getCheckedRadioButtonId());
        final RadioButton dateRadioButton = (RadioButton) deliveryDatesRadioGroup.findViewById(deliveryDatesRadioGroup.getCheckedRadioButtonId());
        final SafeLocation selectedSafeLocation = (SafeLocation) safePlaceRadioButton.getTag();
        final String safeLocationInstruction = driverNotesEditText.getText().toString();
        final String selectedDate = dateRadioButton == null ? "" : (String) dateRadioButton.getTag();

        final AlertDialog.Builder builder =
                new AlertDialog.Builder(getActivity(), R.style.AlertDialogTheme);
        builder.setTitle(getString(R.string.change_delivery_dialog_title));
        builder.setMessage(String.format(getString(R.string.change_delivery_leave_safe_place_dialog_message), safePlaceRadioButton.getText()));
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                // TODO: upload image?
                leaveInSafeLocation(selectedSafeLocation, safeLocationInstruction, selectedDate);
            }
        });
        builder.setNegativeButton(android.R.string.cancel, null);
        builder.show();
    }

    private void showActionConfirmedDialog() {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(getContext(), R.style.AlertDialogTheme);
        builder.setTitle(R.string.parcel_updated);
        builder.setMessage(getString(R.string.parcel_leave_safe_place_confirmed));
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // all done - return from this Fragment
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
        builder.create().show();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
//        public void onInflightPickupFragmentInteraction(Uri uri);
    }
}
