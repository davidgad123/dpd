package com.dpd.yourdpd.ui.yourdeliveries;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.TextView;

import com.dpd.yourdpd.BuildConfig;
import com.dpd.yourdpd.R;
import com.dpd.yourdpd.model.ConsumerProfile;
import com.dpd.yourdpd.model.Parcel;
import com.dpd.yourdpd.model.ParcelHistory;
import com.dpd.yourdpd.net.response.GetParcelHistoryResponse;
import com.dpd.yourdpd.ui.BaseActivity;
import com.dpd.yourdpd.ui.BaseFragment;
import com.dpd.yourdpd.ui.settings.SettingsActivity;
import com.dpd.yourdpd.ui.widget.DividerItemDecoration;
import com.dpd.yourdpd.ui.widget.SectionedRecyclerViewAdapter;
import com.dpd.yourdpd.util.DebugUtils;
import com.dpd.yourdpd.util.PhotoUtils;
import com.dpd.yourdpd.util.UIUtils;
import com.tumblr.bookends.Bookends;
import com.util.WearableUtil;

import java.util.ArrayList;
import java.util.List;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Presents the user with a list of their Deliveries
 */
public class YourDeliveriesActivityFragment extends BaseFragment implements View.OnClickListener {
    private static final String TAG = YourDeliveriesActivityFragment.class.getSimpleName();

    public static final int REQUEST_FURTHER_IMPROVE_PROMPT = 1;

    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView listView;

    /**
     * Current ParcelHistory being displayed
     */
    private ParcelHistory parcelHistory;
    private Subscription parcelHistorySubscription;

    private int firstItem = 0;
    private float topOffset = 0;

    public static YourDeliveriesActivityFragment newInstance() {
        return new YourDeliveriesActivityFragment();
    }

    public YourDeliveriesActivityFragment() {
        // required empty Fragment constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        parcelHistory = dataStore.getParcelHistory();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        parcelHistory = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_your_deliveries, container, false);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        listView = (RecyclerView) view.findViewById(R.id.deliveries_recycler_view);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                saveScrollPosition();
                reloadParcelHistory();
            }
        });

        final DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(ResourcesCompat.getDrawable(getResources(), R.drawable.list_divider, null));
//        dividerItemDecoration.setInitialPositionOffset(2);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        listView.setLayoutManager(layoutManager);
        listView.setItemAnimator(new DefaultItemAnimator());
        listView.addItemDecoration(dividerItemDecoration);
        listView.setVisibility(View.INVISIBLE); // until data is loaded
    }

    @Override
    public void onResume() {
        super.onResume();
        ParcelHistory parcelHistory = dataStore.getParcelHistory();
        if (parcelHistory == null) {
            reloadParcelHistory();
        } else {
            setParcelHistory(parcelHistory);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        saveScrollPosition();
        if (parcelHistorySubscription != null && !parcelHistorySubscription.isUnsubscribed()) {
            parcelHistorySubscription.unsubscribe();
            parcelHistorySubscription = null;
        }
    }

    public void saveScrollPosition() {
        LinearLayoutManager manager = (LinearLayoutManager) listView.getLayoutManager();
        firstItem = manager.findFirstCompletelyVisibleItemPosition();

        View firstItemView = manager.findViewByPosition(firstItem);
        if (firstItemView != null)
            topOffset = firstItemView.getTop();
    }

    public void loadScrollPosition() {
        LinearLayoutManager manager = (LinearLayoutManager) listView.getLayoutManager();
        manager.scrollToPositionWithOffset(firstItem, (int) topOffset);
    }

    private void reloadParcelHistory() {
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);
            }
        });

        if (parcelHistorySubscription != null && !parcelHistorySubscription.isUnsubscribed()) {
            parcelHistorySubscription.unsubscribe();
        }
        parcelHistorySubscription =
                api.getParcelHistory(1, BuildConfig.MAX_PREVIOUS_DELIVERIES)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                new Action1<GetParcelHistoryResponse>() {
                                    @Override
                                    public void call(GetParcelHistoryResponse response) {
                                        swipeRefreshLayout.setRefreshing(false);
                                        listView.setVisibility(View.VISIBLE);
                                        if (response.hasErrors()) {
                                            Log.e(TAG, response.getErrors().toString());
                                            showErrorAlertDialog(getString(R.string.unable_to_load_parcels));
                                            return;
                                        }
                                        final ParcelHistory parcelHistory = response.getParcelHistory();
                                        // force certain parcel to be first in list in debug mode
                                        // if specified via the secret menu
                                        if (BuildConfig.DEBUG && DebugUtils.forcedParcelId != null) {
                                            parcelHistory.getActiveParcels().get(0).parcelCode = DebugUtils.forcedParcelId;
                                        }
                                        dataStore.setParcelHistory(parcelHistory);
                                        setParcelHistory(parcelHistory);

                                        // For wearable
                                        WearableUtil wearableUtil = new WearableUtil(getContext());
                                        wearableUtil.refreshParcelHistory();

                                        // Init spotz service.
                                        ((BaseActivity) getActivity()).initSpotzPushService();
                                    }
                                },
                                new Action1<Throwable>() {
                                    @Override
                                    public void call(Throwable throwable) {
                                        swipeRefreshLayout.setRefreshing(false);
                                        listView.setVisibility(View.VISIBLE);
                                        showErrorAlertDialog(getString(R.string.unable_to_load_parcels));
                                    }
                                });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_FURTHER_IMPROVE_PROMPT) {
            setParcelHistory(dataStore.getParcelHistory());
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.notifications_prompt: {
                Snackbar.make(getView(),
                        "This is only required for Android M, coming in October.",
                        Snackbar.LENGTH_SHORT).show();
                break;
            }
            case R.id.further_improve_prompt: {
                final Intent intent = new Intent(getActivity(), SettingsActivity.class);
                intent.putExtra(SettingsActivity.EDIT_PROFILE_ONLY, true);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivityForResult(intent, REQUEST_FURTHER_IMPROVE_PROMPT);
                break;
            }
        }
    }

    private void setParcelHistory(ParcelHistory parcelHistory) {
        this.parcelHistory = parcelHistory;

        swipeRefreshLayout.setRefreshing(false);
        listView.setVisibility(View.VISIBLE);

        final DeliveriesAdapter deliveriesAdapter = new DeliveriesAdapter(getContext(), this.parcelHistory, BuildConfig.MAX_PREVIOUS_DELIVERIES);
        final List<SectionedRecyclerViewAdapter.Section> sections = new ArrayList<>(2);

        final int activeParcelCount = this.parcelHistory.getActiveParcels().size();
        final int previousParcelCount = this.parcelHistory.getPreviousParcels().size();
        final SpannableStringBuilder activeSectionTitle = new SpannableStringBuilder();
        activeSectionTitle.append(getString(R.string.active_deliveries_, activeParcelCount));

        int lengthOfParcelCount = String.valueOf(activeParcelCount).length();
        activeSectionTitle.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.textColorSecondary)),
                //here 2 + lengthOfParcelCount means the numbers and open and close bracket.
                activeSectionTitle.length() - (2 + lengthOfParcelCount), activeSectionTitle.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        sections.add(new SectionedRecyclerViewAdapter.Section(0, activeSectionTitle));

        SpannableStringBuilder previousSectionTitle = new SpannableStringBuilder();
        lengthOfParcelCount = String.valueOf(previousParcelCount).length();
        previousSectionTitle.append(getString(R.string.previous_deliveries_, Math.min(previousParcelCount, BuildConfig.MAX_PREVIOUS_DELIVERIES)));
        previousSectionTitle.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.textColorSecondary)),
                previousSectionTitle.length() - (2 + lengthOfParcelCount), previousSectionTitle.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        sections.add(new SectionedRecyclerViewAdapter.Section(Math.max(activeParcelCount, 1), previousSectionTitle));

        SectionedRecyclerViewAdapter sectionsAdapter = new SectionedRecyclerViewAdapter(
                getActivity(),
                R.layout.item_delivery_list_section_header,
                R.id.text,
                deliveriesAdapter
        );
        final SectionedRecyclerViewAdapter.Section[] sectionsArray = new SectionedRecyclerViewAdapter.Section[sections.size()];
        sections.toArray(sectionsArray);
        sectionsAdapter.setSections(sectionsArray);

        // add a "Bookends" adapter which allows for header prompting user to enable notifications (not required on Android)
        final Bookends<SectionedRecyclerViewAdapter> adapter = new Bookends<>(sectionsAdapter);
        // this is the wrong permission entirely, commenting out as this code may be useful elsewhere
//        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
//                != PackageManager.PERMISSION_GRANTED) {
//            final View notificationsPrompt =
//                    getActivity().getLayoutInflater().inflate(R.layout.prompt_notifications, null);
//            notificationsPrompt.setOnClickListener(this);
//            adapter.addHeader(notificationsPrompt);
//        }

        // Add prompt regarding to further improve.
        ConsumerProfile profile = user.getConsumerProfile();
        if ((profile.mobile == null || profile.email == null) &&
                (user.getAppShowCount() == 1 || user.getAppShowCount() % 3 == 0)) {
            @SuppressLint("InflateParams")
            final View furtherImprovePrompt = getActivity().getLayoutInflater().inflate(R.layout.prompt_further_improve, null);
            final TextView textView = (TextView) furtherImprovePrompt.findViewById(R.id.further_improve_prompt);

            textView.setCompoundDrawablesWithIntrinsicBounds(profile.email == null ? R.drawable.ic_email_white : R.drawable.ic_phone_white, 0, R.drawable.arrow_white, 0);
            textView.setText(getString(R.string.further_improve_message, getString(profile.email == null ? R.string.email : R.string.mobile)));
            furtherImprovePrompt.setOnClickListener(this);
            furtherImprovePrompt.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
            adapter.addHeader(furtherImprovePrompt);
        }

        listView.setAdapter(adapter);
        loadScrollPosition();
    }

    private class DeliveryViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {
        private Parcel parcel;
        private final SpannableStringBuilder builder = new SpannableStringBuilder();
        private final Context context;
        private final Resources resources;
        public final ImageView iconImageView;
        public final TextView labelTextView;
        public final TextView labelTrackingStatus;

        public DeliveryViewHolder(Context context, View itemView) {
            super(itemView);
            this.context = context;
            this.resources = context.getResources();
            iconImageView = (ImageView) itemView.findViewById(R.id.icon_imageview);
            labelTextView = (TextView) itemView.findViewById(R.id.label_textview);
            labelTrackingStatus = (TextView) itemView.findViewById(R.id.txt_trackingStatusCurrent);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (parcel != null) {
                final Intent intent = new Intent(context, ParcelDetailActivity.class);
                intent.putExtra(ParcelDetailActivity.EXTRA_PARCEL_CODE, parcel.parcelCode);
                context.startActivity(intent);
            }
        }

        public void setParcel(Parcel parcel) {
            this.parcel = parcel;
            if (parcel == null) {
                return;
            }

            final String parcelShipperName = parcel.getShipperName();
            final String parcelNumber = parcel.parcelNumber.replace(" ", "\u00A0");
            final CharSequence trackingStatus = UIUtils.formatText(parcel.trackingStatusCurrent, context);
            PhotoUtils.setCustomerImageLogo(context, parcel.customerImageLogo, iconImageView);

            builder.clear();
            builder.append(parcelShipperName);
            builder.setSpan(new ForegroundColorSpan(resources.getColor(R.color.dark_grey)), 0, parcelShipperName.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            builder.setSpan(new StyleSpan(Typeface.BOLD), 0, parcelShipperName.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            builder.append("\n");

            int length = builder.length();
            builder.append(parcelNumber);
            builder.setSpan(new RelativeSizeSpan(0.75f), length, length + parcelNumber.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            builder.setSpan(new ForegroundColorSpan(resources.getColor(R.color.dark_grey)), length, length + parcelNumber.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            //builder.append(trackingStatus);
            labelTextView.setText(builder);
            labelTrackingStatus.setText(trackingStatus);
        }
    }

    private class DeliveriesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        private static final int VIEW_TYPE_NORMAL = 0;
        private static final int VIEW_TYPE_NO_ACTIVE_DELIVERIES = 1;
        private static final int VIEW_TYPE_NO_PREVIOUS_DELIVERIES = 2;

        private final Context context;
        private final ParcelHistory parcelHistory;
        private final int maxPreviousDeliveries;

        public DeliveriesAdapter(Context context, ParcelHistory parcelHistory, int maxPreviousDeliveries) {
            this.context = context;
            this.parcelHistory = parcelHistory;
            this.maxPreviousDeliveries = maxPreviousDeliveries;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            final View view;
            if (viewType == VIEW_TYPE_NO_ACTIVE_DELIVERIES) {
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_delivery_no_active_deliveries, parent, false);
            } else if (viewType == VIEW_TYPE_NO_PREVIOUS_DELIVERIES) {
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_delivery_no_previous_deliveries, parent, false);
            } else {
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_delivery, parent, false);
            }
            return new DeliveryViewHolder(context, view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
            DeliveryViewHolder holder = (DeliveryViewHolder) viewHolder;
            int viewType = getItemViewType(position);
            if (viewType == VIEW_TYPE_NORMAL) {
                final Parcel parcel = getParcel(position);
                holder.setParcel(parcel);
            }
        }

        private Parcel getParcel(int position) {
            final int activeParcelCount = parcelHistory.getActiveParcels().size();
            if (position < activeParcelCount) {
                return parcelHistory.getActiveParcels().get(position);
            } else {
                // remember that we always have 1 position taken either by an active parcel, or
                // the row showing "no active parcels", hence Math.max(1, activeParcelCount)
                return parcelHistory.getPreviousParcels().get(position - Math.max(1, activeParcelCount));
            }
        }

        @Override
        public int getItemViewType(int position) {
            final int activeDeliveryCount = parcelHistory.getActiveParcels().size();
            final int previousParcelCount = parcelHistory.getPreviousParcels().size();
            if (activeDeliveryCount == 0 && position == 0) {
                return VIEW_TYPE_NO_ACTIVE_DELIVERIES;
            } else if (previousParcelCount == 0 && position == Math.max(activeDeliveryCount, 1)) {
                return VIEW_TYPE_NO_PREVIOUS_DELIVERIES;
            } else {
                return VIEW_TYPE_NORMAL;
            }
        }

        @Override
        public int getItemCount() {
            // max 1 per section, so that we can display a "no parcels" row for each
            return Math.max(parcelHistory.getActiveParcels().size(), 1) + Math.min(Math.max(parcelHistory.getPreviousParcels().size(), 1), maxPreviousDeliveries);
        }
    }
}
