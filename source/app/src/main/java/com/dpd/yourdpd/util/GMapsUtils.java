package com.dpd.yourdpd.util;

import android.location.Location;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.util.ArrayList;
import java.util.List;

/**
 * Google Map utilities
 */
public class GMapsUtils {
    private static final String TAG = GMapsUtils.class.getSimpleName();

    public static final float MILES_PER_METER = 0.000621371192f;

    /**
     * Decode a Google Maps string of map points
     *
     * @param encoded String
     * @return List<LatLng>
     */
    public static List<LatLng> decodePoly(String encoded) {
        List<LatLng> poly = new ArrayList<>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((double) lat / 1E5, (double) lng / 1E5);
            poly.add(p);
        }
        return poly;
    }

    /**
     * @param latLng   Target LatLng
     * @param location From location
     * @return Distance in miles, or -1 if distance cannot be calculated
     */
    public static float getDistanceFromLocation(LatLng latLng, Location location) {
        float distance = 0;
        if (location != null) {
            final float[] results = new float[1];
            Location.distanceBetween(
                    location.getLatitude(), location.getLongitude(),
                    latLng.latitude, latLng.longitude, results);
            try {
                distance = results[0] * GMapsUtils.MILES_PER_METER;
            } catch (IllegalArgumentException e) {
                Log.e(TAG, "Unable to calculate distance", e);
            }
        } else {
            return -1;
        }
        return distance;
    }

    public static void zoomWithPadding(final GoogleMap map, final LatLngBounds latLngBounds, LatLng[] latLngs, final int padding) {
        final LatLngBounds.Builder boundsBuilder = LatLngBounds.builder();
        if (latLngBounds != null)
            boundsBuilder.include(latLngBounds.northeast).include(latLngBounds.southwest);

        for (LatLng latLng : latLngs) {
            if (latLng == null) continue;
            boundsBuilder.include(new LatLng(latLng.latitude, latLng.longitude));
        }
        map.moveCamera(CameraUpdateFactory.newLatLngBounds(boundsBuilder.build(), padding));
    }

    public static void zoomWithPadding(final SupportMapFragment mapFragment, LatLng[] latLngs, final int padding, int markerWidth, int markerHeight) {
        zoomWithPadding(mapFragment, latLngs, padding, markerWidth, markerHeight, false);
    }

    public static void zoomWithPadding(final SupportMapFragment mapFragment, LatLng[] latLngs, final int padding, int markerWidth, int markerHeight, final boolean isAnimate) {
        zoomWithPadding(mapFragment, latLngs, padding, markerWidth, markerHeight, isAnimate, null);
    }

    public static void zoomWithPadding(final SupportMapFragment mapFragment, LatLng[] latLngs, final int padding, int markerWidth, int markerHeight, final boolean isAnimate, final ZoomCallback zoomCallback) {
        // Get bounds.
        LatLngBounds oriBounds;
        final LatLngBounds.Builder boundsBuilder = LatLngBounds.builder();
        for (LatLng latLng : latLngs) {
            if (latLng == null) continue;
            boundsBuilder.include(new LatLng(latLng.latitude, latLng.longitude));
        }
        oriBounds = boundsBuilder.build();

        // Get marker height with LatLng and adjust bounds.
        if (mapFragment.getView() != null) {
            double markerWidthWithLatLng = Math.abs(oriBounds.northeast.longitude - oriBounds.southwest.longitude) * markerWidth / mapFragment.getView().getWidth() / 2;
            double markerHeightWithLatLng = Math.abs(oriBounds.northeast.latitude - oriBounds.southwest.latitude) * markerHeight / mapFragment.getView().getHeight();
            for (LatLng latLng : latLngs) {
                if (latLng == null) continue;
                boundsBuilder.include(new LatLng(latLng.latitude + markerHeightWithLatLng, latLng.longitude - markerWidthWithLatLng));
                boundsBuilder.include(new LatLng(latLng.latitude + markerHeightWithLatLng, latLng.longitude + markerWidthWithLatLng));
            }
        }

        // Move camera.
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(boundsBuilder.build(), UIUtils.dpToPx(padding, mapFragment.getContext()));
                if (isAnimate) googleMap.animateCamera(cameraUpdate, 300, null);
                else googleMap.moveCamera(cameraUpdate);

                if (zoomCallback != null) {
                    zoomCallback.onMapReady(googleMap);
                }
            }
        });
    }

    public interface ZoomCallback {
        void onMapReady(GoogleMap googleMap);
    }
}
