package com.dpd.yourdpd.ui.help;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.dpd.yourdpd.R;
import com.dpd.yourdpd.ui.AboutActivity;
import com.dpd.yourdpd.ui.BaseMenuActivity;
import com.dpd.yourdpd.ui.TermsActivity;

/**
 * Displays various help material to user
 */
public class HelpActivity extends BaseMenuActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_help);

        setupToolbar(getString(R.string.help_titlecase), false);
        setSelectedMenuButton(R.id.help_button);

//        findViewById(R.id.how_to_video_button).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(HelpActivity.this, HelpVideoActivity.class);
//                startActivity(intent);
//            }
//        });

        findViewById(R.id.terms_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HelpActivity.this, TermsActivity.class);
                startActivity(intent);
            }
        });

        findViewById(R.id.faqs_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://www.dpd.co.uk/lp/app/index.html"));
                startActivity(browserIntent);
            }
        });

        findViewById(R.id.about_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HelpActivity.this, AboutActivity.class);
                startActivity(intent);
            }
        });
    }
}
