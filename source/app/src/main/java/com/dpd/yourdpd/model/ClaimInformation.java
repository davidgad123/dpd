package com.dpd.yourdpd.model;

import java.io.Serializable;

public class ClaimInformation implements Serializable {
    public String goodsDescription;
    public String packagingDescription;
    public float goodsValue;
}
