package com.dpd.yourdpd.util;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.dpd.yourdpd.BuildConfig;
import com.dpd.yourdpd.R;
import com.dpd.yourdpd.ui.dialog.ImagePickerDialogFragment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Utility methods for loading and uploading images that are picked by the user
 */
public class ImagePickerUtils {
    private static final String TAG = ImagePickerUtils.class.getSimpleName();

    // note that these are bitshifted in FragmentActivity so they will only invoke
    // Fragment.onActivityResult() not the listener Activity.onActivityResult()
    public static final int REQUEST_CAMERA = 1002;
    public static final int REQUEST_GALLERY = 1003;

    public static final int ERROR_NO_IMAGE = 0;
    public static final int ERROR_LOADING_IMAGE = 2;
    public static final int ERROR_SAVING_IMAGE = 3;
    public static final int ERROR_UPLOADING_IMAGE = 4;

    private static final int MAX_SIDE_LENGTH = BuildConfig.MAX_BITMAP_SIZE;

    public interface LoadImageCallback {
        /**
         * @param image raw Bitmap data
         */
        void onSuccess(Bitmap image);

        /**
         * @param progress 0-100
         */
        void onProgress(int progress);

        /**
         * @param code see ERROR code constants
         */
        void onError(int code);
    }

    /**
     * Load image from Intent containing a Content URI, displaying bitmap in target ImageView
     * ensuring maximum image dimensions do not exceed {@link #MAX_SIDE_LENGTH}
     *
     * @param data      Intent where data.getData() is the content URI of the picked image
     * @param context   Context
     * @param imageView (optional) ImageView to display image in
     * @param callback  Callback will receive the Bitmap data
     */
    public static void loadAndDisplayBitmap(Intent data,
                                            Context context,
                                            @Nullable ImageView imageView,
                                            LoadImageCallback callback) {
        if (data == null || data.getData() == null) {
            callback.onError(ERROR_NO_IMAGE);
            return;
        }

        final Uri uri = data.getData();
        // handle images from Photos app, which may require download first
        // NOTE: picking from Gallery is different to picking from Photos app
        Bitmap bitmap;
        try {
            bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);

        } catch (OutOfMemoryError e) {
            e.printStackTrace();

            // Retry operation after free collection.
            System.gc();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);

            } catch (Exception | OutOfMemoryError e1) {
                Log.e(TAG, "Problem loading image", e1);
                callback.onError(ERROR_SAVING_IMAGE);
                return;
            }

        } catch (Exception e) {
            Log.e(TAG, "Problem loading image", e);
            callback.onError(ERROR_SAVING_IMAGE);
            return;
        }
        /*
        // determine orientation
        int rotation = 0;
        String[] columns = {MediaStore.Images.Media.DATA, MediaStore.Images.Media.ORIENTATION};
        Cursor cursor = context.getContentResolver().query(uri, columns, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            int orientationColumnIndex = cursor.getColumnIndex(columns[1]);
            rotation = cursor.getInt(orientationColumnIndex);
            cursor.close();
        }

        // handle maxSideLength scaling
        final Matrix matrix = new Matrix();
        if (bitmap.getWidth() > MAX_SIDE_LENGTH || bitmap.getHeight() > MAX_SIDE_LENGTH) {
            float scale = 1f;
            if (bitmap.getWidth() > bitmap.getHeight()) {
                // landscape
                scale = (float) MAX_SIDE_LENGTH / (float) bitmap.getWidth();
            } else {
                scale = (float) MAX_SIDE_LENGTH / (float) bitmap.getHeight();
            }
            matrix.postScale(scale, scale);
        }

        // handle orientation metatag (bitmap may physically need rotating)
        if (rotation != 0) {
            matrix.postRotate(rotation);
        }
        */

        final Bitmap outBitmap = scaleBitmap(bitmap, MAX_SIDE_LENGTH);
        if (imageView != null) {
            imageView.setVisibility(View.VISIBLE);
            imageView.setImageBitmap(outBitmap);
        }

        callback.onProgress(100);
        callback.onSuccess(outBitmap);
    }

    /**
     * Load image from Intent containing a Content URI, displaying bitmap in target ImageView
     *
     * @param file      bitmap File
     * @param imageView target ImageView to display loaded Bitmap in
     * @param callback  Callback with methods to invoke at various stages
     */
    public static void loadAndDisplayBitmap(File file,
                                            @Nullable ImageView imageView,
                                            LoadImageCallback callback) {
        Bitmap bitmap;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        try {
            bitmap = BitmapFactory.decodeStream(new FileInputStream(file), null, options);
        } catch (FileNotFoundException e) {
            Log.e(TAG, "File not found", e);
            callback.onError(ERROR_LOADING_IMAGE);
            return;
        }

        // determine orientation
        int rotation = 0;
        try {
            final ExifInterface exif = new ExifInterface(file.getPath());
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
            if (orientation == ExifInterface.ORIENTATION_ROTATE_90) rotation = 90;
            else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) rotation = 180;
            else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) rotation = 270;
        } catch (IOException e) {
            Log.w(TAG, "Unable to determine image orientation");
        }

        // handle maxSideLength scaling
        final Matrix matrix = new Matrix();
        if (bitmap.getWidth() > MAX_SIDE_LENGTH || bitmap.getHeight() > MAX_SIDE_LENGTH) {
            float scale = 1f;
            if (bitmap.getWidth() > bitmap.getHeight()) {
                // landscape
                scale = (float) MAX_SIDE_LENGTH / (float) bitmap.getWidth();
            } else {
                scale = (float) MAX_SIDE_LENGTH / (float) bitmap.getHeight();
            }
            matrix.postScale(scale, scale);
        }

        // handle orientation metatag (bitmap may physically need rotating)
        if (rotation != 0) {
            matrix.postRotate(rotation);
        }
        final Bitmap outBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);

        if (imageView != null) {
            imageView.setVisibility(View.VISIBLE);
            imageView.setImageBitmap(outBitmap);
        }
        callback.onProgress(100);
        callback.onSuccess(outBitmap);
    }

    private static Bitmap scaleBitmap(Bitmap bitmap, int maxWidth) {
        float ratio = (float) bitmap.getHeight() / (float) bitmap.getWidth();
        int width, height;
        if (ratio > 1) {
            height = Math.min(bitmap.getHeight(), maxWidth);
            width = (int) (height / ratio);
        } else {
            width = Math.min(bitmap.getWidth(), maxWidth);
            height = (int) (ratio * width);
        }
        return Bitmap.createScaledBitmap(bitmap, width, height, true);
    }

    public static BitmapFactory.Options getDimensionFromResource(Context context, int resourceId) {
        BitmapFactory.Options dimensions = new BitmapFactory.Options();
        dimensions.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(context.getResources(), resourceId, dimensions);
        return dimensions;
    }

    /*
     * Image Picker Utils.
     */
    public static void startActivityForCamera(Fragment fragment) {
        final Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        final File file = new File(fragment.getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES), "temp.jpg");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
        fragment.startActivityForResult(intent, REQUEST_CAMERA);
    }

    public static void showPickerDialog(final Fragment fragment) {
        final ImagePickerDialogFragment dialog = ImagePickerDialogFragment.newInstance();
        dialog.setListener(new ImagePickerDialogFragment.Listener() {
            /**
             * ImagePickerDialogFragment listener interface methods
             */
            @Override
            public void onTakePicture() {
                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1 && PermissionUtil.shouldWeAsk(fragment.getContext(), Manifest.permission.CAMERA)) {
                    fragment.requestPermissions(new String[]{Manifest.permission.CAMERA}, PermissionUtil.REQUEST_CODE);
                } else {
                    startActivityForCamera(fragment);
                }
            }

            @Override
            public void onChooseFromGallery() {
                final Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                fragment.startActivityForResult(Intent.createChooser(intent, fragment.getString(R.string.choose_photo)), REQUEST_GALLERY);
            }

            @Override
            public void onCancel() {
            }
        });
        dialog.show(fragment.getFragmentManager(), null);
    }

    public static void onRequestPermissionsResult(Fragment fragment, int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == PermissionUtil.REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                PermissionUtil.markAsAsked(fragment.getContext(), Manifest.permission.CAMERA);

                // Now user should be able to use camera
                startActivityForCamera(fragment);
            }
        }
    }

    // NOTE: we were previously planning on saving the file to disk for upload to server, since
    // switching to purely reading Bitmap data and converting to base64, leaving code in place
    // commented in case we switch to file uploads again

//    public interface UploadImageCallback {
//        void onSuccess();
//        /**
//         * @param progress 0-100
//         */
//        void onProgress(int progress);
//
//        /**
//         * @param code see ERROR code constants
//         */
//        void onError(int code);
//    }

//    /**
//     * @param data Intent where data.getData() is the content URI of the image
//     * @param context Context
//     * @param imageView (optional) ImageView to display image in
//     * @param callback Callback
//     */
//    public static void uploadAndDisplayImage(Intent data,
//                                             Context context,
//                                             @Nullable ImageView imageView,
//                                             UploadImageCallback callback) {
//        if(data==null || data.getData()==null) {
//            callback.onError(ERROR_NO_IMAGE);
//            return;
//        }
//        final Uri contentURI = data.getData();
//        uploadAndDisplayImage(contentURI, context, imageView, callback);
//    }
//
//    /**
//     * @param file image File
//     * @param context Context
//     * @param imageView (optional) ImageView to display image in
//     * @param callback Callback
//     */
//    public static void uploadAndDisplayImage(File file,
//                                             Context context,
//                                             @Nullable ImageView imageView,
//                                             UploadImageCallback callback) {
//        final Uri contentURI = Uri.parse("file://"+file.getAbsolutePath());
//        uploadAndDisplayImage(contentURI, context, imageView, callback);
//    }
//
//    /**
//     * @param uri content Uri
//     * @param context Context
//     * @param imageView (optional) ImageView to display image in
//     * @param callback Callback
//     */
//    public static void uploadAndDisplayImage(Uri uri,
//                                             Context context,
//                                             @Nullable ImageView imageView,
//                                             UploadImageCallback callback) {
//        final File tempFile = new File(context
//                .getExternalFilesDir(Environment.DIRECTORY_PICTURES), "upload.jpg");
//        final InputStream is;
//        final OutputStream os;
//        // handle images from Photos app, which may require download first
//        // NOTE: picking from Gallery is different to picking from Photos app
////                if (contentURI.toString()
////                        .startsWith("content://com.google.android.apps.photos.content")) {
//        // save image to disk so we can upload it
//        try {
//            // TODO: do on background thread
//            // TODO: consider shrinking/compressing bitmap first
//            is = context.getContentResolver().openInputStream(uri);
//            os = new BufferedOutputStream(
//                    new FileOutputStream(tempFile));
//            int bufferSize = 1024;
//            byte[] buffer = new byte[bufferSize];
//            int len;
//            while ((len = is.read(buffer)) != -1) {
//                os.write(buffer, 0, len);
//            }
//            os.close();
//
//            // TODO: upload image? (also see InFlightLeaveInSafePlaceFragment)
//
//            tempFile.delete();
//
//            callback.onProgress(100);
//
//        } catch (IOException e) {
//            // TODO: unable to upload - warn user
//            callback.onError(ERROR_SAVING_IMAGE);
//        }
//
//        if(imageView != null) {
//            imageView.setVisibility(View.VISIBLE);
//            Picasso.with(context)
//                    .load(uri)
//                    .fit() // required for camera images which are too large to use in ImageView
//                    .into(imageView);
//        }
//
//        callback.onSuccess();
//    }
}
