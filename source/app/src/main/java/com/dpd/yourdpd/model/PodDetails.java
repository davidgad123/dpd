package com.dpd.yourdpd.model;

import java.util.Date;

/**
 * Proof of delivery details
 */
public class PodDetails {
    public String podName;
    public String podNeighbour;
    public String podSafePlaceCode;
    public String podDate;
}
