package com.dpd.yourdpd.ui.pin;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.transitions.everywhere.TransitionManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.dpd.yourdpd.R;
import com.dpd.yourdpd.model.PinMethod;
import com.dpd.yourdpd.net.APIError;
import com.dpd.yourdpd.net.response.AuthenticatePinParams;
import com.dpd.yourdpd.net.response.AuthenticatePinResponse;
import com.dpd.yourdpd.ui.BaseFragment;
import com.dpd.yourdpd.util.AppConstant;
import com.dpd.yourdpd.util.CommonUtils;
import com.dpd.yourdpd.util.TextUtils;

import br.com.sapereaude.maskedEditText.MaskedEditText;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Fragment allowing user to input a PIN which is verified against an API.
 */
public class AuthPinFragment extends BaseFragment {
    private static final String TAG = AuthPinFragment.class.getSimpleName();

    private final static String ARG_PIN_METHOD = "pin_method";

    private View errorTooltip;
    private Button requestNewPinButton;
    private ProgressBar progressBar;
    private EditText pinPrefixEditText;
    private MaskedEditText pinEditText;
    private InputMethodManager inputMethodManager;
    private PinMethod pinMethod; // TODO: could use this to customise user messaging

    private FragmentInteractionListener listener;
    private Subscription authPinSubscription;
    private String requestStr;

    public interface FragmentInteractionListener {
        void onRequestNewPin();

        void onPinVerified(String consumerId, boolean deviceConflict);
    }

    /**
     * @param pinMethod The PinMethod used to request the PIN number
     * @return a new AuthPinFragment
     */
    public static AuthPinFragment newInstance(PinMethod pinMethod) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_PIN_METHOD, pinMethod);
        AuthPinFragment fragment = new AuthPinFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public AuthPinFragment() {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (FragmentInteractionListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() +
                    " must implement " + FragmentInteractionListener.class.getName());
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pinMethod = (PinMethod) getArguments().getSerializable(ARG_PIN_METHOD);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.fragment_enter_pin, container, false);
        errorTooltip = root.findViewById(R.id.error_tooltip);
        requestNewPinButton = (Button) root.findViewById(R.id.request_new_pin_button);
        requestNewPinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestNewPin();
            }
        });
        progressBar = (ProgressBar) root.findViewById(R.id.progressBar);
        pinPrefixEditText = (EditText) root.findViewById(R.id.pin_prefix_edittext);
        pinEditText = (MaskedEditText) root.findViewById(R.id.pin_edittext);
        pinEditText.setTypeface(Typeface.MONOSPACE);
        pinEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                final int pinLength = getResources().getInteger(R.integer.pin_length);
                if (actionId == EditorInfo.IME_ACTION_GO
                        && TextUtils.getRealTextFromPIN(pinEditText.getText().toString(), pinEditText.getMask(), pinEditText.getViewHolderChar()).length() == pinLength) {
                    performAuthPinAPICall();
                }
                return false;
            }
        });
        pinEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                hideError();
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!pinEditText.isEditingAfter()) return;

                final int pinLength = getResources().getInteger(R.integer.pin_length);
                String pin = TextUtils.getRealTextFromPIN(s.toString(), pinEditText.getMask(), pinEditText.getViewHolderChar());
                if (pin.length() == pinLength) {
                    performAuthPinAPICall();
                }
            }
        });

        if (getArguments() != null) {
            int request_type = getArguments().getInt(AppConstant.BUNDLE_PIN_TYPE, 0);
            requestStr = getArguments().getString(AppConstant.BUNDLE_REQUEST_STRING);
            TextView subtitle = (TextView) root.findViewById(R.id.subtitle);
            switch (request_type) {
                case AppConstant.REQUEST_PIN_MAIL: {
                    subtitle.setText(getString(R.string.enter_verification_pin_email));
                }
                break;
                case AppConstant.REQUEST_PIN_PHONE: {
                    subtitle.setText(getString(R.string.enter_verification_pin_phone));
                }
                break;
                default:
                    break;
            }
        }

        CommonUtils.setupUIForKeyboard(getActivity(), root);

        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        inputMethodManager =
                (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
    }

    private void closeKeyboard() {
        final View view = getView();
        if (view != null) {
            inputMethodManager.hideSoftInputFromWindow(
                    view.getWindowToken(), InputMethodManager.HIDE_IMPLICIT_ONLY);
        }
    }

    private void requestNewPin() {
        listener.onRequestNewPin();
    }

    private void showErrorText(String error) {
        final TextView textView = (TextView) errorTooltip.findViewById(R.id.tooltip_textview);
        textView.setText(error);
        TransitionManager.beginDelayedTransition((ViewGroup) getView());
        errorTooltip.setVisibility(View.VISIBLE);
    }

    private void hideError() {
        if (errorTooltip.getVisibility() == View.VISIBLE) {
            TransitionManager.beginDelayedTransition((ViewGroup) getView());
            errorTooltip.setVisibility(View.GONE);
        }
    }

    private void performAuthPinAPICall() {
        hideError();
        showLoading();

        final AuthenticatePinParams params = new AuthenticatePinParams();
        params.deviceId = user.getConsumerProfile().deviceId;
        params.pinNumber = TextUtils.getRealTextFromPIN(pinEditText.getText().toString(), pinEditText.getMask(), pinEditText.getViewHolderChar());
        params.mediumValue = requestStr;
        params.medium = user.getPinMethod().equals(PinMethod.SMS) ? "sms" : "email";

        if (authPinSubscription != null && !authPinSubscription.isUnsubscribed()) {
            authPinSubscription.unsubscribe();
        }
        authPinSubscription = api.authenticatePin(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<AuthenticatePinResponse>() {
                               @Override
                               public void call(AuthenticatePinResponse response) {
                                   hideLoading();
                                   if (response.hasErrors()) {
                                       Log.e(TAG, "Error authenticating pin: " + response.printErrors());
                                       pinEditText.setText("");
                                       final APIError error = response.getErrors().get(0);
//                                       if (APIError.ErrorCode.INVALID_FIELD_VALUE.toString().equals(error.errorCode)) {
                                           showErrorText(getString(R.string.pin_not_recognised_message));
//                                       } else {
//                                           showErrorText(getString(R.string.pin_not_recognised_message));
//                                       }
                                   } else {
                                       listener.onPinVerified(
                                               response.data.consumerId,
                                               response.data.deviceConflict);
                                   }
                               }
                           },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                Log.e(TAG, "Error authenticating pin: ", throwable);
                                hideLoading();
                                showErrorText(getString(R.string.a_problem_has_occurred_try_again));
                            }
                        });
    }

    private void showLoading() {
        requestNewPinButton.setEnabled(false);
        progressBar.setVisibility(View.VISIBLE);
        pinPrefixEditText.setEnabled(false);
        pinEditText.setEnabled(false);
    }

    private void hideLoading() {
        requestNewPinButton.setEnabled(true);
        progressBar.setVisibility(View.GONE);
        pinPrefixEditText.setEnabled(true);
        pinEditText.setEnabled(true);
    }
}