package com.dpd.yourdpd.net.response;

import com.dpd.yourdpd.model.RouteView;
import com.google.gson.annotations.SerializedName;

/**
 * @see com.dpd.yourdpd.net.Api#getRouteViewOnRoute(String, String)
 */
public class GetRouteViewOnRouteResponse extends BaseResponse {
    @SerializedName("data")
    private RouteView routeView;

    public RouteView getRouteView() {
        return routeView;
    }
}
