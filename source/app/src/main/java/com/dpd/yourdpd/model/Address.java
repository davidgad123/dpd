package com.dpd.yourdpd.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

public class Address implements Parcelable {

    /**
     * addressId is only populated by certain API calls, e.g. Lookup Address Key
     */
    public String addressId;
    /**
     * addressPoint is not populated by ALL API calls and so may be null
     */
    public AddressPoint addressPoint;
    public String organisation;
    public String property;
    public String street;
    public String locality;
    public String town;
    public String county;
    public String postcode;
    public String countryCode;
    public String countryName;

    public Address() {
    }

    public Address(Parcel in) {
        organisation = in.readString();
        property = in.readString();
        street = in.readString();
        locality = in.readString();
        town = in.readString();
        county = in.readString();
        postcode = in.readString();
        countryCode = in.readString();
        countryName = in.readString();
        addressId = in.readString();
        hasAgentDepot = in.readInt() == 1;
    }

    public static final Creator<Address> CREATOR = new Creator<Address>() {
        @Override
        public Address createFromParcel(Parcel in) {
            return new Address(in);
        }

        @Override
        public Address[] newArray(int size) {
            return new Address[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(organisation);
        dest.writeString(property);
        dest.writeString(street);
        dest.writeString(locality);
        dest.writeString(town);
        dest.writeString(county);
        dest.writeString(postcode);
        dest.writeString(countryCode);
        dest.writeString(countryName);
        dest.writeString(addressId);
        dest.writeInt(hasAgentDepot ? 1 : 0);
    }

    @Override
    public String toString() {
        return toFormattedString();
    }

    /**
     * Constructs the first line of the address, e.g. number/name + street
     */
    public String getFirstLine() {
        return (property == null ? "" : property + " ") +
                (street == null ? "" : street);
    }

    public String getFirstLineWithTown() {
        return getFirstLine() + (town == null ? "" : ", " + town);
    }

    public String toFormattedString(boolean multipleLines) {
        StringBuilder builder = new StringBuilder();
        /*if (!TextUtils.isEmpty(property)) {
            builder.append(property);
            builder.append(", ");
            if (multipleLines) {
                builder.append("\n");
            }
        }*/
        if (!TextUtils.isEmpty(street)) {
            builder.append(street);
            builder.append(", ");
            if (multipleLines) {
                builder.append("\n");
            }
        }
        if (!TextUtils.isEmpty(town)) {
            builder.append(town);
            builder.append(", ");
            if (multipleLines) {
                builder.append("\n");
            }
        }
        if (!TextUtils.isEmpty(county)) {
            builder.append(county);
            builder.append(", ");
            if (multipleLines) {
                builder.append("\n");
            }
        }
        if (!TextUtils.isEmpty(postcode)) {
            builder.append(postcode);
        }
        return builder.toString();
    }

    public String toFormattedString() {
        return toFormattedString(false);
    }

    public String getStreetAndTown() {
        StringBuilder builder = new StringBuilder();
        if (!TextUtils.isEmpty(street)) {
            builder.append(street);
            builder.append(", ");
        }
        if (!TextUtils.isEmpty(town)) {
            builder.append(town);
            builder.append(", ");
        }
        if (!TextUtils.isEmpty(postcode)) {
            builder.append(postcode);
        }
        return builder.toString();
    }

    /**
     * Virtual property (not returned by API) indicating this property has an agent (not DPD) depot
     * This is determined by the Check Property API. An agent depot means certain customer prefs
     * are not available (such as delivery to neighbour, safe place etc).
     */
    private boolean hasAgentDepot;

    public boolean hasAgentDepot() {
        return hasAgentDepot;
    }

    public void setHasAgentDepot(boolean hasAgentDepot) {
        this.hasAgentDepot = hasAgentDepot;
    }
}
