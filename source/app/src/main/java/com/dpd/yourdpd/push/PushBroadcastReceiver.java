package com.dpd.yourdpd.push;

import com.localz.spotzpush.sdk.receiver.DefaultBroadcastReceiver;

public class PushBroadcastReceiver extends DefaultBroadcastReceiver {
    public PushBroadcastReceiver() {
        super(PushIntentService.class.getName());
    }
}
