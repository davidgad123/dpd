package com.dpd.yourdpd.net.response;

import com.dpd.yourdpd.net.request.GeneratePinBySmsParams;

/**
 * @see com.dpd.yourdpd.net.Api#generatePinBySMS(GeneratePinBySmsParams)
 */
public class GeneratePinResponse extends BaseResponse {
    public SuccessResponse data;
}
