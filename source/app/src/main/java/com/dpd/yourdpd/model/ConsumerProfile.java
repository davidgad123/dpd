package com.dpd.yourdpd.model;

import android.content.Context;
import android.util.Log;

import com.dpd.yourdpd.net.Api;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * A ConsumerProfile contains personal details and preferences pertaining to a user of the system
 */
public class ConsumerProfile {
    private static final String TAG = ConsumerProfile.class.getSimpleName();

    public String deviceId;
    public String notificationId;
    public String mobile;
    public String email;
    public String firstname;
    public String surname;
    public String photograph;
    @SerializedName("consumerProperty")
    private List<ConsumerProperty> consumerProperties;

    /**
     * Collection which maps ConsumerProperties to related UDPRNAddresses for easy lookup.
     * This collection is populated as part of the {@link User#updateConsumerProfile(Api, Context)} call
     * NOTE: this field is excluded from {@link User#putConsumerProfile(Api, boolean)}
     */
    private List<ConsumerPropertyAddressInfo> consumerPropertyAddressInfos;

    public ConsumerProfile(final ConsumerProfile profile) {
        deviceId = profile.deviceId;
        notificationId = profile.notificationId;
        mobile = profile.mobile;
        email = profile.mobile;
        firstname = profile.firstname;
        surname = profile.surname;
        photograph = profile.photograph;
        consumerProperties = new ArrayList<>(profile.consumerProperties);
        consumerPropertyAddressInfos = new ArrayList<>(profile.consumerPropertyAddressInfos);
    }

    public ConsumerProfile() {
        if (consumerProperties == null) {
            consumerProperties = new ArrayList<>(1);
        }
    }

    /**
     * @return Whether the user has 1 or more ConsumerProperties
     */
    public boolean hasConsumerProperties() {
        return consumerProperties != null && consumerProperties.size() > 0;
    }

    public List<ConsumerProperty> getConsumerProperties() {
        return consumerProperties;
    }

    /**
     * @param consumerPropertyId String
     * @return ConsumerProperty with matching consumerPropertyId else null
     */
    public ConsumerProperty getConsumerPropertyWithId(String consumerPropertyId) {
        if (consumerProperties == null) return null;
        for (ConsumerProperty consumerProperty : consumerProperties) {
            if (consumerProperty.consumerPropertyId.equals(consumerPropertyId)) {
                return consumerProperty;
            }
        }
        Log.w(TAG, String.format("Unable to find ConsumerProperty with consumerPropertyId %s", consumerPropertyId));
        return null;
    }

    public boolean removeConsumerPropertyWithId(String consumerPropertyId) {
        if (consumerProperties == null) {
            return false;
        }
        // remove ConsumerProperty
        for (ConsumerProperty consumerProperty : consumerProperties) {
            if (consumerProperty.consumerPropertyId.equals(consumerPropertyId)) {
                consumerProperties.remove(consumerProperty);
                // also remove matching ConsumerPropertyAddressInfo entry
                removeConsumerPropertyAddressInfoWithId(consumerPropertyId);
                return true;
            }
        }
        Log.w(TAG, String.format("Unable to find ConsumerProperty with consumerPropertyId %s", consumerPropertyId));
        return false;
    }

    public void setConsumerPropertyAddressInfo(List<ConsumerPropertyAddressInfo> consumerPropertyAddresses) {
        this.consumerPropertyAddressInfos = consumerPropertyAddresses;
    }

    public List<ConsumerPropertyAddressInfo> getConsumerPropertyAddressInfos() {
        if (this.consumerPropertyAddressInfos == null) {
            this.consumerPropertyAddressInfos = new ArrayList<>(1);
        }
        return this.consumerPropertyAddressInfos;
    }

    /**
     * Utility method that returns a ConsumerPropertyAddressInfo with matching ConsumerProperty ID
     *
     * @param consumerPropertyId String
     * @return null if not found
     */
    public ConsumerPropertyAddressInfo getConsumerPropertyAddressInfoWithId(String consumerPropertyId) {
        if (consumerPropertyAddressInfos == null) {
            return null;
        }
        for (ConsumerPropertyAddressInfo addressInfo : consumerPropertyAddressInfos) {
            final ConsumerProperty consumerProperty = addressInfo.consumerProperty;
            if (consumerProperty.consumerPropertyId.equals(consumerPropertyId)) {
                return addressInfo;
            }
        }
        return null;
    }

    public boolean removeConsumerPropertyAddressInfoWithId(String consumerPropertyId) {
        if (consumerPropertyAddressInfos == null) {
            return false;
        }
        for (ConsumerPropertyAddressInfo addressInfo : consumerPropertyAddressInfos) {
            final ConsumerProperty consumerProperty = addressInfo.consumerProperty;
            if (consumerProperty.consumerPropertyId.equals(consumerPropertyId)) {
                consumerPropertyAddressInfos.remove(addressInfo);
                return true;
            }
        }
        return false;
    }
}