package com.dpd.yourdpd.util;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.transitions.everywhere.AutoTransition;
import android.transitions.everywhere.Transition;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.dpd.yourdpd.R;
import com.dpd.yourdpd.model.Parcel;

/**
 * User interface related utility methods
 */
public class UIUtils {
    private static final String TAG = UIUtils.class.getSimpleName();

    private static float DIP_SCALE = -1;

    public static int dpToPx(int dp, Context context) {
        if (DIP_SCALE == -1) DIP_SCALE = context.getResources().getDisplayMetrics().density;
        return (int) (dp * DIP_SCALE + 0.5f); // 0.5f for rounding
    }

    public static int pxToDp(int px, Context context) {
        if (DIP_SCALE == -1) DIP_SCALE = context.getResources().getDisplayMetrics().density;
        return (int) (px / DIP_SCALE + 0.5f); // 0.5f for rounding
    }

    public static int getActionBarHeight(Context context) {
        int actionBarHeight = 0;
        TypedValue tv = new TypedValue();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            if (context.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv,
                    true))
                actionBarHeight = TypedValue.complexToDimensionPixelSize(
                        tv.data, context.getResources().getDisplayMetrics());
        } else {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data,
                    context.getResources().getDisplayMetrics());
        }
        return actionBarHeight;
    }

    /**
     * Scroll ScrollView to show View on screen after Transition completes
     *
     * @param scrollView ScrollView to scroll
     * @param view       View to scroll to
     * @param transition Transition to monitor
     */
    public static void smoothScrollToViewAfterTransition(final ScrollView scrollView,
                                                         final View view,
                                                         AutoTransition transition) {
        transition.addListener(new Transition.TransitionListenerAdapter() {
            @Override
            public void onTransitionEnd(Transition transition) {
                transition.removeListener(this);
                int[] location = {0, 0};
                if (view != null && view.isShown()) {
                    view.getLocationOnScreen(location);
                    scrollView.smoothScrollTo(0, location[1] + view.getHeight());
                }
            }
        });
    }

    /**
     * Colour "REDTEXT" span tags, and replace text between special **, $$ and ## character
     * tokens with bold, small text and coloured spans respectively.
     *
     * @param text    String unformatted tracking status
     * @param context Context to aquire resources from
     * @return Formatted CharSequence to be applied to a TextView
     */
    public static CharSequence formatText(String text, Context context) {
        CharSequence trackingStatus = text
                .replace("<SPAN class=\"REDTEXT\">", "##")
                .replace("</SPAN>", "##");

        // replace tokenism text with special formatting spans
        trackingStatus = TextUtils.setSpanBetweenTokens(trackingStatus, "**", new StyleSpan(Typeface.BOLD));
        trackingStatus = TextUtils.setSpanBetweenTokens(trackingStatus, "§§", new RelativeSizeSpan(0.75f));
        trackingStatus = TextUtils.setSpanBetweenTokens(trackingStatus, "##", new ForegroundColorSpan(context.getResources().getColor(R.color.colorPrimary)));
        return trackingStatus;
    }

    /**
     * Populate an inflated parcel_status.xml View with given Parcel information
     *
     * @param view    inflated View
     * @param context Context
     * @param parcel  Parcel
     */
    public static void populateParcelStatus(View view, Context context, Parcel parcel) {
        final ImageView logoImageView = (ImageView) view.findViewById(R.id.icon_imageview);
        final TextView labelTextView = (TextView) view.findViewById(R.id.label_textview);
        final TextView trackingStatusView = (TextView) view.findViewById(R.id.txt_trackingStatusCurrent);
        PhotoUtils.setCustomerImageLogo(context, parcel.customerImageLogo, logoImageView);

        final SpannableStringBuilder builder = new SpannableStringBuilder();
        final String parcelShipperName = parcel.getShipperName();
        final String parcelNumber = parcel.parcelNumber.replace(" ", "\u00A0");

        builder.clear();
        builder.append(parcelShipperName);
        builder.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.dark_grey)), 0, parcelShipperName.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.setSpan(new StyleSpan(Typeface.BOLD), 0, parcelShipperName.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.append("\n");

        int length = builder.length();
        builder.append(parcelNumber);
        builder.setSpan(new RelativeSizeSpan(0.75f), length, length + parcelNumber.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.dark_grey)), length, length + parcelNumber.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        labelTextView.setText(builder);
        trackingStatusView.setText(UIUtils.formatText(parcel.trackingStatusCurrent, context));
    }

    public static CharSequence makesAddBoldText(CharSequence original, CharSequence boldAddText) {
        SpannableStringBuilder builder = new SpannableStringBuilder();

        builder.append(boldAddText == null ? "Destination Address" : boldAddText);
        builder.setSpan(new StyleSpan(Typeface.BOLD), 0, builder.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.append("\n" + original);
        return builder;
    }

    public static String getEndTime(String startAndEnd) {
        String end = startAndEnd.substring(startAndEnd.indexOf("-") + 2);
        return end;
    }
}
