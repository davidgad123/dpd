package com.dpd.yourdpd.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.dpd.yourdpd.R;
import com.dpd.yourdpd.model.Base64Photo;
import com.dpd.yourdpd.util.PhotoUtils;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.List;

/**
 * Displays a slideshow of Photos with optional descriptions
 */
public class PhotosActivity extends BaseMenuActivity {
    public static final String EXTRA_PHOTOS = "photos";
    public static final String EXTRA_STARTING_POSITION = "starting_position";
    public static final String EXTRA_TITLE = "title";
    public static final String EXTRA_DPD_SESSION = "dpdsession";

    private List<Base64Photo> photos;
    private ViewPager viewPager;
    private TextView labelTextView;
    private ImageButton previousButton;
    private ImageButton nextButton;

    @SuppressWarnings("unchecked")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photos);

        photos = (List<Base64Photo>) getIntent().getSerializableExtra(EXTRA_PHOTOS);
        final String dpdSession = getIntent().getStringExtra(EXTRA_DPD_SESSION); // was required for hitting GET raw image API
        final int startingPosition = getIntent().getIntExtra(EXTRA_STARTING_POSITION, 0);

        String title = getIntent().getStringExtra(EXTRA_TITLE);
        if (title == null) {
            title = "";
        }
        setupToolbar(title, true);

        labelTextView = (TextView) findViewById(R.id.label_textview);
        previousButton = (ImageButton) findViewById(R.id.previous_button);
        nextButton = (ImageButton) findViewById(R.id.next_button);

        previousButton.setOnClickListener(prevNextOnClickListener);
        nextButton.setOnClickListener(prevNextOnClickListener);

        final CirclePageIndicator pageIndicator = (CirclePageIndicator) findViewById(R.id.viewPagerIndicator);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        viewPager.addOnPageChangeListener(onPageChangeListener);
        viewPager.setAdapter(new PhotosPagerAdapter(this, photos));
        pageIndicator.setViewPager(viewPager);
        viewPager.setCurrentItem(startingPosition);

        updateForPage(startingPosition);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home: {
                finish();
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    private View.OnClickListener prevNextOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.previous_button: {
                    viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
                    break;
                }
                case R.id.next_button: {
                    viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                    break;
                }
            }
        }
    };

    private ViewPager.OnPageChangeListener onPageChangeListener =
            new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                }

                @Override
                public void onPageSelected(int position) {
                    updateForPage(position);
                }

                @Override
                public void onPageScrollStateChanged(int state) {
                }
            };

    private void updateForPage(int position) {
        labelTextView.setText(photos.get(position).caption);
        previousButton.setEnabled(position > 0);
        previousButton.setAlpha(position > 0 ? 1 : 0.2f);
        nextButton.setEnabled(position < photos.size() - 1);
        nextButton.setAlpha(position < photos.size() - 1 ? 1 : 0.2f);
    }

    private static class PhotosPagerAdapter extends PagerAdapter {
        private static final String TAG = PhotosPagerAdapter.class.getSimpleName();

        private final LayoutInflater inflater;
        private final List<Base64Photo> photos;

        public PhotosPagerAdapter(Context context, List<Base64Photo> photos) {
            this.photos = photos;
            this.inflater = LayoutInflater.from(context);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            final Base64Photo photo = photos.get(position);
            final ImageView imageView = (ImageView) inflater.inflate(R.layout.item_photo, container, false);
            try {
                PhotoUtils.loadBase64ImageIntoImageView(photo.base64Data, imageView);
            } catch (Exception e) {
                Log.e(TAG, "Unable to decode image");
            }
            container.addView(imageView);
            return imageView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getCount() {
            return photos == null ? 0 : photos.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }
    }
}
