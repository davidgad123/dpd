package com.dpd.yourdpd.model;

import org.joda.time.DateTime;

/**
 * Created by richardleggett on 16/06/15.
 */
public class PlaceholderParcelEvent {
    public DateTime dateTime;
    public String location;
    public String description;

    public PlaceholderParcelEvent(DateTime dateTime, String location, String description) {
        this.dateTime = dateTime;
        this.location = location;
        this.description = description;
    }
}
