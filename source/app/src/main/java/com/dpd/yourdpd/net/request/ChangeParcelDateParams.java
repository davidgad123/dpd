package com.dpd.yourdpd.net.request;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * @see com.dpd.yourdpd.net.Api#changeParcelDate(ChangeParcelDateParams)
 */
public class ChangeParcelDateParams {
    private String caseCode;
    private String contactName;
    private String deliveryInstructions;
    private String diaryDate;
    private String email;
    private String mobile;
    private String[] parcelCode;
    private boolean isInternationalParcel;

    public static class Builder {
        private ChangeParcelDateParams params = new ChangeParcelDateParams();

        public Builder() {
            // setup defaults
            params.caseCode = "";
            params.deliveryInstructions = "";
            params.contactName = "";
            params.parcelCode = new String[] {};
            params.isInternationalParcel = false;
        }

        /**
         * caseCode is only needed if there is an issue (Salesforce case code)
         */
        public Builder setCaseCode(String caseCode) {
            params.caseCode= caseCode;
            return this;
        }
        public Builder setContactName(String contactName) {
            params.contactName = contactName;
            return this;
        }
        public Builder setDeliveryInstructions(String deliveryInstructions) {
            params.deliveryInstructions = deliveryInstructions;
            return this;
        }
        public Builder setDiaryDate(String date) {
            params.diaryDate = date;
            return this;
        }
        public Builder setEmail(String email) {
            params.email = email;
            return this;
        }
        public Builder setMobile(String mobile) {
            params.mobile = mobile;
            return this;
        }
        public Builder setParcelCode(String parcelCode) {
            params.parcelCode = new String[] { parcelCode };
            return this;
        }
        // TODO: what tells us if it's an international parcel?
        public Builder setIsInternationalParcel(boolean isInternationalParcel) {
            params.isInternationalParcel = isInternationalParcel;
            return this;
        }

        public ChangeParcelDateParams build() {
            return params;
        }
    }

}
