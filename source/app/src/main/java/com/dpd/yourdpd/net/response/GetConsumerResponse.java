package com.dpd.yourdpd.net.response;

import com.dpd.yourdpd.model.ConsumerProfile;
import com.dpd.yourdpd.net.Api;
import com.google.gson.annotations.SerializedName;

/**
 * @see Api#getConsumer()
 */
public class GetConsumerResponse extends BaseResponse {
    @SerializedName("data")
    public ConsumerProfile consumerProfile;
}
