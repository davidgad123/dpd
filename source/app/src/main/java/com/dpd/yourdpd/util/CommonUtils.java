package com.dpd.yourdpd.util;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import java.util.List;

public class CommonUtils {
    public static final String PHONE_PREFIX_REGCODE = "^(0|44|\\+44)";
    private static InputMethodManager imm = null;

    public static boolean isNetworkAvailable(Context context) {
        boolean status = false;
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getNetworkInfo(0);
            if (netInfo != null && netInfo.getState() == NetworkInfo.State.CONNECTED) {
                status = true;

            } else {
                netInfo = cm.getNetworkInfo(1);
                if (netInfo != null && netInfo.getState() == NetworkInfo.State.CONNECTED)
                    status = true;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return status;
    }

    public static String removePrefixNumber(String number) {
        return TextUtils.isEmpty(number) ? number : number.replaceAll(PHONE_PREFIX_REGCODE, "");
    }

    public static String addPrefixNumber(String number) {
        return TextUtils.isEmpty(number) ? number : ("+44" + removePrefixNumber(number));
    }

    public static void setupUIForKeyboard(final Activity activity, View view) {
        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(activity);
                    return false;
                }
            });
        }

        // If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUIForKeyboard(activity, innerView);
            }
        }
    }

    public static boolean hideSoftKeyboard(Activity activity) {
        return hideSoftKeyboard(activity.getCurrentFocus(), 0);
    }

    public static boolean hideSoftKeyboard(View view, int flag) {
        if (view == null)
            return false;

        if (imm == null)
            imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);

        return imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), flag);
    }

    public static boolean isAppIsInBackground(Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            List<ActivityManager.RunningAppProcessInfo> appProcesses = am.getRunningAppProcesses();
            if (appProcesses != null) {
                final String packageName = context.getPackageName();
                for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
                    if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND && appProcess.processName.equals(packageName)) {
                        return false;
                    }
                }
            }
            return true;
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                return false;
            }
            return true;
        }
    }
}
