package com.dpd.yourdpd.analytics;

import android.app.Application;

import com.dpd.yourdpd.PerApplication;

import dagger.Module;
import dagger.Provides;

/**
 * Provides analytics tracker
 */
@Module
public class AnalyticsModule {
    /**
     * @return AnalyticsManager singleton
     */
    @Provides @PerApplication public AnalyticsManager provideTracker(Application app) {
        return new AnalyticsManager(app);
    }
}
