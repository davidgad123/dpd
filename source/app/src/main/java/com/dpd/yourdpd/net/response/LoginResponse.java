package com.dpd.yourdpd.net.response;

import com.dpd.yourdpd.net.Api;
import com.google.gson.annotations.SerializedName;

/**
 * @see Api#login(String)
 */
public class LoginResponse extends BaseResponse {
    @SerializedName("data")
    private SessionData sessionData;

    private class SessionData {
        public String dpdsession;
    }

    public String getDpdSession() {
        return sessionData==null ? null : sessionData.dpdsession;
    }
}
