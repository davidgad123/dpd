package com.dpd.yourdpd.ui.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import com.dpd.yourdpd.R;

/**
 * Simple dialog that prompts user to "Take a photo", "Choose from Gallery", or "Cancel"
 */
public class ImagePickerDialogFragment extends DialogFragment {

    private Listener listener;

    public ImagePickerDialogFragment() {
        // empty
    }

    public static ImagePickerDialogFragment newInstance() {
        final Bundle args = new Bundle();

        final ImagePickerDialogFragment fragment = new ImagePickerDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final CharSequence[] items = {getString(R.string.take_photo),
                getString(R.string.choose_from_gallery),
                getString(R.string.cancel)};
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(getActivity(), R.style.AlertDialogTheme);
        builder.setTitle(getString(R.string.choose_photo));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (item==0) {
                    if(listener != null) {
                        listener.onTakePicture();
                    }
                    dismiss();
                } else if (item==1) {
                    if(listener != null) {
                        listener.onChooseFromGallery();
                    }
                    dismiss();
                } else {
                    if(listener != null) {
                        listener.onCancel();
                    }
                    dialog.dismiss();
                }
            }
        });
        return builder.create();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        listener = null;
    }

    public void setListener(ImagePickerDialogFragment.Listener listener) {
        this.listener = listener;
    }

    public interface Listener {
        void onTakePicture();
        void onChooseFromGallery();
        void onCancel();
    }
}
