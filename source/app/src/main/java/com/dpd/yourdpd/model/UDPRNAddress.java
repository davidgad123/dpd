package com.dpd.yourdpd.model;

/**
 * Address information returned when looking up a property by UDPRN service
 *
 * @see com.dpd.yourdpd.net.Api#getAddressByUDPRN(String)
 */
public class UDPRNAddress {
    public String postcode;
    public String street;
    public String locality;
    public String town;
    public String county;
    public String organisation;
    public String property;
    public String udprnLongitude;
    public String udprnLatitude;
    public int occupancy;
    private int streetAddress;

    /**
     * Constructs the first line of the address, e.g. number/name + street
     */
    public String getStreetOnly() {
        return street == null ? "" : street;
    }

    public String getFirstLine() {
        return (property == null ? "" : property + " ") + (street == null ? "" : street);
    }

    public String getFirstLineWithTown() {
        return getFirstLine() + (town == null ? "" : ", " + town);
    }
}
