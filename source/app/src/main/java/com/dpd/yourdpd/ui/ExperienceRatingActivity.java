package com.dpd.yourdpd.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.transitions.everywhere.TransitionManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;

import com.dpd.yourdpd.R;
import com.dpd.yourdpd.model.Parcel;
import com.dpd.yourdpd.net.request.SubmitRatingParams;
import com.dpd.yourdpd.net.response.SubmitRatingResponse;
import com.dpd.yourdpd.ui.yourdeliveries.YourDeliveriesActivity;
import com.dpd.yourdpd.util.CommonUtils;
import com.dpd.yourdpd.util.UIUtils;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class ExperienceRatingActivity extends BaseMenuActivity
        implements View.OnClickListener {
    private static final String TAG = ExperienceRatingActivity.class.getSimpleName();

    public static final String EXTRA_PARCEL_CODE = "parcel_code";

    private String parcelCode;
    private Parcel parcel;

    private Button submitButton;
    private boolean ratingSubmitted;
    private RatingBar ratingBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_experience_rating);

        if (!user.hasAuthenticatedSession()) {
            Log.e(TAG, "User is not logged in");
            finish();
            return;
        }

        parcelCode = getIntent().getStringExtra(EXTRA_PARCEL_CODE);
        parcel = dataStore.getParcelWithParcelCode(parcelCode);
        if (TextUtils.isEmpty(parcelCode) || parcel == null) {
            Log.e(TAG, "Missing EXTRA_PARCEL_CODE");
            finish();
            return;
        }

        setupToolbar(getTitle().toString(), true);
        initParcelStatusView();

        submitButton = (Button) findViewById(R.id.submit_button);
        submitButton.setEnabled(false);
        submitButton.setOnClickListener(this);
        ratingBar = (RatingBar) findViewById(R.id.ratingbar);
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                submitButton.setEnabled(rating != 0);
            }
        });
        findViewById(R.id.leave_review_button).setOnClickListener(this);

        CommonUtils.setupUIForKeyboard(this, findViewById(android.R.id.content));
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        parcelCode = intent.getStringExtra(EXTRA_PARCEL_CODE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.submit_button:
                if (ratingSubmitted) {
                    Intent intent = new Intent(this, YourDeliveriesActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                } else {
//                    if (ratingBar.getRating() == 0) {
//                        Snackbar snackbar = Snackbar.make(findViewById(R.id.root),
//                                getString(R.string.please_rate_your_experience),
//                                Snackbar.LENGTH_SHORT);
//                        snackbar.show();
//                    } else {
                    submitRating();
//                    }
                }
                break;
            case R.id.leave_review_button: {
                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        }
    }

    private void initParcelStatusView() {
        final View parcelStatusView = findViewById(R.id.parcel_status);
        UIUtils.populateParcelStatus(parcelStatusView, this, parcel);
    }

    private void submitRating() {
        showProgressDialog(getString(R.string.please_wait_));

        final EditText notesEditText = (EditText) findViewById(R.id.rating_reason_edittext);
        final String notes = notesEditText.getText().toString();
        final int rating = ratingBar.getNumStars();
        final SubmitRatingParams params = new SubmitRatingParams();
        params.experienceRating = rating;
        params.notes = notes;
        api.submitRating(parcelCode, params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        new Action1<SubmitRatingResponse>() {
                            @Override
                            public void call(SubmitRatingResponse response) {
                                if (response.hasErrors()) {
                                    Log.e(TAG, "Submit rating error" + response.printErrors());
                                    onFailedToSubmitRating();
                                    return;
                                }
                                onRatingSubmitted();
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                Log.e(TAG, "Submit rating error", throwable);
                            }
                        });
    }

    private void onFailedToSubmitRating() {
        hideProgressDialog();
        showErrorDialogWithRetryAndCancel(getString(R.string.problem_submitting_rating_message),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        submitRating();
                    }
                },
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
    }

    private void onRatingSubmitted() {
        hideProgressDialog();
        ratingSubmitted = true;

        // show rate app button
        submitButton.setText(R.string.view_your_deliveries_titlecase);
        TransitionManager.beginDelayedTransition((ViewGroup) findViewById(R.id.root));

        findViewById(R.id.rating_reason_container).setVisibility(View.GONE);
        findViewById(R.id.rating_submitted_container).setVisibility(View.VISIBLE);
        findViewById(R.id.leave_review_button).setVisibility(ratingBar.getRating() >= 3.0f ? View.VISIBLE : View.GONE);

        ratingBar.setEnabled(false);
    }
}
