package com.dpd.yourdpd.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import com.dpd.yourdpd.ui.yourdeliveries.ParcelDetailActivity;
import com.dpd.yourdpd.ui.yourdeliveries.YourDeliveriesActivity;

import java.util.List;

/**
 * Activity launched by our custom schema
 */
public class SchemaLauncherActivity extends BaseActivity {
    private static final String TAG = SchemaLauncherActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!user.hasAuthenticatedSession()) {
            Log.e(TAG, "User is not logged in, aborting schema/deep link.");
        } else {
            final Intent intent = getIntent();
            if (Intent.ACTION_VIEW.equals(intent.getAction())) {
                final Uri uri = intent.getData();
                if (uri != null) {
                    Log.i(TAG, "Received deep link: " + uri);
                    if (!TextUtils.isEmpty(uri.getHost()) && uri.getHost().equals("parcel")) {
                        final List<String> pathSegments = uri.getPathSegments();
                        final String parcelCode = pathSegments.get(0);
                        final Intent activityIntent = new Intent(this, ParcelDetailActivity.class);
                        activityIntent.putExtra(ParcelDetailActivity.EXTRA_PARCEL_CODE, parcelCode);
                        startActivity(activityIntent);
                    } else {
                        final Intent activityIntent = new Intent(this, YourDeliveriesActivity.class);
                        startActivity(activityIntent);
                    }
                }
            }
        }

        finish();
    }
}
