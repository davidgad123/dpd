package com.dpd.yourdpd.analytics;

import android.content.Context;
import android.support.annotation.Nullable;

import com.dpd.yourdpd.R;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;

/**
 * Provides analytics tracking
 */
public class AnalyticsManager {
    private GoogleAnalytics analytics;
    private com.google.android.gms.analytics.Tracker tracker;

    public AnalyticsManager(Context context) {
        analytics = GoogleAnalytics.getInstance(context.getApplicationContext());
        analytics.setLocalDispatchPeriod(1800);

        tracker = analytics.newTracker("UA-360430-38");
        tracker.enableExceptionReporting(true);
        tracker.enableAdvertisingIdCollection(true);
        tracker.enableAutoActivityTracking(true);
        tracker.setAppName(context.getString(R.string.app_name));
    }

    public void setUserId(String userId) {
        tracker.set("&uid", userId);
    }

    public void setScreenName(String screenName) {
        tracker.setScreenName(screenName);
    }

    public void trackEvent(String category, @Nullable String action, @Nullable String label) {
        assert category != null;

        final HitBuilders.EventBuilder eventBuilder = new HitBuilders.EventBuilder();
        eventBuilder.setCategory(category);

        if(action != null) {
            eventBuilder.setAction(action);
        }
        if(label != null) {
            eventBuilder.setLabel(label);
        }
        tracker.send(eventBuilder.build());
    }
}
