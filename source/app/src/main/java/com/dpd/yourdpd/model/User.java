package com.dpd.yourdpd.model;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;

import com.dpd.yourdpd.R;
import com.dpd.yourdpd.net.Api;
import com.dpd.yourdpd.net.response.GetAddressByUDPRNResponse;
import com.dpd.yourdpd.net.response.GetConsumerResponse;
import com.dpd.yourdpd.net.response.PutConsumerResponse;
import com.dpd.yourdpd.util.CommonUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.functions.Func2;
import rx.schedulers.Schedulers;

/**
 * Application User
 *
 * @see ConsumerProfile for the "domain: user data
 */
public class User {

    public static final String USER_PREFS = "u";

    /**
     * NOTE: These methods are not guaranteed to be called on the UI Thread, so make use of
     * Handler/runOnUIThread() if UI needs to be touched as a result
     */
    public interface Listener {
        void onUserLoggedOut();

        void onConsumerProfileUpdated(boolean success);
    }

    private static final String TAG = User.class.getSimpleName();

    private Context context;
    private Gson gson;
    private List<Listener> listeners;

    private PinMethod pinMethod;
    private String dpdSession;
    private String consumerId;
    private String spotzDeviceId;
    private boolean hasSeenCoachScreens;
    private boolean hasAuthenticatedPin;
    private boolean hasLoggedInWithAuditUser;
    private ProgressDialog progressDialog;
    private boolean hasWelcomeScreen;
    private boolean agreedTermsOfUse;
    private boolean acceptedResponsibility;
    private int appShowCount;

    /**
     * Represents a user/consumer in the DPD domain
     */
    private ConsumerProfile consumerProfile;
    /**
     * JSON representation of the consumerProfile. This value is updated whenever
     * {@link #save()} is called.
     */
    private String consumerProfileJson;
    /**
     * Server generated checksum allowing us to determine changes to ConsumerProperties
     */
    private String consumerProfileChecksum;

    /**
     * Whether the user is currently updating ConsumerProfile (thus edits may be lost)
     */
    private boolean isUpdatingConsumerProfile;

    /**
     * The ConsumerProperty being added or edited at this time.
     *
     * @see com.dpd.yourdpd.ui.profile.ProfileSetupActivity
     * @see com.dpd.yourdpd.ui.address.AddAddressActivity
     * @see com.dpd.yourdpd.ui.address.EditAddressActivity
     * Later saved to {@link ConsumerProfile#consumerProperties} once the user has acknowledged changes/addition.
     */
    public transient ConsumerProperty selectedConsumerProperty;

    // SharedPreference keys
    private static final String PREF_PIN_METHOD = "pin_method";
    public static final String PREF_DPD_SESSION = "dpd_session";
    private static final String PREF_CONSUMER_ID = "consumer_id";
    private static final String PREF_SPOTZ_DEVICE_ID = "spotz_device_id";
    private static final String PREF_HAS_SEEN_COACH_SCREENS = "has_seen_coach_screens";
    private static final String PREF_HAS_AUTHENTICATED_PIN = "has_authenticated_pin";
    private static final String PREF_HAS_LOGGED_IN_WITH_AUDIT_USER = "has_logged_in_with_audit_user";
    private static final String PREF_AGREED_TERMS_OF_USE = "agreed_terms_of_use";
    private static final String PREF_ACCEPTED_RESPONSIBILITY = "accepted_responsibility";
    private static final String PREF_APP_SHOW_COUNT = "app_show_count";
    private static final String PREF_CONSUMER_PROFILE_CHECKSUM = "consumer_profile_checksum";
    private static final String PREF_CONSUMER_PROFILE = "consumer_profile";

    public User(Context context) {
        this.context = context.getApplicationContext();
        this.gson = new GsonBuilder().create();
        this.listeners = new ArrayList<>(1);
    }

    public void addListener(Listener listener) {
        if (!listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public void removeListener(Listener listener) {
        listeners.remove(listener);
    }

    public void load() {
        final SharedPreferences prefs = context.getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE);
        final JsonParser parser = new JsonParser();

        this.pinMethod = PinMethod.fromOrdinal(prefs.getInt(PREF_PIN_METHOD, PinMethod.SMS.ordinal()));
        this.dpdSession = prefs.getString(PREF_DPD_SESSION, null);
        this.consumerId = prefs.getString(PREF_CONSUMER_ID, null);
        this.spotzDeviceId = prefs.getString(PREF_SPOTZ_DEVICE_ID, null);
        this.hasSeenCoachScreens = prefs.getBoolean(PREF_HAS_SEEN_COACH_SCREENS, false);
        this.hasAuthenticatedPin = prefs.getBoolean(PREF_HAS_AUTHENTICATED_PIN, false);
        this.hasLoggedInWithAuditUser = prefs.getBoolean(PREF_HAS_LOGGED_IN_WITH_AUDIT_USER, false);
        this.agreedTermsOfUse = prefs.getBoolean(PREF_AGREED_TERMS_OF_USE, false);
        this.acceptedResponsibility = prefs.getBoolean(PREF_ACCEPTED_RESPONSIBILITY, false);
        this.appShowCount = prefs.getInt(PREF_APP_SHOW_COUNT, 0);

        this.consumerProfileChecksum = prefs.getString(PREF_CONSUMER_PROFILE_CHECKSUM, null);
        this.consumerProfile = new ConsumerProfile();
        this.consumerProfileJson = prefs.getString(PREF_CONSUMER_PROFILE, null);
        if (consumerProfileJson != null) {
            try {
                final JsonElement jsonElement = parser.parse(consumerProfileJson);
                this.consumerProfile = gson.fromJson(jsonElement, ConsumerProfile.class);
            } catch (JsonSyntaxException e) {
                Log.e(TAG, "Unable to parse ParcelHistory json", e);
            }
        }
    }

    /**
     * Save user data to disk/cache
     */
    public void save() {
        // store JSON version of consumerProfile to allow future "has edited" comparison
        consumerProfileJson = gson.toJson(consumerProfile);

        final SharedPreferences prefs = context.getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(PREF_PIN_METHOD, pinMethod.ordinal());
        editor.putString(PREF_DPD_SESSION, dpdSession);
        editor.putString(PREF_CONSUMER_ID, consumerId);
        editor.putString(PREF_SPOTZ_DEVICE_ID, spotzDeviceId);
        editor.putBoolean(PREF_HAS_SEEN_COACH_SCREENS, hasSeenCoachScreens);
        editor.putBoolean(PREF_HAS_AUTHENTICATED_PIN, hasAuthenticatedPin);
        editor.putBoolean(PREF_HAS_LOGGED_IN_WITH_AUDIT_USER, hasLoggedInWithAuditUser);
        editor.putBoolean(PREF_AGREED_TERMS_OF_USE, agreedTermsOfUse);
        editor.putBoolean(PREF_ACCEPTED_RESPONSIBILITY, acceptedResponsibility);
        editor.putInt(PREF_APP_SHOW_COUNT, appShowCount);
        editor.putString(PREF_CONSUMER_PROFILE_CHECKSUM, consumerProfileChecksum);
        editor.putString(PREF_CONSUMER_PROFILE, consumerProfileJson);
        editor.apply();
    }

    public void setDpdSession(String dpdSession) {
        this.dpdSession = dpdSession;
    }

    public String getDpdSession() {
        return this.dpdSession;
    }

    public String getConsumerProfileChecksum() {
        return consumerProfileChecksum;
    }

    public boolean hasAuthenticatedPin() {
        return hasAuthenticatedPin;
    }

    public void setHasAuthenticatedPin(boolean hasAuthenticatedPin) {
        this.hasAuthenticatedPin = hasAuthenticatedPin;
    }

    public boolean getHasWelcomeScreen() {
        return hasWelcomeScreen;
    }

    public void setHasWelcomeScreen(boolean hasWelcomeScreen) {
        this.hasWelcomeScreen = hasWelcomeScreen;
    }

    public boolean hasSeenCoachScreens() {
        return hasSeenCoachScreens;
    }

    public void setHasSeenCoachScreens(boolean hasSeenCoachScreens) {
        this.hasSeenCoachScreens = hasSeenCoachScreens;
    }

    public boolean isAgreedTermsOfUse() {
        return agreedTermsOfUse;
    }

    public void setAgreedTermsOfUse(boolean agreedTermsOfUse) {
        this.agreedTermsOfUse = agreedTermsOfUse;
    }

    public boolean isAcceptedResponsibility() {
        return acceptedResponsibility;
    }

    public void setAcceptedResponsibility(boolean acceptedResponsibility) {
        this.acceptedResponsibility = acceptedResponsibility;
    }

    public int getAppShowCount() {
        return appShowCount;
    }

    public void incAppShowCount() {
        appShowCount++;
    }

    public String getConsumerId() {
        return consumerId;
    }

    public void setConsumerId(String consumerId) {
        this.consumerId = consumerId;
    }

    public String getSpotzDeviceId() {
        return spotzDeviceId;
    }

    public void setSpotzDeviceId(String spotzDeviceId) {
        this.spotzDeviceId = spotzDeviceId;
    }

    public PinMethod getPinMethod() {
        return pinMethod;
    }

    public void setPinMethod(PinMethod pinMethod) {
        this.pinMethod = pinMethod;
    }

    /**
     * @return true if the ConsumerProfile has been edited since last being saved
     */
    public boolean hasConsumerProfileChanged() {
        final String json = gson.toJson(consumerProfile);
        return !json.equals(consumerProfileJson);
    }

    public ConsumerProfile getConsumerProfile() {
        return consumerProfile;
    }

    public void setConsumerProfile(ConsumerProfile consumerProfile) {
        this.consumerProfile = consumerProfile;
    }

    public void setConsumerProfileChecksum(String consumerProfileChecksum) {
        this.consumerProfileChecksum = consumerProfileChecksum;
    }

    public void setHasLoggedInWithAuditUser(boolean hasLoggedInWithAuditUser) {
        this.hasLoggedInWithAuditUser = hasLoggedInWithAuditUser;
    }

    /**
     * Utility method that combines first and last name
     */
    public String getFullName() {
        return consumerProfile.firstname + " " + consumerProfile.surname;
    }

    /**
     * @return whether the user has a session, a consumerId AND has logged in with an auditUser
     */
    public boolean hasAuthenticatedSession() {
        return !TextUtils.isEmpty(dpdSession) &&
                !TextUtils.isEmpty(consumerId) &&
                hasLoggedInWithAuditUser;
    }

    public void logout() {
        setConsumerId(null);
        setDpdSession(null);
        setConsumerProfile(new ConsumerProfile());
        setConsumerProfileChecksum(null);
        setHasAuthenticatedPin(false);
        setHasLoggedInWithAuditUser(false);
        setHasSeenCoachScreens(false);
        save();
        notifyUserLoggedOut();
    }

    /**
     * Saves the modified ConsumerProfile on the server
     *
     * @param api               Inject Api to use
     * @param excludePhotograph Improve payload size by omitting photograph (non-destructive)
     * @return Observable with server response
     */
    public Observable<PutConsumerResponse> putConsumerProfile(Api api, boolean excludePhotograph) {
        // create a clone to perform pre-upload modification on
        final JsonObject jsonObject = gson.toJsonTree(consumerProfile).getAsJsonObject();
        final ConsumerProfile profileClone = gson.fromJson(jsonObject, ConsumerProfile.class);

        // Change mobile number format
        profileClone.mobile = CommonUtils.addPrefixNumber(profileClone.mobile);

        // optionally strip photograph data (indicates no change required to this property)
        if (excludePhotograph) {
            profileClone.photograph = null;
        }

        // ensure we do not erase notificationId remotely if it has not been registered locally yet
        // remember that server does not return it as part of the Consumer JSON so it will get wiped
        if (TextUtils.isEmpty(consumerProfile.notificationId)) {
            profileClone.notificationId = null;
        }

        // strip photograph data for any ConsumerProperties that are not the currently edited
        // the API only allows for us to send X bytes data, just about one photo at a time :/
        // NOTE! Proposed solution is to upload first, then .[safeLocation]Photograph will be URLs
        if (profileClone.hasConsumerProperties()) {
            for (ConsumerProperty consumerProperty : profileClone.getConsumerProperties()) {
                if (selectedConsumerProperty == null || selectedConsumerProperty.safeLocationPhotograph == null ||
                        !selectedConsumerProperty.safeLocationPhotograph.equals(consumerProperty.safeLocationPhotograph)) {
                    consumerProperty.safeLocationPhotograph = null;
                }
            }
        }
        // strip locally used only fields
        profileClone.setConsumerPropertyAddressInfo(null);

        final JsonObject profileCloneJson = gson.toJsonTree(profileClone).getAsJsonObject();
        checkJsonField("email", profileCloneJson);
        checkJsonField("mobile", profileCloneJson);
        return api.putConsumer(profileCloneJson)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public void updateNotificationId(Api api, String notificationId, String deviceId) {
        if (notificationId == null)
            return;

        consumerProfile.notificationId = notificationId;
        consumerProfile.deviceId = deviceId;
        JsonObject jsonObject = new JsonObject();
        if (!TextUtils.isEmpty(deviceId)) jsonObject.addProperty("deviceId", consumerProfile.deviceId);
        jsonObject.addProperty("notificationId", consumerProfile.notificationId);
        jsonObject.addProperty("email", consumerProfile.email);
        api.putConsumer(jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<PutConsumerResponse>() {
                    @Override
                    public void call(PutConsumerResponse response) {
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                    }
                });
    }

    private void checkJsonField(String fieldName, JsonObject profileCloneJson) {
        JsonElement element = profileCloneJson.get(fieldName);
        if (element != null && element.toString().replace("\"", "").equals("")) {
            profileCloneJson.remove(fieldName);
        }
    }

    /**
     * Gets the ConsumerProfile from the server including ConsumerProperty details and related
     * {@link ConsumerPropertyAddressInfo}es. Invokes {@link Listener#onConsumerProfileUpdated(boolean)}
     * when complete on UI Thread.
     *
     * @param api Api to use
     */
    public Subscription updateConsumerProfile(final Api api, final Context context) {
        Log.i(TAG, "Getting Consumer...");

        isUpdatingConsumerProfile = true;
        progressDialog = ProgressDialog.show(context, null, context.getString(R.string.please_wait_), true, false);

        return api.getConsumer()
                .flatMap(new Func1<GetConsumerResponse, Observable<ConsumerPropertyAddressInfo>>() {
                    @Override
                    public Observable<ConsumerPropertyAddressInfo> call(GetConsumerResponse response) {
                        if (response.hasErrors()) {
                            Log.w(TAG, "Unable to getConsumer: " + response.printErrors());
                            notifyConsumerUpdated(false);
                            progressDialog.dismiss();
                            return null;
                        }
                        setConsumerProfile(response.consumerProfile);

                        return Observable.from(consumerProfile.getConsumerProperties())
                                .flatMap(new Func1<ConsumerProperty, Observable<ConsumerPropertyAddressInfo>>() {
                                    @Override
                                    public Observable<ConsumerPropertyAddressInfo> call(
                                            ConsumerProperty consumerProperty) {

                                        Log.i(TAG, "Getting addressByUDPRN for ConsumerProperty " + consumerProperty.consumerPropertyId);
                                        return Observable.zip(
                                                Observable.just(consumerProperty),
                                                api.getAddressByUDPRN(consumerProperty.consumerPropertyId),
                                                new Func2<ConsumerProperty, GetAddressByUDPRNResponse, ConsumerPropertyAddressInfo>() {
                                                    @Override
                                                    public ConsumerPropertyAddressInfo call(ConsumerProperty consumerProperty,
                                                                                            GetAddressByUDPRNResponse response) {
                                                        if (response.hasErrors()) {
                                                            Log.w(TAG, "Unable to getAddressByUDPRN: " + response.printErrors());
                                                            notifyConsumerUpdated(false);
                                                            progressDialog.dismiss();
                                                            return null;
                                                        }
                                                        final ConsumerPropertyAddressInfo propertyAddress = new ConsumerPropertyAddressInfo();
                                                        propertyAddress.consumerProperty = consumerProperty;
                                                        propertyAddress.addressFirstLine = response.address.getFirstLine();
                                                        propertyAddress.addressFirstLineWithTown = response.address.getFirstLineWithTown();
                                                        return propertyAddress;
                                                    }
                                                }
                                        );
                                    }
                                });
                    }
                })
                .toList()
                .subscribeOn(Schedulers.io())
                .subscribe(
                        new Action1<List<ConsumerPropertyAddressInfo>>() {
                            @Override
                            public void call(List<ConsumerPropertyAddressInfo> addresses) {
                                Collections.reverse(addresses);
                                consumerProfile.setConsumerPropertyAddressInfo(addresses);
                                save();
                                notifyConsumerUpdated(true);
                                progressDialog.dismiss();
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                Log.e(TAG, "Error updating Consumer", throwable);
                                Log.e(TAG, "Disconnected");
                                notifyConsumerUpdated(false);
                                progressDialog.dismiss();
                            }
                        });
    }

    public boolean isUpdatingConsumerProfile() {
        return this.isUpdatingConsumerProfile;
    }

    private void notifyUserLoggedOut() {
        for (Listener listener : listeners) {
            listener.onUserLoggedOut();
        }
    }

    private void notifyConsumerUpdated(boolean success) {
        isUpdatingConsumerProfile = false;
        Log.e(TAG, "notifyConsumerUpdated Called");
        for (Listener listener : listeners) {
            listener.onConsumerProfileUpdated(success);
        }
    }
}