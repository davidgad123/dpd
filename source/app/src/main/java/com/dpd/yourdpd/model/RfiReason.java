package com.dpd.yourdpd.model;

import java.io.Serializable;

/**
 * Created by richardleggett on 01/09/15.
 */
public class RfiReason implements Serializable {
    public boolean rfiWithCustomer;
    public String rfiReasonDescription;
    public String rfiReasonRxplanation;
    public String rfiGroupCode;
    public String rfiCarrierEmail;
}
