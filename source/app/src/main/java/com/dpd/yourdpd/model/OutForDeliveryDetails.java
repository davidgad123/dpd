package com.dpd.yourdpd.model;

import java.io.Serializable;
import java.util.Date;

public class OutForDeliveryDetails implements Serializable {
    public boolean outForDelivery;
    public String[] outForDeliveryDate;
    public boolean mapAvailable;
}
