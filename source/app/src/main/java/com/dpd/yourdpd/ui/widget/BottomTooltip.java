package com.dpd.yourdpd.ui.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.DrawableRes;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.dpd.yourdpd.R;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

/**
 * Tooltip with arrow pointing up
 */
public class BottomTooltip extends FrameLayout {

    private static final String TAG = BottomTooltip.class.getSimpleName();

    private TextView textView;

    public BottomTooltip(Context context) {
        this(context, null);
    }
    public BottomTooltip(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }
    public BottomTooltip(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.tooltip_bottom, this, true);
        textView = (TextView)findViewById(R.id.tooltip_textview);
    }

    public void setText(CharSequence text) {
        textView.setText(text);
    }

    public void setIcon(@DrawableRes int iconResourceId) {
        textView.setCompoundDrawablesWithIntrinsicBounds(iconResourceId, 0, 0, 0);
        textView.setGravity(Gravity.LEFT);
    }

    public void setIcon(Uri uri) {
        Picasso
                .with(getContext())
                .load(uri)
                .resizeDimen(R.dimen.issue_icon_size, R.dimen.issue_icon_size)
                .into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        final Drawable icon = new BitmapDrawable(getResources(), bitmap);
                        textView.setCompoundDrawablesWithIntrinsicBounds(icon, null, null, null);
                        textView.setGravity(Gravity.LEFT);
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {
                        Log.e(TAG, "Unable to load tooltip icon");
                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {
                    }
                });
    }

}
