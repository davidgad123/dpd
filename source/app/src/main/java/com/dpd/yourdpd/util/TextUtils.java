package com.dpd.yourdpd.util;

import android.text.SpannableStringBuilder;
import android.text.style.CharacterStyle;

public class TextUtils {
    public static final String POSTCODE_REGEX =
            "(GIR[ ]?0AA)|([a-zA-Z]{1,2}(([0-9]{1,2})|([0-9][a-zA-Z]))[ ]?[0-9][a-zA-Z]{2})";

    /**
     * Given either a Spannable String or a regular String and a token, apply
     * the given CharacterStyle to the span between the tokens, and also
     * remove tokens.
     * <p>
     * For example, {@code setSpanBetweenTokens("Hello ##world##!", "##",
     * new ForegroundColorSpan(0xFFFF0000));} will return a CharSequence
     * {@code "Hello world!"} with {@code world} in red.
     *
     * @param text The text, with the tokens, to adjust.
     * @param token The token string; there should be at least two instances
     *             of token in text.
     * @param cs The style to apply to the CharSequence. WARNING: You cannot
     *            send the same two instances of this parameter, otherwise
     *            the second call will remove the original span.
     * @return A Spannable CharSequence with the new style applied.
     */
    public static CharSequence setSpanBetweenTokens(CharSequence text,
                                                    String token, CharacterStyle... cs) {
        // Start and end refer to the points where the span will apply
        int tokenLen = token.length();
        int start, end;
        while(true) {
            start = text.toString().indexOf(token) + tokenLen;
            end = text.toString().indexOf(token, start);

            if (start > -1 && end > -1) {
                // Copy the spannable string to a mutable spannable string
                SpannableStringBuilder ssb = new SpannableStringBuilder(text);
                for (CharacterStyle c : cs)
                    ssb.setSpan(CharacterStyle.wrap(c), start, end, 0);

                // Delete the tokens before and after the span
                ssb.delete(end, end + tokenLen);
                ssb.delete(start - tokenLen, start);

                text = ssb;
            } else {
                break;
            }
        }

        return text;
    }

    public static CharSequence trimTrailingWhitespace(CharSequence source) {

        if(source == null)
            return "";

        int i = source.length();

        // loop back to the first non-whitespace character
        while(--i >= 0 && Character.isWhitespace(source.charAt(i))) {
        }

        return source.subSequence(0, i+1);
    }

    public static String getRealTextFromPIN(String formatedPIN, String mask, String holder_letter) {
        String realText = "";
        for(int i = 0; i < formatedPIN.length(); i++) {
            String letter = "" + formatedPIN.charAt(i);
            if(!mask.contains(letter) && !holder_letter.contains(letter)){
                realText += letter;
            }
        }
        return realText;
    }
}
