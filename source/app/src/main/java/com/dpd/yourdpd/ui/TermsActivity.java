package com.dpd.yourdpd.ui;

import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.TextView;

import com.dpd.yourdpd.R;

/**
 * Display of app Terms and Use
 */
public class TermsActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);

        TextView textView = (TextView) findViewById(R.id.terms_textview);
        if (!TextUtils.isEmpty(dataStore.getSiteContent().termsAndConditions)) {
            //textView.setText(dataStore.getSiteContent().termsAndConditions);
            //Linkify.addLinks(textView, Linkify.ALL);
            textView.setText(Html.fromHtml(dataStore.getSiteContent().termsAndConditions));
        } else {
            textView.setText(Html.fromHtml(getString(R.string.terms)));
        }
        textView.setMovementMethod(LinkMovementMethod.getInstance());

        findViewById(R.id.back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
