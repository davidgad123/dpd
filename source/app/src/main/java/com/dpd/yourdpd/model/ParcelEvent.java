package com.dpd.yourdpd.model;

public class ParcelEvent {
    public AddressPoint addressPoint;
    public String eventCode;
    public String eventDate;
    public String eventLocation;
    public String eventNotes;
    /**
     * e.g. Forwarded to Oldbury depot
     */
    public String eventText;
    public String[] imageKey;
    public String[] imageType;
    public String[] imageCaption;
    public String parcelCode;
    public String parcelEventCode;
}
