package com.dpd.yourdpd.net.maps;

import android.util.Log;

import com.dpd.yourdpd.util.GMapsUtils;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Performs a Google Maps route request
 */
public class MapsRouteRequest {
    private static final String TAG = MapsRouteRequest.class.getSimpleName();

    private Call mapsAPICall;

    public void execute(float originLat, float originLon,
                        float destLat, float destLon,
                        final MapsRouteRequest.Callback callback) {
        final OkHttpClient client = new OkHttpClient();
        final Request request = new Request.Builder()
                .url("https://maps.googleapis.com/maps/api/directions/json?units=imperial&" +
                        "origin=" + originLat + "," + originLon +
                        "&destination=" + destLat + "," + destLon +
                        "&sensor=false")//&key=" + getContext().getString(R.string.GOOGLE_MAPS_KEY))
                .build();

        mapsAPICall = client.newCall(request);
        mapsAPICall.enqueue(new com.squareup.okhttp.Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                Log.e(TAG, "Failed to call Google Maps route API", e);
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (!response.isSuccessful()) {
                    Log.e(TAG, String.format("Unable to download directions %d", response.code()));
                    return;
                }

                final MapsRouteResponse routeResponse = new MapsRouteResponse();
                try {
                    final JSONObject json = new JSONObject(response.body().string());
                    final JSONArray routes = json.getJSONArray("routes");
                    final JSONObject route = routes.getJSONObject(0);
                    final JSONObject bounds = route.getJSONObject("bounds");
                    final String pointsStr = route.getJSONObject("overview_polyline").getString("points");
                    routeResponse.points = GMapsUtils.decodePoly(pointsStr);

                    final JSONObject leg = route.getJSONArray("legs").getJSONObject(0);
                    routeResponse.distance = leg.getJSONObject("distance").getString("text");
                    routeResponse.duration = leg.getJSONObject("duration").getString("text");
                    routeResponse.start = new LatLng(
                            leg.getJSONObject("start_location").getDouble("lat"),
                            leg.getJSONObject("start_location").getDouble("lng"));
                    routeResponse.end = new LatLng(
                            leg.getJSONObject("end_location").getDouble("lat"),
                            leg.getJSONObject("end_location").getDouble("lng"));
                    routeResponse.steps = leg.getJSONArray("steps");

                    // zoom to bounds
                    final LatLng southwest = new LatLng(
                            bounds.getJSONObject("southwest").getDouble("lat"),
                            bounds.getJSONObject("southwest").getDouble("lng"));
                    final LatLng northeast = new LatLng(
                            bounds.getJSONObject("northeast").getDouble("lat"),
                            bounds.getJSONObject("northeast").getDouble("lng"));
                    routeResponse.boundsLatLng = new LatLngBounds(southwest, northeast);

                    callback.onResponse(routeResponse);

                } catch (JSONException je) {
                    Log.e(TAG, "Unable to parse directions, JSONException", je);
                    callback.onError(je);
                } catch (Exception e) {
                    Log.e(TAG, "Unable to parse directions", e);
                    callback.onError(e);
                }

            }
        });
    }

    public void cancel() {
        if (mapsAPICall != null) {
            mapsAPICall.cancel();
            mapsAPICall = null;
        }
    }

    public interface Callback {
        void onResponse(MapsRouteResponse response);

        void onError(Throwable e);
    }
}
