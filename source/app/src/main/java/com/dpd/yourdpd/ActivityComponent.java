package com.dpd.yourdpd;

import com.dpd.yourdpd.analytics.AnalyticsManager;
import com.dpd.yourdpd.datastore.DataStore;
import com.dpd.yourdpd.model.User;
import com.dpd.yourdpd.net.Api;
import com.dpd.yourdpd.ui.BaseActivity;
import com.paypal.android.sdk.payments.PayPalConfiguration;

import dagger.Component;

/**
 * Module scoped to an Activity that depends on {@link AppComponent}
 * TODO: we could make this a base class to have different injects for different Activities
 */
@PerActivity
@Component(dependencies = AppComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {
    Api api();
    DataStore dataStore();
    AnalyticsManager analyticsManager();
    User user();
    PayPalConfiguration paypalConfiguration();

    void inject(BaseActivity activity);
}
