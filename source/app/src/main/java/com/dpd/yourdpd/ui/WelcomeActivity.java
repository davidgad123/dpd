package com.dpd.yourdpd.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.common.Constants;
import com.dpd.yourdpd.R;
import com.util.WearableUtil;

/**
 * Display licenses and other acknowledgements
 */
public class WelcomeActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        initUI();
    }

    private void initUI() {
        Button btn_view_your_delivery = (Button) findViewById(R.id.btn_view_your_deliveries);
        btn_view_your_delivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
