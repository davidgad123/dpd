package com.dpd.yourdpd.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Wraps a List of {@link Parcel}'s to match JSON API response
 */
public class ParcelListWrapper implements Serializable {
    // JSON responses incorrectly label the array "parcel" not parcels
    @SerializedName("parcel")
    public List<Parcel> parcels;
}
