package com.dpd.yourdpd.ui;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.TextView;

import com.dpd.yourdpd.R;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;

/**
 * The coach Activity introduces aspects of the app to the user for the first time
 */
public class CoachActivity extends BaseActivity {
    private ViewPager viewPager;
    private CirclePageIndicator viewPagerIndicator;
    private boolean didAnimatedPin;
    private boolean didInitSpotz;

    private ViewPager.PageTransformer transformer = new ViewPager.PageTransformer() {
        @Override
        public void transformPage(View view, float position) {
            int pageWidth = view.getWidth();
            if (position < -0.5) { // [-Infinity,-1)
                // This page is way off-screen to the left.
                view.setAlpha(0);

            } else if (position >= -0.5 && position <= 0) { // [-1,0]
                // Use the default slide transition when moving to the left page
                view.setAlpha(1 + 2 * position);
            } else if (position <= 0.5 && position > 0) { // (0,1]
                // Fade the page out.
                view.setAlpha(1 - 2 * position);
            } else { // (1,+Infinity]
                view.setAlpha(0);
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coach);

        didAnimatedPin = false;
        didInitSpotz = false;

        findViewById(R.id.next_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int currentItem = viewPager.getCurrentItem();
                if (currentItem + 1 < viewPager.getAdapter().getCount()) {
                    viewPager.setCurrentItem(currentItem + 1, true);
                } else {
                    startNextActivity();
                }
            }
        });

        viewPager = (ViewPager) findViewById(R.id.viewPager);
        viewPagerIndicator = (CirclePageIndicator) findViewById(R.id.viewPagerIndicator);
        viewPager.setAdapter(new PagerAdapter() {
            @Override
            public int getCount() {
                return 4;
            }

            @Override
            public Object instantiateItem(ViewGroup container, int position) {
                int layout = R.layout.coach_step_1;
                switch (position) {
                    case 0:
                        layout = R.layout.coach_step_1;
                        break;
                    case 1:
                        layout = R.layout.coach_step_2;
                        break;
                    case 2:
                        layout = R.layout.coach_step_3;
                        break;
                    case 3:
                        layout = R.layout.coach_step_4;
                        break;
                }

                View view = LayoutInflater.from(CoachActivity.this).inflate(layout, container, false);
                final TextView messageTextView = (TextView) view.findViewById(R.id.message_textview);
                /*if (messageTextView != null) {
                    int start, end;
                    SpannableStringBuilder builder = new SpannableStringBuilder(messageTextView.getText());
                    String text = messageTextView.getText().toString();
                    start = text.indexOf("{");
                    end = text.indexOf("}");
                    builder.delete(end, end + 1);
                    builder.delete(start, start + 1);
                    builder.setSpan(new ClickableSpan() {
                        @Override
                        public void onClick(@NonNull View widget) {
                            startActivity(new Intent(CoachActivity.this, TermsActivity.class));
                            overridePendingTransition(R.anim.slide_in_bottom, R.anim.abc_fade_out);
                        }
                    }, start, end - 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    messageTextView.setText(builder);
                    messageTextView.setMovementMethod(LinkMovementMethod.getInstance());
                    messageTextView.setLinksClickable(true);
                }*/
                container.addView(view);
                return view;
            }

            @Override
            public void destroyItem(ViewGroup container, int position, Object object) {
                container.removeView((View) object);
            }

            @Override
            public boolean isViewFromObject(View view, Object object) {
                return view == object;
            }
        });
        viewPagerIndicator.setViewPager(viewPager);
        viewPager.setPageTransformer(true, transformer);
        viewPagerIndicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if (position == 1 && !didAnimatedPin) {
                    animatePin();
                    didAnimatedPin = true;
                } else if (position == 2) {
                    animateBell();
                } else if (position == 3) {
                    animateClock();
                }

                if (!didInitSpotz) {
                    initSpotzPushService();
                    didInitSpotz = true;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        checkTermOfUse();
    }

    private void checkTermOfUse() {
        if (user.isAgreedTermsOfUse()) return;

        final TermsUseDialog dialog = new TermsUseDialog(new ContextThemeWrapper(this, R.style.DialogSlideAnim));
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface d) {
                dialog.setOnViewTermsClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(CoachActivity.this, TermsActivity.class));
                        overridePendingTransition(R.anim.slide_in_bottom, R.anim.abc_fade_out);
                    }
                });
                dialog.setOnAgreeClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        user.setAgreedTermsOfUse(true);
                        dialog.dismiss();
                    }
                });
            }
        });
        dialog.setCancelable(false);
        dialog.show();
    }

    private void animatePin() {
        final View pin = viewPager.findViewById(R.id.pin);
        pin.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                pin.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                pin.clearAnimation();
                pin.setPivotX(pin.getWidth() / 2);
                pin.setPivotY(pin.getHeight());
                pin.setTranslationY(-(pin.getY() + pin.getHeight()));

                int durationDrop = 300;
                int durationScale1 = 50;
                int durationScale2 = 257;
                int durationRotate1 = durationScale2;
                int durationRotate2 = 360;
                int durationRotate3 = 360;
                int durationRotate4 = 205;

                ObjectAnimator animDrop = ObjectAnimator.ofFloat(pin, View.TRANSLATION_Y, 0);
                animDrop.setDuration(durationDrop);
                //animDrop.setInterpolator(new AccelerateDecelerateInterpolator());

                ObjectAnimator animScale1 = ObjectAnimator.ofFloat(pin, View.SCALE_Y, 0.8f);
                animScale1.setDuration(durationScale1);
                //animScale1.setInterpolator(new AccelerateDecelerateInterpolator());

                ObjectAnimator animScale2 = ObjectAnimator.ofFloat(pin, View.SCALE_Y, 1.0f);
                animScale2.setDuration(durationScale2);
                //animScale2.setInterpolator(new AccelerateDecelerateInterpolator());

                ObjectAnimator animRotate1 = ObjectAnimator.ofFloat(pin, View.ROTATION, 25);
                animRotate1.setDuration(durationRotate1);
                //animRotate1.setInterpolator(new AccelerateDecelerateInterpolator());

                ObjectAnimator animRotate2 = ObjectAnimator.ofFloat(pin, View.ROTATION, -12.5f);
                animRotate2.setDuration(durationRotate2);
                //animRotate2.setInterpolator(new AccelerateDecelerateInterpolator());

                ObjectAnimator animRotate3 = ObjectAnimator.ofFloat(pin, View.ROTATION, 5);
                animRotate3.setDuration(durationRotate3);
                //animRotate3.setInterpolator(new AccelerateDecelerateInterpolator());

                ObjectAnimator animRotate4 = ObjectAnimator.ofFloat(pin, View.ROTATION, 0);
                animRotate4.setDuration(durationRotate4);
                //animRotate4.setInterpolator(new AccelerateDecelerateInterpolator());

                AnimatorSet set = new AnimatorSet();
                set.play(animDrop);
                set.play(animScale1).after(durationDrop - durationScale1);
                set.play(animRotate1).with(animScale2).after(durationDrop);
                set.play(animRotate2).after(durationDrop + durationRotate1);
                set.play(animRotate3).after(durationDrop + durationRotate1 + durationRotate2);
                set.play(animRotate4).after(durationDrop + durationRotate1 + durationRotate2 + durationRotate3);
                set.start();
            }
        });
    }

    private void animateBell() {
        final View bell = viewPager.findViewById(R.id.bell);
        bell.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                bell.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                bell.setPivotX(bell.getWidth() / 2);
                bell.setPivotY(0);
                bell.setRotation(0);
                AnimatorSet set = new AnimatorSet();
                float[] deltas = new float[]{-20, 15, -10, 8, -5, 0};
                List<Animator> animators = new ArrayList<>(10);
                for (float delta : deltas) {
                    ObjectAnimator animator = ObjectAnimator.ofFloat(bell, View.ROTATION, delta);
                    animator
                            .setDuration(450 - (120 - (int) Math.abs(delta)))
                            .setInterpolator(new AccelerateDecelerateInterpolator());
                    animators.add(animator);
                }

                set.playSequentially(animators);
                set.start();
            }
        });
    }

    private void animateClock() {
        final View clockHand = viewPager.findViewById(R.id.clock_hand);
        clockHand.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                clockHand.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                clockHand.setRotation(0);
                ObjectAnimator animator = ObjectAnimator.ofFloat(clockHand, View.ROTATION, 359);
                animator.setRepeatMode(ValueAnimator.INFINITE);
                animator.setRepeatCount(-1);
                animator.setDuration(30000);
                animator.setInterpolator(new LinearInterpolator());
                animator.start();
            }
        });
    }

    private void startNextActivity() {
        final Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
