package com.dpd.yourdpd.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.dpd.yourdpd.R;
import com.dpd.yourdpd.util.UIUtils;

/**
 * Displays a horizontal row of steps that can be activated one at a time
 */
public class StepsView extends FrameLayout {
    private int activeStep;
    private ViewGroup stepsContainer;

    public StepsView(Context context) {
        this(context, null);
    }
    public StepsView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }
    public StepsView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        activeStep = -1;
        stepsContainer =
                (ViewGroup) LayoutInflater.from(getContext()).inflate(R.layout.steps, this, false);
        LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.CENTER_HORIZONTAL;
        params.topMargin = UIUtils.dpToPx(8, getContext());
        params.bottomMargin = UIUtils.dpToPx(8, getContext());
        addView(stepsContainer, params);

        if(isInEditMode()) {
            addStep(new Step("1", "Profile"));
            addStep(new Step("2", "Address"));
            addStep(new Step("3", "Safe Place"));
            addStep(new Step("3", "Neighbours"));
            addStep(new Step("3", "Shops"));
        }
    }

    public void addStep(Step step) {
        View stepView =
                LayoutInflater.from(getContext()).inflate(R.layout.step, stepsContainer, false);
        ((TextView)stepView.findViewById(R.id.step_label_textview)).setText(step.label);
        ((TextView)stepView.findViewById(R.id.step_number_textview)).setText(step.number);
        stepsContainer.addView(stepView);
    }

    public void setActiveStep(int step) {
        activeStep = step;
        final int count = stepsContainer.getChildCount();
        if(step<count) {
            for(int i=0; i<=step; i++) {
                stepsContainer.getChildAt(i).setActivated(true);
            }
            for(int i=step+1; i<count; i++) {
                stepsContainer.getChildAt(i).setActivated(false);
            }
        }
    }

    /**
     * @return -1 if no step selected, else 0-[steps-1]
     */
    public int getActiveStep() {
        return activeStep;
    }

    public static class Step {
        public String number;
        public String label;
        public Step(String number, String label) {
            this.number = number;
            this.label = label;
        }
    }
}
