package com.dpd.yourdpd.ui.yourdeliveries;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.UiThread;
import android.support.v4.app.Fragment;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.app.AlertDialog;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import com.dpd.yourdpd.BuildConfig;
import com.dpd.yourdpd.R;
import com.dpd.yourdpd.model.Parcel;
import com.dpd.yourdpd.model.ParcelActionCodes;
import com.dpd.yourdpd.net.request.UpgradeParcelParams;
import com.dpd.yourdpd.net.response.GetAlternativeDeliveryTimesResponse;
import com.dpd.yourdpd.net.response.GetAlternativeDeliveryTimesResponse.DeliveryTime;
import com.dpd.yourdpd.net.response.GetParcelUpdateDatesResponse;
import com.dpd.yourdpd.net.response.UpgradeParcelResponse;
import com.dpd.yourdpd.ui.BaseFragment;
import com.dpd.yourdpd.ui.BaseMenuActivity;
import com.dpd.yourdpd.util.CommonUtils;
import com.dpd.yourdpd.util.DateUtils;
import com.dpd.yourdpd.util.TextUtils;
import com.dpd.yourdpd.util.UIUtils;
import com.dpd.yourdpd.util.ViewIdGenerator;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.functions.Func2;
import rx.schedulers.Schedulers;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link InFlightUpgradeDeliveryFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link InFlightUpgradeDeliveryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class InFlightUpgradeDeliveryFragment extends BaseFragment {
    private static final String TAG = InFlightUpgradeDeliveryFragment.class.getSimpleName();
    private static final SimpleDateFormat DIARY_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd", Locale.UK);
    private static final SimpleDateFormat DIARY_DATE_FORMAT_FOR_UPGRADE = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.UK);

    // the fragment initialization parameters
    private static final String ARG_PARCEL_CODE = "parcel_code";

    private OnFragmentInteractionListener listener;
    private Parcel parcel;

    private ScrollView scrollView;
    private ContentLoadingProgressBar progressBar;
    private RadioGroup deliveryTimesRadioGroup;
    private RadioGroup deliveryDatesRadioGroup;
    private TextView costTextView;
    private Button payNowButton;

    private List<DeliveryUpgradeOption> deliveryUpgradeOptions;

    private String selectedDiaryDate;
    private DeliveryTime selectedDeliveryTime;

    /**
     * PayPal's returned payment confirmation ID
     */
    private String paymentId;
    /**
     * Our internal Order ID
     */
    private String orderId;

    private Subscription getDeliveryOptionsSubscription;
    private Subscription upgradeParcelSubscription;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param parcelCode Parcel.parcelCode
     * @return A new instance of fragment InFlightUpgradeDeliveryFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static InFlightUpgradeDeliveryFragment newInstance(String parcelCode) {
        InFlightUpgradeDeliveryFragment fragment = new InFlightUpgradeDeliveryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARCEL_CODE, parcelCode);
        fragment.setArguments(args);
        return fragment;
    }

    public InFlightUpgradeDeliveryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            String parcelCode = getArguments().getString(ARG_PARCEL_CODE);
            parcel = dataStore.getParcelWithParcelCode(parcelCode);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.fragment_inflight_upgrade_delivery, container, false);
        scrollView = (ScrollView) root.findViewById(R.id.scrollView);
        progressBar = (ContentLoadingProgressBar) root.findViewById(R.id.progressBar);
        deliveryTimesRadioGroup = (RadioGroup) root.findViewById(R.id.delivery_times_radiogroup);
        deliveryDatesRadioGroup = (RadioGroup) root.findViewById(R.id.delivery_dates_radiogroup);
        deliveryDatesRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId != -1) {
                    RadioButton radioButton = (RadioButton) group.findViewById(checkedId);
                    selectedDiaryDate = (String) radioButton.getTag();
                    for (DeliveryUpgradeOption option : deliveryUpgradeOptions) {
                        if (option.diaryDate.equals(selectedDiaryDate)) {
                            showTimeOptions(option.deliveryTimes);
                        }
                    }
                }
                updateCost();
            }
        });
        deliveryTimesRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                updateCost();
            }
        });
        costTextView = (TextView) root.findViewById(R.id.upgrade_cost_textview);
        payNowButton = (Button) root.findViewById(R.id.pay_now_button);
        payNowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                payNow();
            }
        });
        payNowButton.setEnabled(false);
        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDeliveryOptions();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (OnFragmentInteractionListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ((BaseMenuActivity) getActivity()).setupToolbar(getString(R.string.upgrade_delivery_titlecase), true);
        initParcelStatusView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getDeliveryOptionsSubscription != null && !getDeliveryOptionsSubscription.isUnsubscribed()) {
            getDeliveryOptionsSubscription.unsubscribe();
        }
        getDeliveryOptionsSubscription = null;
        if (upgradeParcelSubscription != null && !upgradeParcelSubscription.isUnsubscribed()) {
            upgradeParcelSubscription.unsubscribe();
        }
        upgradeParcelSubscription = null;
    }

    /**
     * Wrapper class to facilitate mapping of data to UI
     */
    private static class DeliveryUpgradeOption {
        public String diaryDate;
        public List<DeliveryTime> deliveryTimes;

        public DeliveryUpgradeOption() {
            deliveryTimes = new ArrayList<>(5);
        }
    }

    private void getDeliveryOptions() {
        showProgressBar();

        final List<DeliveryUpgradeOption> deliveryUpgradeOptions = new ArrayList<>(5);

        if (getDeliveryOptionsSubscription != null && !getDeliveryOptionsSubscription.isUnsubscribed()) {
            getDeliveryOptionsSubscription.unsubscribe();
        }
        getDeliveryOptionsSubscription =
                api.getParcelUpdateDates(parcel.parcelCode,
                        ParcelActionCodes.UPGRADE_DELIVERY.getValue())
                        .flatMapIterable(new Func1<GetParcelUpdateDatesResponse, Iterable<String>>() {
                            @Override
                            public Iterable<String> call(GetParcelUpdateDatesResponse response) {
                                return response.getDiaryDates();
                            }
                        })
                        .flatMap(new Func1<String, Observable<List<DeliveryUpgradeOption>>>() {
                            @Override
                            public Observable<List<DeliveryUpgradeOption>> call(String diaryDate) {
                                final String userType = "R";
                                // zip up diaryDate with alternative delivery times to add those to our
                                // deliveryUpgradeOptions by deliveryDate
                                return Observable.zip(
                                        Observable.just(diaryDate),
                                        api.getAlternativeDeliveryTimes(
                                                parcel.parcelCode,
                                                diaryDate,
                                                "", // TODO: address doesn't contain city?
                                                parcel.deliveryDetails.address.countryCode,
                                                parcel.deliveryDetails.address.county,
                                                parcel.deliveryDetails.address.locality,
                                                parcel.deliveryDetails.address.postcode,
                                                parcel.deliveryDetails.address.street,
                                                parcel.deliveryDetails.address.town,
                                                userType
                                        ),
                                        new Func2<String, GetAlternativeDeliveryTimesResponse, List<DeliveryUpgradeOption>>() {
                                            @Override
                                            public List<DeliveryUpgradeOption> call(String diaryDate, GetAlternativeDeliveryTimesResponse response) {
                                                if (response.hasErrors()) {
                                                    Log.e(TAG, "Unable to get alternative delivery times" + response.printErrors());
                                                    return null;
                                                }
                                                final List<DeliveryTime> deliveryTimes = response.getDeliveryTimes();
                                                final DeliveryUpgradeOption upgradeOption = new DeliveryUpgradeOption();
                                                upgradeOption.diaryDate = diaryDate;
                                                upgradeOption.deliveryTimes = deliveryTimes;
                                                deliveryUpgradeOptions.add(upgradeOption);
                                                return deliveryUpgradeOptions;
                                            }
                                        });
                            }
                        })
                        .last()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Action1<List<DeliveryUpgradeOption>>() {
                                       @Override
                                       public void call(List<DeliveryUpgradeOption> deliveryUpgradeOptions) {
                                           hideProgressBar();
                                           setDeliveryUpgradeOptions(deliveryUpgradeOptions);
                                           payNowButton.setEnabled(true);
                                       }
                                   },
                                new Action1<Throwable>() {
                                    @Override
                                    public void call(Throwable throwable) {
                                        hideProgressBar();
                                        Log.e(TAG, "Unable to load upgrade options", throwable);
                                        showErrorAlertDialog(getString(R.string.a_problem_has_occurred_try_again));
                                        getActivity().getSupportFragmentManager().popBackStack();
                                    }
                                });

    }

    private void payForUpgrade() {
        // TODO: remove hardcoded order ID
        orderId = "a0122ba4bd8a519c1f91db70bfed788e";

        // show PayPal activity to process upgrade fee...

        // PAYMENT_INTENT_SALE will cause the payment to complete immediately.
        // Change PAYMENT_INTENT_SALE to
        //   - PAYMENT_INTENT_AUTHORIZE to only authorize payment and capture funds later.
        //   - PAYMENT_INTENT_ORDER to create a payment for authorization and capture
        //     later via calls from your server.

        PayPalConfiguration paypalConfig = new PayPalConfiguration()

                // Start with mock environment.  When ready, switch to sandbox (ENVIRONMENT_SANDBOX)
                // or live (ENVIRONMENT_PRODUCTION)
                .environment(PayPalConfiguration.ENVIRONMENT_NO_NETWORK)
                .acceptCreditCards(false)
                .clientId(BuildConfig.PAYPAL_CLIENT_ID);

        final View view = getView();
        assert view != null;

        final RadioButton timeSlotRadioButton = (RadioButton) view.findViewById(deliveryTimesRadioGroup.getCheckedRadioButtonId());
        final RadioButton dateRadioButton = (RadioButton) view.findViewById(deliveryDatesRadioGroup.getCheckedRadioButtonId());
        final String selectedTimeSlot = timeSlotRadioButton.getText().toString();
        final String selectedDate = dateRadioButton.getText().toString();

        final PayPalPayment payment = new PayPalPayment(
                new BigDecimal(Float.parseFloat(selectedDeliveryTime.upgradeCost)),
                "GBP",
                getString(R.string.delivery_upgrade_to, selectedTimeSlot, selectedDate),
//                PayPalPayment.PAYMENT_INTENT_SALE);
                PayPalPayment.PAYMENT_INTENT_AUTHORIZE);

        final Intent intent = new Intent(getActivity(), PaymentActivity.class);
        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, paypalConfig);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);
        startActivityForResult(intent, 0);
    }

    private void upgradeParcel() {
        if (upgradeParcelSubscription != null && !upgradeParcelSubscription.isUnsubscribed()) {
            upgradeParcelSubscription.unsubscribe();
        }

        if (paymentId == null) {
            Log.e(TAG, "paymentId is null, cannot upgrade parcel");
            showErrorAlertDialog(getString(R.string.paypal_not_possible), getString(R.string.paypal));
            return;
        }

        showProgressBar();

        final View view = getView();
        assert view != null;

        // change data format.
        Date diaryDate = null;
        try {
            diaryDate = DIARY_DATE_FORMAT.parse(selectedDiaryDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        final UpgradeParcelParams.Builder params = new UpgradeParcelParams.Builder();
        params.setMobile(CommonUtils.addPrefixNumber(user.getConsumerProfile().mobile));
        params.setEmail(user.getConsumerProfile().email);
        params.setContactName(user.getFullName());
        params.setPaymentVendor(BuildConfig.PAYPAL_PAYMENT_VENDOR);
        params.setVendorRef(paymentId);
        params.setOrderId(orderId);
        params.setParcelCode(parcel.parcelCode);
        params.setDiaryDate(diaryDate != null ? DIARY_DATE_FORMAT_FOR_UPGRADE.format(diaryDate) : selectedDiaryDate);
        params.setUpgradeId(selectedDeliveryTime.upgradeId);

        upgradeParcelSubscription = api.upgradeParcel(params.build())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<UpgradeParcelResponse>() {
                               @Override
                               public void call(UpgradeParcelResponse response) {
                                   hideProgressBar();
                                   if (response.hasErrors()) {
                                       Log.e(TAG, "UpgradeParcelResponse error " + response.printErrors());
                                       showErrorWithRetryUpgradeParcel(getString(R.string.problem_changing_date));
                                       return;
                                   }
                                   showActionConfirmedDialog();
                               }
                           },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                hideProgressBar();
                                Log.e(TAG, "Upgrade parcel error", throwable);
                                showErrorWithRetryUpgradeParcel(getString(R.string.problem_changing_date));
                            }
                        });
    }

    private void showProgressBar() {
        progressBar.show();
        scrollView.setVisibility(View.GONE);
    }

    private void hideProgressBar() {
        progressBar.hide();
        scrollView.setVisibility(View.VISIBLE);
    }

    private void initParcelStatusView() {
        final View view = getView();
        assert view != null;
        final Context context = getContext();
        UIUtils.populateParcelStatus(view, context, parcel);
    }

    public void setDeliveryUpgradeOptions(List<DeliveryUpgradeOption> deliveryUpgradeOptions) {
        this.deliveryUpgradeOptions = deliveryUpgradeOptions;
        if (deliveryUpgradeOptions != null || deliveryUpgradeOptions.size() == 0) {
            final List<String> diaryDates = new ArrayList<>(deliveryUpgradeOptions.size());
            for (DeliveryUpgradeOption option : deliveryUpgradeOptions) {
                diaryDates.add(option.diaryDate);
            }
            setDeliveryDates(diaryDates);
        } else {
            showErrorAlertDialog(getString(R.string.no_delivery_upgrade_options_available));
        }
    }

    private void setDeliveryDates(List<String> diaryDates) {
        final LayoutInflater inflater = LayoutInflater.from(getActivity());

        deliveryDatesRadioGroup.removeAllViews();
        sortDateByTime(diaryDates);
        for (String diaryDate : diaryDates) {
            try {
                final RadioButton radioButton = (RadioButton) inflater.inflate(R.layout.item_radiobutton, deliveryDatesRadioGroup, false);
                final Date date = DIARY_DATE_FORMAT.parse(diaryDate);
                final Calendar cal = Calendar.getInstance();
                cal.setTime(date);
                final String dateSuffix = DateUtils.getDayOfMonthSuffix(cal.get(Calendar.DATE));
                final SimpleDateFormat dateFormat = new SimpleDateFormat(String.format("cccc d'%s' MMMM", dateSuffix), Locale.UK);
                radioButton.setText(dateFormat.format(date));
                radioButton.setId(ViewIdGenerator.generateViewId());
                radioButton.setTag(diaryDate);
                deliveryDatesRadioGroup.addView(radioButton);
            } catch (Exception e) {
                Log.e(TAG, "Unable to parse date", e);
            }
        }
        if (deliveryDatesRadioGroup.getChildCount() > 0) {
            deliveryDatesRadioGroup.check(deliveryDatesRadioGroup.getChildAt(0).getId());
        }
    }

    private void sortDateByTime(List<String> dates) {
        if (dates == null) return;

        Collections.sort(dates, new Comparator<String>() {
            @Override
            public int compare(String lhs, String rhs) {
                int result = 0;
                try {
                    Date lhsDate = DIARY_DATE_FORMAT.parse(lhs);
                    Date rhsDate = DIARY_DATE_FORMAT.parse(rhs);
                    result = (lhsDate.getTime() >= rhsDate.getTime()) ? 1 : -1;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return result;
            }
        });
    }

    private void showTimeOptions(List<DeliveryTime> deliveryTimes) {
        final LayoutInflater inflater = LayoutInflater.from(getActivity());

        deliveryTimesRadioGroup.removeAllViews();
        for (DeliveryTime deliveryTime : deliveryTimes) {
            final RadioButton radioButton = (RadioButton) inflater.inflate(R.layout.item_radiobutton, deliveryTimesRadioGroup, false);
            radioButton.setId(ViewIdGenerator.generateViewId());
            radioButton.setText(deliveryTime.startTime + " - " + deliveryTime.endTime);
            radioButton.setTag(deliveryTime);
            deliveryTimesRadioGroup.addView(radioButton);
        }
        deliveryTimesRadioGroup.check(deliveryTimesRadioGroup.getChildAt(0).getId());
    }

    private void payNow() {
        final View view = getView();
        assert view != null;

        final RadioButton timeSlotRadioButton = (RadioButton) view.findViewById(deliveryTimesRadioGroup.getCheckedRadioButtonId());
        final RadioButton dateRadioButton = (RadioButton) view.findViewById(deliveryDatesRadioGroup.getCheckedRadioButtonId());
        final String selectedTimeSlot = timeSlotRadioButton.getText().toString();
        final String selectedDate = dateRadioButton.getText().toString();

        final float price = Float.parseFloat(selectedDeliveryTime.upgradeCost);
        String priceString = String.format("%.2f", price);

        final AlertDialog.Builder builder =
                new AlertDialog.Builder(getActivity(), R.style.AlertDialogTheme);
        builder.setTitle(getString(R.string.change_delivery_dialog_title));
        builder.setMessage(getString(R.string.upgrade_delivery_dialog_message,
                selectedTimeSlot, selectedDate, priceString));
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                payForUpgrade();
            }
        });
        builder.setNegativeButton(android.R.string.cancel, null);
        builder.show();
    }

    private void updateCost() {
        final int selectedTimeRadioId = deliveryTimesRadioGroup.getCheckedRadioButtonId();
        if (selectedTimeRadioId == -1) {
            return;
        }

        final RadioButton radio = (RadioButton) deliveryTimesRadioGroup.findViewById(selectedTimeRadioId);
        selectedDeliveryTime = (DeliveryTime) radio.getTag();

        final float price = Float.parseFloat(selectedDeliveryTime.upgradeCost);
        String priceString = String.format("%.2f", price);
        String costString = getString(R.string.upgrade_cost_message, priceString);
        costTextView.setText(TextUtils.setSpanBetweenTokens(costString, "##",
                new ForegroundColorSpan(getResources().getColor(R.color.colorPrimary))));
    }

    @UiThread
    protected void showErrorWithRetryUpgradeParcel(String message) {
        AlertDialog.Builder builder =
                new AlertDialog.Builder(getActivity(), R.style.AlertDialogTheme);
        builder.setMessage(message);
        builder.setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                upgradeParcel();
            }
        });
        builder.create().show();
    }

    private void showActionConfirmedDialog() {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(getContext(), R.style.AlertDialogTheme);
        builder.setTitle(R.string.parcel_updated);
        builder.setMessage(getString(R.string.parcel_upgrade_confirmed));
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // all done - return from this Fragment
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
        builder.create().show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        hideProgressBar();
        if (resultCode == Activity.RESULT_OK) {
            PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
            if (confirm != null) {
                try {
                    paymentId = confirm.getProofOfPayment().getPaymentId();
                    Log.i(TAG, "Payment success: " + paymentId);
                } catch (Exception e) {
                    Log.e(TAG, "Problem reading paypal payment confirmation", e);
                    showErrorAlertDialog(getString(R.string.check_your_paypal_account), getString(R.string.failed_confirm_payment));
                }
                upgradeParcel();
            }
        } else if (resultCode == Activity.RESULT_CANCELED) {
            Log.i(TAG, "The user canceled payment.");
            showErrorAlertDialog(getString(R.string.canceled_payment), getString(R.string.paypal));
        } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
            Log.w(TAG, "An invalid Payment or PayPal Configuration was submitted. Please see the docs.");
            showErrorAlertDialog(getString(R.string.check_your_paypal_account), getString(R.string.failed_confirm_payment));
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
//        public void onInFlightPickupFragmentInteraction(Uri uri);
    }
}
