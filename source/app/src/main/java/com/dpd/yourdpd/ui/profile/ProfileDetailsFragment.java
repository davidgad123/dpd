package com.dpd.yourdpd.ui.profile;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.transitions.everywhere.TransitionManager;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.dpd.yourdpd.BuildConfig;
import com.dpd.yourdpd.R;
import com.dpd.yourdpd.model.ConsumerProfile;
import com.dpd.yourdpd.model.PinMethod;
import com.dpd.yourdpd.net.APIError;
import com.dpd.yourdpd.net.request.GeneratePinByEmailParams;
import com.dpd.yourdpd.net.request.GeneratePinBySmsParams;
import com.dpd.yourdpd.net.response.AuthenticatePinParams;
import com.dpd.yourdpd.net.response.AuthenticatePinResponse;
import com.dpd.yourdpd.net.response.GeneratePinResponse;
import com.dpd.yourdpd.ui.BaseFragment;
import com.dpd.yourdpd.ui.Validating;
import com.dpd.yourdpd.util.CommonUtils;
import com.dpd.yourdpd.util.ImagePickerUtils;
import com.dpd.yourdpd.util.PhotoUtils;

import java.io.File;

import br.com.sapereaude.maskedEditText.MaskedEditText;
import retrofit.RetrofitError;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Fragment allowing user to specify profile details like name and photograph
 */
public class ProfileDetailsFragment extends BaseFragment implements Validating {
    private static final String TAG = ProfileDetailsFragment.class.getSimpleName();

    private ProfileDetailsFragmentListener listener;
    private boolean wasNextButtonEnabled;
    private boolean isVerifyLater;

    private EditText firstNameEditText;
    private EditText lastNameEditText;
    private ImageView profilePhotoImageView;
    private Button photoEditButton;
    private View nameTooltip;

    private String szPreviousEmail = "", szPreviousPhoneNumber = "";

    private EditText emailEditText;
    private View emailTooltip;
    private Button editEmailButton;
    private Button emailRequestPinButton;
    private MaskedEditText emailPinEditText;
    private LinearLayout emailPinContainer;

    private EditText phonePrefixEditText;
    private EditText phoneEditText;
    private View phoneTooltip;
    private Button editPhoneButton;
    private Button phoneRequestPinButton;
    private MaskedEditText phonePinEditText;
    private LinearLayout phonePinContainer;

    private String selectedPhotoBase64Data;
    private Toolbar toolbar;

    private ProgressDialog progressDialog;
    private InputMethodManager inputMethodManager;
    private Subscription generatePinSubscription;
    private Subscription authPinSubscription;

    private static final int NONE_CONFIRM_PROGRESSING = 0;
    private static final int START_CONFIRM_PROGRESSING = 1;
    private static final int END_CONFIRM_PROGRESSING = 2;
    private int confirm_progressing = NONE_CONFIRM_PROGRESSING;

    public static ProfileDetailsFragment newInstance() {
        return new ProfileDetailsFragment();
    }

    public ProfileDetailsFragment() {
        // required empty Fragment constructor
        isVerifyLater = false;
    }

    public interface ProfileDetailsFragmentListener {
        void enableNextButton();

        void disableNextButton();

        void doNext();

        boolean isNextButtonEnabled();

        void setTitle(CharSequence title);

        void scrollToTop();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (ProfileDetailsFragmentListener) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString() + " must implement " +
                    ProfileDetailsFragmentListener.class.getSimpleName());
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_add_profile_details, container, false);

        profilePhotoImageView = (ImageView) root.findViewById(R.id.profile_photo_imageview);
        profilePhotoImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editPhoto();
            }
        });
        root.findViewById(R.id.profile_photo_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editPhoto();
            }
        });
        photoEditButton = (Button) root.findViewById(R.id.edit_photo_button);
        photoEditButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editPhoto();
            }
        });

        firstNameEditText = (EditText) root.findViewById(R.id.first_name_edittext);
        lastNameEditText = (EditText) root.findViewById(R.id.last_name_edittext);

        nameTooltip = root.findViewById(R.id.name_tooltip);
        nameTooltip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTooltip(nameTooltip, false);
            }
        });

        // Email
        emailEditText = (EditText) root.findViewById(R.id.email_edittext);
        emailEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (PinMethod.EMAIL.equals(user.getPinMethod())) return;

                if (s.toString().isEmpty() || s.toString().equals(szPreviousEmail)) {
                    confirm_progressing = NONE_CONFIRM_PROGRESSING;
                    emailRequestPinButton.setVisibility(View.GONE);
                } else {
                    confirm_progressing = START_CONFIRM_PROGRESSING;
                    emailRequestPinButton.setVisibility(View.VISIBLE);
                }
            }
        });
        emailTooltip = root.findViewById(R.id.email_tooltip);
        emailTooltip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTooltip(emailTooltip, false);
            }
        });
        editEmailButton = (Button) root.findViewById(R.id.edit_email_button);
        editEmailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                szPreviousEmail = emailEditText.getText().toString();
                editEmail();
            }
        });
        emailRequestPinButton = (Button) root.findViewById(R.id.email_request_pin_button);
        emailRequestPinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestEmailPin();
            }
        });
        emailPinContainer = (LinearLayout) root.findViewById(R.id.email_pin_container);
        emailPinEditText = (MaskedEditText) root.findViewById(R.id.email_pin_edittext);
        emailPinEditText.setTypeface(Typeface.MONOSPACE);
        emailPinEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!emailPinEditText.isEditingAfter()) return;

                final int pinLength = getResources().getInteger(R.integer.pin_length);
                String pin = com.dpd.yourdpd.util.TextUtils.getRealTextFromPIN(s.toString(), emailPinEditText.getMask(), emailPinEditText.getViewHolderChar());
                if (pin.length() == pinLength) {
                    authEmailPin();
                }
            }
        });
        root.findViewById(R.id.email_info_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTooltip(emailTooltip, emailTooltip.getVisibility() != View.VISIBLE);
            }
        });
        root.findViewById(R.id.resend_email_pin_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestEmailPin();
            }
        });

        // Phone
        phonePrefixEditText = (EditText) root.findViewById(R.id.phone_prefix_edittext);
        phoneEditText = (EditText) root.findViewById(R.id.phone_edittext);
        phoneEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (PinMethod.SMS.equals(user.getPinMethod())) return;

                if (s.toString().isEmpty() || s.toString().equals(szPreviousPhoneNumber)) {
                    confirm_progressing = NONE_CONFIRM_PROGRESSING;
                    phoneRequestPinButton.setVisibility(View.GONE);
                } else {
                    confirm_progressing = START_CONFIRM_PROGRESSING;
                    phoneRequestPinButton.setVisibility(View.VISIBLE);
                }
            }
        });
        phoneTooltip = root.findViewById(R.id.phone_tooltip);
        phoneTooltip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTooltip(phoneTooltip, false);
            }
        });
        editPhoneButton = (Button) root.findViewById(R.id.edit_phone_button);
        editPhoneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                szPreviousPhoneNumber = phoneEditText.getText().toString();
                editPhone();
            }
        });
        phoneRequestPinButton = (Button) root.findViewById(R.id.phone_request_pin_button);
        phoneRequestPinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestPhonePin();
            }
        });
        phonePinContainer = (LinearLayout) root.findViewById(R.id.phone_pin_container);
        phonePinEditText = (MaskedEditText) root.findViewById(R.id.phone_pin_edittext);
        phonePinEditText.setTypeface(Typeface.MONOSPACE);
        phonePinEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!phonePinEditText.isEditingAfter()) return;

                final int pinLength = getResources().getInteger(R.integer.pin_length);
                String pin = com.dpd.yourdpd.util.TextUtils.getRealTextFromPIN(s.toString(), phonePinEditText.getMask(), phonePinEditText.getViewHolderChar());
                if (pin.length() == pinLength) {
                    authPhonePin();
                }
            }
        });

        root.findViewById(R.id.resend_phone_pin_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestPhonePin();
            }
        });
        root.findViewById(R.id.phone_info_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTooltip(phoneTooltip, phoneTooltip.getVisibility() != View.VISIBLE);
            }
        });

        CommonUtils.setupUIForKeyboard(getActivity(), root);
        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // populate controls
        View view = getView();
        assert view != null;

        if (PinMethod.EMAIL.equals(user.getPinMethod())) {
            emailEditText.setEnabled(false);
            emailEditText.setFocusable(false);
            ((TextView) view.findViewById(R.id.phone_tooltip_textview)).setText(getString(R.string.invalid_phone_tooltip));
            view.findViewById(R.id.phone_info_button).setVisibility(View.GONE);
        } else {
            view.findViewById(R.id.email_info_button).setVisibility(View.GONE);
            ((TextView) view.findViewById(R.id.email_tooltip_textview)).setText(getString(R.string.invalid_email_tooltip));
            view.findViewById(R.id.phone_prefix_edittext).setEnabled(false);
            phoneEditText.setEnabled(false);
            phoneEditText.setFocusable(false);
        }

        if (!TextUtils.isEmpty(user.getConsumerProfile().email)) {
            emailEditText.setText(user.getConsumerProfile().email);
        }

        if (!TextUtils.isEmpty(user.getConsumerProfile().mobile)) {
            String phoneNum = user.getConsumerProfile().mobile;
            phoneEditText.setText(CommonUtils.removePrefixNumber(phoneNum));
        }

        if (!TextUtils.isEmpty(user.getConsumerProfile().firstname)) {
            firstNameEditText.setText(user.getConsumerProfile().firstname);
        }

        if (!TextUtils.isEmpty(user.getConsumerProfile().surname)) {
            lastNameEditText.setText(user.getConsumerProfile().surname);
        }

        selectedPhotoBase64Data = user.getConsumerProfile().photograph;
        if (!TextUtils.isEmpty(selectedPhotoBase64Data)) {
            PhotoUtils.loadUnknownImageDataIntoImageView(getContext(), selectedPhotoBase64Data, profilePhotoImageView, 0);
            profilePhotoImageView.setVisibility(View.VISIBLE);
            photoEditButton.setVisibility(View.VISIBLE);
        } else {
            profilePhotoImageView.setImageBitmap(null);
            photoEditButton.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        setToolbarWithoutLeftIcon();
    }

    private void setToolbarWithoutLeftIcon() {
        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        ImageView marginImg = (ImageView) toolbar.findViewById(R.id.to_get_sameIndent);
        marginImg.setVisibility(View.INVISIBLE);
        toolbar.setNavigationIcon(null);
        TextView title = (TextView) toolbar.findViewById(R.id.txt_title);
        title.setText(getString(R.string.your_profile_titlecase));
        ((ProfileSetupActivity) getActivity()).setSupportActionBar(toolbar);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (hidden) {
            // keep track of the Next button state for when we return to this Fragment through
            // the history back stack
            wasNextButtonEnabled = listener.isNextButtonEnabled();
        } else {
            //listener.setTitle(title);
            setToolbarWithoutLeftIcon();
            if (wasNextButtonEnabled) {
                listener.enableNextButton();
            } else {
                listener.disableNextButton();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            final File file = new File(getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES), "temp.jpg");
            if (requestCode == ImagePickerUtils.REQUEST_CAMERA) {
                ImagePickerUtils.loadAndDisplayBitmap(file,
                        profilePhotoImageView,
                        new ImagePickerUtils.LoadImageCallback() {
                            @Override
                            public void onSuccess(Bitmap bitmap) {
                                onImageLoaded(bitmap);
                            }

                            @Override
                            public void onProgress(int progress) {
                            }

                            @Override
                            public void onError(int code) {
                                Toast.makeText(getActivity(),
                                        getString(R.string.problem_loading_image),
                                        Toast.LENGTH_SHORT).show();
                            }
                        });

            } else if (requestCode == ImagePickerUtils.REQUEST_GALLERY) {
                ImagePickerUtils.loadAndDisplayBitmap(data,
                        getActivity(),
                        profilePhotoImageView,
                        new ImagePickerUtils.LoadImageCallback() {
                            @Override
                            public void onSuccess(Bitmap bitmap) {
                                onImageLoaded(bitmap);
                            }

                            @Override
                            public void onProgress(int progress) {
                            }

                            @Override
                            public void onError(int code) {
                                Toast.makeText(getActivity(),
                                        getString(R.string.problem_loading_image),
                                        Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        }
    }

    private void onImageLoaded(Bitmap bitmap) {
        selectedPhotoBase64Data = PhotoUtils.bitmapToBase64(bitmap);
        if (!TextUtils.isEmpty(selectedPhotoBase64Data))
            photoEditButton.setVisibility(View.VISIBLE);
    }

    /**
     * @param title (optional)
     */
    private void showLoadingDialog(String title) {
        if (title == null) {
            title = getString(R.string.please_wait_);
        }
        hideLoadingDialog();
        progressDialog = ProgressDialog.show(getActivity(),
                null,
                title,
                true,
                false);
    }

    private void hideLoadingDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        ImagePickerUtils.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
    }

    private void editPhoto() {
        ImagePickerUtils.showPickerDialog(this);
    }

    private void requestEmailPin() {
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(emailEditText.getWindowToken(), 0);
        }
        requestPin(PinMethod.EMAIL);
    }

    private void requestPhonePin() {
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(phoneEditText.getWindowToken(), 0);
        }
        requestPin(PinMethod.SMS);
    }

    private void requestPin(PinMethod method) {
        // NOTE: RequestPinFragment contains similar validations
        if (method.equals(PinMethod.EMAIL)) {
            final String email = emailEditText.getText().toString();
            if (TextUtils.isEmpty(email) || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                showTooltip(emailTooltip, true);
                return;
            }
        } else if (method.equals(PinMethod.SMS)) {
            final String phone = phoneEditText.getText().toString();
            if (phone.length() < 7 || !Patterns.PHONE.matcher(phone).matches()) {
                showTooltip(phoneTooltip, true);
                return;
            }
        }
        performRequestPinAPICall(method);
    }

    private void performRequestPinAPICall(final PinMethod method) {
        final String email = emailEditText.getText().toString();
        String mobile = CommonUtils.addPrefixNumber(phoneEditText.getText().toString());

        showLoadingDialog(null);

        final Observable<GeneratePinResponse> observable = method.equals(PinMethod.SMS) ?
                api.generatePinBySMS(new GeneratePinBySmsParams(mobile)) :
                api.generatePinByEmail(new GeneratePinByEmailParams(email));

        if (generatePinSubscription != null && !generatePinSubscription.isUnsubscribed()) {
            generatePinSubscription.unsubscribe();
        }
        generatePinSubscription = observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        new Action1<GeneratePinResponse>() {
                            @Override
                            public void call(GeneratePinResponse response) {
                                progressDialog.hide();
                                if (response.hasErrors()) {
                                    Log.e(TAG, "Generate pin error: " + response.printErrors());
                                    if (BuildConfig.LOG_ERRORS_TO_SERVER && !BuildConfig.DEBUG) {
                                        Crashlytics.log(response.printErrors());
                                        Crashlytics.logException(new Exception("SettingsGeneratePin"));
                                    }
                                    showErrorAlertDialog(getString(R.string.problem_generating_pin_error));
                                } else {
                                    onPinRequested(method);
                                }
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                progressDialog.hide();
                                if (BuildConfig.LOG_ERRORS_TO_SERVER && !BuildConfig.DEBUG) {
                                    Crashlytics.logException(throwable);
                                }
                                if (throwable instanceof RetrofitError) {
                                    RetrofitError retrofitError = (RetrofitError) throwable;
                                    if (retrofitError.getResponse().getStatus() == 429) {
                                        showErrorAlertDialog(getString(R.string.too_many_requests));
                                        return;
                                    }
                                }
                                Log.e(TAG, "Generate pin error", throwable);
                                showErrorAlertDialog(getString(R.string.problem_generating_pin_error));
                            }
                        });
    }

    private void onPinRequested(PinMethod method) {
        // show email or SMS pin entry field
        TransitionManager.beginDelayedTransition((ViewGroup) getView());
        if (method.equals(PinMethod.EMAIL)) {
            emailEditText.setEnabled(false);
            editEmailButton.setVisibility(View.VISIBLE);
            emailTooltip.setVisibility(View.GONE);
            emailRequestPinButton.setVisibility(View.GONE);
            emailPinContainer.setVisibility(View.VISIBLE);
            emailEditText.post(new Runnable() {
                @Override
                public void run() {
                    emailPinEditText.setText("");
                    emailPinEditText.requestFocus();
                }
            });
        } else if (method.equals(PinMethod.SMS)) {
            phoneEditText.setEnabled(false);
            phonePrefixEditText.setEnabled(false);
            editPhoneButton.setVisibility(View.VISIBLE);
            phoneTooltip.setVisibility(View.GONE);
            phoneRequestPinButton.setVisibility(View.GONE);
            phonePinContainer.setVisibility(View.VISIBLE);
            phoneEditText.post(new Runnable() {
                @Override
                public void run() {
                    phonePinEditText.setText("");
                    phonePinEditText.requestFocus();
                }
            });
        }
    }

    private void authEmailPin() {
        authenticatePin(PinMethod.EMAIL, com.dpd.yourdpd.util.TextUtils.getRealTextFromPIN(emailPinEditText.getText().toString(), emailPinEditText.getMask(), emailPinEditText.getViewHolderChar()));

        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromInputMethod(emailEditText.getWindowToken(), InputMethodManager.SHOW_IMPLICIT);
        }
    }

    private void authPhonePin() {
        authenticatePin(PinMethod.SMS, com.dpd.yourdpd.util.TextUtils.getRealTextFromPIN(phonePinEditText.getText().toString(), phonePinEditText.getMask(), phonePinEditText.getViewHolderChar()));

        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromInputMethod(phoneEditText.getWindowToken(), InputMethodManager.SHOW_IMPLICIT);
        }
    }

    private void authenticatePin(final PinMethod method, String pin) {
        final String email = emailEditText.getText().toString();
        final String mobile = CommonUtils.addPrefixNumber(phoneEditText.getText().toString());

        final AuthenticatePinParams params = new AuthenticatePinParams();
        params.deviceId = user.getConsumerProfile().deviceId;
        params.pinNumber = pin;
        params.mediumValue = method.equals(PinMethod.SMS) ? mobile : email;
        params.medium = method.equals(PinMethod.SMS) ? "sms" : "email";

        showLoadingDialog(getString(R.string.verifying_pin));

        if (authPinSubscription != null && !authPinSubscription.isUnsubscribed()) {
            authPinSubscription.unsubscribe();
        }
        authPinSubscription = api.authenticatePin(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<AuthenticatePinResponse>() {
                               @Override
                               public void call(AuthenticatePinResponse response) {
                                   hideLoadingDialog();
                                   if (response.hasErrors()) {
                                       final APIError error = response.getErrors().get(0);
//                                       if (APIError.ErrorCode.INVALID_FIELD_VALUE.toString().equals(error.errorCode)) {
                                           showErrorAlertDialog(getString(R.string.pin_incorrect_message), getString(R.string.pin_incorrect));
//                                       } else {
//                                           showErrorAlertDialog(getString(R.string.a_problem_has_occurred_try_again));
//                                       }
                                       onPinVerifyFailed(method);
                                   } else {
                                       onPinVerified(method);
                                   }
                               }
                           },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                hideLoadingDialog();
                                showErrorAlertDialog(getString(R.string.a_problem_has_occurred_try_again));
                                onPinVerifyFailed(method);
                            }
                        });
    }

    private void onPinVerified(PinMethod method) {
        confirm_progressing = END_CONFIRM_PROGRESSING;

        final ConsumerProfile consumerProfile = user.getConsumerProfile();
        if (method.equals(PinMethod.EMAIL)) {
            consumerProfile.email = emailEditText.getText().toString();

            // Reset and Hide pin container.
            emailPinEditText.setText("");
            TransitionManager.beginDelayedTransition((ViewGroup) getView());
            emailPinContainer.setVisibility(View.GONE);

            // Show verified dialog.
            showEmailVerifiedDialog();

        } else {
            consumerProfile.mobile = phoneEditText.getText().toString();

            // Reset and Hide pin container.
            phonePinEditText.setText("");
            TransitionManager.beginDelayedTransition((ViewGroup) getView());
            phonePinContainer.setVisibility(View.GONE);

            // Show verified dialog.
            showMobileVerifiedDialog();
        }
    }

    private void onPinVerifyFailed(PinMethod method) {
        if (method.equals(PinMethod.EMAIL)) {
            // Reset and Hide pin container.
            emailPinEditText.setText("");
        } else {
            // Reset and Hide pin container.
            phonePinEditText.setText("");
        }
    }

    private void showEmailVerifiedDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AlertDialogTheme);
        builder.setTitle(getString(R.string.email_verified_title));
        builder.setMessage(getString(R.string.email_verified_message));
        builder.setPositiveButton(getString(android.R.string.ok), null);
        builder.create().show();
    }

    private void showMobileVerifiedDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AlertDialogTheme);
        builder.setTitle(getString(R.string.phone_pin_verified_title));
        builder.setMessage(getString(R.string.phone_pin_verified_message));
        builder.setPositiveButton(getString(android.R.string.ok), null);
        builder.create().show();
    }

    private void editEmail() {
        emailPinContainer.setVisibility(View.GONE);
        emailEditText.setEnabled(true);
        emailEditText.setSelection(0, emailEditText.length());
        emailEditText.requestFocus();
        TransitionManager.beginDelayedTransition((ViewGroup) getView());
        editEmailButton.setVisibility(View.INVISIBLE);
        emailTooltip.setVisibility(View.GONE);
        emailRequestPinButton.setVisibility(View.VISIBLE);
        confirm_progressing = START_CONFIRM_PROGRESSING;

        if (inputMethodManager != null) {
            inputMethodManager.showSoftInput(emailEditText, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    private void editPhone() {
        phonePinContainer.setVisibility(View.GONE);
        phonePrefixEditText.setEnabled(true);
        phoneEditText.setEnabled(true);
        phoneEditText.setSelection(0, phoneEditText.length());
        phoneEditText.requestFocus();
        TransitionManager.beginDelayedTransition((ViewGroup) getView());
        editPhoneButton.setVisibility(View.INVISIBLE);
        phoneTooltip.setVisibility(View.GONE);
        phoneRequestPinButton.setVisibility(View.VISIBLE);
        confirm_progressing = START_CONFIRM_PROGRESSING;

        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.showSoftInput(phoneEditText, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    private void showTooltip(View tooltip, boolean isShow) {
        TransitionManager.beginDelayedTransition((ViewGroup) getActivity().getWindow().findViewById(R.id.fragment_container));
        tooltip.setVisibility(isShow ? View.VISIBLE : View.GONE);
    }

    private void resetFields() {
        if (PinMethod.EMAIL.equals(user.getPinMethod()) && phoneEditText.getText().length() > 0) {
            phoneEditText.setText("");
            phonePinContainer.setVisibility(View.GONE);
            phonePrefixEditText.setEnabled(true);
            phoneEditText.setEnabled(true);
            editPhoneButton.setVisibility(View.INVISIBLE);
            phoneTooltip.setVisibility(View.GONE);
            confirm_progressing = NONE_CONFIRM_PROGRESSING;
        } else if (PinMethod.SMS.equals(user.getPinMethod()) && emailEditText.getText().length() > 0) {
            emailEditText.setText("");
            emailPinContainer.setVisibility(View.GONE);
            emailEditText.setEnabled(true);
            editEmailButton.setVisibility(View.INVISIBLE);
            emailTooltip.setVisibility(View.GONE);
            confirm_progressing = NONE_CONFIRM_PROGRESSING;
        }
    }

    @Override
    public boolean validate() {
        String firstName = firstNameEditText.getText().toString().trim();
        String lastName = lastNameEditText.getText().toString().trim();
        if (firstName.isEmpty() && lastName.isEmpty()) {
            ((TextView) nameTooltip.findViewById(R.id.name_tooltip_textview)).setText(getString(R.string.please_enter_firstname_surname));
            showTooltip(nameTooltip, true);
            return false;
        } else if (firstName.isEmpty()) {
            ((TextView) nameTooltip.findViewById(R.id.name_tooltip_textview)).setText(getString(R.string.please_enter_first_name));
            showTooltip(nameTooltip, true);
            return false;
        } else if (lastName.isEmpty()) {
            ((TextView) nameTooltip.findViewById(R.id.name_tooltip_textview)).setText(getString(R.string.please_enter_last_name));
            showTooltip(nameTooltip, true);
            return false;
        }
        nameTooltip.setVisibility(View.GONE);

        boolean isSMSPin = user.getPinMethod().equals(PinMethod.EMAIL);
        if (!isVerifyLater && confirm_progressing != END_CONFIRM_PROGRESSING) {
            if ((isSMSPin && phoneEditText.getText().length() > 0)
                    || (!isSMSPin && emailEditText.getText().length() > 0)) {
//                showErrorAlertDialogWithOk(
//                        getString(isSMSPin ? R.string.mobile_not_confirmed_message : R.string.email_not_confirmed_message),
//                        getString(isSMSPin ? R.string.mobile_not_confirmed : R.string.email_not_confirmed),
//                        new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                resetFields();
//                            }
//                        });
                final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AlertDialogTheme);
                builder.setTitle(getString(isSMSPin ? R.string.mobile_not_confirmed : R.string.email_not_confirmed));
                builder.setMessage(getString(R.string.not_confirmed_message, getString(isSMSPin ? R.string.mobile : R.string.email)));
                builder.setNeutralButton(getString(R.string.verify_later), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        isVerifyLater = true;
                        applyChanges();
                        listener.doNext();
                    }
                });
                builder.setPositiveButton(getString(R.string.verify_now), null);
                builder.create().show();
                return false;
            }
        }

        // Reset verify later flag.
        isVerifyLater = false;
        return true;
    }

    @Override
    public void applyChanges() {
        // store data in user for next step
        final ConsumerProfile consumerProfile = user.getConsumerProfile();
        consumerProfile.photograph = selectedPhotoBase64Data;
        consumerProfile.firstname = firstNameEditText.getText().toString().trim();
        consumerProfile.surname = lastNameEditText.getText().toString().trim();
        consumerProfile.email = emailEditText.getText().toString().trim();
        consumerProfile.mobile = phoneEditText.getText().toString().trim();
    }
}
