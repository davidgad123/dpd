package com.dpd.yourdpd.ui.yourdeliveries;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Typeface;
import android.location.Location;
import android.provider.Settings;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.transitions.everywhere.TransitionManager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.dpd.yourdpd.R;
import com.dpd.yourdpd.model.Depot;
import com.dpd.yourdpd.model.Parcel;
import com.dpd.yourdpd.model.ParcelActionCodes;
import com.dpd.yourdpd.model.PickupLocation;
import com.dpd.yourdpd.model.RouteView;
import com.dpd.yourdpd.model.User;
import com.dpd.yourdpd.net.maps.MapsRouteRequest;
import com.dpd.yourdpd.net.maps.MapsRouteResponse;
import com.dpd.yourdpd.ui.widget.CircleImageTransform;
import com.dpd.yourdpd.util.DateUtils;
import com.dpd.yourdpd.util.GMapsUtils;
import com.dpd.yourdpd.util.ImagePickerUtils;
import com.dpd.yourdpd.util.MapMarkerUtils;
import com.dpd.yourdpd.util.PhotoUtils;
import com.dpd.yourdpd.util.UIUtils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Displays all details related to the location of a Parcel, depending on its current status
 */
public class ParcelLocationView extends FrameLayout {
    private static final String TAG = ParcelLocationView.class.getSimpleName();

    private User user;
    private Parcel parcel;
    private PickupLocation pickupLocation;
    private Depot depot;
    private RouteView routeView;
    private String routeDriveImage;
    private Location userLocation;

    private final ViewGroup mapContainer;
    private SupportMapFragment mapFragment;
    private FragmentManager fragmentManager;
    private List<Marker> mapMarkers;
    private List<Polyline> mapPolylines;
    private final ViewGroup mapIconBar;
    private MapsRouteRequest mapsRouteRequest;
    private final ViewGroup directionsContainer;
    private final View customerPreferenceTooltip;
    private final TextView headingTextView;
    private final TextView driverName;

    private final ViewGroup locationDescriptionContainer;
    private final ImageView locationDriverPhoto;
    private final TextView locationDescriptionTextView;
    private final TextView locationEstimateMinutesToStopTextView;
    private final Button locationExpandButton;

    private final TextView collectFromDepotDetailsTextView;
    private final ViewGroup collectFromDepotDetailsContainer;
    private final Button expandDirectionsButton;

    public ParcelLocationView(Context context) {
        this(context, null);
    }

    public ParcelLocationView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ParcelLocationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        LayoutInflater.from(getContext()).inflate(R.layout.parcel_location, this, true);

        mapContainer = (ViewGroup) findViewById(R.id.map_container);
        mapIconBar = (ViewGroup) findViewById(R.id.map_icon_bar);
        directionsContainer = (ViewGroup) findViewById(R.id.directions_container);
        customerPreferenceTooltip = findViewById(R.id.customer_preference_tooltip);
        headingTextView = (TextView) findViewById(R.id.heading_textview);
        driverName = (TextView) findViewById(R.id.txt_driver_name);

        locationDescriptionContainer = (ViewGroup) findViewById(R.id.parcel_location_description_container);
        locationDriverPhoto = (ImageView) findViewById(R.id.parcel_driver_photo);
        locationDescriptionTextView = (TextView) findViewById(R.id.parcel_location_description_textview);
        locationEstimateMinutesToStopTextView = (TextView) findViewById(R.id.parcel_estimate_minutes_to_stop_textview);
        locationExpandButton = (Button) findViewById(R.id.parcel_location_expand_button);
        locationExpandButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleExpandLocation();
            }
        });

        collectFromDepotDetailsTextView = (TextView) findViewById(R.id.collect_from_depot_details_title);
        collectFromDepotDetailsContainer = (ViewGroup) findViewById(R.id.collect_from_depot_details_container);

        expandDirectionsButton = (Button) findViewById(R.id.expand_directions_button);
        expandDirectionsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleExpandDirections();
            }
        });
    }

    public void setFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setIsLoading(boolean isLoading) {
        findViewById(R.id.locationContent).setVisibility(isLoading ? GONE : VISIBLE);
        findViewById(R.id.locationProgressBar).setVisibility(isLoading ? VISIBLE : GONE);
    }

    public void showPickupLocation(Parcel parcel, PickupLocation pickupLocation) {
        this.parcel = parcel;
        this.pickupLocation = pickupLocation;
        init();
    }

    public void showOutForDelivery(Parcel parcel, RouteView routeView, String routeDriveImage) {
        this.parcel = parcel;
        this.routeView = routeView;
        this.routeDriveImage = routeDriveImage;
        init();
    }

    public void showDepot(Parcel parcel, Depot depot, RouteView routeView) {
        this.parcel = parcel;
        this.depot = depot;
        this.routeView = routeView;
        init();
    }

    public void showDelivered(Parcel parcel) {
        this.parcel = parcel;
        init();
    }

    public void showEstimatedDeliveryDate(Parcel parcel) {
        this.parcel = parcel;
        init();
    }

    public void destroy() {
        user = null;
        fragmentManager = null;

        if (mapsRouteRequest != null) {
            mapsRouteRequest.cancel();
        }
        mapsRouteRequest = null;
    }

    private void init() {
        final Context context = getContext();
        CharSequence parcelLocationDescription = null, parcelEstimateMinutesToStopDescription = null;
        boolean showMap = false;

        headingTextView.setVisibility(GONE);
        locationExpandButton.setVisibility(View.VISIBLE);
        locationDescriptionContainer.setVisibility(View.VISIBLE);
        collectFromDepotDetailsTextView.setVisibility(View.GONE);
        collectFromDepotDetailsContainer.setVisibility(View.GONE);
        expandDirectionsButton.setVisibility(VISIBLE);
        directionsContainer.setVisibility(GONE);
        mapContainer.setVisibility(GONE);
        mapIconBar.setVisibility(GONE);
        customerPreferenceTooltip.setVisibility(View.GONE);

        // determine parcel location message and map mode depending on parcelStatusType
        try {
            switch (parcel.parcelStatusType) {
                case OUT_FOR_DELIVERY: {
                    showMap = true;
                    int completedDeliveryStops = routeView.completedDeliveryStops + 1;
                    int stopNumber = parcel.deliveryDepot.route.stop.stopNumber;
                    if (completedDeliveryStops != stopNumber) {
                        parcelLocationDescription = context.getString(
                                R.string.parcel_location_description_1,
                                routeView.getDriverName(context),
                                context.getString(R.string.number) + " " + completedDeliveryStops,
                                context.getString(R.string.number) + " " + stopNumber);
                    } else
                        parcelLocationDescription = context.getString(
                                R.string.parcel_location_description_2,
                                routeView.getDriverName(context));

                    // When under 1 hour, the text should state 'X minutes away from you'
                    // When over 1 hour the text should state: 'X hour and X minutes away from you'
                    // When it hits a 1 hour with no minutes it should state: 'X hour away from you'
                    int hours = parcel.deliveryDepot.route.stop.estimatedMinsToStop / 60;
                    int minutes = parcel.deliveryDepot.route.stop.estimatedMinsToStop % 60;
                    if (hours > 0) {
                        if (minutes == 0)
                            parcelEstimateMinutesToStopDescription = context.getString(
                                    R.string.parcel_location_estimated_hours_to_stop, routeView.getDriverName(context),
                                    hours, hours > 1 ? "s" : "");
                        else
                            parcelEstimateMinutesToStopDescription = context.getString(
                                    R.string.parcel_location_estimated_hours_minutes_to_stop, routeView.getDriverName(context),
                                    hours, hours > 1 ? "s" : "", minutes);
                    } else
                        parcelEstimateMinutesToStopDescription = context.getString(
                                R.string.parcel_location_estimated_minutes_to_stop, routeView.getDriverName(context),
                                minutes);

                    // optionally show consumer preference / safe place tooltip
                    if (!TextUtils.isEmpty(parcel.consumerPreferenceText)) {
                        final TextView consumerPreferenceTooltip = (TextView) customerPreferenceTooltip.findViewById(R.id.tooltip_textview);
                        consumerPreferenceTooltip.setText(parcel.consumerPreferenceText);
                        customerPreferenceTooltip.setVisibility(View.VISIBLE);
                    }

                    driverName.setText(routeView.driverName != null ?
                            context.getString(R.string.your_driver_name, routeView.getDriverName(context)) :
                            context.getString(R.string.your_driver));
                    break;
                }
                case COLLECT_FROM_DEPOT: {
                    showMap = true;
                    locationExpandButton.setVisibility(View.GONE);
                    collectFromDepotDetailsTextView.setVisibility(View.VISIBLE);
                    collectFromDepotDetailsContainer.setVisibility(View.VISIBLE);

                    // show collect from depot details
                    if (parcel.collectFromDepotDiaryDate != null) {
                        final DateTimeFormatter dateFormat = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
                        final DateTime dateTime1 = dateFormat.parseDateTime(parcel.collectFromDepotDiaryDate);
                        final DateTime dateTime2 = dateTime1.withDurationAdded(1000 * 60 * 60, 1);
                        final String formattedCollectionDate = DateTimeFormat.forPattern("d").print(dateTime1) + DateUtils.getDayOfMonthSuffix(dateTime1.getDayOfMonth())
                                + DateTimeFormat.forPattern(" MMMM").print(dateTime1);
                        final String formattedCollectionTime1 = DateTimeFormat.forPattern("HH:mm").print(dateTime1);
                        final String formattedCollectionTime2 = DateTimeFormat.forPattern("HH:mm").print(dateTime2);
                        parcelLocationDescription =
                                context.getString(R.string.parcel_location_collect_from_pickup_message,
                                        formattedCollectionDate,
                                        formattedCollectionTime1,
                                        formattedCollectionTime2);
                    } else
                        locationDescriptionContainer.setVisibility(View.GONE);

                    final TextView toTextViewTop = (TextView) collectFromDepotDetailsContainer.findViewById(R.id.to_address_textview_top);
                    toTextViewTop.setText(UIUtils.makesAddBoldText(depot.address.toFormattedString(), depot.address.organisation));
                    break;
                }
                case COLLECT_FROM_PICKUP:
                case COLLECT_FROM_PICKUP_WITH_PASS: {
                    showMap = true;
                    locationExpandButton.setVisibility(GONE);
                    locationDescriptionContainer.setVisibility(GONE);
                    headingTextView.setVisibility(VISIBLE);
                    headingTextView.setText(context.getString(R.string.parcel_location_directions_to_x, pickupLocation.getName()));
                    break;
                }
                case SHOW_ESTIMATED_DELIVERY_DATE: {
                    showMap = false;
                    locationExpandButton.setVisibility(GONE);
                    final DateTime dateTime = parcel.getEstimatedDeliveryDate();
//                    final String dateSuffix = DateUtils.getDayOfMonthSuffix(dateTime.getDayOfMonth());
//                    final DateTimeFormatter dateFormat = DateTimeFormat.forPattern(String.format("EEEE d'%s' MMMM yyyy", dateSuffix));
                    final DateTimeFormatter dateFormat = DateTimeFormat.forPattern("EEEE d MMMM yyyy");
                    final String formattedDate = dateFormat.print(dateTime);

                    final String requestString;
                    if (ParcelActionCodes.DELIVER_TO_NEIGHBOUR.getValue().equals(parcel.estimatedDeliveryActionCode)) {
                        requestString = context.getString(R.string.parcel_location_estimated_delivery_date_request_string_dtn);

                    } else if (ParcelActionCodes.LEAVE_IN_SAFE_PLACE.getValue().equals(parcel.estimatedDeliveryActionCode)) {
                        requestString = context.getString(R.string.parcel_location_estimated_delivery_date_request_string_sfp);

                    } else if (ParcelActionCodes.UPGRADE_DELIVERY.getValue().equals(parcel.estimatedDeliveryActionCode)) {
                        final String startTime = parcel.getEstimatedDeliveryStartTime();
                        final String endTime = parcel.getEstimatedDeliveryEndTime();
                        requestString = context.getString(
                                R.string.parcel_location_estimated_delivery_date_request_string_upg,
                                startTime,
                                endTime);

                    } else {
                        requestString = context.getString(R.string.parcel_location_estimated_delivery_date_request_string_red);
                    }
                    parcelLocationDescription = context.getString(
                            R.string.parcel_location_estimated_delivery_date,
                            formattedDate, requestString
                    );
                    break;
                }
                case DELIVERED: {
                    showMap = isAvailableToShowMap();
                    if (showMap) {
                        headingTextView.setVisibility(VISIBLE);
                        headingTextView.setText(context.getString(R.string.delivery_location_on_map));
                    }
                    expandDirectionsButton.setVisibility(GONE);
                    locationExpandButton.setVisibility(GONE);
                    locationDescriptionContainer.setVisibility(GONE);
                    break;
                }
                default: {
                    showMap = false;
                }
            }

        } catch (Exception e) {
            Log.e(TAG, "Cannot construct parcelLocationDescription", e);

        } finally {
            if (parcelLocationDescription == null) {
                parcelLocationDescription = context.getString(R.string.parcel_location_unavailable);
            }

            if (routeDriveImage != null) {
                final Bitmap photo = PhotoUtils.base64ToBitmap(routeDriveImage);
                final CircleImageTransform circleImageTransform = new CircleImageTransform();
                final Bitmap circularPhoto = circleImageTransform.transform(photo);
                locationDriverPhoto.setImageBitmap(circularPhoto);
                locationDriverPhoto.setVisibility(View.VISIBLE);
            } else {
                locationDriverPhoto.setImageBitmap(null);
                locationDriverPhoto.setVisibility(View.GONE);
            }

            // apply colour and text styling
            parcelLocationDescription = UIUtils.formatText(String.valueOf(parcelLocationDescription), context);
            locationDescriptionTextView.setText(parcelLocationDescription);

            if (parcelEstimateMinutesToStopDescription != null) {
                parcelEstimateMinutesToStopDescription = UIUtils.formatText(String.valueOf(parcelEstimateMinutesToStopDescription), context);
                locationEstimateMinutesToStopTextView.setText(parcelEstimateMinutesToStopDescription);
                locationEstimateMinutesToStopTextView.setVisibility(View.VISIBLE);
            }
        }

        // add MapFragment and request directions
        if (showMap) { // TODO: check parcel.outForDeliveryDetails.mapAvailable?
            mapContainer.setVisibility(VISIBLE);

            if (mapFragment == null) {
                mapFragment = SupportMapFragment.newInstance();
            }
            if (!mapFragment.isAdded()) {
                assert fragmentManager != null;
                final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.add(mapContainer.getId(), mapFragment);
                fragmentTransaction.commit();
            }

            mapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap map) {
                    // show directions if required
                    switch (parcel.parcelStatusType) {
                        case DELIVERED: {
                            showParcelDeliveredOnMap(map);
                            break;
                        }
                        case OUT_FOR_DELIVERY: {
                            showParcelOutForDeliveryOnMap(map);
                            break;
                        }
                        case COLLECT_FROM_DEPOT: {
                            final LatLng from = new LatLng(
                                    parcel.deliveryDetails.address.addressPoint.latitude,
                                    parcel.deliveryDetails.address.addressPoint.longitude);
                            final LatLng to = new LatLng(
                                    depot.addressPoint.latitude,
                                    depot.addressPoint.longitude);
                            // immediately show destination in case directions fail
                            setMapLocation(map, to);
                            getDirections(from, to);
                            map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                                @Override
                                public void onMapClick(LatLng latLng) {
                                    getDirections(from, to);
                                }
                            });
                            break;
                        }
                        case COLLECT_FROM_PICKUP:
                        case COLLECT_FROM_PICKUP_WITH_PASS: {
                            final LatLng from = new LatLng(
                                    parcel.deliveryDetails.address.addressPoint.latitude,
                                    parcel.deliveryDetails.address.addressPoint.longitude);
                            final LatLng to = new LatLng(
                                    pickupLocation.addressPoint.latitude,
                                    pickupLocation.addressPoint.longitude);
                            // immediately show destination in case directions fail
                            //setMapLocation(to);
                            getDirections(from, to);
                            map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                                @Override
                                public void onMapClick(LatLng latLng) {
                                    getDirections(from, to);
                                }
                            });
                            break;
                        }
                    }
                    map.getUiSettings().setAllGesturesEnabled(false);
                }
            });

        } else {
            mapContainer.setVisibility(GONE);
            mapIconBar.setVisibility(GONE);
            directionsContainer.setVisibility(GONE);
        }
    }

    private boolean isAvailableToShowMap() {
        switch (parcel.parcelStatusType) {
            case DELIVERED:
                if (parcel.deliveredToConsumer) {
                    if (parcel.collectedFromShop &&
                            pickupLocation != null &&
                            pickupLocation.addressPoint != null) {
                        return true;
                    } else if (!parcel.collectedFromShop &&
                            parcel.deliveryDetails != null &&
                            parcel.deliveryDetails.address != null &&
                            parcel.deliveryDetails.address.addressPoint != null) {
                        return true;
                    }
                }
                break;
        }
        return false;
    }

    private void showParcelDeliveredOnMap(final GoogleMap map) {
        try {
            LatLng deliveredLatLng = null;
            if (parcel.deliveredToConsumer) {
                if (parcel.collectedFromShop) {
                    deliveredLatLng = new LatLng(
                            pickupLocation.addressPoint.latitude,
                            pickupLocation.addressPoint.longitude);
                } else {
                    deliveredLatLng = new LatLng(
                            parcel.deliveryDetails.address.addressPoint.latitude,
                            parcel.deliveryDetails.address.addressPoint.longitude);
                }
            }
            if (deliveredLatLng == null)
                return;

            final Resources resources = getResources();

            // zoom
            final LatLng houseLatLng = deliveredLatLng;
            map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                @Override
                public void onMapLoaded() {
                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(houseLatLng, 15.0f));
                }
            });

            // remove previous markers and polyline
            resetMap();

            // add map markers
            mapMarkers = new ArrayList<>();
            mapMarkers.add(
                    map.addMarker(new MarkerOptions()
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin_house))
                            .position(houseLatLng)
                            .alpha(1f)
                            .anchor(0.5f, 1.0f)
                            .title(resources.getString(R.string.your_address)))
            );
            mapIconBar.setVisibility(GONE);

            // Disable marker click listener
            map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    return true;
                }
            });

        } catch (Exception e) {
            Log.e(TAG, "Unable to display parcel on map", e);
        }
    }

    /**
     * For Out for Delivery we show the delivery address, driver location and if possible
     * user location
     */
    private void showParcelOutForDeliveryOnMap(final GoogleMap map) {
        try {
            final Resources resources = getResources();
            final LatLng driverLatLng = new LatLng(
                    routeView.latitude,
                    routeView.longitude);
            final LatLng houseLatLng = new LatLng(
                    parcel.deliveryDetails.address.addressPoint.latitude,
                    parcel.deliveryDetails.address.addressPoint.longitude);
            final LatLng userLatLng;
            GMapsUtils.ZoomCallback zoomCallback;

            if (userLocation != null) {
                userLatLng = new LatLng(userLocation.getLatitude(), userLocation.getLongitude());
            } else {
                userLatLng = null;
            }
            final float userDistance = userLocation != null ? GMapsUtils.getDistanceFromLocation(houseLatLng, userLocation) : Float.MAX_VALUE;
            final boolean isFarAwayFromHome = userDistance > 10.0f;

            // add map markers
            mapIconBar.setVisibility(View.VISIBLE);

            // remove previous markers and polyline
            resetMap();

            final Marker userLocationMarker;
            mapMarkers = new ArrayList<>();
            mapMarkers.add(
                    map.addMarker(new MarkerOptions()
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin_house))
                            .position(houseLatLng)
                            .alpha(1f)
                            .anchor(0.5f, 1.0f)
                            .title(resources.getString(R.string.your_address)))
            );
            mapMarkers.add(
                    map.addMarker(new MarkerOptions()
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin_driver))
                            .position(driverLatLng)
                            .alpha(1f)
                            .anchor(0.5f, 1.0f)
                            .title(resources.getString(R.string.driver)))
            );

            // load customer avatar image
            final View customerPinContainer = findViewById(R.id.customer_pin_container);
            final ImageView customerPin = (ImageView) findViewById(R.id.customer_pin);
            final ImageView customerPinImageView = (ImageView) findViewById(R.id.customer_pin_imageview);
            customerPinImageView.setVisibility(View.VISIBLE);
            zoomCallback = null;

            if (userLatLng == null) {
                customerPin.setImageResource(R.drawable.ic_pin_no_location);
                customerPinImageView.setVisibility(GONE);

                // Show red box.
                final TextView tooltipTextView = (TextView) customerPreferenceTooltip.findViewById(R.id.tooltip_textview);
                final ImageView tooltipArrow = (ImageView) customerPreferenceTooltip.findViewById(R.id.tooltip_arrow);
                tooltipTextView.setText(R.string.enable_location_settings_message);
                tooltipTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_warning_white, 0, R.drawable.arrow_white, 0);

                customerPreferenceTooltip.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        customerPreferenceTooltip.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        int test1[] = new int[2];
                        customerPin.getLocationOnScreen(test1);
                        int test2[] = new int[2];
                        customerPreferenceTooltip.getLocationOnScreen(test2);
                        tooltipArrow.setPadding(test1[0] - test2[0] - customerPreferenceTooltip.getWidth() / 2 + customerPin.getWidth() / 2, 0, 0, 0);
                    }
                });
                customerPreferenceTooltip.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        getContext().startActivity(intent);
                    }
                });
                customerPreferenceTooltip.setVisibility(View.VISIBLE);

            } else if (isFarAwayFromHome) {
                findViewById(R.id.customer_container).setVisibility(View.GONE);

            } else {
                customerPin.setImageResource(R.drawable.ic_pin);

                // render user photograph as circular Bitmap to add to map pin
                final String userPhotoBase64Data = user.getConsumerProfile().photograph;
                if (!TextUtils.isEmpty(userPhotoBase64Data)) {
                    final Bitmap photo = PhotoUtils.base64ToBitmap(userPhotoBase64Data);
                    final CircleImageTransform circleImageTransform = new CircleImageTransform();
                    final Bitmap circularPhoto = circleImageTransform.transform(photo);
                    customerPinImageView.setImageBitmap(circularPhoto);
                } else {
                    Log.w(TAG, "Problem loading user image / user image is null");
                    customerPinImageView.setImageResource(R.drawable.ic_avatar);
                }

                // snapshot pin View to grab a bitmap we can replace the user map marker
                customerPinContainer.buildDrawingCache(true);
                customerPinContainer.post(new Runnable() {
                    @Override
                    public void run() {
                        mapMarkers.add(
                                map.addMarker(
                                        new MarkerOptions()
                                                .icon(BitmapDescriptorFactory.fromBitmap(MapMarkerUtils.getBitmapForMarker(customerPinContainer)))
                                                .position(userLatLng)
                                                .alpha(0.9f)
                                                .anchor(0.5f, 1.0f)
                                                .title(resources.getString(R.string.your_location_titlecase))
                                ));
                    }
                });

                zoomCallback = new GMapsUtils.ZoomCallback() {
                    @Override
                    public void onMapReady(GoogleMap googleMap) {
                        if (mapMarkers == null || mapMarkers.size() < 2)
                            return;

                        Point housePoint = googleMap.getProjection().toScreenLocation(houseLatLng);
                        Point userPoint = googleMap.getProjection().toScreenLocation(userLatLng);
                        Marker houseMarker = mapMarkers.get(0);
                        Marker userLocationMarker = mapMarkers.get(2);

                        if (houseMarker != null && Math.abs(housePoint.x - userPoint.x) <= 25 && Math.abs(housePoint.y - userPoint.y) <= 25) {
                            if (housePoint.x > userPoint.x) {
                                houseMarker.setRotation(30);
                                userLocationMarker.setRotation(-30);
                            } else {
                                houseMarker.setRotation(-30);
                                userLocationMarker.setRotation(30);
                            }
                        }
                    }
                };
            }

            // zoom to bounds
            final GMapsUtils.ZoomCallback zoomCallback1 = zoomCallback;
            final LatLng[] latLngs = new LatLng[]{houseLatLng, driverLatLng, isFarAwayFromHome ? null : userLatLng};
            final BitmapFactory.Options bmpOptions = ImagePickerUtils.getDimensionFromResource(getContext(), R.drawable.ic_pin_house);
            map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                @Override
                public void onMapLoaded() {
                    GMapsUtils.zoomWithPadding(mapFragment, latLngs, 20, bmpOptions.outWidth, bmpOptions.outHeight, false, zoomCallback1);
                }
            });

            // Disable marker click listener
            map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    return true;
                }
            });

            // Show icon bar.
            mapIconBar.setVisibility(VISIBLE);

        } catch (Exception e) {
            Log.e(TAG, "Unable to display out for delivery parcel on map", e);
        }
    }

    private void setMapLocation(final GoogleMap map, final LatLng latLng) {
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 30));
    }

    // This function should be call after sync map.
    private void resetMap() {
        if (mapMarkers != null) {
            for (Marker mapMarker : mapMarkers)
                mapMarker.remove();
        }
        if (mapPolylines != null) {
            for (Polyline polyline : mapPolylines)
                polyline.remove();
        }
    }

    /**
     * Using Google Maps API, calculate the route/steps from delivery address to delivery depot or
     * pickup shop
     */
    private void getDirections(final LatLng from, final LatLng to) {
        final float originLat = (float) from.latitude;
        final float originLon = (float) from.longitude;
        final float destLat = (float) to.latitude;
        final float destLon = (float) to.longitude;

        new MapsRouteRequest().execute(originLat, originLon, destLat, destLon,
                new MapsRouteRequest.Callback() {
                    @Override
                    public void onResponse(final MapsRouteResponse response) {
                        post(new Runnable() {
                            @Override
                            public void run() {
                                if (getParent() == null) {
                                    return;
                                }

                                mapFragment.getMapAsync(new OnMapReadyCallback() {
                                    @Override
                                    public void onMapReady(GoogleMap map) {
                                        final Resources resources = getResources();

                                        // remove previous markers and polyline
                                        resetMap();

                                        // add start/end
                                        mapMarkers = new ArrayList<>();
                                        mapMarkers.add(
                                                map.addMarker(new MarkerOptions()
                                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin_start))
                                                        .position(response.start)
                                                        .alpha(1f)
                                                        .anchor(0.5f, 1.0f)
                                                        .title(resources.getString(R.string.your_address))));
                                        mapMarkers.add(
                                                map.addMarker(new MarkerOptions()
                                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin_end_dpd))
                                                        .position(response.end)
                                                        .alpha(1f)
                                                        .anchor(0.5f, 1.0f)
                                                        .title(parcel.getShipperName())));

                                        // plot route
                                        mapPolylines = new ArrayList<>();
                                        final PolylineOptions options = new PolylineOptions()
                                                .width(9)
                                                .color(getResources().getColor(R.color.map_route))
                                                .geodesic(true);
                                        for (int z = 0; z < response.points.size(); z++) {
                                            LatLng point = response.points.get(z);
                                            options.add(point);
                                        }
                                        mapPolylines.add(map.addPolyline(options));

                                        // zoom to bounds
                                        final LatLng[] latLngs = new LatLng[]{response.boundsLatLng.northeast, response.boundsLatLng.southwest, response.start, response.end};
                                        final BitmapFactory.Options bmpOptions = ImagePickerUtils.getDimensionFromResource(getContext(), R.drawable.ic_pin_end_dpd);
                                        map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                                            @Override
                                            public void onMapLoaded() {
                                                GMapsUtils.zoomWithPadding(mapFragment, latLngs, 20, bmpOptions.outWidth, bmpOptions.outHeight);
                                            }
                                        });

                                        // Disable marker click listener
                                        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                                            @Override
                                            public boolean onMarkerClick(Marker marker) {
                                                return true;
                                            }
                                        });

                                        // add summary
                                        TextView summaryTextView = (TextView) findViewById(R.id.directions_summary_textview);
                                        summaryTextView.setText(resources.getString(R.string.directions_summary, response.distance, response.duration));

                                        // add list of textual steps
                                        showDirections(response.steps);
                                    }
                                });
                            }
                        });
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "Problem loading route", e);
                    }
                }
        );
    }

    private void showDirections(JSONArray steps) {
        final TextView fromTextView = (TextView) directionsContainer.findViewById(R.id.from_address_textview);
        final TextView toTextView = (TextView) directionsContainer.findViewById(R.id.to_address_textview);
        final ViewGroup stepsContainer = (ViewGroup) directionsContainer.findViewById(R.id.directions_steps_container);
        final LayoutInflater inflater = LayoutInflater.from(getContext());

        // display from and to addresses
        try {
            SpannableStringBuilder builder = new SpannableStringBuilder();
            builder.append(getResources().getString(R.string.your_address));
            builder.setSpan(new StyleSpan(Typeface.BOLD), 0, builder.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            builder.append("\n").append(parcel.deliveryDetails.address.toFormattedString());
            fromTextView.setText(UIUtils.makesAddBoldText(parcel.deliveryDetails.address.toFormattedString(), getResources().getString(R.string.your_address)));
        } catch (Exception e) {
            Log.e(TAG, "Problem displaying from address", e);
        }

        try {
            if (parcel.parcelStatusType.equals(Parcel.ParcelStatusType.COLLECT_FROM_DEPOT)) {
                toTextView.setText(UIUtils.makesAddBoldText(depot.address.toFormattedString(), depot.address.organisation));
            } else if (parcel.parcelStatusType.equals(Parcel.ParcelStatusType.COLLECT_FROM_PICKUP)
                    || parcel.parcelStatusType.equals(Parcel.ParcelStatusType.COLLECT_FROM_PICKUP_WITH_PASS)) {
                toTextView.setText(UIUtils.makesAddBoldText(pickupLocation.address.toFormattedString(), pickupLocation.getName()));
            }
        } catch (Exception e) {
            Log.e(TAG, "Problem displaying to address", e);
        }

        // display direction steps
        try {
            int length = steps.length();
            for (int j = 0; j < length; j++) {
                final JSONObject step = steps.getJSONObject(j);
                final View stepView = inflater.inflate(R.layout.item_direction_step, stepsContainer, false);
                ((TextView) stepView.findViewById(R.id.direction_step_number_textview)).setText(String.valueOf(j + 1) + ".");
                ((TextView) stepView.findViewById(R.id.direction_distance_textview)).setText(step.getJSONObject("distance").getString("text"));
                ((TextView) stepView.findViewById(R.id.direction_description_textview)).setText(com.dpd.yourdpd.util.TextUtils.trimTrailingWhitespace(Html.fromHtml(step.getString("html_instructions"))));
                stepsContainer.addView(stepView);
            }
        } catch (JSONException e) {
            Log.e(TAG, "Problem processing steps", e);
        }

        directionsContainer.setVisibility(VISIBLE);
    }

    private void toggleExpandDirections() {
        final ViewGroup directions =
                (ViewGroup) findViewById(R.id.directions_steps_container);
        final boolean isVisible = directions.getVisibility() == View.VISIBLE;
        expandDirectionsButton.setCompoundDrawablesWithIntrinsicBounds(
                0, 0, isVisible ? R.drawable.ic_plus : R.drawable.ic_minus, 0);
        directions.setVisibility(isVisible ? View.GONE : View.VISIBLE);
    }

    private void toggleExpandLocation() {
        TransitionManager.beginDelayedTransition((ViewGroup) getParent());
        final boolean isVisible = locationDescriptionContainer.getVisibility() == View.VISIBLE;
        locationDescriptionContainer.setVisibility(isVisible ? View.GONE : View.VISIBLE);
        locationExpandButton.setCompoundDrawablesWithIntrinsicBounds(
                0, 0, isVisible ? R.drawable.ic_plus : R.drawable.ic_minus, 0);
    }

    /**
     * Allows us to optionally display the user's physical location on the map when adding markers
     *
     * @param location Location
     */
    public void setUserLocation(Location location) {
        userLocation = location;
    }
}
