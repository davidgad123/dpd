package com.dpd.yourdpd.ui.yourdeliveries;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.common.Constants;
import com.dpd.yourdpd.BuildConfig;
import com.dpd.yourdpd.R;
import com.dpd.yourdpd.ui.BaseMenuActivity;
import com.dpd.yourdpd.ui.WelcomeActivity;
import com.dpd.yourdpd.util.DebugUtils;
import com.util.WearableUtil;

public class YourDeliveriesActivity extends BaseMenuActivity {
    YourDeliveriesActivityFragment fragment;
    private WearableUtil wearableUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_with_menu_fragment);

        setupToolbar(getString(R.string.your_deliveries_titlecase), false);
        setSelectedMenuButton(R.id.your_deliveries_button);

        if (BuildConfig.DEBUG) {
            toolbar.findViewById(R.id.logo).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DebugUtils.showDebugMenu(YourDeliveriesActivity.this);
                }
            });
        }

        if (fragment == null) {
            final FragmentManager fragmentManager = getSupportFragmentManager();
            fragment = YourDeliveriesActivityFragment.newInstance();

            final FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.container, fragment, YourDeliveriesActivityFragment.class.getSimpleName());
            transaction.commit();
        }

        // If location push functions are to be utilised, then need to ask the user's permission to ACCESS_FINE_LOCATION
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                final AlertDialog.Builder builder =
                        new AlertDialog.Builder(this, R.style.AlertDialogTheme);
                builder.setMessage(getString(R.string.location_permission_message));
                builder.setPositiveButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(YourDeliveriesActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_ACCESS_FINE_LOCATION);
                            }
                        });
                builder.setNegativeButton(R.string.cancel, null);
                builder.create().show();

            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_ACCESS_FINE_LOCATION);
            }
        }

        // Show dialog of parcel.
        String parcelCode = getIntent().getStringExtra("parcelCode");
        if (parcelCode != null)
            showParcelNotFoundDialog(parcelCode);

        // Show welcome screen.
        final boolean isShowWelcome = getIntent().getBooleanExtra("showWelcome", false);
        if (isShowWelcome) {
            Intent intent = new Intent(this, WelcomeActivity.class);
            startActivity(intent);
        }

        wearableUtil = new WearableUtil(getApplicationContext());
        wearableUtil.startConnection();
        wearableUtil.setWearableConnectionListener(new WearableUtil.WearableConnectListener() {
            @Override
            public void connectionSuccess() {
                if (isShowWelcome) {
                    wearableUtil.sendRequest(Constants.REQUEST_REFRESH);
                }
            }

            @Override
            public void connectionFailed() {
            }
        });
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        String parcelCode = intent.getStringExtra("parcelCode");
        if (parcelCode != null)
            showParcelNotFoundDialog(parcelCode);
    }

    private void showParcelNotFoundDialog(String parcelCode) {
        showErrorDialog(getString(R.string.parcel_not_found_message, parcelCode), getString(R.string.parcel_not_found));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!user.hasAuthenticatedSession()) {
            finish();
        }
        user.incAppShowCount();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_your_deliveries, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a listener activity in AndroidManifest.xml.
        return super.onOptionsItemSelected(item);
    }
}
