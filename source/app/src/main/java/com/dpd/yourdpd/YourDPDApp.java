package com.dpd.yourdpd;

import android.app.Application;
import android.support.multidex.MultiDex;

import com.crashlytics.android.Crashlytics;
import com.dpd.yourdpd.analytics.AnalyticsManager;
import com.dpd.yourdpd.analytics.AnalyticsModule;
import com.dpd.yourdpd.datastore.DataStoreModule;
import com.dpd.yourdpd.net.ApiModule;

import javax.inject.Inject;

import io.fabric.sdk.android.Fabric;


public class YourDPDApp extends Application {
    private static YourDPDApp app;

    @Inject AnalyticsManager analyticsManager;

    // Application/Singleton scoped components
    private AppComponent appComponent;

    public static AppComponent getAppComponent() {
        return app.appComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
        if (!BuildConfig.DEBUG) {
            Crashlytics crashlytics = new Crashlytics();
            Fabric.with(this, crashlytics);
        }

        app = this;

        // setup application-scoped graph
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .userModule(new UserModule(this))
                .dataStoreModule(new DataStoreModule(this))
                .analyticsModule(new AnalyticsModule())
                .apiModule(new ApiModule(this)) // TODO: look into the correct way to inject dependencies amongst modules
                .build();
        appComponent.inject(this);

        analyticsManager.trackEvent("General", "App Launched", null);
    }
}
