package com.dpd.yourdpd.ui;

/**
 * Interface for Fragments that require validation and can apply changes to given data
 */
public interface Validating {
    /**
     * Validate inputs and show any relevant errors.
     * @return true if all inputs validate, else false
     */
    boolean validate();

    /**
     * Apply any changes / user input to data model
     */
    void applyChanges();
}
