package com.dpd.yourdpd.ui;

import android.os.Bundle;
import android.text.Html;
import android.text.util.Linkify;
import android.view.View;
import android.widget.TextView;

import com.dpd.yourdpd.R;
import com.google.android.gms.common.GoogleApiAvailability;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;

/**
 * Display licenses and other acknowledgements
 */
public class AboutActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        String openSourceSoftwareLicenseInfo = GoogleApiAvailability.getInstance().getOpenSourceSoftwareLicenseInfo(this);

        if (openSourceSoftwareLicenseInfo != null) {
            StringReader sr = new StringReader(openSourceSoftwareLicenseInfo);
            BufferedReader br = new BufferedReader(sr);

            String line;
            StringBuilder sb = new StringBuilder();

            try {
                line = br.readLine();

                while(line != null) {

                    if (line.startsWith(" */")) {
                        line = line.substring(3, line.length());
                    } else if (line.startsWith("/*") || line.startsWith(" *")) {
                        line = line.substring(2, line.length());
                    }

                    String trimmed = line.trim();

                    if (trimmed.matches("^[-]+") || trimmed.matches("^[=]+")) {
                        trimmed = "";
                    }

                    if (trimmed.equals("")) {
                        sb.append("\n");
                    } else if (trimmed.endsWith(".")) {
                        sb.append(trimmed).append("\n");
                    } else {
                        sb.append(trimmed).append(" ");
                    }
                    line = br.readLine();
                }

                openSourceSoftwareLicenseInfo = sb.toString();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            openSourceSoftwareLicenseInfo = "";
        }


        TextView textView = (TextView) findViewById(R.id.about_textview);
        textView.setText(Html.fromHtml(getString(R.string.about_text)) + openSourceSoftwareLicenseInfo);
        Linkify.addLinks(textView, Linkify.ALL);
//        textView.setMovementMethod(LinkMovementMethod.getInstance());

        findViewById(R.id.back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
