package com.dpd.yourdpd.net;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;

import retrofit.client.Client;
import retrofit.client.OkClient;
import retrofit.client.Request;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

class MockClient implements Client {
    private static final String TAG = MockClient.class.getSimpleName();
    private OkClient client;
    private Context context;

    public MockClient(Context context, OkClient okClient) {
        this.context = context;
        this.client = okClient;
    }

    @Override
    public Response execute(Request request) throws IOException {
        final Uri uri = Uri.parse(request.getUrl());

        Log.d(TAG, "Requested URI: " + uri.toString());

//        if (request.getMethod().equals("GET") && uri.toString().contains("/home")) {
//            // force specific match status
//            request = new Request(request.getMethod(),
//                    uri.toString(),
//                    request.getHeaders(), request.getBody());
//        }

        Log.d(TAG, "Fetching URI: " + request.getUrl());

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

//      Response response = client.execute(request);
        String filename = null;
        if (request.getUrl().contains("action=login")) {
            filename = "login.json";

        } else if (request.getUrl().contains("consumer/pin/?action=Generate")) {
            filename = "success.json";
//            filename = "error_general.json";

        } else if (request.getUrl().contains("consumer/pin/?action=Authenticate")) {
            filename = "authenticatePin.json";
//            filename = "error_general.json";

        } else if (request.getUrl().contains("filter=parcelHistory")) {
            filename = "parcelHistory.json";
//            filename = "parcelHistory_test1.json";

        } else if (request.getUrl().contains("filter=content")) {
            filename = "getContent.json";

        } else if (request.getUrl().contains("/consumerapp/?action=startup")) {
            filename = "mobileStartupChecks.json";

        } else if (request.getUrl().contains("/consumer/")) {
            switch (request.getMethod()) {
                case "GET":
                    filename = "getConsumer.json";
//                    filename = "getConsumer_Null.json";
                    break;
                case "PUT":
                case "DELETE":
                    filename = "success.json";
                    break;
            }

        } else if (request.getUrl().contains("/address/?filter=udprn")) {
            filename = "getAddressByUdprn.json";

        } else if (request.getUrl().contains("/reference/address/?filter=depotDetails")) {
            filename = "checkProperty.json";

        } else if (request.getUrl().contains("shipping/shipment/_/parcel/?parcelCode=")) {
            if (request.getUrl().contains("15501564661572*17560"))
                filename = "parcelDetails_0_unknown.json";
            else if (request.getUrl().contains("15501564661572*17562"))
                filename = "parcelDetails_2_delivered.json";
            else if (request.getUrl().contains("15501564661572*17563"))
                filename = "parcelDetails_3_collect_from_depot.json";
            else if (request.getUrl().contains("15501564661572*17564"))
                filename = "parcelDetails_4_collect_from_pickup.json";
            else if (request.getUrl().contains("15501564661572*17565"))
                filename = "parcelDetails_5_collect_from_pickup_pass.json";
            else if (request.getUrl().contains("15501564661572*17566"))
                filename = "parcelDetails_6_out_for_delivery.json";
            else if (request.getUrl().contains("15501564661572*17568"))
                filename = "parcelDetails_8_show_delivery_date.json";

//            filename = "parcelDetails_6_out_for_delivery_2.json";
//            filename = "parcelDetails_5_collect_from_pickup_pass.json";
//            filename = "parcelDetails_2_delivered.json";
//            filename = "parcelDetails__error.json";

        } else if (request.getUrl().contains("action=GetParcelActions")) {
            filename = "getParcelActions.json";

        } else if (request.getUrl().contains("/event/")) {
            filename = "getParcelEvents.json";

        } else if (request.getUrl().contains("?view=onRoute")) {
            filename = "getRouteViewByOnRoute.json";
//            filename = "getRouteViewByOnRoute_804.json";

        } else if (request.getUrl().contains("/organisation/pickuplocation/?filter=parcel")) {
            filename = "getPickupLocationParcel.json";

        } else if (request.getUrl().contains("/organisation/pickuplocation/?filter=nearAddress")) {
            filename = "getPickupLocationNearAddress.json";

        } else if (request.getUrl().contains("/organisation/pickuplocation/GB")) {
            filename = "getPickupLocationGB.json";

        } else if (request.getUrl().contains("action=GetParcelUpdateDates")) {
            filename = "parcelGetUpdateDates.json";
//            filename = "parcelGetUpdateDatesEmpty.json";

        } else if (request.getUrl().contains("action=ChangeDate")) {
            filename = "parcelChangeDate.json";

        } else if (request.getUrl().contains("/delivery/?filter=CollectParcelTimes")) {
            filename = "getCollectParcelTimes.json";

        } else if (request.getUrl().contains("action=CollectFromDepot")) {
            filename = "parcelCollectFromDepot.json";

        } else if (request.getUrl().contains("action=CollectFromPickupLocation")) {
            filename = "parcelCollectFromPickupLocation.json";

        } else if (request.getUrl().contains("action=DeliverToNeighbour")) {
            filename = "parcelDeliverToNeighbour.json";

        } else if (request.getUrl().contains("action=LeaveInSafePlace")) {
            filename = "parcelLeaveInSafePlace.json";

        } else if (request.getUrl().contains("reference/address/")) { // note trailing slash
            filename = "lookupPostKey.json";

        } else if (request.getUrl().contains("reference/address?")) {
            if (request.getUrl().contains("IV24RX"))
                filename = "empty.json";
            else if (request.getUrl().contains("DY138ET"))
                filename = "searchByAddress_DY138ET.json";
            else
                filename = "searchByAddress.json";

        } else if (request.getUrl().contains("reference/safelocation")) {
            filename = "getSafeLocations.json";

        } else if (request.getUrl().contains("AlternativeDeliveryTimes")) {
            filename = "getAlternativeDeliveryTimes.json";

        } else if (request.getUrl().contains("/depot/")) {
            filename = "getDepot.json";

        } else if (request.getUrl().contains("/image/pickuppass")) {
            filename = "getPickupPass.json";

        } else if (request.getUrl().contains("/image/")) {
            filename = "getImage.json";
//            filename = "getImage_test1.json";
        }

        InputStream is = context.getAssets().open(filename);
        int size = is.available();
        byte[] buffer = new byte[size];
        int read = is.read(buffer);
        is.close();
        String responseString = new String(buffer);
        return new Response(request.getUrl(),
                200,
                "OK",
                Collections.EMPTY_LIST,
                new TypedByteArray("application/json", responseString.getBytes())
        );
    }
}