package com.dpd.yourdpd.datastore;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.dpd.yourdpd.model.Parcel;
import com.dpd.yourdpd.model.ParcelHistory;
import com.dpd.yourdpd.model.SafeLocation;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * In memory and persistent local data storage
 * TODO: enhancement: split these out into different models for ParcelHistory, SafeLocations and SiteContent, each with their own DaggerModule
 */
public class DataStore {
    private static final String TAG = DataStore.class.getSimpleName();

    // data filenames
    private static final String PARCEL_HISTORY_FILENAME = "parcel_events.dat";
    private static final String SAFE_LOCATIONS_FILENAME = "safe_locations.dat";
    private static final String SITE_CONTENT_FILENAME = "site_content.dat";

    // SharedPreference keys
    private static final String PREF_SITE_CONTENT_CHECKSUM = "site_content_checksum";

    private Context context;
    private Gson gson;
    private SharedPreferences prefs;

    private ParcelHistory parcelHistory;
    private List<SafeLocation> safeLocations;

    /**
     * Holds the server-generated checksum used to compare if there is new SiteContent available
     */
    private String siteContentChecksum;
    private SiteContent siteContent;


    public DataStore(Context context) {
        final GsonBuilder gsonBuilder = new GsonBuilder();

        this.context = context.getApplicationContext();
        this.prefs = PreferenceManager.getDefaultSharedPreferences(context);
        this.gson = gsonBuilder.create();

        // restore previously saved data
        loadSafeLocations();
        loadParcelHistory();
        loadSiteContentChecksum();
        loadSiteContent();
    }

    public void setParcelHistory(ParcelHistory parcelHistory) {
        this.parcelHistory = parcelHistory;
        saveParcelHistory();
    }

    private void loadParcelHistory() {
        parcelHistory = (ParcelHistory) readJsonFromDisk(PARCEL_HISTORY_FILENAME, ParcelHistory.class);
    }

    private void saveParcelHistory() {
        writeJsonToDisk(PARCEL_HISTORY_FILENAME, parcelHistory);
    }

    public ParcelHistory getParcelHistory() {
        return parcelHistory;
    }

    public Parcel getParcelWithParcelCode(String parcelCode) {
        if (parcelHistory == null) {
            return null;
        } else {
            return parcelHistory.getParcelWithParcelCode(parcelCode);
        }
    }

    public List<SafeLocation> getSafeLocations() {
        return this.safeLocations;
    }

    public void setSafeLocations(List<SafeLocation> safeLocations) {
        this.safeLocations = safeLocations;
        saveSafeLocations();
    }

    private void saveSafeLocations() {
        writeJsonToDisk(SAFE_LOCATIONS_FILENAME, safeLocations);
    }

    private void loadSafeLocations() {
        Type listType = new TypeToken<ArrayList<SafeLocation>>() {}.getType();
        safeLocations = (List<SafeLocation>) readJsonFromDisk(SAFE_LOCATIONS_FILENAME, listType);
    }

    private void loadSiteContent() {
        siteContent = (SiteContent) readJsonFromDisk(SITE_CONTENT_FILENAME, SiteContent.class);
    }

    public void saveSiteContent() {
        writeJsonToDisk(SITE_CONTENT_FILENAME, siteContent);
    }

    public String getSiteContentChecksum() {
        return siteContentChecksum;
    }

    public void setSiteContentChecksum(String siteContentChecksum) {
        this.siteContentChecksum = siteContentChecksum;
        prefs.edit().putString(PREF_SITE_CONTENT_CHECKSUM, siteContentChecksum).apply();
    }

    private void loadSiteContentChecksum() {
        this.siteContentChecksum = prefs.getString(PREF_SITE_CONTENT_CHECKSUM, null);
    }

    public SiteContent getSiteContent() {
        if (siteContent == null) {
            siteContent = new SiteContent();
        }
        return siteContent;
    }

    /**
     * Read and parse JSON into target type
     *
     * @param filename filename within app files dir
     * @param clazz    Target class type
     * @return Typed object if successful, else null
     */
    private Object readJsonFromDisk(String filename, Type clazz) {
        final String filePath =
                context.getFilesDir().getAbsolutePath() + "/" + filename;
        String json = null;
        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader bufferedReader = null;
        try {
            FileReader fileReader = new FileReader(filePath);
            bufferedReader = new BufferedReader(fileReader);
            String receiveString;

            while ((receiveString = bufferedReader.readLine()) != null) {
                stringBuilder.append(receiveString);
            }
            json = stringBuilder.toString();

        } catch (FileNotFoundException e) {
            Log.w(TAG, "Unable to read " + filename + " from disk, file not found");
        } catch (IOException ioe) {
            Log.e(TAG, "Unable to read " + filename + " from disk, IOException", ioe);
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        if (json != null) {
            final JsonParser parser = new JsonParser();
            final JsonElement jsonElement = parser.parse(json);
            try {
                return gson.fromJson(jsonElement, clazz);
            } catch (JsonSyntaxException e) {
                Log.e(TAG, "Unable to parse " + filename + " json", e);
            }
        }
        return null;
    }

    /**
     * Attempts to write target object to disk as JSON file
     *
     * @param filename filename within app files dir
     * @param object   Target object to serialize to JSON
     */
    private void writeJsonToDisk(String filename, Object object) {
        if (object == null) {
            Log.w(TAG, "Unable to " + filename + " to disk, object is null");
            return;
        }

        final String json = gson.toJson(object);
        FileWriter writer = null;
        try {
            final String filePath = context.getFilesDir().getAbsolutePath() + "/" + filename;
            writer = new FileWriter(filePath);
            writer.write(json);
        } catch (IOException e) {
            Log.e(TAG, "Cannot write " + filename + " to disk", e);
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void clear() {
        prefs.edit().clear().apply();
        parcelHistory = null;
        final String[] filenames = { PARCEL_HISTORY_FILENAME, SAFE_LOCATIONS_FILENAME, SITE_CONTENT_FILENAME };
        for (String filename : filenames) {
            final String filePath =
                    context.getFilesDir().getAbsolutePath() + "/" + filename;
            File file = new File(filePath);
            if (file.exists()) {
                try {
                    file.delete();
                } catch (Exception e) {
                    Log.e(TAG, "Problem deleting file", e);
                }
            }
        }
    }
}
