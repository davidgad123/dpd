package com.dpd.yourdpd.net.response;

/**
 * @see com.dpd.yourdpd.net.Api#authenticatePin(AuthenticatePinParams)
 */
public class AuthenticatePinResponse extends BaseResponse {

    public AuthPinData data;

    public class AuthPinData {
        public String consumerId;
        public boolean deviceConflict;
    }
}