package com.dpd.yourdpd.ui.yourdeliveries;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.transitions.everywhere.AutoTransition;
import android.transitions.everywhere.TransitionManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import com.dpd.yourdpd.R;
import com.dpd.yourdpd.model.DateAndTimes;
import com.dpd.yourdpd.model.Depot;
import com.dpd.yourdpd.model.Parcel;
import com.dpd.yourdpd.model.ParcelActionCodes;
import com.dpd.yourdpd.net.request.CollectFromDepotParams;
import com.dpd.yourdpd.net.response.CollectFromDepotResponse;
import com.dpd.yourdpd.net.response.GetCollectParcelTimesResponse;
import com.dpd.yourdpd.net.response.GetCollectParcelTimesResponse.TimePair;
import com.dpd.yourdpd.net.response.GetDepotResponse;
import com.dpd.yourdpd.net.response.GetParcelUpdateDatesResponse;
import com.dpd.yourdpd.ui.BaseFragment;
import com.dpd.yourdpd.ui.BaseMenuActivity;
import com.dpd.yourdpd.util.CommonUtils;
import com.dpd.yourdpd.util.DateUtils;
import com.dpd.yourdpd.util.GMapsUtils;
import com.dpd.yourdpd.util.ImagePickerUtils;
import com.dpd.yourdpd.util.UIUtils;
import com.dpd.yourdpd.util.ViewIdGenerator;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.functions.Func2;
import rx.schedulers.Schedulers;


/**
 * Activities that contain this fragment must implement the
 * {@link InFlightCollectFromDepotFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link InFlightCollectFromDepotFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class InFlightCollectFromDepotFragment extends BaseFragment
        implements OnMapReadyCallback {
    private static final String TAG = InFlightCollectFromDepotFragment.class.getSimpleName();

    // the fragment initialization parameters
    private static final String ARG_PARCEL_CODE = "parcel_code";

    private Parcel parcel;
    private Depot depot;

    private OnFragmentInteractionListener listener;

    private ScrollView scrollView;
    private ContentLoadingProgressBar progressBar;
    private ViewGroup datesContainer;
    private ViewGroup timeSlotsContainer;
    private RadioGroup deliveryTimesRadioGroup;
    private RadioGroup deliveryDatesRadioGroup;
    private Button expandDirectionsButton;
    private Button confirmChangesButton;
    private SupportMapFragment mapFragment;
    private Call mapsAPICall;

    private Subscription getCollectionDatesSubscription;
    private Subscription getDepotSubscription;
    private Subscription collectFromDepotSubscription;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param parcelCode Parcel.parcelCode
     * @return A new instance of fragment InFlightCollectFromDepotFragment.
     */
    public static InFlightCollectFromDepotFragment newInstance(String parcelCode) {
        InFlightCollectFromDepotFragment fragment = new InFlightCollectFromDepotFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARCEL_CODE, parcelCode);
        fragment.setArguments(args);
        return fragment;
    }

    public InFlightCollectFromDepotFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            String parcelCode = getArguments().getString(ARG_PARCEL_CODE);
            parcel = dataStore.getParcelWithParcelCode(parcelCode);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_inflight_collect_from_depot, container, false);
        scrollView = (ScrollView) view.findViewById(R.id.scrollView);
        progressBar = (ContentLoadingProgressBar) view.findViewById(R.id.progressBar);
        datesContainer = (ViewGroup) view.findViewById(R.id.dates_container);
        timeSlotsContainer = (ViewGroup) view.findViewById(R.id.time_slots_container);
        deliveryDatesRadioGroup = (RadioGroup) view.findViewById(R.id.delivery_dates_radiogroup);
        deliveryDatesRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId != -1) {
                    final RadioButton radio = (RadioButton) deliveryDatesRadioGroup.findViewById(checkedId);
                    final DateAndTimes dateAndTimes = (DateAndTimes) radio.getTag();
                    showTimeOptions(dateAndTimes.timePairs);
                }
            }
        });
        deliveryTimesRadioGroup = (RadioGroup) view.findViewById(R.id.delivery_times_radiogroup);
        deliveryTimesRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                confirmChangesButton.setEnabled(true);
            }
        });
        confirmChangesButton = (Button) view.findViewById(R.id.confirm_changes_button);
        confirmChangesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmChanges();
            }
        });
        confirmChangesButton.setEnabled(false);
        expandDirectionsButton = (Button) view.findViewById(R.id.expand_directions_button);
        expandDirectionsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expandDirections();
            }
        });
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initParcelStatusView();
        getCollectionDates();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (OnFragmentInteractionListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ((BaseMenuActivity) getActivity()).setupToolbar(getString(R.string.collect_from_depot), true);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (mapsAPICall != null) {
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    mapsAPICall.cancel();
                    mapsAPICall = null;
                    return null;
                }
            }.execute();
        }
        listener = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getCollectionDatesSubscription != null && !getCollectionDatesSubscription.isUnsubscribed()) {
            getCollectionDatesSubscription.unsubscribe();
            getCollectionDatesSubscription = null;
        }
        if (getDepotSubscription != null && !getDepotSubscription.isUnsubscribed()) {
            getDepotSubscription.unsubscribe();
            getDepotSubscription = null;
        }
        if (collectFromDepotSubscription != null && !collectFromDepotSubscription.isUnsubscribed()) {
            collectFromDepotSubscription.unsubscribe();
            collectFromDepotSubscription = null;
        }
    }

    private void getCollectionDates() {
        showProgressBar();
        getCollectionDatesSubscription =
                api.getParcelUpdateDates(
                        parcel.parcelCode,
                        ParcelActionCodes.COLLECT_FROM_DEPOT.getValue())
                        .subscribeOn(Schedulers.io())
                        .flatMapIterable(new Func1<GetParcelUpdateDatesResponse, Iterable<String>>() {
                            @Override
                            public Iterable<String> call(GetParcelUpdateDatesResponse response) {
                                if (response.hasErrors()) {
                                    Log.e(TAG, String.format("Unable to get parcel update dates %s",
                                            response.printErrors()));
                                }
                                return response.getDiaryDates();
                            }
                        })
                        .flatMap(new Func1<String, Observable<DateAndTimes>>() {
                            @Override
                            public Observable<DateAndTimes> call(String diaryDate) {
                                if (diaryDate == null) {
                                    return Observable.error(new Exception("null diaryDate"));
                                }
                                final String depotCode = parcel.deliveryDepot.depotCode;
                                // combine diaryDate with parcel times in return Observable
                                return Observable.zip(
                                        Observable.just(diaryDate),
                                        api.getCollectParcelTimes(depotCode, diaryDate),
                                        new Func2<String, GetCollectParcelTimesResponse, DateAndTimes>() {
                                            @Override
                                            public DateAndTimes call(String diaryDate, GetCollectParcelTimesResponse response) {
                                                if (response.hasErrors()) {
                                                    Log.e(TAG, "Error getting collection dates: " + response.printErrors());
                                                    return null;
                                                }
                                                final DateAndTimes dateAndTimes = new DateAndTimes();
                                                dateAndTimes.diaryDate = diaryDate;
                                                dateAndTimes.timePairs = response.getTimes();
                                                return dateAndTimes;
                                            }
                                        });
                            }
                        })
                        .toList()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Action1<List<DateAndTimes>>() {
                                       @Override
                                       public void call(List<DateAndTimes> dateAndTimes) {
                                           hideProgressBar();
                                           if (dateAndTimes == null) {
                                               showErrorAlertDialog(getString(R.string.problem_getting_collection_dates));
                                               return;
                                           }
                                           setCollectionDateAndTimes(dateAndTimes);
                                           initMap();
                                       }
                                   },
                                new Action1<Throwable>() {
                                    @Override
                                    public void call(Throwable throwable) {
                                        hideProgressBar();
                                        Log.e(TAG, "Error getting collection dates/times", throwable);
                                        showErrorAlertDialog(getString(R.string.problem_getting_collection_dates));
                                    }
                                });
    }

    private void getDepot() {
        getDepotSubscription = api.getDepot(parcel.deliveryDepot.depotCode)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<GetDepotResponse>() {
                               @Override
                               public void call(GetDepotResponse response) {
                                   if (response.hasErrors()) {
                                       Log.e(TAG, String.format("Get depot error %s", response.printErrors()));
                                       showErrorAlertDialog(getString(R.string.problem_getting_depot_directions));
                                       return;
                                   }
                                   setDepot(response.getDepot());
                               }
                           },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                showErrorAlertDialog(getString(R.string.problem_getting_depot_directions));
                            }
                        });
    }

    private void setDepot(Depot depot) {
        this.depot = depot;

        if (depot != null) {
            View view = getView();
            assert view != null;

            // add start and destination info
            final ViewGroup markerDescriptionContainer = (ViewGroup) view.findViewById(R.id.marker_description_container);
            final TextView fromTextView = (TextView) markerDescriptionContainer.findViewById(R.id.from_address_textview);
            final TextView toTextView = (TextView) markerDescriptionContainer.findViewById(R.id.to_address_textview);

            // display from and to addresses
            try {
                SpannableStringBuilder builder = new SpannableStringBuilder();
                builder.append(getResources().getString(R.string.your_address));
                builder.setSpan(new StyleSpan(Typeface.BOLD), 0, builder.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                builder.append("\n").append(parcel.deliveryDetails.address.toFormattedString());
                fromTextView.setText(UIUtils.makesAddBoldText(parcel.deliveryDetails.address.toFormattedString(), getResources().getString(R.string.your_address)));
            } catch (Exception e) {
                Log.e(TAG, "Problem displaying from address", e);
            }

            try {
                toTextView.setText(UIUtils.makesAddBoldText(depot.address.toFormattedString(), depot.address.organisation));
            } catch (Exception e) {
                Log.e(TAG, "Problem displaying to address", e);
            }
            markerDescriptionContainer.setVisibility(View.VISIBLE);

            // zoom in on depot
            final LatLng location = new LatLng(depot.addressPoint.latitude, depot.addressPoint.longitude);
            mapFragment.getMap().setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                @Override
                public void onMapLoaded() {
                    mapFragment.getMap().moveCamera(CameraUpdateFactory.newLatLngZoom(location, 100));
                }
            });
            mapFragment.getMap().getUiSettings().setAllGesturesEnabled(false);

            // load a route to depot via Directions API
            getDirectionsToDepot();
        }
    }

    private void collectFromDepot(String diaryDate, String time) {
        showProgressBar();
        confirmChangesButton.setEnabled(false);

        final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm", Locale.UK);
        final Date date;
        try {
            date = format.parse(diaryDate + " " + time);
        } catch (ParseException e) {
            Log.e(TAG, "Could not parse diaryDate/time", e);
            return;
        }

        final CollectFromDepotParams params = new CollectFromDepotParams.Builder()
                .setParcelCode(parcel.parcelCode)
                .setDiaryDate(date)
                .setEmail(user.getConsumerProfile().email)
                .setMobile(CommonUtils.addPrefixNumber(user.getConsumerProfile().mobile))
                .setContactName(user.getFullName())
                .build();

        collectFromDepotSubscription = api.collectFromDepot(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<CollectFromDepotResponse>() {
                               @Override
                               public void call(CollectFromDepotResponse response) {
                                   hideProgressBar();
                                   confirmChangesButton.setEnabled(true);
                                   if (response.hasErrors()) {
                                       showErrorAlertDialog(getString(R.string.parcel_status_not_changed), getString(R.string.error_occurred));
                                       return;
                                   }
                                   showActionConfirmedDialog();
                               }
                           },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                hideProgressBar();
                                confirmChangesButton.setEnabled(true);
                                showErrorAlertDialog(getString(R.string.parcel_status_not_changed), getString(R.string.error_occurred));
                            }
                        });
    }

    private void initParcelStatusView() {
        final View view = getView();
        assert view != null;
        final Context context = getContext();
        final View parcelStatusView = view.findViewById(R.id.parcel_status);
        UIUtils.populateParcelStatus(parcelStatusView, context, parcel);
    }

    public void setCollectionDateAndTimes(List<DateAndTimes> datesAndTimes) {
        hideProgressBar();
        if (datesAndTimes != null && datesAndTimes.size() > 0) {
            showDateOptions(datesAndTimes);
        } else {
            hideDateAndTimeOptions();
        }
    }

    private void showProgressBar() {
        progressBar.show();
        scrollView.setVisibility(View.GONE);
    }

    private void hideProgressBar() {
        progressBar.hide();
        scrollView.setVisibility(View.VISIBLE);
    }

    private void hideDateAndTimeOptions() {
        datesContainer.setVisibility(View.GONE);
        timeSlotsContainer.setVisibility(View.GONE);
        confirmChangesButton.setEnabled(true);
    }

    private void showDateOptions(List<DateAndTimes> dateAndTimesList) {
        final View view = getView();
        assert view != null;
        final LayoutInflater inflater = LayoutInflater.from(getActivity());
        final SimpleDateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd", Locale.UK);

        if (dateAndTimesList != null) {
            Collections.sort(dateAndTimesList, new Comparator<DateAndTimes>() {
                @Override
                public int compare(DateAndTimes lhs, DateAndTimes rhs) {
                    if (lhs == null || lhs.diaryDate == null || rhs == null || rhs.diaryDate == null)
                        return 0;
                    return lhs.diaryDate.compareToIgnoreCase(rhs.diaryDate);
                }
            });
        }

        datesContainer.setVisibility(View.VISIBLE);
        deliveryDatesRadioGroup.removeAllViews();

        if (dateAndTimesList != null) {
            for (DateAndTimes dateAndTimes : dateAndTimesList) {
                try {
                    final Date date = dateParser.parse(dateAndTimes.diaryDate);
                    final Calendar cal = Calendar.getInstance();
                    cal.setTime(date);
                    final String dateSuffix = DateUtils.getDayOfMonthSuffix(cal.get(Calendar.DATE));
                    final SimpleDateFormat dateFormat = new SimpleDateFormat(String.format("cccc d'%s' MMMM", dateSuffix), Locale.UK);
                    final RadioButton radioButton = (RadioButton) inflater.inflate(R.layout.item_radiobutton, deliveryDatesRadioGroup, false);
                    radioButton.setId(ViewIdGenerator.generateViewId());
                    radioButton.setText(dateFormat.format(date));
                    radioButton.setTag(dateAndTimes);
                    deliveryDatesRadioGroup.addView(radioButton);
                } catch (ParseException e) {
                    Log.e(TAG, "Exception parsing diaryDates for UI display", e);
                }
            }
        }

        deliveryDatesRadioGroup.clearCheck();
        if (deliveryDatesRadioGroup.getChildCount() > 0) {
            deliveryDatesRadioGroup.check(deliveryDatesRadioGroup.getChildAt(0).getId());
        }
    }

    @SuppressLint("SetTextI18n")
    private void showTimeOptions(final List<TimePair> times) {
        final View view = getView();
        assert view != null;
        final LayoutInflater inflater = LayoutInflater.from(getActivity());
        final SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm", Locale.UK);

        timeSlotsContainer.setVisibility(View.VISIBLE);
        deliveryTimesRadioGroup.removeAllViews();

        for (TimePair timePair : times) {
            Date startTime = DateUtils.getDate(timePair.startTime, "HH:mm");
            Date endTime = DateUtils.getDate(timePair.endTime, "HH:mm");
            Date tempTime = new Date();

            while (startTime.getTime() < endTime.getTime()) {
                final RadioButton radioButton = (RadioButton) inflater.inflate(R.layout.item_radiobutton, deliveryTimesRadioGroup, false);
                radioButton.setId(ViewIdGenerator.generateViewId());

                tempTime.setTime(Math.min(startTime.getTime() + 60 * 60 * 1000, endTime.getTime()));
                radioButton.setText(timeFormat.format(startTime) + " - " + timeFormat.format(tempTime));

                radioButton.setTag(timePair);
                deliveryTimesRadioGroup.addView(radioButton);
                startTime.setTime(tempTime.getTime());
            }
        }

        // check the first button
        deliveryTimesRadioGroup.clearCheck();
        deliveryTimesRadioGroup.check(deliveryTimesRadioGroup.getChildAt(0).getId());
    }

    private void initMap() {
        // add MapFragment
        if (mapFragment == null) {
            mapFragment = SupportMapFragment.newInstance();
        }
        if (!mapFragment.isAdded()) {
            final FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.map_container, mapFragment);
            fragmentTransaction.commit();
            mapFragment.getMapAsync(this);
        }

        // wait for onMapReady()
    }

    private void getDirectionsToDepot() {
        final OkHttpClient client = new OkHttpClient();
        final Request request;

        if (parcel.deliveryDetails == null) {
            return;
        }

        final float userLat = parcel.deliveryDetails.address.addressPoint.latitude;
        final float userLon = parcel.deliveryDetails.address.addressPoint.longitude;
        final float depotLat = depot.addressPoint.latitude;
        final float depotLon = depot.addressPoint.longitude;

        try {
            request = new Request.Builder()
                    .url(String.format(
                            "https://maps.googleapis.com/maps/api/directions/json?units=imperial&origin=%s,%s&destination=%s,%s&sensor=false",
                            userLat,
                            userLon,
                            depotLat,
                            depotLon))
                    .build();
        } catch (Exception e) {
            Log.e(TAG, "Error creating directions request.", e);
            return;
        }

        mapsAPICall = client.newCall(request);
        mapsAPICall.enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                Log.e(TAG, "Maps API call failure:", e);
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (!response.isSuccessful()) {
                    Log.e(TAG, String.format("Unable to download directions %d", response.code()));
                    return;
                }

                if (!isAdded()) {
                    return;
                }

                try {
                    final String body = response.body().string();
                    final JSONObject json = new JSONObject(body);
                    final JSONArray routes = json.getJSONArray("routes");
                    final JSONObject route = routes.getJSONObject(0);
                    final JSONObject bounds = route.getJSONObject("bounds");
                    final String pointsStr = route.getJSONObject("overview_polyline").getString("points");

                    final List<LatLng> points = GMapsUtils.decodePoly(pointsStr);
                    final JSONObject leg = route.getJSONArray("legs").getJSONObject(0);
                    final String distance = leg.getJSONObject("distance").getString("text");
                    final String duration = leg.getJSONObject("duration").getString("text");
                    final LatLng start = new LatLng(
                            leg.getJSONObject("start_location").getDouble("lat"),
                            leg.getJSONObject("start_location").getDouble("lng"));
                    final LatLng end = new LatLng(
                            leg.getJSONObject("end_location").getDouble("lat"),
                            leg.getJSONObject("end_location").getDouble("lng"));
                    final JSONArray steps = leg.getJSONArray("steps");

                    // zoom to bounds
                    final LatLng southwest = new LatLng(
                            bounds.getJSONObject("southwest").getDouble("lat"),
                            bounds.getJSONObject("southwest").getDouble("lng"));
                    final LatLng northeast = new LatLng(
                            bounds.getJSONObject("northeast").getDouble("lat"),
                            bounds.getJSONObject("northeast").getDouble("lng"));

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            final View view = getView();
                            final GoogleMap map = mapFragment.getMap();

                            if (view == null) {
                                return;
                            }

                            // add start/end
                            map.addMarker(new MarkerOptions()
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin_start))
                                    .position(start)
                                    .alpha(0.9f)
                                    .anchor(0.5f, 1.0f)
                                    .title(getString(R.string.your_address)));
                            map.addMarker(new MarkerOptions()
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin_end_dpd))
                                    .position(end)
                                    .alpha(0.9f)
                                    .anchor(0.5f, 1.0f)
                                    .title(parcel.getShipperName()));

                            // plot route
                            final PolylineOptions options = new PolylineOptions()
                                    .width(7)
                                    .color(getResources().getColor(R.color.map_route))
                                    .geodesic(true);
                            for (int z = 0; z < points.size(); z++) {
                                LatLng point = points.get(z);
                                options.add(point);
                            }
                            map.addPolyline(options);

                            // zoom to bounds
                            final LatLng[] latLngs = new LatLng[]{southwest, northeast, new LatLng(userLat, userLon), new LatLng(depotLat, depotLon)};
                            final BitmapFactory.Options bmpOptions = ImagePickerUtils.getDimensionFromResource(getContext(), R.drawable.ic_pin_end_dpd);
                            map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                                @Override
                                public void onMapLoaded() {
                                    GMapsUtils.zoomWithPadding(mapFragment, latLngs, 20, bmpOptions.outWidth, bmpOptions.outHeight);
                                }
                            });

                            // Disable marker click listener
                            map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                                @Override
                                public boolean onMarkerClick(Marker marker) {
                                    return true;
                                }
                            });

                            // add summary
                            TextView summaryTextView = (TextView) view.findViewById(R.id.directions_summary_textview);
                            summaryTextView.setText(getString(R.string.directions_summary, distance, duration));
                            summaryTextView.setVisibility(View.VISIBLE);

                            // add list of textual steps
                            addDirectionSteps(steps);
                        }
                    });
                } catch (JSONException je) {
                    Log.e(TAG, "Unable to parse directions, JSONException", je);
                } catch (Exception e) {
                    Log.e(TAG, "Unable to parse directions", e);
                }
            }
        });
    }

    private void addDirectionSteps(JSONArray steps) {
        final View view = getView();
        assert view != null;
        final ViewGroup stepsContainer = (ViewGroup) view.findViewById(R.id.directions_container);
        final LayoutInflater inflater = LayoutInflater.from(getActivity());

        try {
            int length = steps.length();
            for (int j = 0; j < length; j++) {
                final JSONObject step = steps.getJSONObject(j);
                final View stepView = inflater.inflate(R.layout.item_direction_step, stepsContainer, false);
                ((TextView) stepView.findViewById(R.id.direction_step_number_textview)).setText(String.valueOf(j + 1));
                ((TextView) stepView.findViewById(R.id.direction_distance_textview)).setText(step.getJSONObject("distance").getString("text"));
                ((TextView) stepView.findViewById(R.id.direction_description_textview)).setText(Html.fromHtml(step.getString("html_instructions")));
                stepsContainer.addView(stepView);
            }
        } catch (JSONException e) {
            Log.e(TAG, "Problem processing steps", e);
        }
    }

    private void expandDirections() {
        final View view = getView();
        assert view != null;
        final ViewGroup directions = (ViewGroup) view.findViewById(R.id.directions_container);
        final boolean isVisible = directions.getVisibility() == View.VISIBLE;
        final AutoTransition transition = new AutoTransition(view.getContext(), null);
        TransitionManager.beginDelayedTransition((ViewGroup) view, transition);
        directions.setVisibility(isVisible ? View.GONE : View.VISIBLE);
        expandDirectionsButton.setCompoundDrawablesWithIntrinsicBounds(0, 0, isVisible ? R.drawable.ic_plus : R.drawable.ic_minus, 0);
    }

    private void confirmChanges() {
        final View view = getView();
        assert view != null;
        final RadioButton dateRadioButton = (RadioButton) deliveryDatesRadioGroup.findViewById(deliveryDatesRadioGroup.getCheckedRadioButtonId());
        final RadioButton timeRadioButton = (RadioButton) deliveryTimesRadioGroup.findViewById(deliveryTimesRadioGroup.getCheckedRadioButtonId());
        final String selectedDate = dateRadioButton.getText().toString();
        final String selectedTime = timeRadioButton.getText().toString();
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(getActivity(), R.style.AlertDialogTheme);
        builder.setTitle(getString(R.string.change_delivery_dialog_title));
        builder.setMessage(getString(R.string.change_delivery_collect_from_depot_message, selectedTime, selectedDate));
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

                final RadioButton dateRadioButton = (RadioButton) deliveryDatesRadioGroup.findViewById(deliveryDatesRadioGroup.getCheckedRadioButtonId());
                final RadioButton timeRadioButton = (RadioButton) deliveryTimesRadioGroup.findViewById(deliveryTimesRadioGroup.getCheckedRadioButtonId());
                final DateAndTimes dateAndTimes = (DateAndTimes) dateRadioButton.getTag();
                final String diaryDate = dateAndTimes.diaryDate;
                final TimePair timePair = (TimePair) timeRadioButton.getTag();
                final String startTime = timePair.startTime;
                collectFromDepot(diaryDate, startTime);
            }
        });
        builder.setNegativeButton(android.R.string.cancel, null);
        builder.show();
    }

    private void showActionConfirmedDialog() {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(getContext(), R.style.AlertDialogTheme);
        builder.setTitle(R.string.parcel_updated);
        builder.setMessage(getString(R.string.parcel_collection_depot_confirmed));
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // all done - return from this Fragment
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
        builder.create().show();
    }

    @Override
    public void onMapReady(GoogleMap map) {
        getDepot();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
    }
}
