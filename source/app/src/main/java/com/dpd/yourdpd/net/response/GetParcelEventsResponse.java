package com.dpd.yourdpd.net.response;

import com.dpd.yourdpd.model.ParcelEvent;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @see com.dpd.yourdpd.net.Api#getParcelEvents(String)
 */
public class GetParcelEventsResponse extends BaseResponse {
    @SerializedName("data")
    private List<ParcelEvent> events;

    public List<ParcelEvent> getParcelEvents() {
        return events;
    }
}
