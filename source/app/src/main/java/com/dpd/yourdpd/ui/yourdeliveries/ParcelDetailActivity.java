package com.dpd.yourdpd.ui.yourdeliveries;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.dpd.yourdpd.R;
import com.dpd.yourdpd.ui.BaseMenuActivity;
import com.paypal.android.sdk.payments.PayPalService;


public class ParcelDetailActivity extends BaseMenuActivity
        implements ParcelDetailActivityFragment.OnFragmentInteractionListener,
        FragmentManager.OnBackStackChangedListener,
        InFlightCollectFromPickupShopFragment.OnFragmentInteractionListener,
        InFlightCollectFromDepotFragment.OnFragmentInteractionListener,
        InFlightChangeDeliveryDateFragment.OnFragmentInteractionListener,
        InFlightDeliverToNeighbourFragment.OnFragmentInteractionListener,
        InFlightUpgradeDeliveryFragment.OnFragmentInteractionListener,
        InFlightLeaveInSafePlaceFragment.OnFragmentInteractionListener {

    private static final String TAG = ParcelDetailActivity.class.getSimpleName();
    private static final String FRAGMENT_TAG = ParcelDetailActivity.class.getSimpleName();

    public static final String EXTRA_PARCEL_CODE = "parcel_code";

    private String parcelCode;
    private ContentLoadingProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        parcelCode = getIntent().getStringExtra(EXTRA_PARCEL_CODE);
        setContentView(R.layout.activity_parcel_detail);
        progressBar = (ContentLoadingProgressBar) findViewById(R.id.parcelProgressBar);
        setupToolbar(getTitle().toString(), true);

        final FragmentManager fragmentManager = getSupportFragmentManager();
        final Fragment fragment = ParcelDetailActivityFragment.newInstance(parcelCode);
        final FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.container, fragment, FRAGMENT_TAG);
        transaction.commit();

        fragmentManager.addOnBackStackChangedListener(this);

        // start PayPal service
        final Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, paypalConfig);
        startService(intent);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        String parcelCode = intent.getStringExtra(EXTRA_PARCEL_CODE);
        if (parcelCode != null) {
            this.parcelCode = parcelCode;
            final FragmentManager fragmentManager = getSupportFragmentManager();
            final Fragment fragment = fragmentManager.findFragmentByTag(FRAGMENT_TAG);
            if (fragment != null && fragment instanceof ParcelDetailActivityFragment) {
                ((ParcelDetailActivityFragment) fragment).setAndReloadParcel(parcelCode);
            }
            fragmentManager.popBackStack();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_delivery_detail, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a listener activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                getSupportFragmentManager().popBackStack();
                return true;
            } else {
                final Intent intent = new Intent(this, YourDeliveriesActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackStackChanged() {
        // reset title and reload parcel on navigating back to root Fragment
        // (e.g. from an action Fragment)
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            setupToolbar(getTitle().toString(), true);
            reloadParcel();
        }
    }

    private void reloadParcel() {
        // update UI
        ParcelDetailActivityFragment fragment = (ParcelDetailActivityFragment) getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG);
        fragment.reloadParcel(false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(new Intent(this, PayPalService.class));
    }

    @Override
    public void onShowProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
        progressBar.show();
        findViewById(R.id.container).setVisibility(View.INVISIBLE);
    }

    @Override
    public void onHideProgressBar() {
        progressBar.hide();
        progressBar.setVisibility(View.GONE);
        findViewById(R.id.container).setVisibility(View.VISIBLE);
    }
}