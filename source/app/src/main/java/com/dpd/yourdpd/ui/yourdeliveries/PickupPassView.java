package com.dpd.yourdpd.ui.yourdeliveries;

import android.content.Context;
import android.graphics.Typeface;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.dpd.yourdpd.R;
import com.dpd.yourdpd.model.Parcel;
import com.dpd.yourdpd.util.PhotoUtils;

/**
 * Displays details regarding a PickupLocation and related barcode
 */
public class PickupPassView extends FrameLayout {
    private static final String TAG = PickupPassView.class.getSimpleName();

    private final TextView detailsTextView;
    private final ImageView barcodeImageView;

    public PickupPassView(Context context) {
        this(context, null);
    }

    public PickupPassView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PickupPassView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater.from(getContext()).inflate(R.layout.pickup_pass, this, true);

        detailsTextView = (TextView) findViewById(R.id.details_textview);
        barcodeImageView = (ImageView) findViewById(R.id.barcode_imageview);
    }

    public void setBarcodeImage(String barcodeDataOrUrl) {
        try {
            PhotoUtils.loadUnknownImageDataIntoImageView(getContext(), barcodeDataOrUrl, barcodeImageView, 0);
        } catch (Exception e) {
            Log.e(TAG, "Unable to decode logo");
        }
    }

    public void setParcel(Parcel parcel) {
        int start = 0, end = 0;
        SpannableStringBuilder details = new SpannableStringBuilder();
        details.append(getContext().getString(R.string.your_name_bold));
        end = details.length();
        details.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        details.append("\n");
        try {
            details.append(parcel.deliveryDetails.notificationDetails.contactName);
        } catch (NullPointerException npe) {
            Log.w(TAG, "Contact name missing", npe);
        }
        details.append("\n");
        details.append("\n");
        start = details.length();
        details.append(getContext().getString(R.string.your_address_bold));
        end = details.length();
        details.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        details.append("\n");
        details.append(parcel.deliveryDetails.address.toFormattedString(true));
        details.append("\n");
        details.append("\n");
        start = details.length();
        details.append(getContext().getString(R.string.parcel_reference_bold));
        end = details.length();
        details.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        details.append("\n");
        details.append(parcel.parcelNumber);
        details.append("\n");
        details.append("\n");
        start = details.length();
        details.append(getContext().getString(R.string.number_of_parcels_to_collect_bold));
        end = details.length();
        details.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        details.append("\n");
        details.append(String.valueOf(parcel.getRelatedParcelCount() + 1));
        detailsTextView.setText(details);
    }
}
