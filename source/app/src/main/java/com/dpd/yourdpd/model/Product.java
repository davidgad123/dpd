package com.dpd.yourdpd.model;

import java.io.Serializable;

/**
 * Created by richardleggett on 01/09/15.
 */
public class Product implements Serializable {
    /**
     * e.g. 1^1
     */
    public String productCode;
    /**
     * e.g. Parcel
     */
    public String productDescription;
}
