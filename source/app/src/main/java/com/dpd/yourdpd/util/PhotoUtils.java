package com.dpd.yourdpd.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;

import com.dpd.yourdpd.BuildConfig;
import com.dpd.yourdpd.R;
import com.dpd.yourdpd.image.CropSquareTransformation;
import com.dpd.yourdpd.model.Photo;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Helper for loading images from the server
 */
public class PhotoUtils {

    private static final String TAG = PhotoUtils.class.getSimpleName();

    /**
     * Convert a Bitmap to a PNG encoded as a base64 string, prefixed with "data:image/png;base64,"
     *
     * @param bitmap Bitmap to convert
     * @return padded base64 string
     */
    public static String bitmapToBase64(Bitmap bitmap) {
        if (bitmap == null) {
            Log.w(TAG, "Cannot convert null bitmap to base64");
            return "";
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        String base64 = Base64.encodeToString(byteArray, Base64.DEFAULT);
        base64 = base64.replace("\n", "");
        return "data:image/png;base64," + base64;
    }

    public static Bitmap base64ToBitmap(String base64Data) {
        // ignore "data:png;base64," portion if found
        if (base64Data.indexOf(',') != -1) {
            base64Data = base64Data.substring(base64Data.lastIndexOf(",") + 1);
        }
        base64Data = base64Data.replace("\n", "");
        // handle padded and non-padded base64 data
        byte[] decoded = Base64.decode(base64Data, base64Data.contains("=") ? Base64.NO_PADDING : Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(decoded, 0, decoded.length);
    }

    /**
     * Decode base64 data and display in ImageView
     *
     * @param base64Data Base 64 data
     * @param imageView  Target ImageView
     * @throws Exception
     */
    public static void loadBase64ImageIntoImageView(String base64Data, ImageView imageView)
            throws Exception {
        Bitmap bmp = base64ToBitmap(base64Data);
        if (bmp == null) throw new Exception();
        imageView.setImageBitmap(bmp);
    }

    /**
     * @param context    Context
     * @param photo      Photo
     * @param imageView  ImageView
     * @param dpdSession DPD session ID
     * @deprecated The endpoints now return JSON, not actual image data
     */
    @Deprecated
    public static void loadPhotoIntoImageView(Context context,
                                              Photo photo,
                                              ImageView imageView,
                                              String dpdSession) {
        final String photoUrl = BuildConfig.SERVER_URL + "/shipping/shipment/_/parcel/" +
                photo.parcelCode +
                "/image/" + photo.imageKey +
                "/?base64Data=false";// + (base64Data?"true":"false");

        // because the above service requires certain headers we have to construct a
        // RequestInterceptor to inject these headers each time
        final OkHttpClient picassoClient = new OkHttpClient();
        picassoClient
                .interceptors()
                .add(new PhotoDownloadInterceptor(photo.imageType, dpdSession));
        final Picasso picasso = new Picasso.Builder(context)
                .listener(new Picasso.Listener() {
                    @Override
                    public void onImageLoadFailed(Picasso picasso, Uri uri, Exception exception) {
                        Log.e(TAG, "Unable to load photo at " + uri, exception);
                    }
                })
                .downloader(new OkHttpDownloader(picassoClient))
                .build();

        picasso.load(photoUrl)
                .transform(new CropSquareTransformation())
                .into(imageView);
    }

    /**
     * Attempts to determine if given imageData string is a URL or base64 and load image into
     * target ImageView
     *
     * @param imageData May be a URL or base64
     * @param imageView target ImageView
     */
    public static void loadUnknownImageDataIntoImageView(final Context context,
                                                         String imageData,
                                                         final ImageView imageView,
                                                         final int defaultResId) {
        if (imageData.startsWith("http") || imageData.startsWith("www.")) {
            if (imageData.startsWith("www.")) {
                imageData = "http://" + imageData;
            }

            com.squareup.picasso.Callback callback = new com.squareup.picasso.Callback() {
                @Override
                public void onSuccess() {
                }

                @Override
                public void onError() {
                    if (defaultResId != 0) imageView.setImageResource(defaultResId);
                    else imageView.setImageBitmap(null);
                }
            };
            Picasso.with(context)
                    .load(Uri.parse(imageData))
                    .into(imageView, callback);

        } else {
            try {
                PhotoUtils.loadBase64ImageIntoImageView(imageData, imageView);
            } catch (Exception e) {
                Log.e(TAG, "Unable to decode image");
                imageView.setImageResource(defaultResId);
            }
        }
    }

    public static void setCustomerImageLogo(Context context, String customerImageLogo, ImageView imageView) {
        if (!android.text.TextUtils.isEmpty(customerImageLogo)) {
            PhotoUtils.loadUnknownImageDataIntoImageView(context,
                    customerImageLogo,
                    imageView,
                    R.drawable.dpd_cube_grey);
        } else {
            imageView.setImageResource(R.drawable.dpd_cube_grey);
        }
    }

    /**
     * Injects headers expected by the image download service
     */
    private static class PhotoDownloadInterceptor implements Interceptor {
        private String imageType;
        private String dpdSession;

        public PhotoDownloadInterceptor(String imageType, String dpdSession) {
            this.imageType = imageType;
            this.dpdSession = dpdSession;
        }

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request newRequest = chain.request().newBuilder()
                    .addHeader("Accept", imageType)
                    .addHeader("dpdsession", dpdSession)
                    .addHeader("dpdclient", BuildConfig.DPD_CLIENT)
                    .build();
            return chain.proceed(newRequest);
        }
    }
}
