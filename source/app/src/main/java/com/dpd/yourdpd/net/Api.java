package com.dpd.yourdpd.net;

import com.dpd.yourdpd.model.ParcelActionCodes;
import com.dpd.yourdpd.net.request.ChangeParcelDateParams;
import com.dpd.yourdpd.net.request.CollectFromDepotParams;
import com.dpd.yourdpd.net.request.CollectFromPickupLocationParams;
import com.dpd.yourdpd.net.request.DeliverToNeighbourParams;
import com.dpd.yourdpd.net.request.GeneratePinByEmailParams;
import com.dpd.yourdpd.net.request.GeneratePinBySmsParams;
import com.dpd.yourdpd.net.request.LeaveInSafeLocationParams;
import com.dpd.yourdpd.net.request.SubmitRatingParams;
import com.dpd.yourdpd.net.request.UpgradeParcelParams;
import com.dpd.yourdpd.net.response.AuthenticatePinParams;
import com.dpd.yourdpd.net.response.AuthenticatePinResponse;
import com.dpd.yourdpd.net.response.ChangeParcelDateResponse;
import com.dpd.yourdpd.net.response.CheckPropertyResponse;
import com.dpd.yourdpd.net.response.CollectFromDepotResponse;
import com.dpd.yourdpd.net.response.CollectFromPickupLocationResponse;
import com.dpd.yourdpd.net.response.DeleteConsumerResponse;
import com.dpd.yourdpd.net.response.DeliverToNeighbourResponse;
import com.dpd.yourdpd.net.response.GeneratePinResponse;
import com.dpd.yourdpd.net.response.GetAddressByUDPRNResponse;
import com.dpd.yourdpd.net.response.GetAlternativeDeliveryTimesResponse;
import com.dpd.yourdpd.net.response.GetCollectParcelTimesResponse;
import com.dpd.yourdpd.net.response.GetConsumerResponse;
import com.dpd.yourdpd.net.response.GetContentResponse;
import com.dpd.yourdpd.net.response.GetDepotResponse;
import com.dpd.yourdpd.net.response.GetImageResponse;
import com.dpd.yourdpd.net.response.GetParcelActionsResponse;
import com.dpd.yourdpd.net.response.GetParcelDetailsResponse;
import com.dpd.yourdpd.net.response.GetParcelEventsResponse;
import com.dpd.yourdpd.net.response.GetParcelHistoryResponse;
import com.dpd.yourdpd.net.response.GetParcelPickupLocationsResponse;
import com.dpd.yourdpd.net.response.GetParcelUpdateDatesResponse;
import com.dpd.yourdpd.net.response.GetPickupLocationResponse;
import com.dpd.yourdpd.net.response.GetPickupLocationsByAddressResponse;
import com.dpd.yourdpd.net.response.GetPickupPassResponse;
import com.dpd.yourdpd.net.response.GetRouteViewOnRouteResponse;
import com.dpd.yourdpd.net.response.LeaveInSafePlaceResponse;
import com.dpd.yourdpd.net.response.LoginResponse;
import com.dpd.yourdpd.net.response.LookupPostKeyResponse;
import com.dpd.yourdpd.net.response.MobileStartupChecksResponse;
import com.dpd.yourdpd.net.response.PutConsumerResponse;
import com.dpd.yourdpd.net.response.SafeLocationsResponse;
import com.dpd.yourdpd.net.response.SearchByAddressResponse;
import com.dpd.yourdpd.net.response.SubmitRatingResponse;
import com.dpd.yourdpd.net.response.UpgradeParcelResponse;
import com.google.gson.JsonObject;

import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.Query;
import rx.Observable;

/**
 * Defines the remote API service which Retrofit uses to construct a RestAdapter for
 * in {@link ApiModule}
 */
public interface Api {
    /**
     * Anonymous login to gain dpdsession
     */
    @POST("/user/?action=login")
    Observable<LoginResponse> login(@Header("Authorization") String authHeader);

    /**
     * Registered user login (passing auditUser which is returned as consumerId from authenticatePin)
     */
    @POST("/user/?action=login")
    Observable<LoginResponse> loginWithAuditUser(@Query("auditUser") String auditUser,
                                                 @Header("Authorization") String authHeader);

    @POST("/consumer/pin/?action=GenerateBySMS")
    Observable<GeneratePinResponse> generatePinBySMS(@Body GeneratePinBySmsParams params);

    @POST("/consumer/pin/?action=GenerateByEmail")
    Observable<GeneratePinResponse> generatePinByEmail(@Body GeneratePinByEmailParams params);

    @POST("/consumer/pin/?action=Authenticate")
    Observable<AuthenticatePinResponse> authenticatePin(@Body AuthenticatePinParams params);

    @GET("/user/device/consumerapp/?action=startup")
    Observable<MobileStartupChecksResponse> mobileStartupChecks(@Query("appVersion") String appVersion,
                                                                @Query("deviceId") String deviceId);

    @GET("/consumer/")
    Observable<GetConsumerResponse> getConsumer();

    @GET("/system/mobileapp/?filter=content")
    Observable<GetContentResponse> getContent(@Query("appVersion") String appVersion);

    @PUT("/consumer/")
    Observable<PutConsumerResponse> putConsumer(@Body JsonObject consumerProfileJson);

    @DELETE("/consumer/")
    Observable<DeleteConsumerResponse> deleteConsumer();

    @GET("/shipping/shipment/_/parcel/?filter=parcelHistory")
    Observable<GetParcelHistoryResponse> getParcelHistory(@Query("searchPage") int searchPage,
                                                          @Query("searchPageSize") int searchPageSize);

    @GET("/shipping/shipment/_/parcel/")
    Observable<GetParcelDetailsResponse> getParcelDetails(@Query("parcelCode") String parcelCode);

    @GET("/shipping/shipment/_/parcel/{parcelCode}/event/")
    Observable<GetParcelEventsResponse> getParcelEvents(@Path("parcelCode") String parcelCode);

    @GET("/shipping/shipment/_/parcel/{parcelCode}?action=GetParcelActions")
    Observable<GetParcelActionsResponse> getParcelActions(@Path("parcelCode") String parcelCode);

    /**
     * Upgrade parcel delivery (at cost)
     *
     * @param params UpgradeParcelParams
     */
    @POST("/shipping/shipment/_/parcel/?action=Upgrade")
    Observable<UpgradeParcelResponse> upgradeParcel(@Body UpgradeParcelParams params);

    @GET("/organisation/pickuplocation/?filter=parcel&orderBy=nearest&searchCriteria=")
    Observable<GetParcelPickupLocationsResponse> getParcelPickupLocations(@Query("parcelCode") String parcelCode,
                                                                          @Query("searchPage") int searchPage,
                                                                          @Query("searchPageSize") int searchPageSize,
                                                                          @Query("maxDistance") int maxDistance);

    @GET("/organisation/pickuplocation/?filter=nearAddress&searchCriteria=&countryCode=GB&searchBrand=")
    Observable<GetPickupLocationsByAddressResponse> getPickupLocationsByAddress(@Query("searchAddress") String searchAddress, // postcode?
                                                                                @Query("maxDistance") int maxDistance, // e.g. 30
                                                                                @Query("searchPage") int searchPage,
                                                                                @Query("searchPageSize") int searchPageSize);

    @GET("/organisation/pickuplocation/{pickupLocationCode}/")
    Observable<GetPickupLocationResponse> getPickupLocation(@Path("pickupLocationCode") String pickupLocationCode);


    @GET("/image/pickuppass?base64=true")
    Observable<GetPickupPassResponse> getPickupPass(@Query("parcelCode") String parcelCode);

    /**
     * Request to collect a Parcel from a given PickupLocation
     *
     * @param params CollectFromPickupLocationParams
     */
    @POST("/shipping/shipment/_/parcel/?action=CollectFromPickupLocation")
    Observable<CollectFromPickupLocationResponse> collectFromPickupLocation(@Body CollectFromPickupLocationParams params);

    /**
     * Obtain a list of dates available for changing delivery, requires an action code to determine
     * said dates to return.
     *
     * @param parcelCode     Parcel code
     * @param parcelActionId {@link ParcelActionCodes}
     */
    @GET("/shipping/shipment/_/parcel/{parcelCode}?action=GetParcelUpdateDates")
    Observable<GetParcelUpdateDatesResponse> getParcelUpdateDates(@Path("parcelCode") String parcelCode,
                                                                  @Query("parcelActionId") String parcelActionId);

    @POST("/shipping/shipment/_/parcel/?action=ChangeDate")
    Observable<ChangeParcelDateResponse> changeParcelDate(@Body ChangeParcelDateParams params);

    @GET("/depot/{depot}")
    Observable<GetDepotResponse> getDepot(@Path("depot") String depot);

    /**
     * Obtain collection times for the CollectFromDepot action
     * // TODO: do we ever receive an array of times as per UI?
     *
     * @param depotCode e.g. 0030
     * @param diaryDate Format must be 2015-09-04
     */
    @GET("/shipping/delivery/?filter=CollectParcelTimes&canCollectParcel=on&outForDeliveryDate&userType")
    Observable<GetCollectParcelTimesResponse> getCollectParcelTimes(@Query("depotCode") String depotCode,
                                                                    @Query("diaryDate") String diaryDate);

    @POST("/shipping/shipment/_/parcel/?action=CollectFromDepot")
    Observable<CollectFromDepotResponse> collectFromDepot(@Body CollectFromDepotParams params);

    @GET("/reference/address/?filter=udprn")
    Observable<GetAddressByUDPRNResponse> getAddressByUDPRN(@Query("udprn") String udprn);

    /**
     * Search for addresses near target location
     * Note that parameters can be left empty if not known
     * e.g. /reference/address?county&locality=Rubery%2C+Rednal&organisation=Jordan+Motor+...&find
     */
    @GET("/reference/address?&find")
    Observable<SearchByAddressResponse> searchByAddress(@Query("locality") String locality,
                                                        @Query("organisation") String organisation,
                                                        @Query("postcode") String postcode,
                                                        @Query("street") String street,
                                                        @Query("town") String town,
                                                        @Query("county") String county,
                                                        @Query("searchPage") int searchPage,
                                                        @Query("searchPageSize") int searchPageSize);

    @POST("/shipping/shipment/_/parcel/?action=DeliverToNeighbour")
    Observable<DeliverToNeighbourResponse> deliverToNeighbour(@Body DeliverToNeighbourParams params);

    @GET("/reference/safelocation?safeLocationCode=")
    @Deprecated
    Observable<SafeLocationsResponse> getSafeLocations();

    @POST("/shipping/shipment/_/parcel/?action=LeaveInSafePlace")
    Observable<LeaveInSafePlaceResponse> leaveInSafePlace(@Body LeaveInSafeLocationParams params);

    @GET("/reference/address/{addressKey}")
    Observable<LookupPostKeyResponse> lookupPostKey(@Path("addressKey") String addressKey);

    @GET("/reference/address/?filter=depotDetails")
    Observable<CheckPropertyResponse> checkProperty(@Query("postcode") String postcode);

    @GET("/shipping/shipment/_/parcel/{parcelCode}?filter=AlternativeDeliveryTimes")
    Observable<GetAlternativeDeliveryTimesResponse> getAlternativeDeliveryTimes(@Path("parcelCode") String parcelCode,
                                                                                @Query("date") String date,
                                                                                @Query("newDeliveryAddress.city") String city,
                                                                                @Query("newDeliveryAddress.countryCode") String countryCode,
                                                                                @Query("newDeliveryAddress.county") String county,
                                                                                @Query("newDeliveryAddress.locality") String locality,
                                                                                @Query("newDeliveryAddress.postcode") String postcode,
                                                                                @Query("newDeliveryAddress.street") String street,
                                                                                @Query("newDeliveryAddress.town") String town,
                                                                                @Query("userType") String userType
    );

    @GET("/depot/{depotCode}/route/{routeKey}/?view=onRoute&_source=&archiveYear=&archiveMonth=")
    Observable<GetRouteViewOnRouteResponse> getRouteViewOnRoute(@Path("depotCode") String depotCode,
                                                                @Path("routeKey") String routeKey);

    /**
     * @param imageKey  uniquely identifies the image on the server
     * @param imageType mime type e.g. image/jpg
     * @return Image URL string
     */
    @GET("/shipping/shipment/_/parcel/{parcelCode}/image/{imageKey}/?base64Data=false")
    Observable<GetImageResponse> getImage(@Query("parcelCode") String parcelCode,
                                          @Query("imageKey") String imageKey,
                                          @Header("Accept") String imageType);

    /**
     * @param imageKey  uniquely identifies the image on the server
     * @param imageType mime type e.g. image/jpg
     * @deprecated Replaced by getRackspaceImage
     */
    @GET("/shipping/shipment/_/parcel/{parcelCode}/image/{imageKey}/")
    Observable<GetImageResponse> getImage(@Query("parcelCode") String parcelCode,
                                          @Query("imageKey") String imageKey,
                                          @Query("base64") boolean base64,
                                          @Header("Accept") String imageType);

    /**
     * @param imageKey  uniquely identifies the image on the server
     * @param imageType e.g. S for delivery (SATURN SAFE PLACE) images etc
     *                  1. Drivers face = `DRIVER`
     *                  2. Consumers face = `CONSUMER`
     *                  3. Saturn Safe Place = `S`
     *                  4. Consumer Safe place = `PROPERTY`
     *                  5. Customer Logos = TO BE CONFIRMED
     */
    @GET("/image/{imageKey}")
    Observable<GetImageResponse> getRackspaceImage(@Path("imageKey") String imageKey,
                                                   @Query("imageType") String imageType);

    @POST("/shipping/shipment/_/parcel/{parcelCode}/?action=rateExperience")
    Observable<SubmitRatingResponse> submitRating(@Path("parcelCode") String parcelCode,
                                                  @Body SubmitRatingParams params);
}
