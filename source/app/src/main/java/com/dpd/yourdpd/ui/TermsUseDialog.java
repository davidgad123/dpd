package com.dpd.yourdpd.ui;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.dpd.yourdpd.R;

public class TermsUseDialog extends Dialog {
    public TermsUseDialog(Context context) {
        super(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_terms_of_use);

        final WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        final Window window = getWindow();
        window.setGravity(Gravity.BOTTOM);
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.windowAnimations = R.style.DialogSlideAnim;
        lp.dimAmount = 0f;
        window.setAttributes(lp);
        window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
    }

    public void setOnViewTermsClickListener(View.OnClickListener listener) {
        findViewById(R.id.btn_view_terms_of_use).setOnClickListener(listener);
    }

    public void setOnAgreeClickListener(View.OnClickListener listener) {
        findViewById(R.id.btn_agree).setOnClickListener(listener);
    }
}