package com.dpd.yourdpd.net.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @see com.dpd.yourdpd.net.Api#searchByAddress(String, String, String, String, String, String, int, int)
 */
public class SearchByAddressResponse extends BaseResponse {
    @SerializedName("data")
    private List<AddressSearchResult> addressList;

    public List<AddressSearchResult> getSearchResults() {
        return addressList;
    }

    public static class AddressSearchResult {
        /**
         * Uniquely identifies the address
         * e.g. used with Deliver To Neighbour
         */
        public String addressKey;
        public String postcode;
        /**
         * Short readable address
         */
        public String quickviewAddress;

        @Override
        public String toString() {
            return quickviewAddress;
        }
    }

}
