package com.dpd.yourdpd.ui;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.UiThread;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;

import com.dpd.yourdpd.DaggerFragmentComponent;
import com.dpd.yourdpd.FragmentComponent;
import com.dpd.yourdpd.FragmentModule;
import com.dpd.yourdpd.R;
import com.dpd.yourdpd.YourDPDApp;
import com.dpd.yourdpd.datastore.DataStore;
import com.dpd.yourdpd.model.User;
import com.dpd.yourdpd.net.Api;
import com.paypal.android.sdk.payments.PayPalConfiguration;

import javax.inject.Inject;

/**
 * Base Fragment for Fragments that require the FragmentComponent injection
 */
public class BaseFragment extends Fragment implements User.Listener, Validating {

    protected FragmentComponent component;

    @Inject protected DataStore dataStore;
    @Inject protected Api api;
    @Inject protected User user;
    @Inject protected PayPalConfiguration paypalConfig;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // inject dependencies
        component = DaggerFragmentComponent.builder()
                .appComponent(YourDPDApp.getAppComponent())
                .fragmentModule(new FragmentModule())
                .build();
        component.inject(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        user.addListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        user.removeListener(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        api = null;
        dataStore = null;
        user = null;
        component = null;
    }

    @UiThread
    protected void showErrorAlertDialog(String message) {
        showErrorAlertDialog(message, null);
    }

    @UiThread
    protected void showErrorAlertDialog(String message, String title) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AlertDialogTheme);
        if (title != null) builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(android.R.string.ok, null);
        builder.create().show();
    }

    @UiThread
    protected void showErrorAlertDialogWithOk(String message, String title, DialogInterface.OnClickListener callback) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AlertDialogTheme);
        if (title != null) builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(android.R.string.ok, callback);
        builder.create().show();
    }

    @UiThread
    protected void showErrorAlertWithRetryOption(String message, @NonNull DialogInterface.OnClickListener retryCallback) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AlertDialogTheme);
        builder.setMessage(message);
        builder.setPositiveButton(getString(R.string.retry), retryCallback);
        builder.setNegativeButton(android.R.string.cancel, null);
        builder.create().show();
    }

    @UiThread
    protected void showErrorAlertWithOkOption(String message, @NonNull DialogInterface.OnClickListener retryCallback) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AlertDialogTheme);
        builder.setMessage(message);
        builder.setPositiveButton(getString(android.R.string.ok), retryCallback);
        builder.setNegativeButton(android.R.string.cancel, null);
        builder.create().show();
    }

    @Override
    public void onUserLoggedOut() {
    }

    @Override
    public void onConsumerProfileUpdated(boolean success) {
    }

    @Override
    public boolean validate() {
        return true;
    }

    @Override
    public void applyChanges() {

    }
}
