package com.dpd.yourdpd.datastore;

/**
 * Stores server-driven content like T&Cs
 */
public class SiteContent {
    public String termsAndConditions;
}
