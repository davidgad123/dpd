package com.dpd.yourdpd.ui.address;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.dpd.yourdpd.R;
import com.dpd.yourdpd.model.ConsumerProfile;
import com.dpd.yourdpd.model.ConsumerProperty;
import com.dpd.yourdpd.model.ConsumerPropertyAddressInfo;
import com.dpd.yourdpd.net.response.GetAddressByUDPRNResponse;
import com.dpd.yourdpd.net.response.PutConsumerResponse;
import com.dpd.yourdpd.ui.BaseFragment;
import com.dpd.yourdpd.ui.Validating;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Top level Fragment for editing an existing {@link com.dpd.yourdpd.model.ConsumerProperty}
 */
public class EditAddressActivityFragment extends BaseFragment implements
        View.OnClickListener, EditAddressSafePlaceFragment.SafePlaceListener {
    private static final String TAG = EditAddressActivityFragment.class.getSimpleName();
    private static final String EDIT_ADDRESS_FRAGMENT_TAG = "edit_address_fragment";

    public static final String BUNDLE_SINGLE_ADDRESS = "is_single_address";
    public static final String BUNDLE_IS_DEPOT_ADDRESS = "is_depot";
    public static final String BUNDLE_IS_ACCEPTED_RESPONSIBILITY = "is_accepted_responsibility";

    private ConsumerProperty consumerProperty;
    private TextView addressNameTextView;
    private Subscription getAddressPostCode;
    private Button btn_edit_basic;
    private Button btn_edit_safe_place;
    private Button btn_edit_neighbour;
    private Button btn_edit_pickup_shop;
    private Button btn_edit_attempt;
    private Button btn_delete_address;

    private boolean is_depot_address = false;

    /**
     * Set to the newly navigated Fragment so that we can validate before returning to the previous
     * Fragment on back/up
     */
    private Validating currentFragment;
    private ProgressDialog progressDialog;

    public static EditAddressActivityFragment newInstance() {
        final Bundle args = new Bundle();
        final EditAddressActivityFragment fragment = new EditAddressActivityFragment();
        fragment.setArguments(args);

        return fragment;
    }

    public EditAddressActivityFragment() {
        // required empty Fragment constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // store reference to the ConsumerProperty we are editing
        consumerProperty = user.selectedConsumerProperty;
        if (consumerProperty == null) {
            throw new IllegalStateException("ConsumerProfile.selectedConsumerProperty must not be null");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_edit_address_activity, container, false);
        addressNameTextView = (TextView) root.findViewById(R.id.address_name_textview);
        btn_edit_basic = (Button) root.findViewById(R.id.edit_basic_info_button);
        btn_edit_basic.setOnClickListener(this);
        btn_edit_safe_place = (Button) root.findViewById(R.id.edit_safe_place_details_button);
        btn_edit_safe_place.setOnClickListener(this);
        btn_edit_neighbour = (Button) root.findViewById(R.id.edit_neighbours_button);
        btn_edit_neighbour.setOnClickListener(this);
        btn_edit_pickup_shop = (Button) root.findViewById(R.id.edit_pickup_shop_button);
        btn_edit_pickup_shop.setOnClickListener(this);
        btn_edit_attempt = (Button) root.findViewById(R.id.edit_first_attempt_button);
        btn_edit_attempt.setOnClickListener(this);
        btn_delete_address = (Button) root.findViewById(R.id.delete_address_button);
        btn_delete_address.setOnClickListener(this);

        if (getArguments() != null) {
            if (getArguments().containsKey(BUNDLE_SINGLE_ADDRESS)) {
                boolean isSingleAddress = getArguments().getBoolean(BUNDLE_SINGLE_ADDRESS);
                if (isSingleAddress) {
                    root.findViewById(R.id.layout_delete_btn).setVisibility(View.GONE);
                } else {
                    root.findViewById(R.id.layout_delete_btn).setVisibility(View.VISIBLE);
                }
            }
        }

        // get post code to detect if it's postcode is iv274rx
        showProgressDialog();
        getAddressPostCode = api.getAddressByUDPRN(consumerProperty.consumerPropertyId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<GetAddressByUDPRNResponse>() {
                    @Override
                    public void call(GetAddressByUDPRNResponse response) {
                        hideProgressDialog();
                        if (response.hasErrors()) {
                            showErrorAlertDialog("Error occured");
                        }
                        String postcodeOfThisAddress = response.address.postcode.toUpperCase().replace(" ", "");
                        if (postcodeOfThisAddress.equals(getResources().getString(R.string.address_has_no_options))) {
                            hideEditAddressOptions();
                            is_depot_address = true;
                        } else {
                            if (consumerProperty.pickupLocationCode == null) {
                                hidePickupShopAndFirstAttempt();
                            }
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        showErrorAlertDialog(getString(R.string.a_problem_has_occurred_try_again));
                    }
                });
        return root;
    }

    private void hideEditAddressOptions() {
        btn_edit_attempt.setVisibility(View.GONE);
        btn_edit_neighbour.setVisibility(View.GONE);
        btn_edit_pickup_shop.setVisibility(View.GONE);
        btn_edit_safe_place.setVisibility(View.GONE);
    }

    private void hidePickupShopAndFirstAttempt() {
        btn_edit_attempt.setVisibility(View.GONE);
        btn_edit_pickup_shop.setVisibility(View.GONE);
    }

    @Override
    public void onResume() {
        super.onResume();
        getFragmentManager().addOnBackStackChangedListener(onBackStackChangedListener);

        // populate address label at top of EditAddressActivityFragment layouts
        final ConsumerPropertyAddressInfo addressInfo = user.getConsumerProfile().getConsumerPropertyAddressInfoWithId(consumerProperty.consumerPropertyId);
        addressNameTextView.setText(getString(R.string.address_name_preferences, addressInfo.addressFirstLine));
    }

    @Override
    public void onPause() {
        super.onPause();
        getFragmentManager().removeOnBackStackChangedListener(onBackStackChangedListener);
        if (getAddressPostCode != null && !getAddressPostCode.isUnsubscribed()) {
            getAddressPostCode.unsubscribe();
            getAddressPostCode = null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        consumerProperty = null;
    }

    private void showEditBasicAddressInfoFragment() {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        Fragment fragment = EditAddressBasicDetailsFragment.newInstance();
        Bundle bundle = new Bundle();
        bundle.putBoolean(BUNDLE_IS_DEPOT_ADDRESS, is_depot_address);
        fragment.setArguments(bundle);

        transaction.addToBackStack(null);
//        transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right);
        transaction.add(R.id.container, fragment);
        transaction.commit();
        currentFragment = (Validating) fragment;
    }

    private void showEditAddressSafePlaceFragment() {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        Fragment fragment = EditAddressSafePlaceFragment.newInstance();
        Bundle bundle = new Bundle();
        bundle.putBoolean(BUNDLE_IS_ACCEPTED_RESPONSIBILITY, user.isAcceptedResponsibility());
        fragment.setArguments(bundle);
        ((EditAddressSafePlaceFragment) fragment).setSafePlaceListener(this);

        transaction.addToBackStack(null);
//        transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right);
        transaction.add(R.id.container, fragment);
        transaction.commit();
        currentFragment = (Validating) fragment;
    }

    private void showEditAddressNeighboursFragment() {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        Fragment fragment = EditAddressNeighboursFragment.newInstance();

        transaction.addToBackStack(null);
//        transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right);
        transaction.add(R.id.container, fragment);
        transaction.commit();
        currentFragment = (Validating) fragment;
    }

    private void showEditAddressPickupShopFragment() {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        Fragment fragment = EditAddressPickupShopFragment.newInstance();

        transaction.addToBackStack(null);
//        transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left,  R.anim.slide_in_left, R.anim.slide_out_right);
        transaction.add(R.id.container, fragment);
        transaction.commit();
        currentFragment = (Validating) fragment;
    }

    private void showFirstAttemptPreferenceFragment() {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        Fragment fragment = EditFirstAttemptPreferenceFragment.newInstance();

        transaction.addToBackStack(null);
//        transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right);
        transaction.add(R.id.container, fragment, EDIT_ADDRESS_FRAGMENT_TAG);
        transaction.commit();
        currentFragment = (Validating) fragment;
    }

    private void promptDeleteAddress() {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(getActivity(), R.style.AlertDialogTheme);
        builder.setTitle(getString(R.string.delete_address_prompt_title));
        builder.setMessage(getString(R.string.delete_address_prompt_message));
        builder.setCancelable(true);
        builder.setNegativeButton(getString(R.string.cancel), null);
        builder.setPositiveButton(getString(R.string.delete), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteAddress();
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    private void deleteAddress() {
        final ConsumerProfile consumerProfile = user.getConsumerProfile();
        consumerProfile.removeConsumerPropertyWithId(consumerProperty.consumerPropertyId);

        showProgressDialog();
        user.putConsumerProfile(api, true)
                .subscribe(new Action1<PutConsumerResponse>() {
                               @Override
                               public void call(PutConsumerResponse response) {
                                   hideProgressDialog();
                                   if (response.hasErrors()) {
                                       showProblemDeletingAddressDialog();
                                   } else {
                                       getActivity().finish();
                                   }
                               }
                           },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                hideProgressDialog();
                                showProblemDeletingAddressDialog();
                            }
                        });
    }

    private void showProblemDeletingAddressDialog() {
        showErrorAlertWithRetryOption(getString(R.string.a_problem_has_occurred_try_again),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        deleteAddress();
                    }
                });
    }

    private FragmentManager.OnBackStackChangedListener onBackStackChangedListener =
            new FragmentManager.OnBackStackChangedListener() {
                @Override
                public void onBackStackChanged() {
                    if (getFragmentManager().getBackStackEntryCount() == 0) {
                        // reset App Bar title
                        getActivity().setTitle(getString(R.string.manage_address_titlecase));
                        // reset currentFragment so as not to attempt validation on back press
                        currentFragment = null;
                    }
                }
            };

    /**
     * Invoked by Activity when user wishes to navigate back from currentFragment
     *
     * @return True if inputs validate and changes have been applied to ConsumerProfile,
     * else false if validation failed
     */
    public boolean validateAndApplyChanges() {
        if (currentFragment == null) {
            return true;
        } else if (currentFragment.validate()) {
            currentFragment.applyChanges();
            return true;
        } else {
            return false;
        }
    }

    private void showProgressDialog() {
        hideProgressDialog();
        progressDialog = ProgressDialog.show(getActivity(), null, getString(R.string.please_wait_), true, false);
    }

    private void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
        progressDialog = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.edit_basic_info_button:
                showEditBasicAddressInfoFragment();
                break;
            case R.id.edit_safe_place_details_button:
                showEditAddressSafePlaceFragment();
                break;
            case R.id.edit_neighbours_button:
                showEditAddressNeighboursFragment();
                break;
            case R.id.edit_pickup_shop_button:
                showEditAddressPickupShopFragment();
                break;
            case R.id.edit_first_attempt_button:
                showFirstAttemptPreferenceFragment();
                break;
            case R.id.delete_address_button:
                promptDeleteAddress();
                break;
        }
    }

    @Override
    public void onAcceptedResponsibility(boolean accepted) {
        user.setAcceptedResponsibility(accepted);
    }
}
