package com.dpd.yourdpd.model;

/**
 * Represents an address associated with a Consumer (a Consumer being a DPD end user, DPD
 * refers to shippers as Customers and you and me as Consumers)
 */
public class ConsumerProperty {
    public Address selectedAddress;
    public String consumerPropertyId; // UDPRN
    public String propertyLabel; // Home, Work, or custom
    public String accessNotes;
    public boolean avoidRushHour;
    public String firstAttemptType; // H (home) or S (shop), default to H if not specified
    public boolean safeLocationAllow;
    public String safeLocationDescription;
    public String safeLocationPhotograph;
    public String safeLocationId; // comes from GetSafeLocations > safeLocationId
    public boolean neighbourAllow; // allow Any neighbour (false for Let Me Choose)
    public String neighbourPreferred; // UDPRN
    public String neighbourAvoid; // UDPRN
    public String pickupLocationCode;

    // e.g. properties
//    "consumerPropertyId":"00000001",
//    "propertyLabel":"4321",
//    "accessNotes":"Step access to rear",
//    "avoidRushHour":false,
//    "firstAttemptType":"Shop",
//    "safeLocationAllow":false,
//    "safeLocationDescription":"Round the back of the shed",
//    "safeLocationPhotograph":"4123",
//    "neighbourAllow":false,
//    "neighbourPreferred":"4123412",
//    "neighbourAvoid":"2341422",
//    "pickupLocationCode":"GB17426"
}
