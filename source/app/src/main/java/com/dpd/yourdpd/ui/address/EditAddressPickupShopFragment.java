package com.dpd.yourdpd.ui.address;

import android.content.Context;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.transitions.everywhere.AutoTransition;
import android.transitions.everywhere.TransitionManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.dpd.yourdpd.R;
import com.dpd.yourdpd.model.AddressPoint;
import com.dpd.yourdpd.model.PickupLocation;
import com.dpd.yourdpd.model.PickupLocationResult;
import com.dpd.yourdpd.model.UDPRNAddress;
import com.dpd.yourdpd.net.response.GetAddressByUDPRNResponse;
import com.dpd.yourdpd.net.response.GetPickupLocationsByAddressResponse;
import com.dpd.yourdpd.ui.GoogleApiClientActivity;
import com.dpd.yourdpd.ui.widget.CheckableImageView;
import com.dpd.yourdpd.util.GMapsUtils;
import com.dpd.yourdpd.util.MapMarkerUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.exceptions.OnErrorThrowable;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Allows the user to edit the Pickup Shop preferences for a given Address
 */
public class EditAddressPickupShopFragment extends EditAddressFragmentBase {
    @SuppressWarnings("unused")
    private static final String TAG = EditAddressPickupShopFragment.class.getSimpleName();

    private static final int PAGE_SIZE = 10;
    /**
     * Max number of pages that will be loaded in order to find the pre-selected Pickup Shop
     */
    private static final int MAX_PAGES_TO_LOAD = 10;

    private View noShopsMessage;
    private View loadMoreButton;
    private LinearLayout shopsContainer;
    private ArrayList<Object> pickupLocationViews;
    private ContentLoadingProgressBar progressBar;
    private View mapContainer;
    private SupportMapFragment mapFragment;
    private boolean isMapReady;
    private Location lastKnownLocation;
    private UDPRNAddress homeAddress;

    private boolean isSelectableLocationsResult = false;
    private List<PickupLocationResult> pickupLocationResults;
    private PickupLocationResult selectedLocationResult;
    private int page = 1;
    private Subscription getParcelPickupLocationsSubscription;

    public static EditAddressPickupShopFragment newInstance() {
        final Bundle args = new Bundle();
        EditAddressPickupShopFragment fragment = new EditAddressPickupShopFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public EditAddressPickupShopFragment() {
        // required empty Fragment constructor
    }

    @Override
    public void onDetach() {
        super.onDetach();
        final GoogleApiClientActivity activity = (GoogleApiClientActivity) getActivity();
        final GoogleApiClient googleApiClient = activity.getGoogleApiClient();
        googleApiClient.unregisterConnectionCallbacks(googleApiClientConnectionCallbacks);
        googleApiClient.unregisterConnectionFailedListener(googleApiClientConnectionFailedCallbacks);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pickupLocationResults = new ArrayList<>();
        pickupLocationViews = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_edit_address_pickup_shop, container, false);
        mapContainer = root.findViewById(R.id.map_container);
        noShopsMessage = root.findViewById(R.id.no_shops_message);
        loadMoreButton = root.findViewById(R.id.load_more_shops_button);
        loadMoreButton.setOnClickListener(onClickLoadMoreShops);
        shopsContainer = (LinearLayout) root.findViewById(R.id.shops_container);
        progressBar = (ContentLoadingProgressBar) root.findViewById(R.id.progressBar);
        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getLastKnownLocation();
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(getString(R.string.nearby_shops_titlecase));

        // YD-785: Only load shops at first.
        if (pickupLocationResults != null && pickupLocationResults.size() > 0)
            initMapView();
        else
            getPickupLocations();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (getParcelPickupLocationsSubscription != null && !getParcelPickupLocationsSubscription.isUnsubscribed()) {
            getParcelPickupLocationsSubscription.unsubscribe();
            getParcelPickupLocationsSubscription = null;
        }
    }

    private void getPickupLocations() {
        progressBar.setVisibility(View.VISIBLE);

        getParcelPickupLocationsSubscription =
                // find Address (via AddressLocation) for consumerPropertyId
                api.getAddressByUDPRN(consumerProperty.consumerPropertyId)
                        // grab user Address postcode to use for search
                        .flatMap(new Func1<GetAddressByUDPRNResponse, Observable<String>>() {
                            @Override
                            public Observable<String> call(GetAddressByUDPRNResponse response) {
                                if (response.hasErrors()) {
                                    Log.e(TAG, "getAddressByUDPRN error: " + response.printErrors());
                                    throw OnErrorThrowable.from(new Exception("getAddressByUDPRN error"));
                                }
                                setHomeAddress(response.address);
                                return Observable.just(response.address.postcode);
                            }
                        })
                        .flatMap(new Func1<String, Observable<GetPickupLocationsByAddressResponse>>() {
                            @Override
                            public Observable<GetPickupLocationsByAddressResponse> call(String postcode) {
                                final int maxDist = 30;
                                return api.getPickupLocationsByAddress(postcode, maxDist, page, PAGE_SIZE);
                            }
                        })
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                new Action1<GetPickupLocationsByAddressResponse>() {
                                    @Override
                                    public void call(GetPickupLocationsByAddressResponse response) {
                                        if (response.hasErrors()) {
                                            Log.e(TAG, "Error loading pickup locations " + response.printErrors());
                                            showErrorAlertDialog(getString(R.string.problem_getting_pickup_locations));
                                        } else {
                                            addPickupLocationResults(response.getPickupLocationResults());
                                        }
                                    }
                                },
                                new Action1<Throwable>() {
                                    @Override
                                    public void call(Throwable throwable) {
                                        Log.e(TAG, "Error loading pickup locations", throwable);
                                        showErrorAlertDialog(getString(R.string.problem_getting_pickup_locations));
                                    }
                                });

    }

    private void setHomeAddress(UDPRNAddress address) {
        homeAddress = address;
    }

    private GoogleApiClient.ConnectionCallbacks googleApiClientConnectionCallbacks =
            new GoogleApiClient.ConnectionCallbacks() {
                @Override
                public void onConnected(Bundle bundle) {
                    // try getting last known location again
                    getLastKnownLocation();
                }

                @Override
                public void onConnectionSuspended(int i) {
                }
            };
    private GoogleApiClient.OnConnectionFailedListener googleApiClientConnectionFailedCallbacks =
            new GoogleApiClient.OnConnectionFailedListener() {

                @Override
                public void onConnectionFailed(ConnectionResult connectionResult) {
                    // continue without knowing user's location
                }
            };


    private View.OnClickListener onClickLoadMoreShops = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            loadMoreShops();
        }
    };

    private void initMapView() {
        // add MapFragment
        if (mapFragment == null) {
            mapFragment = SupportMapFragment.newInstance();
        }
        if (!mapFragment.isAdded()) {
            final FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.map_container, mapFragment);
            fragmentTransaction.commit();
            mapFragment.getMapAsync(onMapReadyCallback);
        }
    }

    private OnMapReadyCallback onMapReadyCallback = new OnMapReadyCallback() {
        @Override
        public void onMapReady(GoogleMap map) {
            isMapReady = true;
            if (isAdded() && !getActivity().isFinishing()) {
                // allow Map to layout on slower devices
                mapContainer.post(new Runnable() {
                    @Override
                    public void run() {
                        addMapMarkers(pickupLocationResults, 0);
                    }
                });
            }
        }
    };

    private void addMapMarkers(List<PickupLocationResult> results, int indexOffset) {
        if (pickupLocationResults == null) {
            return;
        }

        final Context context = getContext();
        final GoogleMap map = mapFragment.getMap();
        final LatLng[] latLngs = new LatLng[results.size() + 1];

        int markerWidth = 0, markerHeight = 0;
        for (int i = 0; i < results.size(); i++) {
            PickupLocationResult result = results.get(i);
            final PickupLocation location = result.pickupLocation;
            final Bitmap markerBitmap =
                    MapMarkerUtils.getBitmapForMarker(context,
                            R.drawable.ic_location_marker,
                            String.valueOf(i + indexOffset + 1),
                            0xffffffff);

            if (markerHeight == 0 && markerBitmap != null) {
                markerWidth = markerBitmap.getWidth();
                markerHeight = markerBitmap.getHeight();
            }

            final AddressPoint addressPoint = location.addressPoint;
            final LatLng latLng = addressPoint.toLatLng();
            map.addMarker(new MarkerOptions()
                            .icon(BitmapDescriptorFactory.fromBitmap(markerBitmap))
                            .position(latLng)
                            .anchor(0.5f, 0.75f)
                            .title(location.getName())
            );
            latLngs[i] = latLng;
        }

        // Add home marker
        if (indexOffset == 0 && homeAddress != null) {
            try {
                LatLng houseLatLng = new LatLng(Double.parseDouble(homeAddress.udprnLatitude), Double.parseDouble(homeAddress.udprnLongitude));
                map.addMarker(new MarkerOptions()
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin_house))
                        .position(houseLatLng)
                        .alpha(1f)
                        .anchor(0.5f, 1.0f)
                        .title(getString(R.string.your_address)));
                latLngs[latLngs.length - 1] = houseLatLng;
            } catch (Exception ignored) {
            }
        }

        final int markerWidth1 = markerWidth;
        final int markerHeight1 = markerHeight;
        map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                GMapsUtils.zoomWithPadding(mapFragment, latLngs, 20, markerWidth1, markerHeight1);
            }
        });
        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                GMapsUtils.zoomWithPadding(mapFragment, latLngs, 20, markerWidth1, markerHeight1, true);
            }
        });
        map.getUiSettings().setAllGesturesEnabled(false);
    }

    private void addPickupLocationResults(List<PickupLocationResult> results) {
        if (results == null) {
            loadMoreButton.setVisibility(View.GONE);
            isSelectableLocationsResult = false;
            return;
        }
        isSelectableLocationsResult = true;
        final int previousLocationCount = pickupLocationResults.size();
        for (PickupLocationResult result : results) {
            addPickupLocationResult(result);
        }
        if (isMapReady) {
            // show new markers
            addMapMarkers(results, previousLocationCount);
        } else {
            initMapView();
        }
        progressBar.hide();

        /*if(results.size()<PAGE_SIZE) {
            loadMoreButton.setVisibility(View.GONE);
        } else {
            loadMoreButton.setVisibility(View.VISIBLE);
        }*/

        if (results.size() == 0) {
            mapContainer.setVisibility(View.GONE);
            noShopsMessage.setVisibility(View.VISIBLE);
            shopsContainer.setVisibility(View.GONE);
            setSelectedLocationResult(null);
        } else {
            mapContainer.setVisibility(View.VISIBLE);
            noShopsMessage.setVisibility(View.GONE);
            shopsContainer.setVisibility(View.VISIBLE);

            selectUsersPickupShop();
        }
    }

    /**
     * Select the user's current choice in the UI (loading more shops if not found yet, up to a limit)
     */
    private void selectUsersPickupShop() {
        PickupLocationResult resultToSelect = null;
        for (int i = 0; i < pickupLocationResults.size(); i++) {
            final PickupLocationResult result = pickupLocationResults.get(i);
            try {
                final String pickupLocationCode = result.pickupLocation.pickupLocationCode;
                if (pickupLocationCode.equals(consumerProperty.pickupLocationCode)) {
                    resultToSelect = result;
                    break;
                }
            } catch (Exception e) {
                Crashlytics.logException(e);
            }
        }
        if (resultToSelect != null) {
            setSelectedLocationResult(resultToSelect);
        } else {
            if (page < MAX_PAGES_TO_LOAD) {
                loadMoreShops();
            }
        }
    }

    private PickupLocationResult getSelectedLocationResult() {
        View view = shopsContainer.findViewWithTag(selectedLocationResult);
        if (view != null) {
            return (PickupLocationResult) view.getTag();
        }
        return null;
    }

    private void loadMoreShops() {
        page++;
        getPickupLocations();
    }

    private View.OnClickListener shopViewOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            final PickupLocationResult pickupLocationResult = (PickupLocationResult) v.getTag();
            setSelectedLocationResult(pickupLocationResult);
        }
    };

    private void addPickupLocationResult(final PickupLocationResult pickupLocationResult) {
        final PickupLocation location = pickupLocationResult.pickupLocation;
        final int index = pickupLocationViews.size();
        final Context context = getContext();
        final LayoutInflater inflater = LayoutInflater.from(context);
        final ViewGroup view = (ViewGroup) inflater.inflate(R.layout.item_pickup_location, shopsContainer, false);
        final TextView indexTextView = (TextView) view.findViewById(R.id.index_textview);
        final TextView titleTextView = (TextView) view.findViewById(R.id.title_textview);
        final TextView addressTextView = (TextView) view.findViewById(R.id.address_textview);
        final TextView distanceTextView = (TextView) view.findViewById(R.id.distance_textview);
        final Button openingHoursButton = (Button) view.findViewById(R.id.opening_hours_button);

        // TODO: do we not want to calculate this based on user's current location?
        // discussion was that we could move to using GPS for greater accuracy, but for now keep it
        // the same as the website (i.e. using pickupLocation.distance)
//        final float distance = GMapsUtils.getDistanceFromLocation(locationDetails.latLng, lastKnownLocation);

        final String addressText = location.address.getStreetAndTown();
        final String distanceText = (pickupLocationResult.distance > -1 ? getString(R.string.x_miles_away, pickupLocationResult.distance) : "");
        indexTextView.setText(String.valueOf(index + 1));
        titleTextView.setText(location.getName());
        addressTextView.setText(addressText);
        distanceTextView.setText(distanceText);

        // show relevant icons for this location
        view.findViewById(R.id.ic_disabled_access).setVisibility(location.disabledAccess ? View.VISIBLE : View.GONE);
        view.findViewById(R.id.ic_parking).setVisibility(location.parkingAvailable ? View.VISIBLE : View.GONE);
        view.findViewById(R.id.ic_open_late).setVisibility(location.openLate ? View.VISIBLE : View.GONE);
        view.findViewById(R.id.ic_open_saturday).setVisibility(location.openSaturday ? View.VISIBLE : View.GONE);
        view.findViewById(R.id.ic_open_sunday).setVisibility(location.openSunday ? View.VISIBLE : View.GONE);

        // add opening hours
        final ViewGroup openingHoursContainer = (ViewGroup) view.findViewById(R.id.opening_hours_container);
        try {
            final PickupLocation.ParcelLocationAvailability availability = location.pickupLocationAvailability;
            final List<PickupLocation.PickupLocationOpenWindow> openWindows = availability.pickupLocationOpenWindow;

            final String[] daysOfWeek = getResources().getStringArray(R.array.days_of_week);
            final List<String> days = new ArrayList<>(7);
            final List<String> hours = new ArrayList<>(7);
            int i = 0;
            int prevDay = -1;
            PickupLocation.PickupLocationOpenWindow prevWindow = null;

            for (PickupLocation.PickupLocationOpenWindow window : openWindows) {
                final int day = window.pickupLocationOpenWindowDay;
                final String slot = window.pickupLocationOpenWindowStartTime + " - " + window.pickupLocationOpenWindowEndTime;
                if (day != prevDay) {
                    // new day
                    final String dayOfWeek = daysOfWeek[day - 1];
                    days.add(dayOfWeek);
                    hours.add(slot);
                    i++;
                } else {
                    // see if window start time matches prev window close time and merge the hours
                    if (prevWindow != null && prevWindow.pickupLocationOpenWindowEndTime.equals(window.pickupLocationOpenWindowStartTime)) {
                        // merge OpenWindows together into a single slot
                        // so 10:00-12:00 and 12:00-22:00 -> 10:00-22:00
                        hours.set(i - 1, prevWindow.pickupLocationOpenWindowStartTime + " - " + window.pickupLocationOpenWindowEndTime);
                    } else {
                        // append second slot to end of hours
                        hours.set(i - 1, hours.get(i - 1) + "/" + slot);
                    }
                }

                prevWindow = window;
                prevDay = day;
            }

            for (int j = 0; j < days.size(); j++) {
                final String dayOfWeek = days.get(j);
                final String openingHours = hours.get(j);
                final View row = inflater.inflate(R.layout.item_opening_hour, openingHoursContainer, false);
                final TextView dayTextView = (TextView) row.findViewById(R.id.day_textview);
                final TextView hoursTextView = (TextView) row.findViewById(R.id.hours_textview);

                dayTextView.setText(dayOfWeek);
                hoursTextView.setText(openingHours);

                // insert row before divider
                final int childCount = openingHoursContainer.getChildCount();
                openingHoursContainer.addView(row, childCount - 1, new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT));
            }
        } catch (Exception e) {
            Log.e(TAG, "Problem formatting opening times", e);
        }

        openingHoursButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AutoTransition transition = new AutoTransition(getContext(), null);
                TransitionManager.beginDelayedTransition(view, transition);
                openingHoursContainer.setVisibility(openingHoursContainer.getVisibility() ==
                        View.VISIBLE ? View.GONE : View.VISIBLE);
                openingHoursButton.setText(openingHoursContainer.getVisibility() == View.VISIBLE ?
                        getResources().getString(R.string.hide_opening_hours) :
                        getResources().getString(R.string.show_opening_hours));
            }
        });

        view.setOnClickListener(shopViewOnClickListener);
        view.setTag(pickupLocationResult);

        shopsContainer.addView(view, shopsContainer.getChildCount() - 1);
        pickupLocationViews.add(view);
        pickupLocationResults.add(pickupLocationResult);
    }

    private void getLastKnownLocation() {
        final GoogleApiClientActivity activity = (GoogleApiClientActivity) getActivity();
        final GoogleApiClient googleApiClient = activity.getGoogleApiClient();

        if (googleApiClient.hasConnectedApi(LocationServices.API)) {
            lastKnownLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
            // reload data?
        } else {
            // wait for GoogleApiClient to connect or fail before continuing
            googleApiClient.registerConnectionCallbacks(googleApiClientConnectionCallbacks);
            googleApiClient.registerConnectionFailedListener(googleApiClientConnectionFailedCallbacks);
        }
    }

    private void setSelectedLocationResult(PickupLocationResult locationResult) {
        if (selectedLocationResult != null && !selectedLocationResult.equals(locationResult)) {
            final View view = shopsContainer.findViewWithTag(selectedLocationResult);
            final CheckableImageView radioImageView = (CheckableImageView) view.findViewById(R.id.radio);
            radioImageView.setChecked(false);
        }
        if (locationResult != null) {
            selectedLocationResult = locationResult;
            final View view = shopsContainer.findViewWithTag(locationResult);
            final CheckableImageView radioImageView = (CheckableImageView) view.findViewById(R.id.radio);
            radioImageView.setChecked(true);
        }
    }

    @Override
    public boolean validate() {
        final PickupLocationResult selectedLocationResult = getSelectedLocationResult();
        if (isSelectableLocationsResult) {
            if (selectedLocationResult == null) {
                showErrorAlertWithDiscardOption(getString(R.string.please_choose_a_pickup_shop));
                return false;
            }
        }
        return true;
    }

    @Override
    public void applyChanges() {
        if (pickupLocationResults == null || pickupLocationResults.size() == 0) {
            // still loading, no changes to apply
            return;
        }
        final PickupLocationResult selectedLocationResult = getSelectedLocationResult();
        assert selectedLocationResult != null;
        consumerProperty.pickupLocationCode = selectedLocationResult.pickupLocation.pickupLocationCode;
    }
}
