package com.dpd.yourdpd.net.response;

import com.dpd.yourdpd.model.SafeLocation;
import com.dpd.yourdpd.net.Api;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @see Api#getSafeLocations()
 */
public class SafeLocationsResponse extends BaseResponse {
    @SerializedName("data")
    private List<SafeLocation> safeLocations;

    public List<SafeLocation> getSafeLocations() {
        return safeLocations;
    }
}
