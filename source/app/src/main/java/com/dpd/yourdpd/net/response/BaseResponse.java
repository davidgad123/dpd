package com.dpd.yourdpd.net.response;

import com.dpd.yourdpd.net.APIError;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Base class for API responses that includes error(s) field and associated utility methods
 */
public class BaseResponse {
    @SerializedName("error")
    private APIErrorsWrapper errorsWrapper;

    public List<APIError> getErrors() {
        return errorsWrapper.errors;
    }

    public boolean hasErrors() {
        return (errorsWrapper != null && errorsWrapper.errors != null && errorsWrapper.errors.size() > 0);
    }

    public String printErrors() {
        if (!hasErrors()) return "";
        StringBuilder builder = new StringBuilder();
        for (APIError error : errorsWrapper.errors) {
            builder.append(error.toString());
        }
        return builder.toString();
    }

    /**
     * Wrapper required to parse variations in JSON (error property can either be a single object,
     * an array of error objects, an empty string, or null).
     */
    public static class APIErrorsWrapper {
        public List<APIError> errors;
    }
}
