package com.dpd.yourdpd;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Defines a per-Application scope (one that lives as long as the Application)
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface PerApplication {
}