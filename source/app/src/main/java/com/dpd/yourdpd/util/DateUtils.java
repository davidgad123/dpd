package com.dpd.yourdpd.util;

import android.util.Log;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateUtils {
    private static final String TAG = DateUtils.class.getSimpleName();
    private static DateTimeFormatter sISOWithoutMillisOrOffset = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss");
    private static DateTimeFormatter sDateAndTimeFormat = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");

    public static Date parseISO8601DateString(String s) {
        Date d;
        try {
            DateTime dateTime = ISODateTimeFormat.dateTimeParser()
                    .withOffsetParsed()
                    .parseDateTime(s);
            d = dateTime.toDate();
            return d;
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            return null;
        }
    }

    public static Date getDate(String dateStr, String srcFormat) {
        SimpleDateFormat sourceFormatter;
        Date date = new Date();
        if (srcFormat == null)
            sourceFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.UK);
        else
            sourceFormatter = new SimpleDateFormat(srcFormat, Locale.UK);

        try {
            date = sourceFormatter.parse(dateStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String dateToUTCISO8601String(Date d) {
        DateTime utc = new DateTime(d, DateTimeZone.forTimeZone(TimeZone.getTimeZone("UTC")));
        return sISOWithoutMillisOrOffset.print(utc);
    }

    public static String dateToUTCISO8601String(Date d, boolean includeMillisecondsAndOffset) {
        if (includeMillisecondsAndOffset)
            return new DateTime(d, DateTimeZone.forTimeZone(TimeZone.getTimeZone("UTC"))).toString();
        else return dateToUTCISO8601String(d);
    }

    public static String dateToDateTimeFormat(Date d) {
        return sDateAndTimeFormat.print(d.getTime());
    }

    public static String getDayOfMonthSuffix(final int n) {
        if (n < 1 || n > 31) {
            throw new IllegalArgumentException("Illegal day of month: " + n);
        }
        if (n >= 11 && n <= 13) {
            return "th";
        }
        switch (n % 10) {
            case 1:
                return "st";
            case 2:
                return "nd";
            case 3:
                return "rd";
            default:
                return "th";
        }
    }

    public static String getDateFromNextWeekDay(int weekDay) {
        Calendar current = Calendar.getInstance();
        int fromToday = Math.abs(weekDay - current.get(Calendar.DAY_OF_WEEK));
        current.add(Calendar.DAY_OF_WEEK, fromToday);
        Date date = current.getTime();
        return dateToUTCISO8601String(date, true);
    }

    public static String getWeekDayStringFromToday(int fromToday) {
        Calendar current = Calendar.getInstance();
        current.add(Calendar.DAY_OF_WEEK, fromToday);
        Date date = current.getTime();
        return (new SimpleDateFormat("EEEE", Locale.ENGLISH)).format(date.getTime());
    }

    public static String getDayStringFromMonday(int ind) {
        switch (ind) {
            case 0:
                return "Monday";
            case 1:
                return "Tuesday";
            case 2:
                return "Wednesday";
            case 3:
                return "Thursday";
            case 4:
                return "Friday";
            case 5:
                return "Saturday";
            case 6:
                return "Sunday";
            default:
                return "";

        }
    }
}
