package com.dpd.yourdpd.net;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import com.dpd.yourdpd.BuildConfig;
import com.dpd.yourdpd.PerApplication;
import com.dpd.yourdpd.model.Parcel;
import com.dpd.yourdpd.model.User;
import com.dpd.yourdpd.net.response.BaseResponse;
import com.dpd.yourdpd.ui.LoadingActivity;
import com.dpd.yourdpd.util.AppConstant;
import com.dpd.yourdpd.util.CommonUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;
import com.squareup.okhttp.OkHttpClient;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import dagger.Module;
import dagger.Provides;
import retrofit.Profiler;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;

/**
 * Provides a configured {@link Api} instance which facilitates remote API calls
 */
@Module
public class ApiModule {

    private static final String TAG = ApiModule.class.getSimpleName();

    private final Context context;

    public ApiModule(Context context) {
        this.context = context;
    }

    @Provides
    @PerApplication
    Api provideApiService(User user) {
        return buildApiService(context, user);
    }

    private static Api buildApiService(final Context context, final User user) {
        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                request.addHeader("Accept", "application/json");
                request.addHeader("Content-Type", "application/json");
                if (user.getDpdSession() != null) {
                    request.addHeader("dpdsession", user.getDpdSession());
                }
                request.addHeader("dpdclient", BuildConfig.DPD_CLIENT);
            }
        };
        final GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder
                .serializeNulls() // enabling this can cause widespread complications
                .registerTypeAdapter(BaseResponse.APIErrorsWrapper.class, new ErrorsDeserializer())
                .registerTypeAdapter(Date.class, new DateDeserializer())
                .registerTypeAdapter(Parcel.ParcelStatusType.class, new ParcelStatusTypeDeserializer())
                .registerTypeAdapter(Parcel.RelatedParcels.class, new RelatedParcelsDeserializer())
                .registerTypeAdapter(Parcel.CallingCardNumbers.class, new CallingCardNumberDeserializer());

        final OkHttpClient okClient = new OkHttpClient();
        okClient.setConnectTimeout(15, TimeUnit.SECONDS); // connect timeout
        okClient.setReadTimeout(30, TimeUnit.SECONDS);    // socket timeout

        final GsonConverter gsonConverter = new GsonConverter(gsonBuilder.create());
        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setEndpoint(BuildConfig.SERVER_URL)
                .setConverter(gsonConverter)
                .setClient(new OkClient(okClient))
                .setRequestInterceptor(requestInterceptor);

        if (BuildConfig.DEBUG) {
            builder.setProfiler(new Profiler() {
                @Override
                public Object beforeCall() {
                    return null;
                }

                @Override
                public void afterCall(RequestInformation requestInfo, long elapsedTime, int statusCode, Object beforeCallData) {
                    Log.d("Retrofit Profiler", String.format("HTTP %d %s %s (%dms)",
                            statusCode, requestInfo.getMethod(), requestInfo.getRelativePath(), elapsedTime));
                }
            });
        }

        if (BuildConfig.USE_MOCK_DATA) {
            builder.setClient(new MockClient(context, new OkClient()));
        }

        builder.setErrorHandler(new ErrorHandler(user, context));
        final RestAdapter restAdapter = builder.build();
        if (BuildConfig.DEBUG) {
            restAdapter.setLogLevel(RestAdapter.LogLevel.FULL);
        }
        return restAdapter.create(Api.class);
    }

    private static class ErrorHandler implements retrofit.ErrorHandler {
        private User user;
        private Context context;

        ErrorHandler(User user, Context context) {
            this.user = user;
            this.context = context;
        }

        @Override
        public Throwable handleError(RetrofitError cause) {
            try {
//                if (BuildConfig.LOG_ERRORS_TO_SERVER) {
//                    Crashlytics.logException(cause);
//                }

                Response r = cause.getResponse();
                if (r != null && r.getStatus() == 401) {
                    if (!user.hasAuthenticatedSession()) {
                        Log.e(TAG, "User is not logged in");
                        return cause;
                    }

                    Log.e(TAG, "Server responded with 401");
                    Intent intent = new Intent(context, LoadingActivity.class);
                    intent.putExtra(AppConstant.BUNDLE_NO_AUTHORIZED_RESPONSE, false);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);

//                    Log.e(TAG, "Server responded with 401, logging out...");
//                    user.logout();

//                    // could return a custom Exception here instead of RetrofitError
//                    return new UnauthorizedException(cause);

                } else if (!CommonUtils.isNetworkAvailable(context)) {
                    Intent intent = new Intent(context, LoadingActivity.class);
                    intent.putExtra(AppConstant.BUNDLE_NO_CONNECTION_RESPONSE, false);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            } catch (Exception ignored) {
            }
            return cause;
        }
    }

    /**
     * Deserializer to handle .error in JSON responses (which can be array or single error object)
     */
    private static class ErrorsDeserializer
            implements JsonDeserializer<BaseResponse.APIErrorsWrapper> {
        @Override
        public BaseResponse.APIErrorsWrapper deserialize(
                JsonElement json, Type typeOfT, JsonDeserializationContext context)
                throws JsonParseException {
            BaseResponse.APIErrorsWrapper wrapper = new BaseResponse.APIErrorsWrapper();
            wrapper.errors = new ArrayList<>();
            Gson gson = new Gson();

            if (json.isJsonArray()) {
                Type listType = new TypeToken<List<APIError>>() {
                }.getType();
                wrapper.errors = gson.fromJson(json, listType);
            } else {
                wrapper.errors.add(gson.fromJson(json, APIError.class));
            }
            return wrapper;
        }
    }

    /**
     * Deserializer to handle {@link com.dpd.yourdpd.model.Parcel.ParcelStatusType} enum
     */
    private static class ParcelStatusTypeDeserializer
            implements JsonDeserializer<Parcel.ParcelStatusType> {
        @Override
        public Parcel.ParcelStatusType deserialize(
                JsonElement json, Type typeOfT, JsonDeserializationContext context)
                throws JsonParseException {
            return Parcel.ParcelStatusType.fromValue(json.getAsInt());
        }
    }

    /**
     * Deserializer to handle inconsistent JSON for CallingCardNumber
     */
    private static class CallingCardNumberDeserializer
            implements JsonDeserializer<Parcel.CallingCardNumbers> {
        @Override
        public Parcel.CallingCardNumbers deserialize(
                JsonElement json, Type typeOfT, JsonDeserializationContext context)
                throws JsonParseException {
            Parcel.CallingCardNumbers callingCardNumber = new Parcel.CallingCardNumbers();
            if (json.isJsonArray()) {
                JsonArray jsonArray = json.getAsJsonArray();
                Gson gson = new Gson();
                if (jsonArray.size() == 0 || jsonArray.get(0).isJsonPrimitive()) {
                    // i.e. they've just passed [ "" ]
                    callingCardNumber.numbers = new ArrayList<>();
                } else {
                    // parse numbers
                    Type listType = new TypeToken<List<String>>() {
                    }.getType();
                    callingCardNumber.numbers = gson.fromJson(json, listType);
                }
            } else {
                callingCardNumber.numbers = new ArrayList<>();
            }
            return callingCardNumber;
        }
    }

    /**
     * Deserializer to handle inconsistent JSON for Parcel.relatedParcels
     * NOTE: API likes to return nothing, a single string parcel code, or an array of parcel codes
     */
    private static class RelatedParcelsDeserializer
            implements JsonDeserializer<Parcel.RelatedParcels> {
        @Override
        public Parcel.RelatedParcels deserialize(
                JsonElement json, Type typeOfT, JsonDeserializationContext context)
                throws JsonParseException {
            Parcel.RelatedParcels relatedParcels = new Parcel.RelatedParcels();
            relatedParcels.parcelCodes = new ArrayList<>(1);
            Gson gson = new Gson();
            if (json.isJsonArray()) {
                Type listType = new TypeToken<List<String>>() {
                }.getType();
                relatedParcels.parcelCodes = gson.fromJson(json, listType);
            } else {
                relatedParcels.parcelCodes.add(gson.fromJson(json, String.class));
            }
            return relatedParcels;
        }
    }

    /**
     * Attempts several date time formats to parse a Date string, assumes GMT timezone
     * unless date string is in ISO8601 format
     */
    private static class DateDeserializer implements JsonDeserializer<Date> {
        private static final String TAG = DateDeserializer.class.getSimpleName();

        private static DateTimeFormatter sISOWithoutMillisOrOffset = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss");
        private static DateTimeFormatter sDateAndTimeFormat = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        private static DateTimeFormatter sDateOnlyFormat = DateTimeFormat.forPattern("yyyy-MM-dd");
        private static DateTimeFormatter sDateOnlySlashedFormat = DateTimeFormat.forPattern("dd/MM/yyyy");

        private static DateTimeZone gmtTimezone = DateTimeZone.forTimeZone(TimeZone.getTimeZone("GMT"));

        @Override
        public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
                throws JsonParseException {

            final String s = json.getAsJsonPrimitive().getAsString();
            if (TextUtils.isEmpty(s)) {
                return null;
            }
            // try parse via stock DateFormat class
            try {
                DateFormat dateFormat = DateFormat.getInstance();
                dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
                return dateFormat.parse(s);
            } catch (ParseException e) {
                Log.v(TAG, e.toString());
            }
            // try parse yyyy-mm-dd format
            try {
                DateTime dateTime = sDateOnlyFormat.withZone(gmtTimezone).parseDateTime(s);
                return dateTime.toDate();
            } catch (Exception e) {
                Log.v(TAG, e.toString());
            }
            // try parse yyyy-mm-dd hh:mm:ss format
            try {
                DateTime dateTime = sDateAndTimeFormat.withZone(gmtTimezone).parseDateTime(s);
                return dateTime.toDate();
            } catch (Exception e) {
                Log.v(TAG, e.toString());
            }
            // try parse dd/MM/yyyy format
            try {
                DateTime dateTime = sDateOnlySlashedFormat.withZone(gmtTimezone).parseDateTime(s);
                return dateTime.toDate();
            } catch (Exception e) {
                Log.v(TAG, e.toString());
            }
            // try parse ISO date (not that we expect them from this API)
            try {
                DateTime dateTime = ISODateTimeFormat.dateTimeParser()
                        .withOffsetParsed()
                        .parseDateTime(s);
                return dateTime.toDate();
            } catch (Exception e) {
                Log.e(TAG, e.toString());
            }
            return null;
        }
    }
}
