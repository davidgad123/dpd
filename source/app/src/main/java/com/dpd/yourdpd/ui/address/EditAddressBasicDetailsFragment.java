package com.dpd.yourdpd.ui.address;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.transitions.everywhere.TransitionManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.ToggleButton;

import com.dpd.yourdpd.R;
import com.dpd.yourdpd.model.ConsumerProperty;
import com.dpd.yourdpd.model.ConsumerPropertyLabel;
import com.dpd.yourdpd.util.CommonUtils;

/**
 * Allows for editing of certain basic ConsumerProperty details
 */
public class EditAddressBasicDetailsFragment extends EditAddressFragmentBase {
    private RadioButton homeRadioButton;
    private RadioButton workRadioButton;
    private RadioButton otherRadioButton;
    private LinearLayout otherAddressDetailsContainer;
    private EditText otherAddressNameEditText;
    private EditText addressNotesEditText;
    private ToggleButton avoidRushHourYesButton;
    private ToggleButton avoidRushHourNoButton;
    private ConsumerProperty consumerProperty;
    private LinearLayout addressNoteforDriver;

    public EditAddressBasicDetailsFragment() {
        // required empty Fragment constructor
    }

    public static Fragment newInstance() {
        Bundle args = new Bundle();
        EditAddressBasicDetailsFragment fragment = new EditAddressBasicDetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // store reference to the ConsumerProperty we are editing
        consumerProperty = user.selectedConsumerProperty;
        if (consumerProperty == null) {
            throw new IllegalStateException("ConsumerProfile.selectedConsumerProperty must not be null");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.fragment_edit_address_basic_details, container, false);

        homeRadioButton = (RadioButton) root.findViewById(R.id.home_radiobutton);
        workRadioButton = ((RadioButton) root.findViewById(R.id.work_radiobutton));
        otherRadioButton = ((RadioButton) root.findViewById(R.id.other_radiobutton));
        homeRadioButton.setText(getString(R.string.home));
        workRadioButton.setText(getString(R.string.work));
        otherRadioButton.setText(getString(R.string.other));
        homeRadioButton.setOnCheckedChangeListener(onChangeAddressType);
        workRadioButton.setOnCheckedChangeListener(onChangeAddressType);
        otherRadioButton.setOnCheckedChangeListener(onChangeAddressType);
        otherAddressDetailsContainer = (LinearLayout) root.findViewById(R.id.other_address_details_container);
        otherAddressNameEditText = (EditText) root.findViewById(R.id.other_address_name_edittext);
        avoidRushHourYesButton = (ToggleButton) root.findViewById(R.id.avoid_rush_hour_yes_button);
        avoidRushHourNoButton = (ToggleButton) root.findViewById(R.id.avoid_rush_hour_no_button);
        addressNoteforDriver = (LinearLayout) root.findViewById(R.id.layout_driver_note);
        avoidRushHourYesButton.setOnCheckedChangeListener(onAvoidRushCheckChangeListener);
        avoidRushHourNoButton.setOnCheckedChangeListener(onAvoidRushCheckChangeListener);
        addressNotesEditText = (EditText) root.findViewById(R.id.driver_notes_edittext);
        addressNotesEditText.setHorizontallyScrolling(false);
        addressNotesEditText.setMaxLines(Integer.MAX_VALUE);

        if (getArguments() != null) {
            boolean is_depot = getArguments().getBoolean(EditAddressActivityFragment.BUNDLE_IS_DEPOT_ADDRESS, false);
            if (is_depot) {
                addressNoteforDriver.setVisibility(View.GONE);
            }
        }

        CommonUtils.setupUIForKeyboard(getActivity(), root);

        return root;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        consumerProperty = null;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (consumerProperty.avoidRushHour) {
            avoidRushHourYesButton.setChecked(true);
            avoidRushHourNoButton.setChecked(false);
        } else {
            avoidRushHourYesButton.setChecked(false);
            avoidRushHourNoButton.setChecked(true);
        }

        if (ConsumerPropertyLabel.HOME.equals(consumerProperty.propertyLabel)) {
            homeRadioButton.setChecked(true);
        } else if (ConsumerPropertyLabel.WORK.equals(consumerProperty.propertyLabel)) {
            workRadioButton.setChecked(true);
        } else {
            otherAddressDetailsContainer.setVisibility(View.VISIBLE);
            otherRadioButton.setChecked(true);
            otherAddressNameEditText.setText(consumerProperty.propertyLabel);
        }

        addressNotesEditText.setText(consumerProperty.accessNotes);
    }

    private CompoundButton.OnCheckedChangeListener onChangeAddressType =
            new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (buttonView.getId() == R.id.other_radiobutton) {
                        TransitionManager.beginDelayedTransition((ViewGroup) getView());
                        otherAddressDetailsContainer.setVisibility(isChecked ? View.VISIBLE : View.GONE);
                    }
                }
            };

    private CompoundButton.OnCheckedChangeListener onAvoidRushCheckChangeListener =
            new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (!isChecked) {
                        // prevent un-checking
                        buttonView.setChecked(true);
                        return;
                    }
                    switch (buttonView.getId()) {
                        case R.id.avoid_rush_hour_yes_button:
                            avoidRushHourNoButton.setOnCheckedChangeListener(null);
                            avoidRushHourNoButton.setChecked(false);
                            avoidRushHourNoButton.setOnCheckedChangeListener(this);
                            break;
                        case R.id.avoid_rush_hour_no_button:
                            avoidRushHourYesButton.setOnCheckedChangeListener(null);
                            avoidRushHourYesButton.setChecked(false);
                            avoidRushHourYesButton.setOnCheckedChangeListener(this);
                            break;
                    }
                }
            };

    /**
     * @return Work, Home or "other" depending on selected radio button
     */
    private String getPropertyLabel() {
        if (homeRadioButton.isChecked()) return ConsumerPropertyLabel.HOME;
        else if (workRadioButton.isChecked()) return ConsumerPropertyLabel.WORK;
        else if (otherRadioButton.isChecked()) return otherAddressNameEditText.getText().toString();
        else return null;
    }

    @Override
    public boolean validate() {
        if (otherRadioButton.isChecked() && TextUtils.isEmpty(otherAddressNameEditText.getText())) {
            showErrorAlertWithDiscardOption(getString(R.string.please_specify_other_address));
            return false;
        } /*else if (TextUtils.isEmpty(addressNotesEditText.getText())) {
            showErrorAlertWithDiscardOption(getString(R.string.please_enter_note_for_driver));
            return false;
        }*/
        return true;
    }

    @Override
    public void applyChanges() {
        consumerProperty.accessNotes = addressNotesEditText.getText().toString().trim();
        consumerProperty.avoidRushHour = avoidRushHourYesButton.isChecked();
        consumerProperty.propertyLabel = getPropertyLabel();
    }
}
