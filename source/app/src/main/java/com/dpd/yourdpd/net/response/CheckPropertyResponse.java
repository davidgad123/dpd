package com.dpd.yourdpd.net.response;


import java.util.List;

/**
 * @see com.dpd.yourdpd.net.Api#checkProperty(String)
 */
public class CheckPropertyResponse extends BaseResponse {

    private Data data;

    private class Data {
        public List<Depot> depots;
    }

    public class Depot {
        public String depotCode;
        public int businessUnit;
        public boolean agencyDepot;
    }

    public List<Depot> getDepots() {
        return (data==null) ? null : data.depots;
    }

    public boolean hasDpdDepot() {
        if(data==null || data.depots==null || data.depots.size()==0) {
            return false;
        } else {
            for (Depot depot : data.depots) {
                if(!depot.agencyDepot) return true;
            }
            return false;
        }
    }
}
