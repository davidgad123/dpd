package com.dpd.yourdpd;

import com.dpd.yourdpd.net.response.GetParcelHistoryResponse;

import java.util.List;

import rx.observers.TestSubscriber;

public class ParcelHistoryTest extends BaseApiTestCase {

    private static final String TAG = ParcelHistoryTest.class.getSimpleName();

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testParcelHistoryResponseIsNotNull() {
        TestSubscriber<GetParcelHistoryResponse> testSubscriber = TestSubscriber.create();

        api.getParcelHistory(1, 1).subscribe(testSubscriber);
        testSubscriber.assertNoErrors();

        List<GetParcelHistoryResponse> responseList = testSubscriber.getOnNextEvents();

        assertNotNull(responseList);
    }
}
