package com.model;

import java.io.Serializable;
import java.util.ArrayList;

public class ActiveParcelArray implements Serializable {
    public ArrayList<ActiveParcel> activeParcels = new ArrayList<>();

    public void updateParcel(ActiveParcel parcel, boolean updateOnly) {
        int index = activeParcels.indexOf(parcel);
        if (index >= 0) {
            activeParcels.remove(index);
            activeParcels.add(index, parcel);
        } else {
            if (!updateOnly) activeParcels.add(parcel);
        }
    }

    public ActiveParcel getParcelWithCode(String parcelCode) {
        for (int i = 0; i < activeParcels.size(); i++) {
            ActiveParcel parcel = activeParcels.get(i);
            if (parcel.parcelCode != null && parcel.parcelCode.equals(parcelCode)) {
                return parcel;
            }
        }
        return null;
    }

    public int getParcelIndex(String parcelCode) {
        for (int i = 0; i < activeParcels.size(); i++) {
            ActiveParcel parcel = activeParcels.get(i);
            if (parcel.parcelCode != null && parcel.parcelCode.equals(parcelCode)) {
                return i;
            }
        }
        return -1;
    }

    public ActiveParcel getParcel(int index) {
        int size = activeParcels.size();
        if (index > size)
            return null;
        else
            return activeParcels.get(index);
    }
}
