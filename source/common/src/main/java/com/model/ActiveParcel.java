package com.model;

import com.common.Constants;
import com.google.android.gms.wearable.DataMap;

import java.io.Serializable;
import java.util.ArrayList;

public class ActiveParcel implements Serializable {
    // Fields of parcel
    public String parcelNumber;
    public String parcelCode;
    public String trackingStatusCurrent;
    public String organisation;
    public Constants.ParcelStatusType parcelStatusType;
    public int estimatedMinsToStop;
    public float deliveryLatitude;
    public float deliveryLongitude;

    // For collect from depot
    public String depotName;
    public String depotDiaryDate;
    public String depotStartTime;
    public String depotEndTime;

    // For route view
    public String driverName;
    public float routeLatitude;
    public float routeLongitude;

    // For collect from pickup shop
    public String shopAddress;
    public String shopShortAddress;
    public float shopLatitude;
    public float shopLongitude;

    // For estimated date
    public String estimatedDeliveryDate;

    // For issue
    public String issueName;
    public String issueCode;

    // For delivered
    public transient ArrayList<DataMap> photos;

    // For refresh
    public boolean isLoading;

    @Override
    public boolean equals(Object o) {
        if (o != null && o instanceof ActiveParcel) return ((ActiveParcel) o).parcelCode.equals(parcelCode);
        else return super.equals(o);
    }
}
