package com.common;

public class Constants {
    public static final String NOTIFICATION_PATH = "/notification";
    public static final String NOTIFICATION_TIMESTAMP = "timestamp";
    public static final String NOTIFICATION_ID = "id";
    public static final String NOTIFICATION_TITLE = "title";
    public static final String NOTIFICATION_CONTENT = "content";
    public static final String NOTIFICATION_PARCEL_CODE = "parcel_code";

    public static final String DATA_REQUEST_PATH = "/request";
    public static final String REQUEST_TIMESTAMP = "timestamp";
    public static final String REQUEST_CODE = "requestCode";
    public static final String EXTRA_BUNDLE = "extra_bundle";
    public static final String ACTION_DISMISS = "com.dpd.yourdpd.DISMISS";

    public static final String BUNDLE = "bundle";

    public static final String PARCEL_NUMBER = "parcel_number";
    public static final String PARCEL_CODE = "parcel_code";
    public static final String PARCEL_ORGANISATION = "parcel_organisation";
    public static final String PARCEL_TRACKING_STATUS = "parcel_status_current";
    public static final String PARCEL_TYPE = "parcel_type";
    public static final String PARCEL_ESTIMATED_MINS_TO_STOP = "parcel_estimated_mins_to_stop";
    public static final String PARCEL_DELIVERY_LATITUDE = "parcel_driver_latitude";
    public static final String PARCEL_DELIVERY_LONGITUDE = "parcel_driver_longitude";
    public static final String PARCEL_DRIVER_NAME = "parcel_driver_name";
    public static final String PARCEL_ROUTE_LATITUDE = "parcel_house_latitude";
    public static final String PARCEL_ROUTE_LONGITUDE = "parcel_house_longitude";
    public static final String PARCEL_SHOP_LATITUDE = "shop_location_latitude";
    public static final String PARCEL_SHOP_LONGITUDE = "shop_location_longitude";
    public static final String PARCEL_SHOP_LOCATION_ADDRESS = "shop_location_address";
    public static final String PARCEL_SHOP_LOCATION_SHORT_ADDRESS = "shop_location_short_address";
    public static final String PARCEL_ESTIMATED_DATE = "estimated_date";
    public static final String PARCEL_TOTAL_SIZE = "total_size";
    public static final String PARCEL_INDEX = "parcel_index";
    public static final String PARCEL_ISSUE_NAME = "issue_name";
    public static final String PARCEL_ISSUE_CODE = "issue_code";
    public static final String PARCEL_DEPOT_NAME = "depot_name";
    public static final String PARCEL_DEPOT_DIARY_DATE = "depot_diary_date";
    public static final String PARCEL_DEPOT_START_TIME = "depot_start_slot";
    public static final String PARCEL_DEPOT_END_TIME = "depot_end_slot";
    public static final String PARCEL_ASSET_PHOTOS = "asset_photo_";
    public static final String PARCEL_ASSET_PHOTO_IMAGE_NAME = "asset_image_";

    public static final String THROWABLE_TYPE = "throwable_type";
    public static final String THROWABLE_MESSAGE = "throwable_message";

    public static final int REQUEST_PARCEL_HISTORY = 1;
    public static final int REQUEST_PARCEL_DETAIL = 2;
    public static final int REQUEST_OPEN_APP_PARCEL_DETAIL = 3;
    public static final int REQUEST_DISMISS_NOTIFICATION = 4;
    public static final int SEND_THROWABLE = 5;
    public static final int REQUEST_OPEN_APP_TO_AUTH = 6;
    public static final int REQUEST_REFRESH = 7;

    public static final int RESPONSE_PARCEL_HISTORY = 10;
    public static final int RESPONSE_PARCEL_DETAIL = 11;

    public enum ThrowableType {
        UNKNOWN(0),
        UNAUTHORIZED(1);
        private final int value;

        ThrowableType(int value) {
            this.value = value;
        }

        public static ThrowableType fromValue(int value) {
            for (ThrowableType type : ThrowableType.values()) {
                if (type.value == value) {
                    return type;
                }
            }
            return UNKNOWN;
        }
    }

    public enum ParcelStatusType {
        LOADING(-1),
        UNKNOWN(0),
        RFI(1),
        DELIVERED(2),
        COLLECT_FROM_DEPOT(3),
        COLLECT_FROM_PICKUP(4),
        COLLECT_FROM_PICKUP_WITH_PASS(5),
        OUT_FOR_DELIVERY(6),
        ISSUE(7),
        SHOW_ESTIMATED_DELIVERY_DATE(8);

        private final int value;

        ParcelStatusType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static ParcelStatusType fromValue(int value) {
            for (ParcelStatusType type : ParcelStatusType.values()) {
                if (type.value == value) {
                    return type;
                }
            }
            return UNKNOWN;
        }
    }
}
